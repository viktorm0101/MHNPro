//
//  IGP_HooksManager.h
//  igppogosl
//
//  Created by MADHATTER on 11/10/22.
//

#import <Foundation/Foundation.h>

#define IGPHooks [IGP_HooksManager sharedManager]

NS_ASSUME_NONNULL_BEGIN

@interface IGP_HooksManager : NSObject

+ (IGP_HooksManager *)sharedManager;
- (void)enableHooks;
- (void)disableHooks;

@end

NS_ASSUME_NONNULL_END

//
//  JB_FishHooks.m
//  spooferprosl
//
//  Created by Malhar Ambekar on 12/18/22.
//

#import "JB_FishHooks.h"
#import "fishhook.h"
#include <dlfcn.h>

const char *white = "/usr/lib/system/libsystem_pthread.dylib";
const char *black1 = "/usr/lib/libsubstrate.dylib";
const char *black2 = "/usr/lib/librocketbootstrap.dylib";
const char *blackd1 = "/usr/lib/substrate/";
const char *blackd2 = "/Library/MobileSubstrate/";
const char *blackd3 = "/var/mobile/Documents/spooferprojb/";

@implementation JB_FishHooks

+ (void)load
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        struct rebinding rebindingHandler = {};
        rebindingHandler.name = "dladdr";
        rebindingHandler.replacement = (void *)igp_dladdr;
        rebindingHandler.replaced = (void **)&org_dladdr;
        struct rebinding rebindings[] = {rebindingHandler};
        int hooked = rebind_symbols(rebindings, 1);
        DBG(@"Registered for %s -> %d",rebindingHandler.name,hooked);
        
        struct rebinding rebindingHandler1 = {};
        rebindingHandler1.name = "_dyld_get_image_name";
        rebindingHandler1.replacement = (void *)_igp_dyld_get_image_name;
        rebindingHandler1.replaced = (void **)&org_dyld_get_image_name;
        struct rebinding rebindings1[] = {rebindingHandler1};
        int hooked1 = rebind_symbols(rebindings1, 1);
        DBG(@"Registered for %s -> %d",rebindingHandler1.name,hooked1);
    });
}

+ (BOOL)is_blacklisted2:(const char *)path
{
    if(strcmp(black1, path) == 0)
    {
        return YES;
    }
    
    if(strcmp(black2, path) == 0)
    {
        return YES;
    }
    
    if(strstr(path, blackd1) != NULL)
    {
        return YES;
    }

    if(strstr(path, blackd2) != NULL)
    {
        return YES;
    }
    
    if(strstr(path, blackd3) != NULL)
    {
        return YES;
    }
    

    return NO;
}

int (*org_dladdr)(const void* addr, Dl_info* info);
extern "C" int igp_dladdr(const void* addr, Dl_info* info);
int igp_dladdr(const void* addr, Dl_info* info)
{
    auto result = org_dladdr(addr,info);
    if (!result) return result;
    
    BOOL black = [JB_FishHooks is_blacklisted2:info->dli_fname];
    if (black == YES) {
        //DBG(@"Found Blacklisted Path -> %s",info->dli_fname);
        info->dli_fname = white;
        return result;
    }

    return result;
}

const char* (*org_dyld_get_image_name)(uint32_t image_index);
extern "C" const char* _igp_dyld_get_image_name(uint32_t image_index);
const char* _igp_dyld_get_image_name(uint32_t image_index)
{
    auto result = org_dyld_get_image_name(image_index);
    if (!result) return result;

    BOOL black = [JB_FishHooks is_blacklisted2:result];
    if(black == YES)
    {
        //DBG(@"Found Blacklisted Directory -> %s",result);
        return white;
    }

    return result;
}

@end



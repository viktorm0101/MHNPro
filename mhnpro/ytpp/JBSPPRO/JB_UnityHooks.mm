//
//  JB_UnityHooks.m
//  spooferprosl
//
//  Created by Malhar Ambekar on 12/5/22.
//

#import "JB_UnityHooks.h"
#import "DynamicHooks.hpp"
#import "Constants.h"
#import "SpooferxManager.h"
#import "RpcManager.h"
#import "GamedataManager.h"
#import "EncounterManager.h"
#import "IGP_RequestManager.h"
#import "IGP_LocationSimulator.h"
#import "IGP_InventoryManager.h"
#import "IGP_AutoManager.h"
#import "IGP_ActivityManager.h"
#import "IGP_FunctionPointers.h"

#import <mach-o/dyld.h>

bool _hasRenamedPokemons = false;
double lastVerifyTime;
Il2CppString *sendingPlayerId;
unsigned long receivedGiftId;
Il2CppString *playerId;

@implementation JB_UnityHooks

+ (void) loadUnityJB_Hooks {
    @autoreleasepool
    {
        int imgcount = _dyld_image_count();
        for (int i = 0; i < imgcount; i++) {
            const char *imagename = _dyld_get_image_name(i);
            if (strstr(imagename, "/UnityFramework")) {
                Preferences.unityIndex = i;
                DBG(@"UnityIndex -> %d",Preferences.unityIndex);
                break;
            }
        }
        
        auto vmslide = _dyld_get_image_vmaddr_slide(Preferences.unityIndex);
        FridaEngine *fe = new FridaEngine();
        
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_RH_Send),
//                        reinterpret_cast<void*>(&replaced_RpcSend2),
//                        reinterpret_cast<void**>(&orig_RpcSend2));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_AR_OnResponse),
//                        reinterpret_cast<void*>(&replaced_onResponse_k),
//                        reinterpret_cast<void**>(&orig_onResponse_k));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_MES_GymRootComplete),
//                        reinterpret_cast<void*>(&replaced_GymRootComplete),
//                        reinterpret_cast<void**>(&orig_GymRootComplete));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_CP_SetHeading),
//                        reinterpret_cast<void*>(&replaced_SetHeading),
//                        reinterpret_cast<void**>(&orig_CP_SetHeading));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_CDS_GetFastAttackDamage),
//                        reinterpret_cast<void*>(&replaced_GetFastAttackDamage),
//                        reinterpret_cast<void**>(&orig_GetFastAttackDamage));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_MES_SetUIVisible),
//                        reinterpret_cast<void*>(&replaced_SetUIVisible),
//                        reinterpret_cast<void**>(&orig_SetUIVisible));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_PDC_ShowEvolveAnimation),
//                        reinterpret_cast<void*>(&replaced_ShowEvolveAnimation),
//                        reinterpret_cast<void**>(&orig_ShowEvolveAnimation));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_PDC_ShowMegaEvolveAnimation),
//                        reinterpret_cast<void*>(&replaced_ShowMegaEvolveAnimation),
//                        reinterpret_cast<void**>(&orig_ShowMegaEvolveAnimation));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_PDC_ShowNewSpeciesCaughtAnimation),
//                        reinterpret_cast<void*>(&replaced_ShowNewSpeciesCaughtAnimation),
//                        reinterpret_cast<void**>(&orig_ShowNewSpeciesCaughtAnimation));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_EIS_ExitState),
//                        reinterpret_cast<void*>(&replaced_EIS_ExitSate),
//                        reinterpret_cast<void**>(&orig_EIS_ExitState));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_PILL_Sort),
//                        reinterpret_cast<void*>(&replaced_sort),
//                        reinterpret_cast<void**>(&orig_Sort));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_PIDSR_Cleanup),
//                        reinterpret_cast<void*>(&replaced_cleanup),
//                        reinterpret_cast<void**>(&orig_cleanup));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_PIGC_IALLLKNMANI),
//                        reinterpret_cast<void*>(&replaced_IALLLKNMANI),
//                        reinterpret_cast<void**>(&orig_IALLLKNMANI));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_TS_ReportPGP),
//                        reinterpret_cast<void*>(&replaced_ReportPGP),
//                        reinterpret_cast<void**>(&orig_ReportPGP));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_EP_GetAttackProbability),
//                        reinterpret_cast<void*>(&replaced_get_AttackProbability),
//                        reinterpret_cast<void**>(&orig_get_AttackProbability));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_EP_GetDodgeProbability),
//                        reinterpret_cast<void*>(&replaced_get_DodgeProbability),
//                        reinterpret_cast<void**>(&orig_get_DodgeProbability));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_MVH_GetGroundPosition),
//                        reinterpret_cast<void*>(&RP_MVH_GetGroundPosition),
//                        reinterpret_cast<void**>(&MVH_GetGroundPosition));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_PDS_AddPokemon),
//                        reinterpret_cast<void*>(&replaced_addPokemonAlt),
//                        reinterpret_cast<void**>(&orig_addPokemon));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_PDS_RemovePokemon),
//                        reinterpret_cast<void*>(&replaced_removePokemon),
//                        reinterpret_cast<void**>(&orig_removePokemon));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_MES_PokemonSelected),
//                        reinterpret_cast<void*>(&replaced_PokemonSelected),
//                        reinterpret_cast<void**>(&orig_PokemonSelected));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_MS_ProximityWakeup),
//                        reinterpret_cast<void*>(&replaced_proximityWakeUp),
//                        reinterpret_cast<void**>(&ORG_ProximityWakeUp));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_MS_ProximitySleep),
//                        reinterpret_cast<void*>(&replaced_proximitySleep),
//                        reinterpret_cast<void**>(&ORG_ProximitySleep));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_MG_ProximityTriggerEnter),
//                        reinterpret_cast<void*>(&replaced_ProximityTriggerEnter),
//                        reinterpret_cast<void**>(&ORG_ProximityTriggerEnter));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_MG_ProximityTriggerExit),
//                        reinterpret_cast<void*>(&replaced_ProximityTriggerExit),
//                        reinterpret_cast<void**>(&ORG_ProximityTriggerExit));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_PGPS_StartSession),
//                        reinterpret_cast<void*>(&replaced_StartSession),
//                        reinterpret_cast<void**>(&ORG_StartSession));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_EIS_EnterState),
//                        reinterpret_cast<void*>(&replaced_EIS_EnterState),
//                        reinterpret_cast<void**>(&ORG_EIS_EnterState));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_WMP_UpdateData),
//                        reinterpret_cast<void*>(&RPL_WMP_UpdateData),
//                        reinterpret_cast<void**>(&ORG_WMP_UpdateData));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_IRS_UpdateInvasionBattle),
//                        reinterpret_cast<void*>(&RPL_IRS_UpdateInvasionBattle),
//                        reinterpret_cast<void**>(&ORG_IRS_UpdateInvasionBattle));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_IS_ProcessIncidentEndOfCombatData),
//                        reinterpret_cast<void*>(&RPL_IS_ProcessIncidentEndOfCombatData),
//                        reinterpret_cast<void**>(&ORG_IS_ProcessIncidentEndOfCombatData));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_ES_BeginEncounterApproach),
//                        reinterpret_cast<void*>(&RPL_ES_BeginEncounterApproach),
//                        reinterpret_cast<void**>(&ORG_ES_BeginEncounterApproach));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_MES_GymSelected),
//                        reinterpret_cast<void*>(&RPL_GymSelected),
//                        reinterpret_cast<void**>(&ORG_GymSelected));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_GRC_ExitGym),
//                        reinterpret_cast<void*>(&RPL_GRC_ExitGym),
//                        reinterpret_cast<void**>(&ORG_GRC_ExitGym));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_PSS_SetupPokestop),
//                        reinterpret_cast<void*>(&RPL_PSS_SetupPokestop),
//                        reinterpret_cast<void**>(&ORG_PSS_SetupPokestop));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_PSS_ExitState),
//                        reinterpret_cast<void*>(&RPL_PSS_ExitState),
//                        reinterpret_cast<void**>(&ORG_PSS_ExitState));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_MES_OMHODHKMCOE),
//                        reinterpret_cast<void*>(&RPL_MES_OMHODHKMCOE),
//                        reinterpret_cast<void**>(&ORG_MES_OMHODHKMCOE));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_PT_GetGrade),
//                        reinterpret_cast<void*>(&RPL_PT_GetGrade),
//                        reinterpret_cast<void**>(&ORG_PT_GetGrade));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_NCRS_EnterState),
//                        reinterpret_cast<void*>(&RPL_NCRS_EnterState),
//                        reinterpret_cast<void**>(&ORG_NCRS_EnterState));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_CRRS_BuildNpcRewardsProto),
//                        reinterpret_cast<void*>(&RPL_CRRS_BuildNpcRewardsProto),
//                        reinterpret_cast<void**>(&ORG_CRRS_BuildNpcRewardsProto));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_PIS_EnterState),
//                        reinterpret_cast<void*>(&RPL_PIS_EnterState),
//                        reinterpret_cast<void**>(&ORG_PIS_EnterState));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_GRS_CheckGiftingStatus),
//                        reinterpret_cast<void*>(&RPL_GRS_CheckGiftingStatus),
//                        reinterpret_cast<void**>(&ORG_GRS_CheckGiftingStatus));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_GS_MarkViewed),
//                        reinterpret_cast<void*>(&RPL_GS_MarkViewed),
//                        reinterpret_cast<void**>(&ORG_GS_MarkViewed));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_FGS_StartOpenGiftFlow),
//                        reinterpret_cast<void*>(&RPL_FGS_StartOpenGiftFlow),
//                        reinterpret_cast<void**>(&ORG_FGS_StartOpenGiftFlow));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_GST_StartSendGiftFlow),
//                        reinterpret_cast<void*>(&RPL_GST_StartSendGiftFlow),
//                        reinterpret_cast<void**>(&ORG_GST_StartSendGiftFlow));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_GS_TryGetGiftBoxDetails),
//                        reinterpret_cast<void*>(&RPL_GS_TryGetGiftBoxDetails),
//                        reinterpret_cast<void**>(&ORG_GS_TryGetGiftBoxDetails));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_FPGC_OnCloseButtonClicked),
//                        reinterpret_cast<void*>(&RPL_FPGC_OnCloseButtonClicked),
//                        reinterpret_cast<void**>(&ORG_FPGC_OnCloseButtonClicked));
//
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_GRS_OpenGift_b__0),
//                        reinterpret_cast<void*>(&RPL_GRS_OpenGift_b__0),
//                        reinterpret_cast<void**>(&ORG_GRS_OpenGift_b__0));

//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_PDC_PlayPurifyLightSfx),
//                        reinterpret_cast<void*>(&RPL_PDC_PlayPurifyLightSfx),
//                        reinterpret_cast<void**>(&ORG_PDC_PlayPurifyLightSfx));
//        
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_ASDS_GetEnabled),
//                        reinterpret_cast<void*>(&RPL_ASDS_GetEnabled),
//                        reinterpret_cast<void**>(&ORG_ASDS_GetEnabled));
//    
//        fe->Hook(reinterpret_cast<void*>(vmslide + ADR_ACS_EnterState),
//                        reinterpret_cast<void*>(&RPL_ACS_EnterState),
//                        reinterpret_cast<void**>(&ORG_ACS_EnterState));
        
        
        DBG(@"Done UnityFramework hooks");
    }
}

@end

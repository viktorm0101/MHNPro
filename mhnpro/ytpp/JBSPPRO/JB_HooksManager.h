//
//  JB_HooksManager.h
//  igppogosl
//
//  Created by Malhar Ambekar on 11/10/22.
//

#import <Foundation/Foundation.h>

#define JB_Hooks [JB_HooksManager sharedManager]

NS_ASSUME_NONNULL_BEGIN

@interface JB_HooksManager : NSObject

+ (JB_HooksManager *)sharedManager;
- (void)enableJB_Hooks;
- (void)disableJB_Hooks;

@end

NS_ASSUME_NONNULL_END

//
//  JB_Loader.m
//  spooferxsl
//
//  Created by spooferx on 9/17/20
//  
//

#import "JB_Loader.h"
#import "JB_HooksManager.h"
#import "IGP_FunctionPointers.h"
#import "EncounterManager.h"
#import <mach-o/dyld.h>
#import <dlfcn.h>

@implementation JB_Loader

#define REQUEST_HIDE_FILES "com.spooferpro.hide_files"
extern "C" CFNotificationCenterRef CFNotificationCenterGetDistributedCenter(void);

static void BypassJailbreakChecks() {
    pid_t pid = getpid();
    
    NSDictionary *info = @{
        @"Pid" : @(pid),
        @"Paths": @[
            @"/.bootstrapped_electra",
            @"/Applications/Anemone.app",
            @"/Applications/Cydia.app",
            @"/Applications/SafeMode.app",
            @"/Applications/Sileo.app",
            @"/Applications/Zebra.app",
            @"/Applications/vnodebypass.app/Info.plist",
            @"/Applications/vnodebypass.app/vnodebypass",
            @"/Library/Frameworks/CydiaSubstrate.framework",
            @"/Library/MobileSubstrate/DynamicLibraries/FlyJB.dylb",
            @"/Library/MobileSubstrate/MobileSubstrate.dylib",
            @"/Library/PreferenceBundles/LaunchInSafeMode.bundle",
            @"/Library/PreferenceLoader/Preferences/LaunchInSafeMode.plist",
            @"/Library/Themes",
            @"/Library/dpkg/info/com.inoahdev.launchinsafemode.list",
            @"/Library/dpkg/info/com.inoahdev.launchinsafemode.md5sums",
            @"/bin/bash",
            @"/bin/bunzip2",
            @"/bin/bzip2",
            @"/bin/cat",
            @"/bin/chgrp",
            @"/bin/chmod",
            @"/bin/chown",
            @"/bin/cp",
            @"/bin/grep",
            @"/bin/gzip",
            @"/bin/kill",
            @"/bin/ln",
            @"/bin/ls",
            @"/bin/mkdir",
            @"/bin/mv",
            @"/bin/sed",
            @"/bin/sh",
            @"/bin/su",
            @"/bin/tar",
            @"/binpack",
            @"/bootstrap",
            @"/chimera",
            @"/electra",
            @"/etc/apt",
            @"/etc/profile",
            @"/jb",
            @"/private/var/binpack",
            @"/private/var/checkra1n.dmg",
            @"/private/var/lib/apt",
            @"/tmp/vnodeMem.txt",
            @"/usr/bin/diff",
            @"/usr/bin/hostinfo",
            @"/usr/bin/killall",
            @"/usr/bin/passwd",
            @"/usr/bin/recache",
            @"/usr/bin/tar",
            @"/usr/bin/vnodebypass",
            @"/usr/bin/which",
            @"/usr/bin/xargs",
            @"/usr/lib/SBInject",
            @"/usr/lib/SBInject.dylib",
            @"/usr/lib/TweakInject",
            @"/usr/lib/TweakInject.dylib",
            @"/usr/lib/TweakInjectMapsCheck.dylib",
            @"/usr/lib/libjailbreak.dylib",
            @"/usr/lib/libsubstitute.0.dylib",
            @"/usr/lib/libsubstitute.dylib",
            @"/usr/lib/libsubstrate.dylib",
            @"/usr/lib/substitute-loader.dylib",
            @"/usr/libexec/sftp-server",
            @"/usr/sbin/sshd",
            @"/usr/share/terminfo",
            @"/usr/share/vnodebypass/hidePathList.plist",
            @"/var/MobileSoftwareUpdate/mnt1/fakevar",
            @"/var/mobile/Library/.sbinjectSafeMode",
            @"/var/mobile/Library/Preferences/jp.akusio.kernbypass-unofficial.plist",
            @"/var/mobile/Library/Preferences/jp.akusio.kernbypass.plist",
            @"/var/mobile/Library/Preferences/jp.akusio.kernbypass2.plist",
            @"/var/mobile/fakevar",
            @"/var/jb"
        ]
    };
    
    CFNotificationCenterPostNotification(CFNotificationCenterGetDistributedCenter(), CFSTR(REQUEST_HIDE_FILES), NULL, (__bridge CFDictionaryRef)info, YES);
    kill(pid, SIGSTOP);
}

static void image_added(const struct mach_header *mh,intptr_t slide){
    Dl_info image_info;
    int result = dladdr(mh, &image_info);
    if (result == 0) {
        DBG(@"Could not print info for mach_header:%p\n\n",mh);
        return;
    }
    
    const char *image_name = image_info.dli_fname;
    NSString *imgn = [NSString stringWithCString:image_name encoding:NSUTF8StringEncoding];
    if ([[imgn lastPathComponent] isEqualToString:@"UnityFramework"] == YES) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            DBG(@"UnityFramework Loaded");
            int imgcount = _dyld_image_count();
            for (int i = 0; i < imgcount; i++) {
                const char *imagename = _dyld_get_image_name(i);
                NSString *imgn1 = [NSString stringWithCString:imagename encoding:NSUTF8StringEncoding];
                if ([[imgn1 lastPathComponent] isEqualToString:@"UnityFramework"] == YES) {
                    Preferences.unityIndex = i;
                    DBG(@"UnityIndex -> %d",Preferences.unityIndex);
                    break;
                }
            }
            
            [JB_Hooks enableJB_Hooks];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [IL2Functions SetUp];
                [EncounterMan processSpecialEncounter:[NSData data]];
            });
        });
    }
    
    if ([[imgn lastPathComponent] isEqualToString:@"NianticLabsPlugin"] == YES) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            DBG(@"NianticLabsPlugin Loaded");
        });
    }
}

+ (void)load
{
    BypassJailbreakChecks();
    DBG(@"Registered for Image Notifications");
    _dyld_register_func_for_add_image(&image_added);
}


@end

//
//  JB_HooksManager.m
//  igppogosl
//
//  Created by Malhar Ambekar on 11/10/22.
//

#import "JB_HooksManager.h"
#import "DynamicHooks.hpp"
#import "Constants.h"
#import <mach-o/dyld.h>
#import <dlfcn.h>

@interface JB_HooksManager ()

@property (nonatomic,strong) NSArray *JB_hooksList;

@end

@implementation JB_HooksManager

+ (JB_HooksManager *)sharedManager
{
    static JB_HooksManager *sharedMyManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init
{
    if (self = [super init])
    {
       
    }
    
    return self;
}

- (void)enableJB_Hooks
{
    @autoreleasepool
    {
        int imgcount = _dyld_image_count();
        for (int i = 0; i < imgcount; i++) {
            const char *imagename = _dyld_get_image_name(i);
            if (strstr(imagename, "/UnityFramework")) {
                Preferences.unityIndex = i;
                DBG(@"UnityIndex -> %d",Preferences.unityIndex);
                break;
            }
        }
        
        auto vmslide = _dyld_get_image_vmaddr_slide(Preferences.unityIndex);
        FridaEngine *fe = new FridaEngine();
        
        //NSString *dylib = [[[NSBundle mainBundle] bundlePath] stringByAppendingString:@"/Frameworks/igppogo.dylib"];
        //void *libHandle = dlopen([dylib UTF8String], RTLD_LAZY);
        self.JB_hooksList = [[[OffMan patchesList] nonConcurrentDictionary] allKeys];
        DBG(@"Dynamically Hooking -> %lu Functions",(unsigned long)self.JB_hooksList.count);
        for (NSString *key in self.JB_hooksList) {
            NSDictionary *hook = [OffMan GetPatch:key];
            unsigned long hookadr = [[hook objectForKey:@"hook_addr"] longValue];
            NSString *rplname = [hook objectForKey:@"rpl_name"];
            void *rplsym = dlsym(RTLD_DEFAULT, [rplname UTF8String]);
            NSString *orgname = [hook objectForKey:@"org_name"];
            void *orgsym = dlsym(RTLD_DEFAULT, [orgname UTF8String]);
            if(rplsym != 0x0 && orgsym != 0x0)
            {
                bool enabled = fe->Hook(reinterpret_cast<void*>(vmslide + hookadr),
                                reinterpret_cast<void*>(rplsym),
                                reinterpret_cast<void**>(orgsym));
                DBG(@"Enabled -> %@ -> %@",orgname,NSStringFromBool(enabled));
            }
            else
            {
                DBG(@"Failed -> %@",orgname);
            }
        }
        
        DBG(@"Dynamic Hooks Loaded");
    }
}

- (void)disableJB_Hooks
{
    @autoreleasepool
    {
        FridaEngine *fe = new FridaEngine();
        fe->~FridaEngine();
        DBG(@"Dynamic Hooks Unloaded");
    }
}

@end

//
//  JB_UnityHooks.h
//  spooferprosl
//
//  Created by Malhar Ambekar on 12/5/22.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface JB_UnityHooks : NSObject

+ (void) loadUnityJB_Hooks;

@end

NS_ASSUME_NONNULL_END

//
//  spooferx
//
//  Created by spooferx on 6/12/20
//  
//

#pragma once
#ifndef Constants_h
#define Constants_h

#import "il2cppBridge.cpp"
#import "IGP_AddressManager.h"

static Il2CppString* (*SS_Ctor)(const char* value) = (Il2CppString* (*)(const char*))[OffMan GetPtr:@"ADR_SS_Ctor"];
static Il2CppString* (*SS_Replace)(Il2CppString *str, Il2CppString* old_str, Il2CppString *new_str) = (Il2CppString* (*)(Il2CppString*, Il2CppString*, Il2CppString*))[OffMan GetPtr:@"ADR_SS_Replace"];
static void (*UEO_Destroy)(void *obj) = (void (*)(void *))[OffMan GetPtr:@"ADR_UEO_Destroy"];

#endif /* Constants_h */

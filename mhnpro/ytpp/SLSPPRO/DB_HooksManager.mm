//
//  DB_HooksManager.m
//  spooferpro
//
//  Created by Malhar Ambekar on 1/28/23.
//

#import "DB_HooksManager.h"
#import "dobby.h"
#import "Constants.h"
#import <mach-o/dyld.h>
#import <dlfcn.h>

@interface DB_HooksManager ()

@property (nonatomic,strong) NSArray *DB_hooksList;

@end


@implementation DB_HooksManager

+ (DB_HooksManager *)sharedManager
{
    static DB_HooksManager *sharedMyManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init
{
    if (self = [super init])
    {
    }
    
    return self;
}

- (void)enableDB_Hooks
{
    @autoreleasepool
    {
        auto vmslide = _dyld_get_image_vmaddr_slide(Preferences.unityIndex);
        //NSString *dylib = [[[NSBundle mainBundle] bundlePath] stringByAppendingString:@"/Frameworks/igppogo.dylib"];
        //void *libHandle = dlopen([dylib UTF8String], RTLD_LAZY);
        self.DB_hooksList = [SxUtilities hooksList];
        DBG(@"Statically Hooking -> %lu Functions",(unsigned long)self.DB_hooksList.count);
        for (NSDictionary *hook in self.DB_hooksList) {
            unsigned long hookadr = [[hook objectForKey:@"hook_addr"] longValue];
            NSString *rplname = [hook objectForKey:@"rpl_name"];
            void *rplsym = dlsym(RTLD_DEFAULT, [rplname UTF8String]);
            NSString *orgname = [hook objectForKey:@"org_name"];
            void *orgsym = dlsym(RTLD_DEFAULT, [orgname UTF8String]);
            if(rplsym != 0x0 && orgsym != 0x0)
            {
                int enabled = DobbyHook(reinterpret_cast<void*>(vmslide + hookadr),
                                reinterpret_cast<void*>(rplsym),
                                reinterpret_cast<void**>(orgsym));
                DBG(@"Enabled -> %@ -> %d",orgname,enabled);
            }
            else
            {
                DBG(@"Failed -> %@",orgname);
            }
        }
        
        DBG(@"Static Hooks Loaded");
    }
}

- (void)disableDB_Hooks
{
    @autoreleasepool
    {
        auto vmslide = _dyld_get_image_vmaddr_slide(Preferences.unityIndex);
        for (NSDictionary *hook in self.DB_hooksList) {
            unsigned long hookadr = [[hook objectForKey:@"hook_addr"] longValue];
            NSString *orgname = [hook objectForKey:@"org_name"];
            DobbyDestroy(reinterpret_cast<void*>(vmslide + hookadr));
            DBG(@"Disabled -> %@",orgname);
        }
        
        DBG(@"Static Hooks Unloaded");
    }
}

@end

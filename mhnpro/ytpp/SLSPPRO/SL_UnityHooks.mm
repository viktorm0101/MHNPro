//
//  SL_UnityHooks.m
//  spooferprosl
//
//  Created by Malhar Ambekar on 12/5/22.
//

#import "SL_UnityHooks.h"
#import "StaticHooks.hpp"
#import "Constants.h"
#import "SpooferxManager.h"
#import "RpcManager.h"
#import "GamedataManager.h"
#import "EncounterManager.h"
#import "IGP_RequestManager.h"
#import "IGP_LocationSimulator.h"
#import "IGP_InventoryManager.h"
#import "IGP_AutoManager.h"
#import "IGP_ActivityManager.h"
#import "IGP_FunctionPointers.h"

#import <mach-o/dyld.h>

bool _hasRenamedPokemons = false;
double lastVerifyTime;
Il2CppString *sendingPlayerId;
unsigned long receivedGiftId;
Il2CppString *playerId;

@implementation SL_UnityHooks

static void* replaced_RpcSend2 (void *extra, int a, void *inProto, void *a2)
{
    if (a == 103 && Preferences.enableModdedThrow == YES)
    {
        void *cp = [[GamedataManager sharedManager] getEnhancedThrow:inProto];
        return orig_RpcSend2(extra,a,cp,a2);
    }
    
    //    if (a == 112 || a == 125) {
    //        [[GamedataManager sharedManager] processOutgoingRequests:inProto :a];
    //    }
    
    if (a == 155) {
        DBG(@"Got Method 155 hit");
        void *gymdepproto = ME_ToByteString(inProto);
        il2ByteString_k *bstr = (il2ByteString_k*)gymdepproto;
        if (bstr->m_bytes->max_length > 0)
        {
            NSData *rawData = [NSData dataWithBytes:bstr->m_bytes->m_Items length:bstr->m_bytes->max_length];
            NSError *parseError;
            GymDeployProto *grbp = [[GymDeployProto alloc] initWithData:rawData error:&parseError];
            if (parseError == nil) {
                DBG(@"GymDeployProto -> %@",grbp);
                [[GamedataManager sharedManager] setGymDeploy:grbp];
            }
            else
            {
                DBG(@"GymDeployProto -> %@",parseError);
            }
        }
    }
    
    if (a == 164) {
        DBG(@"Got Method 164 hit");
        void *gymfedproto = ME_ToByteString(inProto);
        il2ByteString_k *bstr = (il2ByteString_k*)gymfedproto;
        if (bstr->m_bytes->max_length > 0)
        {
            NSData *rawData = [NSData dataWithBytes:bstr->m_bytes->m_Items length:bstr->m_bytes->max_length];
            NSError *parseError;
            GymFeedPokemonProto *grbp = [[GymFeedPokemonProto alloc] initWithData:rawData error:&parseError];
            if (parseError == nil) {
                DBG(@"GymFeedPokemonProto -> %@",grbp);
                [[GamedataManager sharedManager] setGymFeed:grbp];
            }
            else
            {
                DBG(@"GymFeedPokemonProto -> %@",parseError);
            }
        }
    }
    
    if (a == 1204) {
        Holoholo_Rpc_InvasionEncounterProto_o *invasion = (Holoholo_Rpc_InvasionEncounterProto_o*)inProto;
        NSString *fortid = [SxUtilities NSStringFromIl2Cpp:&invasion->fields.incidentLookup_->fields.fortId_->start_char length:invasion->fields.incidentLookup_->fields.fortId_->length];
        [[EncounterManager sharedManager] setInvasionFort:fortid];
    }
    
    if (a == 112 || a == 125) {
        [[GamedataManager sharedManager] processOutgoingRequests:inProto :a];
    }
    
    //    if (a == 140) {
    //        Il2CppObject_IG *amp = (Il2CppObject_IG*)inProto;
    //        NSString *ptype = [[NSString alloc] initWithUTF8String:amp->klass->_1.name];
    //        DBG(@"Method -> %d Name -> %@",a,ptype);
    //    }
    
    return orig_RpcSend2(extra,a,inProto,a2);
}

static il2ByteArray_k* ReplaceProtoBytes(NSData *newData, il2ByteString_k *Message)
{
    int length = (int)[newData length];
    uint8_t *newBytes = (uint8_t*)[newData bytes];
    
    il2ByteArray_k *editted = (il2ByteArray_k*)calloc(1, sizeof(il2ByteArray_k) + (sizeof(uint8_t) * (length - 1)));
    
    if (!editted) {
        return nullptr;
    }
    editted->max_length = length;
    editted->klass = Message->m_bytes->klass;
    for (int i = 0; i < length; i++) {
        editted->m_Items[i] = newBytes[i];
    }
    return editted;
}

static NSData* UpdateSettingsBoth(NSData* Settings)
{
    NSError *err;
    DownloadSettingsResponseProto *DSR = [DownloadSettingsResponseProto parseFromData:Settings error:&err];
    if (err == nil) {
        if (DSR.values.hasMapSettings) {
            DSR.values.mapSettings.getMapObjectsMaxRefreshSeconds = 11.0;
            DSR.values.mapSettings.getMapObjectsMinRefreshSeconds = 5.0;
            DSR.values.mapSettings.pokemonVisibleRange = 250.0;
            DSR.values.mapSettings.encounterRangeMeters = 250.0;
        }
        return DSR.data;
    }
    
    return Settings;
}

static NSData* UpdateSettingsFastMap(NSData* Settings)
{
    NSError *err;
    DownloadSettingsResponseProto *DSR = [DownloadSettingsResponseProto parseFromData:Settings error:&err];
    if (err == nil) {
        if (DSR.values.hasMapSettings) {
            DSR.values.mapSettings.getMapObjectsMaxRefreshSeconds = 11.0;
            DSR.values.mapSettings.getMapObjectsMinRefreshSeconds = 5.0;
        }
        return DSR.data;
    }
    
    return Settings;
}

static NSData* UpdateSettingsRange(NSData* Settings)
{
    NSError *err;
    DownloadSettingsResponseProto *DSR = [DownloadSettingsResponseProto parseFromData:Settings error:&err];
    if (err == nil) {
        if (DSR.values.hasMapSettings) {
            DSR.values.mapSettings.pokemonVisibleRange = 250.0;
            DSR.values.mapSettings.encounterRangeMeters = 250.0;
        }
        return DSR.data;
    }
    
    return Settings;
}

static void process_response_k(il2ActionResponse_k *response, int method)
{
    il2ByteString_k *ByteString = (il2ByteString_k*)(response->get_CodedInputStream());
    if (ByteString->m_bytes->max_length > 0)
    {
        NSData *rawData = [NSData dataWithBytes:ByteString->m_bytes->m_Items length:ByteString->m_bytes->max_length];
        
        if (method == 5)
        {
            if (Preferences.enableFastMap == YES && Preferences.enableSpawnBooster == NO) {
                NSData *ModifiedData = UpdateSettingsFastMap(rawData);
                if (ModifiedData != nil) {
                    il2ByteArray_k *barray = ReplaceProtoBytes(ModifiedData, ByteString);
                    ByteString->m_bytes = barray;
                }
            }
            else if (Preferences.enableFastMap == NO && Preferences.enableSpawnBooster == YES)
            {
                NSData *ModifiedData = UpdateSettingsRange(rawData);
                if (ModifiedData != nil) {
                    il2ByteArray_k *barray = ReplaceProtoBytes(ModifiedData, ByteString);
                    ByteString->m_bytes = barray;
                }
            }
            else if (Preferences.enableFastMap == YES && Preferences.enableSpawnBooster == YES)
            {
                NSData *ModifiedData = UpdateSettingsBoth(rawData);
                if (ModifiedData != nil) {
                    il2ByteArray_k *barray = ReplaceProtoBytes(ModifiedData, ByteString);
                    ByteString->m_bytes = barray;
                }
            }
        }
        
        if (method == 102 || method == 143 || method == 145)
        {
            if (Preferences.blockNonShiny == YES || Preferences.blockNonHundo == YES) {
                NSData *ModifiedData = [[GamedataManager sharedManager] getModdedEncResponse:rawData method:method];
                if (ModifiedData != nil) {
                    il2ByteArray_k *barray = ReplaceProtoBytes(ModifiedData, ByteString);
                    ByteString->m_bytes = barray;
                }
            }
        }
        
        NSData *newData = [NSData dataWithBytes:ByteString->m_bytes->m_Items length:ByteString->m_bytes->max_length];
        [[GamedataManager sharedManager] processIncomingRequests:newData :method];
    }
}

static void replaced_onResponse_k(int *extra, il2ActionResponse_k *response)
{
    Niantic_Platform_Ditto_Rpc_ActionRequest_o *arq = (Niantic_Platform_Ditto_Rpc_ActionRequest_o*)extra;
    int method = arq->fields._Method_k__BackingField;
    int rpcId = response->get_rpcId_0();
    NSString *key = [NSString stringWithFormat:@"%d_%d",method,rpcId];
    if ([RequestMan.my_rpcidset objectForKey:key] != nil) {
        if (method == 112 || method == 125 || method == 148) {
            void *inProto = arq->fields._Payload_k__BackingField;
            [[GamedataManager sharedManager] processOutgoingRequests:inProto :method];
        }
        
        [RequestMan processRequest:(Method_Enum)method response:response];
    }
    else
    {
        NSString *a = [NSString stringWithFormat:@"%d",method];
        if ([protosArray containsObject:a] == YES)
        {
            //            Il2CppObject_IG *amp = (Il2CppObject_IG*)arq->fields._Payload_k__BackingField;
            //            NSString *ptype = [[NSString alloc] initWithUTF8String:amp->klass->_1.name];
            //            DBG(@"Method -> %d Name -> %@",arq->fields._Method_k__BackingField,ptype);
            process_response_k(response, method);
        }
    }
    
    orig_onResponse_k(extra, response);
}

static void replaced_GymRootComplete (int *extra, void *raidEncounter, void *unknownStruct)
{
    orig_GymRootComplete(extra, raidEncounter, unknownStruct);
    DBG(@"replaced_GymRootComplete");
    if (raidEncounter != 0x0) {
        Holoholo_Rpc_RaidEncounterProto_o *raidEnc = (Holoholo_Rpc_RaidEncounterProto_o*)raidEncounter;
        Holoholo_Rpc_PokemonProto_o *pokemon = raidEnc->pokemon_;
        if (raidEnc->throwsRemaining_ > 0 && pokemon->fields.pokemonId_ > 0) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[EncounterManager sharedManager] addIvOverlayCpp:pokemon isInventory:false];
            });
        }
    }
}

static void replaced_SetHeading(void *a1, float a2)
{
    DBG(@"replaced_SetHeading");
    [Preferences setJoystickHeading:[NSString stringWithFormat:@"%f",a2]];
    Preferences.cameraHeading = (double)a2;
    [LocationSimulator rotateJoystick:Preferences.cameraHeading];
    orig_CP_SetHeading(a1,a2);
}

static void replaced_SetUIVisible (void *extra, bool isVisible)
{
    orig_SetUIVisible (extra, isVisible);
    if (isVisible == false)
    {
        [[SpooferxManager sharedManager] hideControls];
    }
    else
    {
        [[EncounterManager sharedManager] removeIvOverLay];
        [[SpooferxManager sharedManager] showControls];
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"spprotele"] != nil) {
            NSString *teleport = [[[NSUserDefaults standardUserDefaults] objectForKey:@"spprotele"] copy];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"spprotele"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[SpooferxManager sharedManager] jumpFromURL:teleport];
            });
        }
        
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[SxUtilities sharedUtilities] firstLaunch];
            });
        });
        
        Preferences.mapLoaded = YES;
        EncounterMan.isUserEncounter = NO;
        NSTimeInterval timeStamp = [[NSDate date] timeIntervalSince1970];
        NSTimeInterval VerifyInterval = timeStamp - lastVerifyTime;
        if (VerifyInterval > 900) {
            lastVerifyTime = timeStamp;
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                [RpcManager fakeViewAllocated];
                [RpcManager verifySpawnsN];
            });
        }
    }
}

static int replaced_GetFastAttackDamage (void *extra, void *attacker, void *defender)
{
    Holoholo_Rpc_CombatProto_Types_CombatPokemonProto_o *atk = (Holoholo_Rpc_CombatProto_Types_CombatPokemonProto_o*)attacker;
    if (Preferences.rocketOneShot == YES && atk->pokemonId_ != 0)
    {
        return 99999;
    }
    
    return orig_GetFastAttackDamage(extra, attacker, defender);
}

static void replaced_ShowEvolveAnimation (void *extra, void *a1, void *a2)
{
    if (Preferences.skipAnimations == YES) {
        evoComplete(extra);
    }
    else
    {
        orig_ShowEvolveAnimation(extra, a1, a2);
    }
}

static void replaced_ShowMegaEvolveAnimation (void *extra, void *a1, void *a2)
{
    if (Preferences.skipAnimations == YES) {
        megaEvoComplete(extra);
    }
    else
    {
        orig_ShowMegaEvolveAnimation(extra, a1, a2);
    }
}

static void replaced_ShowNewSpeciesCaughtAnimation (void *extra)
{
    if (Preferences.skipAnimations == YES) {
        newsPokemonComplete(extra);
    }
    else
    {
        orig_ShowNewSpeciesCaughtAnimation(extra);
    }
}

static void replaced_EIS_ExitSate (void *eis)
{
    orig_EIS_ExitState(eis);
    dispatch_async(dispatch_get_main_queue(), ^{
        [[EncounterManager sharedManager] removeIvOverLay];
    });
}

static System_Collections_Generic_List_PokemonProto__o* replaced_sort(void *extra, void *protos, void *orderby, bool sortAscending)
{
    System_Collections_Generic_List_PokemonProto__o *list = orig_Sort(extra, protos, orderby, sortAscending);
    
    if (Preferences.statsInInventory == YES)
    {
        for (int i = 0; i < list->_size; i++) {
            Holoholo_Rpc_PokemonProto_o *mon = list->_items->m_Items[i];
            
            if (mon->fields.isEgg_ == false && mon->fields.pokemonId_ > 0)
            {
                int atk = mon->fields.individualAttack_;
                int def = mon->fields.individualDefense_;
                int stm = mon->fields.individualStamina_;
                float percent = ((mon->fields.individualAttack_ + mon->fields.individualDefense_ + mon->fields.individualStamina_) / 45.00) * 100;
                float level = [SxUtilities GetLevel:mon->fields.pokemonId_ fid:mon->fields.pokemonDisplay_->fields.form_ cp:mon->fields.cp_ atk:mon->fields.individualAttack_ def:mon->fields.individualDefense_ stm:mon->fields.individualStamina_];
                if (level == 0.0) {
                    float cpMult = mon->fields.cpMultiplier_+mon->fields.additionalCpMultiplier_;
                    level = [SxUtilities levelForPokemon:cpMult];
                }
                NSString *nick = [NSString stringWithFormat:@"%.f-%.f-%d-%d-%d",percent,level,atk,def,stm];
                mon->fields.nickname_ = (Il2CppString*)[IL2Functions GetIl2StringFromNSString:nick];
            }
        }
        _hasRenamedPokemons = true;
        return list;
    }
    else
    {
        if (_hasRenamedPokemons == true)
        {
            for (int i = 0; i < list->_size; i++) {
                Holoholo_Rpc_PokemonProto_o *mon = list->_items->m_Items[i];
                if (mon->fields.isEgg_ == false && mon->fields.pokemonId_ > 0)
                {
                    NSString *nick = [Inventory nicknameForMon:mon->fields.id_];
                    if([nick length] == 0)
                    {
                        nick = @"";
                    }
                    mon->fields.nickname_ = (Il2CppString*)[IL2Functions GetIl2StringFromNSString:nick];
                }
            }
            
            _hasRenamedPokemons = false;
            return list;
        }
    }
    
    return orig_Sort(extra, protos, orderby, sortAscending);
}

static void replaced_cleanup (void *extra)
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[EncounterManager sharedManager] removeIvOverLay];
    });
    orig_cleanup(extra);
}

static void replaced_IALLLKNMANI (void *extra, void *AMJCIHHOGHD)
{
    orig_IALLLKNMANI(extra, AMJCIHHOGHD);
    dispatch_async(dispatch_get_main_queue(), ^{
        Holoholo_Rpc_PokemonProto_o *proto = (Holoholo_Rpc_PokemonProto_o*)AMJCIHHOGHD;
        if (proto->fields.isEgg_ == false) {
            [[EncounterManager sharedManager] removeIvOverLay];
            [[EncounterManager sharedManager] addIvOverlayCpp:proto isInventory:true];
        }
    });
}

static void replaced_ReportPGP (void *extra, int FKEAABHAMGJ, int BNOEIPBFPJB, int NCDOKNKNDGC)
{
    orig_ReportPGP(extra, FKEAABHAMGJ, BNOEIPBFPJB, NCDOKNKNDGC);
    if (FKEAABHAMGJ == 11 || FKEAABHAMGJ == 14) {
        [Preferences updateCD];
        //DBG(@"updating CD");
    }
}

static int replaced_get_CurrentPokeball (void *extra)
{
    int ball = orig_get_CurrentPokeball(extra);
    return ball;
}

static float replaced_get_AttackProbability(void *extra)
{
    float atkProb = orig_get_AttackProbability(extra);
    if (Preferences.enablePokemonFreeze == YES) {
        return 0.0;
    }
    return atkProb;
}

static float replaced_get_DodgeProbability(void *extra)
{
    float dgeProb = orig_get_DodgeProbability(extra);
    if (Preferences.enablePokemonFreeze == YES) {
        return 0.0;
    }
    return dgeProb;
}

static void replaced_OpenBallSelector (void *extra, int ball)
{
    orig_OpenBallSelector(extra, ball);
    BallType lastBall = (BallType)(ball-1);
    [Preferences setLastBall:lastBall];
}

static bool RP_MVH_GetGroundPosition (void *extra, struct Vector2 vector2, void *vector3)
{
    bool result = MVH_GetGroundPosition(extra, vector2, vector3);
    if (Preferences.enableTapToWalk == YES) {
        LatLng posi;
        MVH_GetGroundLocation(extra, vector2, &posi);
        [[SpooferxManager sharedManager] tapToWalkLat:posi.Latitude Lng:posi.Longitude];
    }
    else if (Preferences.enableTapToTeleport == YES)
    {
        LatLng posi;
        MVH_GetGroundLocation(extra, vector2, &posi);
        [[SpooferxManager sharedManager] jumpToLocation:CLLocationCoordinate2DMake(posi.Latitude, posi.Longitude)];
    }
    
    return result;
}

static void replaced_addPokemonAlt(void *a1, void* a2)
{
    [AutoBot addPokemonToMaplist:a2 Ptr2:a1];
    Il2CppObject_IG *amp = (Il2CppObject_IG*)a2;
    NSString *ptype = [[NSString alloc] initWithUTF8String:amp->klass->_1.name];
    if ([ptype isEqualToString:@"QuestMapPokemon"] == YES ||
        [ptype isEqualToString:@"RaidMapPokemon"] == YES ||
        [ptype isEqualToString:@"IncidentMapPokemon"] == YES)
    {
        SPX_UserEncounterPokemon *mon = [[SPX_UserEncounterPokemon alloc] initWitPtr:a2];
        //DBG(@"SPX_UserEncounterPokemon -> %@",mon.description);
        [EncounterMan setCurrentUserEncounter:mon];
        EncounterMan.isUserEncounter = YES;
        IGP_JournalActivityType activityType;
        if (mon.spx_pokemonType == SPX_PokemonType_QuestMapPokemon) {
            activityType = IGP_JournalActivityType_QuestEncounter;
        }
        else if (mon.spx_pokemonType == SPX_PokemonType_RaidMapPokemon)
        {
            activityType = IGP_JournalActivityType_RaidEncounter;
        }
        else if (mon.spx_pokemonType == SPX_PokemonType_IncidentMapPokemon)
        {
            activityType = IGP_JournalActivityType_InvasionEncounter;
        }
        
        JournalActivity *wilda = [[JournalActivity alloc] init];
        wilda.activityType = activityType;
        wilda.encounterId = mon.spx_encounterid;
        wilda.pokemonId = mon.spx_pokemonid;
        wilda.aStatus = 1;
        wilda.aLatitude = mon.spx_latitude;
        wilda.aLongitude = mon.spx_longitude;
        [ActivityManager SavePokemonEncounterActivity:wilda];
    }
    
    orig_addPokemon(a1,a2);
}

static void replaced_removePokemon(void *a1, void* a2)
{
    [AutoBot removePokemonFromMaplist:a2];
    orig_removePokemon(a1,a2);
}

static void replaced_PokemonSelected (int *extra, void *mapPokemon)
{
    SPX_UserEncounterPokemon *mon = [[SPX_UserEncounterPokemon alloc] initWitPtr:mapPokemon];
    [EncounterMan setCurrentUserEncounter:mon];
    EncounterMan.isUserEncounter = YES;
    Il2CppObject_IG *amp = (Il2CppObject_IG*)mapPokemon;
    NSString *ptype = [[NSString alloc] initWithUTF8String:amp->klass->_1.name];
    if ([ptype isEqualToString:@"StarterMapPokemon"] == YES)
    {
        Preferences.starterPokemonSelected = YES;
    }
    orig_PokemonSelected(extra, mapPokemon);
}

static void replaced_proximityWakeUp(void *MapPokestop)
{
    //DBG(@"Woke up Stop -> %p",MapPokestop);
    [AutoBot addPokestopToMaplist:MapPokestop];
    ORG_ProximityWakeUp(MapPokestop);
}

static void replaced_proximitySleep(void *MapPokestop)
{
    //DBG(@"Sleeping Stop -> %p",MapPokestop);
    [AutoBot removeStopFromMaplist:MapPokestop];
    ORG_ProximitySleep(MapPokestop);
}

static void replaced_ProximityTriggerEnter (void *MapGym)
{
    //DBG(@"Woke up Gym -> %p",MapGym);
    [AutoBot addGymToMaplist:MapGym];
    ORG_ProximityTriggerEnter(MapGym);
}

static void replaced_ProximityTriggerExit (void *MapGym)
{
    //DBG(@"Exit Gym -> %p",MapGym);
    [AutoBot removeGymFromMaplist:MapGym];
    ORG_ProximityTriggerExit(MapGym);
}

static void replaced_LogOut (int *extra)
{
    [Inventory clearInventory];
    [ActivityManager clearActivity];
    
    [Preferences setAutoCatch:NO];
    [Preferences setAutoSpin:NO];
    [Preferences setCatchWhenFound:NO];
    [Preferences setBerryBeforeCatch:NO];
    
    orig_LogOut(extra);
}

static void replaced_StartSession (void *pgpService, void *scannedDevice)
{
    [Preferences setAutoCatch:NO];
    [Preferences setAutoSpin:NO];
    [Preferences setCatchWhenFound:NO];
    [Preferences setBerryBeforeCatch:NO];
    Preferences.pgpService = pgpService;
    
    ORG_StartSession(pgpService, scannedDevice);
}

static void replaced_EIS_EnterState (void *eis)
{
    [EncounterMan setCurrentEis:eis];
    ORG_EIS_EnterState(eis);
}

static void RPL_WMP_UpdateData (void *wmp, void *wmproto)
{
    Holoholo_Rpc_WildPokemonProto_o *wpproto = (Holoholo_Rpc_WildPokemonProto_o*)wmproto;
    if ([Preferences.shiniesArray containsObject:@(wpproto->fields.encounterId_)] == YES) {
        wpproto->fields.pokemon_->fields.pokemonDisplay_->fields.shiny_ = true;
    }
    
    ORG_WMP_UpdateData(wmp, wmproto);
}

static void RPL_IRS_UpdateInvasionBattle (void *irpc, void *updateproto)
{
    if (Preferences.proRocketBattle == YES) {
        Holoholo_Rpc_UpdateInvasionBattleProto_o *upd = (Holoholo_Rpc_UpdateInvasionBattleProto_o*)updateproto;
        if (upd->fields.updateType_ == 2) {
            upd->fields.updateType_ = 1;
        }
    }
    ORG_IRS_UpdateInvasionBattle(irpc,updateproto);
}

static void RPL_ES_BeginEncounterApproach (void *es)
{
    if (Preferences.skipAnimations == YES) {
        ES_ApproachComplete(es);
    }
    else
    {
        ORG_ES_BeginEncounterApproach(es);
    }
}

static void* RPL_IS_ProcessIncidentEndOfCombatData (void *is, void *grop, int combatstate, int remainingMon, bool mapFrag)
{
    if (Preferences.proRocketBattle == YES) {
        int curchar = IS_GetBattleCharacter(is);
        bool eventchar = IS_IsEventNpcCharacter(is, curchar);
        if (eventchar == true) {
            return ORG_IS_ProcessIncidentEndOfCombatData(is,grop,0,remainingMon,mapFrag);
        }
        else
        {
            IS_StartIncidentEncounter(is);
            return ORG_IS_ProcessIncidentEndOfCombatData(is,grop,0,remainingMon,mapFrag);
        }
    }
    
    return ORG_IS_ProcessIncidentEndOfCombatData(is,grop,combatstate,remainingMon,mapFrag);
}

static void RPL_GymSelected (int *extra, void *mapGym, bool isRemote, void *executeOnCleanUp)
{
    ORG_GymSelected(extra, mapGym, isRemote, executeOnCleanUp);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[EncounterManager sharedManager] addCdOverlay];
    });
    
}

static void RPL_GRC_ExitGym (void *grc, void *proto1, void *proto2)
{
    ORG_GRC_ExitGym(grc, proto1, proto2);
    dispatch_async(dispatch_get_main_queue(), ^{
        [[EncounterManager sharedManager] removeCdOverlay];
    });
}

static void RPL_PSS_SetupPokestop (void *pss, void *mps)
{
    ORG_PSS_SetupPokestop(pss, mps);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[EncounterManager sharedManager] addCdOverlay];
    });
}

static void RPL_PSS_ExitState (void *pss)
{
    ORG_PSS_ExitState(pss);
    dispatch_async(dispatch_get_main_queue(), ^{
        [[EncounterManager sharedManager] removeCdOverlay];
    });
}

static void RPL_MES_OMHODHKMCOE (void *mes, void *list)
{
    if (Preferences.skipAnimations == NO) {
        ORG_MES_OMHODHKMCOE(mes,list);
    }
}

static int RPL_PT_GetGrade (void *pt, void *esp)
{
    int grade = ORG_PT_GetGrade(pt,esp);
    Niantic_Holoholo_Encounter_PokeballThrow_o *thr = (Niantic_Holoholo_Encounter_PokeballThrow_o*)pt;
    if(thr != 0x0 && Preferences.enableModdedThrow == YES)
    {
        if (thr->fields.BallType >= 5) {
            return 4;
        }
        else
        {
            switch (Preferences.userThrow) {
                case 0: return grade;
                case 1:
                case 2:
                case 3:
                case 4:
                {
                    return Preferences.userThrow;
                }
                    break;
                case 5: return grade;
                default:
                    return grade;
                    break;
            }
        }
    }
    
    return grade;
}

static void RPL_NCRS_EnterState (void *ncrs, void *api, void *data)
{
    if(Preferences.proTrainerBattle == YES)
    {
        Niantic_Holoholo_Combat_Resolution_NpcCombatResolveState_Data_o *ddata = (Niantic_Holoholo_Combat_Resolution_NpcCombatResolveState_Data_o*)data;
        ddata->fields.CombatCompletionData->fields.EndState = 6;
        ddata->fields.CombatCompletionData->fields.FinishState = 0;
        ORG_NCRS_EnterState(ncrs,api,ddata);
        NCRS_DismissAndComplete(ncrs);
    }
    else
    {
        ORG_NCRS_EnterState(ncrs,api,data);
    }
}

static void* RPL_CRRS_BuildNpcRewardsProto (void *crrs, void *data)
{
    if(Preferences.proTrainerBattle == YES)
    {
        Niantic_Holoholo_Combat_Resolution_CombatResultsRpcService_NpcCombatRewardsData_o *ddata = (Niantic_Holoholo_Combat_Resolution_CombatResultsRpcService_NpcCombatRewardsData_o*)data;
        ddata->fields.FinishState = 0;
        return ORG_CRRS_BuildNpcRewardsProto(crrs,ddata);
    }
    
    return ORG_CRRS_BuildNpcRewardsProto(crrs,data);
}

static void RPL_PIS_EnterState (void *pis)
{
    ORG_PIS_EnterState(pis);
    
    if(Preferences.enableQuickSpin)
    {
        void *map = PIS_GetInteractivePokestop(pis);
        if(map != 0x0)
        {
            void *spinner = MPI_GetItemSpinner(map);
            if(spinner != 0x0)
            {
                PIS_SendFortSearchRpc(spinner);
                PIS_InteractionComplete(pis);
            }
        }
    }
}

static void* RPL_GRS_CheckGiftingStatus (void *grs, Il2CppString *senderPlayerId)
{
    Preferences.GiftingRpcService = grs;
    if(Preferences.enableQuickGift)
    {
        sendingPlayerId = senderPlayerId;
    }
    
    return ORG_GRS_CheckGiftingStatus(grs,senderPlayerId);
}

static void RPL_GS_MarkViewed (void *gs, unsigned long giftId)
{
    if(Preferences.enableQuickGift)
    {
        receivedGiftId = giftId;
    }
    
    ORG_GS_MarkViewed(gs,giftId);
}

static void RPL_FGS_StartOpenGiftFlow (void *fgs, void *gifts)
{
    if(Preferences.enableQuickGift)
    {
        if(receivedGiftId > 0 && sendingPlayerId != 0x0)
        {
            NSString *spid = [SxUtilities NSStringFromIl2Cpp:(const ushort*)sendingPlayerId->chars length:sendingPlayerId->length];
            DBG(@"Opening Gift %lu from PlayerId %@",receivedGiftId,spid);
            GRS_OpenGift(Preferences.GiftingRpcService,receivedGiftId,sendingPlayerId);
            receivedGiftId = 0;
            sendingPlayerId = 0x0;
        }
    }
    else
    {
        ORG_FGS_StartOpenGiftFlow(fgs, gifts);
    }
}

static void RPL_GST_StartSendGiftFlow (void *gst, void* proto)
{
    if(Preferences.enableQuickGift)
    {
        void *psp = FDP_GetPlayer(proto);
        playerId = PSP_GetPlayerId(psp);
        ORG_GST_StartSendGiftFlow(gst,proto);
        GST_Complete(gst);
    }
    else
    {
        ORG_GST_StartSendGiftFlow(gst,proto);
    }
}

static void* RPL_GS_TryGetGiftBoxDetails (void *gs, void *giftproto)
{
    if(Preferences.enableQuickGift)
    {
        unsigned long giftId = GBP_GetGiftboxId(giftproto);
        if(giftId > 0 && playerId != 0x0)
        {
            Holoholo_Rpc_StickerProto_o *sticker = 0x0;
            if(Preferences.StickerService != 0x0)
            {
                bool hasStickers = SS_AnyStickers(Preferences.StickerService);
                if(hasStickers)
                {
                    System_Collections_Generic_List_StickerProto__o *sslist = SS_GetStickerInventory(Preferences.StickerService,false);
                    sticker = sslist->fields._items->m_Items[0];
                }
            }
            
            GRS_SendGift(Preferences.GiftingRpcService, giftId, playerId, sticker);
            playerId = 0x0;
        }
    }
    
    return ORG_GS_TryGetGiftBoxDetails(gs,giftproto);
}

static void RPL_FPGC_OnCloseButtonClicked (void *fpgc)
{
    receivedGiftId = 0;
    sendingPlayerId = 0x0;
    ORG_FPGC_OnCloseButtonClicked(fpgc);
}

static void RPL_GRS_OpenGift_b__0 (void *grs, void *ogop)
{
    ORG_GRS_OpenGift_b__0(grs,ogop);
    if(Preferences.enableQuickGift)
    {
        int32_t result = OGOP_GetResult(ogop);
        switch (result) {
            case 0:
                break;
            case 1:
                break;
            case 2:
                [[SpooferxManager sharedManager] presentTextNotification:@"Open Gift Error" line2:@"Unknown Error Occured"];
                break;
            case 3:
                [[SpooferxManager sharedManager] presentTextNotification:@"Open Gift Error" line2:@"Item bag is Full"];
                break;
            case 4:
                [[SpooferxManager sharedManager] presentTextNotification:@"Open Gift Error" line2:@"Daily Limit Reached"];
                break;
            case 5:
                [[SpooferxManager sharedManager] presentTextNotification:@"Open Gift Error" line2:@"Gift Does not Exist"];
                break;
            case 6:
                break;
            case 7:
                break;
            case 8:
                break;
            default:
                break;
        }
    }
}

static void RPL_PDC_PlayPurifyLightSfx (void *pdc)
{
    purifyComplete(pdc);
}

static bool RPL_ASDS_GetEnabled (void *asds)
{
    bool enabled = ORG_ASDS_GetEnabled(asds);
    if(enabled == false && Preferences.starterPokemonSelected == YES)
    {
        ASDS_Enable(asds, true);
        return true;
    }
    
    if (enabled == false && Preferences.asBugFixed == NO)
    {
        ASDS_Enable(asds, true);
        [Preferences setAsBugFixed:YES];
        return true;
    }
    
    return enabled;
}

+ (void) loadUnitySL_Hooks {
    @autoreleasepool
    {
        auto header = _dyld_get_image_header(Preferences.unityIndex);
        auto vmslide = _dyld_get_image_vmaddr_slide(Preferences.unityIndex);
        auto manager = StaticHooks::InitForImage(header);
        if (manager == nullptr) {
            return;
        }
        
        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_RH_Send),
                        reinterpret_cast<void*>(&replaced_RpcSend2),
                        reinterpret_cast<void**>(&orig_RpcSend2));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_AR_OnResponse),
                        reinterpret_cast<void*>(&replaced_onResponse_k),
                        reinterpret_cast<void**>(&orig_onResponse_k));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_MES_GymRootComplete),
                        reinterpret_cast<void*>(&replaced_GymRootComplete),
                        reinterpret_cast<void**>(&orig_GymRootComplete));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_CP_SetHeading),
                        reinterpret_cast<void*>(&replaced_SetHeading),
                        reinterpret_cast<void**>(&orig_CP_SetHeading));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_CDS_GetFastAttackDamage),
                        reinterpret_cast<void*>(&replaced_GetFastAttackDamage),
                        reinterpret_cast<void**>(&orig_GetFastAttackDamage));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_MES_SetUIVisible),
                        reinterpret_cast<void*>(&replaced_SetUIVisible),
                        reinterpret_cast<void**>(&orig_SetUIVisible));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_PDC_ShowEvolveAnimation),
                        reinterpret_cast<void*>(&replaced_ShowEvolveAnimation),
                        reinterpret_cast<void**>(&orig_ShowEvolveAnimation));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_PDC_ShowMegaEvolveAnimation),
                        reinterpret_cast<void*>(&replaced_ShowMegaEvolveAnimation),
                        reinterpret_cast<void**>(&orig_ShowMegaEvolveAnimation));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_PDC_ShowNewSpeciesCaughtAnimation),
                        reinterpret_cast<void*>(&replaced_ShowNewSpeciesCaughtAnimation),
                        reinterpret_cast<void**>(&orig_ShowNewSpeciesCaughtAnimation));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_EIS_ExitState),
                        reinterpret_cast<void*>(&replaced_EIS_ExitSate),
                        reinterpret_cast<void**>(&orig_EIS_ExitState));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_PILL_Sort),
                        reinterpret_cast<void*>(&replaced_sort),
                        reinterpret_cast<void**>(&orig_Sort));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_PIDSR_Cleanup),
                        reinterpret_cast<void*>(&replaced_cleanup),
                        reinterpret_cast<void**>(&orig_cleanup));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_PIGC_IALLLKNMANI),
                        reinterpret_cast<void*>(&replaced_IALLLKNMANI),
                        reinterpret_cast<void**>(&orig_IALLLKNMANI));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_TS_ReportPGP),
                        reinterpret_cast<void*>(&replaced_ReportPGP),
                        reinterpret_cast<void**>(&orig_ReportPGP));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_EP_GetAttackProbability),
                        reinterpret_cast<void*>(&replaced_get_AttackProbability),
                        reinterpret_cast<void**>(&orig_get_AttackProbability));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_EP_GetDodgeProbability),
                        reinterpret_cast<void*>(&replaced_get_DodgeProbability),
                        reinterpret_cast<void**>(&orig_get_DodgeProbability));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_MVH_GetGroundPosition),
                        reinterpret_cast<void*>(&RP_MVH_GetGroundPosition),
                        reinterpret_cast<void**>(&MVH_GetGroundPosition));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_PDS_AddPokemon),
                        reinterpret_cast<void*>(&replaced_addPokemonAlt),
                        reinterpret_cast<void**>(&orig_addPokemon));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_PDS_RemovePokemon),
                        reinterpret_cast<void*>(&replaced_removePokemon),
                        reinterpret_cast<void**>(&orig_removePokemon));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_MES_PokemonSelected),
                        reinterpret_cast<void*>(&replaced_PokemonSelected),
                        reinterpret_cast<void**>(&orig_PokemonSelected));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_MS_ProximityWakeup),
                        reinterpret_cast<void*>(&replaced_proximityWakeUp),
                        reinterpret_cast<void**>(&ORG_ProximityWakeUp));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_MS_ProximitySleep),
                        reinterpret_cast<void*>(&replaced_proximitySleep),
                        reinterpret_cast<void**>(&ORG_ProximitySleep));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_MG_ProximityTriggerEnter),
                        reinterpret_cast<void*>(&replaced_ProximityTriggerEnter),
                        reinterpret_cast<void**>(&ORG_ProximityTriggerEnter));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_MG_ProximityTriggerExit),
                        reinterpret_cast<void*>(&replaced_ProximityTriggerExit),
                        reinterpret_cast<void**>(&ORG_ProximityTriggerExit));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_AS_Logout),
                        reinterpret_cast<void*>(&replaced_LogOut),
                        reinterpret_cast<void**>(&orig_LogOut));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_PGPS_StartSession),
                        reinterpret_cast<void*>(&replaced_StartSession),
                        reinterpret_cast<void**>(&ORG_StartSession));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_EIS_EnterState),
                        reinterpret_cast<void*>(&replaced_EIS_EnterState),
                        reinterpret_cast<void**>(&ORG_EIS_EnterState));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_WMP_UpdateData),
                        reinterpret_cast<void*>(&RPL_WMP_UpdateData),
                        reinterpret_cast<void**>(&ORG_WMP_UpdateData));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_IRS_UpdateInvasionBattle),
                        reinterpret_cast<void*>(&RPL_IRS_UpdateInvasionBattle),
                        reinterpret_cast<void**>(&ORG_IRS_UpdateInvasionBattle));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_IS_ProcessIncidentEndOfCombatData),
                        reinterpret_cast<void*>(&RPL_IS_ProcessIncidentEndOfCombatData),
                        reinterpret_cast<void**>(&ORG_IS_ProcessIncidentEndOfCombatData));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_ES_BeginEncounterApproach),
                        reinterpret_cast<void*>(&RPL_ES_BeginEncounterApproach),
                        reinterpret_cast<void**>(&ORG_ES_BeginEncounterApproach));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_MES_GymSelected),
                        reinterpret_cast<void*>(&RPL_GymSelected),
                        reinterpret_cast<void**>(&ORG_GymSelected));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_GRC_ExitGym),
                        reinterpret_cast<void*>(&RPL_GRC_ExitGym),
                        reinterpret_cast<void**>(&ORG_GRC_ExitGym));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_PSS_SetupPokestop),
                        reinterpret_cast<void*>(&RPL_PSS_SetupPokestop),
                        reinterpret_cast<void**>(&ORG_PSS_SetupPokestop));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_PSS_ExitState),
                        reinterpret_cast<void*>(&RPL_PSS_ExitState),
                        reinterpret_cast<void**>(&ORG_PSS_ExitState));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_MES_OMHODHKMCOE),
                        reinterpret_cast<void*>(&RPL_MES_OMHODHKMCOE),
                        reinterpret_cast<void**>(&ORG_MES_OMHODHKMCOE));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_PT_GetGrade),
                        reinterpret_cast<void*>(&RPL_PT_GetGrade),
                        reinterpret_cast<void**>(&ORG_PT_GetGrade));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_NCRS_EnterState),
                        reinterpret_cast<void*>(&RPL_NCRS_EnterState),
                        reinterpret_cast<void**>(&ORG_NCRS_EnterState));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_CRRS_BuildNpcRewardsProto),
                        reinterpret_cast<void*>(&RPL_CRRS_BuildNpcRewardsProto),
                        reinterpret_cast<void**>(&ORG_CRRS_BuildNpcRewardsProto));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_PIS_EnterState),
                        reinterpret_cast<void*>(&RPL_PIS_EnterState),
                        reinterpret_cast<void**>(&ORG_PIS_EnterState));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_GRS_CheckGiftingStatus),
                        reinterpret_cast<void*>(&RPL_GRS_CheckGiftingStatus),
                        reinterpret_cast<void**>(&ORG_GRS_CheckGiftingStatus));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_GS_MarkViewed),
                        reinterpret_cast<void*>(&RPL_GS_MarkViewed),
                        reinterpret_cast<void**>(&ORG_GS_MarkViewed));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_FGS_StartOpenGiftFlow),
                        reinterpret_cast<void*>(&RPL_FGS_StartOpenGiftFlow),
                        reinterpret_cast<void**>(&ORG_FGS_StartOpenGiftFlow));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_GST_StartSendGiftFlow),
                        reinterpret_cast<void*>(&RPL_GST_StartSendGiftFlow),
                        reinterpret_cast<void**>(&ORG_GST_StartSendGiftFlow));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_GS_TryGetGiftBoxDetails),
                        reinterpret_cast<void*>(&RPL_GS_TryGetGiftBoxDetails),
                        reinterpret_cast<void**>(&ORG_GS_TryGetGiftBoxDetails));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_FPGC_OnCloseButtonClicked),
                        reinterpret_cast<void*>(&RPL_FPGC_OnCloseButtonClicked),
                        reinterpret_cast<void**>(&ORG_FPGC_OnCloseButtonClicked));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_GRS_OpenGift_b__0),
                        reinterpret_cast<void*>(&RPL_GRS_OpenGift_b__0),
                        reinterpret_cast<void**>(&ORG_GRS_OpenGift_b__0));

        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_PDC_PlayPurifyLightSfx),
                        reinterpret_cast<void*>(&RPL_PDC_PlayPurifyLightSfx),
                        reinterpret_cast<void**>(&ORG_PDC_PlayPurifyLightSfx));
        
        manager->Enable(reinterpret_cast<void*>(vmslide + ADR_ASDS_GetEnabled),
                        reinterpret_cast<void*>(&RPL_ASDS_GetEnabled),
                        reinterpret_cast<void**>(&ORG_ASDS_GetEnabled));
        
        DBG(@"Done UnityFramework hooks");
    }
}

@end

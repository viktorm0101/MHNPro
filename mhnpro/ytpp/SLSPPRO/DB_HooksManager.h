//
//  DB_HooksManager.h
//  spooferpro
//
//  Created by Malhar Ambekar on 1/28/23.
//

#import <Foundation/Foundation.h>

#define DB_Hooks [DB_HooksManager sharedManager]

NS_ASSUME_NONNULL_BEGIN

@interface DB_HooksManager : NSObject

+ (DB_HooksManager *)sharedManager;
- (void)enableDB_Hooks;
- (void)disableDB_Hooks;

@end

NS_ASSUME_NONNULL_END

//
//  ImportsViewController.m
//  pokextest
//
//  Created by MADHATTER on 1/4/23.
//  Copyright © 2023 iphonecake.com. All rights reserved.
//

#import "ImportsViewController.h"
#import "RoutesManager.h"
#import "IGP_PlacesManager.h"

@interface ImportsViewController () <UITextViewDelegate>

@end

@implementation ImportsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController.navigationBar setTranslucent:YES];
    self.navigationController.navigationBar.tintColor = [SxUtilities ThemeColor];
    
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
    
    self.importTextView.delegate = self;
    [self.importTextView.layer setBorderColor: [[UIColor grayColor] CGColor]];
    [self.importTextView.layer setBorderWidth: 1.0];
    [self.importTextView.layer setCornerRadius:5.0f];
    [self.importTextView.layer setMasksToBounds:YES];
    [self setPlaceHolder];
    
    [self.importButton setBackgroundColor:[SxUtilities ThemeColor]];
    [self.importButton setTitle:@"Import" forState:UIControlStateNormal];
    [self.importButton.titleLabel setFont:[UIFont systemFontOfSize:24 weight:UIFontWeightMedium]];
    [self.importButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.importButton.layer setCornerRadius:5.0f];
    [self.importButton.layer setMasksToBounds:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [[IQKeyboardManager sharedManager] setEnable:NO];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
}

- (void)setPlaceHolder
{
    self.importTextView.text = @"Paste your backup file contents here. You can rename your old backup file to .json from .pspxr to open and copy its contents.";
    self.importTextView.textColor = [UIColor systemGrayColor];
}

- (IBAction)importAction:(id)sender
{
    if([self.importTextView.text length] > 0)
    {
        NSError *readError;
        NSString *json = [self.importTextView.text copy];
        NSData *dd = [json dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:dd options:kNilOptions error:&readError];
        if (readError == nil) {
            NSString *filetype = jsonDict[@"fileType"];
            if ([filetype isEqualToString:@"routes"] == YES) {
                [RoutesMan importRoutesFile:jsonDict];
            }
            else if ([filetype isEqualToString:@"prefs"] == YES)
            {

            }
            else if ([filetype isEqualToString:@"spots"] == YES)
            {
                [PlacesManager2 importHotspotsFile:jsonDict];
            }
        }
    }
    
    self.importTextView.text = nil;
    [self textViewDidEndEditing:self.importTextView];
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if(textView.textColor == [UIColor systemGrayColor])
    {
        textView.text = nil;
        textView.textColor = [UIColor labelColor];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if([textView.text length] == 0)
    {
        [self setPlaceHolder];
    }
}

@end

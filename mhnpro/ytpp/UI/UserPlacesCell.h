//
//  UserPlacesCell.h
//  igoprotest
//
//  Created by MADHATTER on 9/12/22.
//

#import <UIKit/UIKit.h>
#import "UserPlacesController.h"
NS_ASSUME_NONNULL_BEGIN

@interface UserPlacesCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *countryFlag;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *clockLabel;
@property (nonatomic, weak) IBOutlet UILabel *extraLabel;
@property (nonatomic, strong) UserPlacesController *parentController;
@property (nonatomic, strong) NSIndexPath *myIndexPath;

- (void)setupPlace:(NSDictionary*)place key:(NSString*)key path:(NSIndexPath*)indexPath;
- (void)stopFakelocationTimer;


@end

NS_ASSUME_NONNULL_END

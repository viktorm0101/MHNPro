//
//  UserPlacesController.h
//  igoprotest
//
//  Created by MADHATTER on 9/12/22.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserPlacesController : UIViewController

@property (nonatomic, weak) IBOutlet UITableView *placesList;
@property (nonatomic, strong) NSMutableArray *editingCells;

@end

NS_ASSUME_NONNULL_END

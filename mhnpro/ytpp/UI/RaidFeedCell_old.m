//
//  RaidFeedCell.m
//  SDWebImage
//
//  Created by c0pyn1nja on 20/03/21
//  Copyright © 2021 Dailymotion. All rights reserved.
//

#import "RaidFeedCell.h"
#import <UIImageView+WebCache.h>
#import "IGP_ActivityManager.h"

@interface RaidFeedCell ()

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *subtitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *bodyLabel;
@property (nonatomic, weak) IBOutlet UILabel *flagLabel;
@property (nonatomic, weak) IBOutlet UIImageView *raidImageView;
@property (nonatomic, weak) IBOutlet UIImageView *gymImageView;
@property (nonatomic, weak) IBOutlet UIImageView *weatherImageView;
@property (nonatomic, weak) IBOutlet UIImageView *exRaidImageView;
@property (nonatomic, strong) RaidFeed *raidMon;

@end

@implementation RaidFeedCell

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.raidImageView.image = nil;
    self.gymImageView.image = nil;
    self.weatherImageView.image = nil;
    self.exRaidImageView.image = nil;
    
    self.titleLabel.text = @"";
    self.subtitleLabel.text = @"";
    self.bodyLabel.text = @"";
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setupRaidPokemon:(RaidFeed*)mon
{
    self.raidMon = mon;
    
    NSString *emoji = @"";
    NSString *cc = [NSString stringWithFormat:@"%@",self.raidMon.countryCode];
    if (cc.length == 2) {
      NSString *emo = [NSLocale  emojiFlagForISOCountryCode:cc];
        emoji = [NSString stringWithFormat:@"%@",emo];
    }
    [self.flagLabel setText:emoji];

    NSString *pimageName = [SxUtilities imageNameRaid:mon];
    if ([pimageName isEqualToString:@""]) {
        NSString *iconFile = [NSString stringWithFormat:@"%d.png",mon.team.intValue];
        iconFile = [kTeamIconUrl stringByAppendingPathComponent:iconFile];
        [self.raidImageView sd_setImageWithURL:[NSURL URLWithString:iconFile]];
    }
    else if ([pimageName hasSuffix:@".png"])
    {
        pimageName = [kPokemonIconUrl stringByAppendingString:pimageName];
        [self.raidImageView sd_setImageWithURL:[NSURL URLWithString:pimageName] placeholderImage:[AssetsManager imageForResource:@"0"]];
    }
    else
    {
        [self.raidImageView setImage:[AssetsManager imageForResource:pimageName]];
    }
    
    //UIImage *gymImage = [AssetsManager imageForResource:self.raidMon.gymImageName];
    //NSString *gimageName = [SxUtilities iconForGym:self.raidMon.team.intValue];
    NSString *iconFile = [NSString stringWithFormat:@"%d.png",self.raidMon.team.intValue];
    iconFile = [kTeamIconUrl stringByAppendingPathComponent:iconFile];
    [self.gymImageView sd_setImageWithURL:[NSURL URLWithString:iconFile] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
    }];
    
    NSString *title = [SxUtilities nameForRaidN:mon];
    [self.titleLabel setText:title];
    NSString *raidkey = [NSString stringWithFormat:@"%@_%@",mon.internalBaseClassIdentifier,mon.raidSpawnTimestamp];
    BOOL encountered = [ActivityManager IsRaidVisited:raidkey];
    if (!encountered) {
        [self.titleLabel setTextColor:[SxUtilities ThemeColor]];
    }
    else
    {
        [self.titleLabel setTextColor:[UIColor labelColor]];
    }
    
    NSString *subtutle = @"";
    NSDate *currentdate = [NSDate date];
    NSDate *battleDate = [NSDate dateWithTimeIntervalSince1970:self.raidMon.raidBattleTimestamp.doubleValue];
    NSDate *endDate = [NSDate dateWithTimeIntervalSince1970:self.raidMon.raidEndTimestamp.doubleValue];
    if ([currentdate isEarlierThan:battleDate]) {
        NSDateComponents *components;
        NSInteger mins;
        components = [[NSCalendar currentCalendar] components: NSCalendarUnitMinute
                                                     fromDate: currentdate toDate: battleDate options: 0];
        mins = [components minute];
        subtutle = [NSString stringWithFormat:@"Starts In %ld Mins",mins];
    }
    else if ([currentdate isLaterThan:battleDate] && [currentdate isEarlierThan:endDate])
    {
        NSDateComponents *components;
        NSInteger mins;
        components = [[NSCalendar currentCalendar] components: NSCalendarUnitMinute
                                                     fromDate: currentdate toDate: endDate options: 0];
        mins = [components minute];
        subtutle = [NSString stringWithFormat:@"Ends In %ld Mins",mins];
    }
    [self.subtitleLabel setText:subtutle];
    [self.subtitleLabel setTextColor:[UIColor secondaryLabelColor]];
    CLLocationCoordinate2D moncord = [SxUtilities CLLocationCoordinate2DMakeNormal:mon.latitude.doubleValue lon:mon.longitude.doubleValue];
    NSString *cdstr = [SxUtilities cooldownStringForDestination:moncord];
    
    NSString *bodyText = [NSString stringWithFormat:@"%@",cdstr];
    NSString *body = [NSString stringWithFormat:@"%@,%@",self.raidMon.countryCode,bodyText];
    [self.bodyLabel setText:body];
    [self.bodyLabel setTextColor:[UIColor secondaryLabelColor]];
}

@end

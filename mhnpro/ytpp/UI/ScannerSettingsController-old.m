//
//  ScannerSettingsController.m
//  igppogosl
//
//  Created by c0pyn1nja on 13/10/21
//  
//

#import "ScannerSettingsController.h"
#import "FCAlertView.h"
#import "RpcManager.h"

@interface ScannerSettingsController () <FCAlertViewDelegate>

@property (nonatomic, strong) UISwitch *scannerSwitch;
@property (nonatomic, strong) UISwitch *catcherSwitch;
@property (nonatomic, strong) UISwitch *berrySwitch;
@property (nonatomic, strong) NSMutableDictionary *scannerPrefs;

@end

@implementation ScannerSettingsController
{
    FCAlertView *swarningFCAlert;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
}

- (void)setupUI
{
    self.navigationController.navigationBar.tintColor = [SxUtilities ThemeColor];
    self.tableView.delegate = self;
    self.title = @"Scanner Settings";
    
    self.scannerPrefs = [[NSMutableDictionary alloc] init];
    self.scannerPrefs = [Preferences.scannerPrefs mutableCopy];
    
    self.scannerSwitch = [[UISwitch alloc] init];
    self.scannerSwitch.onTintColor = [SxUtilities ThemeColor];
    [self.scannerSwitch addTarget:self action:@selector(scannerSwitchToggled:) forControlEvents:UIControlEventTouchUpInside];
    [self.scannerSwitch setOn:Preferences.enablePokemonScanner];
    
    self.catcherSwitch = [[UISwitch alloc] init];
    self.catcherSwitch.onTintColor = [SxUtilities ThemeColor];
    [self.catcherSwitch addTarget:self action:@selector(catcherSwitchToggled:) forControlEvents:UIControlEventTouchUpInside];
    [self.catcherSwitch setOn:Preferences.catchWhenFound];
    
    self.berrySwitch = [[UISwitch alloc] init];
    self.berrySwitch.onTintColor = [SxUtilities ThemeColor];
    [self.berrySwitch addTarget:self action:@selector(berrySwitchToggled:) forControlEvents:UIControlEventTouchUpInside];
    [self.berrySwitch setOn:Preferences.berryBeforeCatch];
    
    [self.tableView reloadData];
}

- (void)scannerSwitchToggled:(UISwitch*)sender
{
    UISwitch *lnswitch = (UISwitch*)sender;
    BOOL isON = lnswitch.on ? YES : NO;

    if (isON == YES) {
        [Preferences setAutoCatch:NO];
        [Preferences setAutoSpin:NO];
        [Preferences setEnablePokemonScanner:YES];
    }
    else
    {
        [Preferences setEnablePokemonScanner:isON];
    }
}

- (void)catcherSwitchToggled:(UISwitch*)sender
{
    UISwitch *lnswitch = (UISwitch*)sender;
    BOOL isON = lnswitch.on ? YES : NO;
    
    if (isON == YES) {
        if (!Preferences.enablePokemonScanner) {
            [self enableScannerError];
        }
        else
        {
            [Preferences setCatchWhenFound:YES];
        }
    }
    else
    {
        [Preferences setCatchWhenFound:isON];
    }
}

- (void)berrySwitchToggled:(UISwitch*)sender
{
    UISwitch *lnswitch = (UISwitch*)sender;
    BOOL isON = lnswitch.on ? YES : NO;

    if (isON == YES) {
        if (!Preferences.catchWhenFound) {
            [self enableBerryError];
        }
        else
        {
            [Preferences setBerryBeforeCatch:YES];
        }
    }
    else
    {
        [Preferences setBerryBeforeCatch:isON];
    }
}

- (void)showWarning
{
    swarningFCAlert = [[FCAlertView alloc] init];
    swarningFCAlert.tag = 0002;
    swarningFCAlert.delegate = self;
    swarningFCAlert.colorScheme = [SxUtilities ThemeColor];
    swarningFCAlert.darkTheme = [SxUtilities isDark];
    swarningFCAlert.detachButtons = YES;
    [swarningFCAlert makeAlertTypeCaution];
    [swarningFCAlert showAlertInView:[UIViewController spxoverlay_topMostController]
                        withTitle:@"Scanner Warning"
                     withSubtitle:@"Pokemon scanner is dangerous feature, effects of using it are not tested. We recommend using it on Alt accounts. Are you sure, you want to enable scanner?"
                  withCustomImage:nil
              withDoneButtonTitle:@"Enable"
                       andButtons:@[@"Cancel"]];
}

- (void)enableScannerError
{
    swarningFCAlert = [[FCAlertView alloc] init];
    swarningFCAlert.tag = 0003;
    swarningFCAlert.delegate = self;
    swarningFCAlert.colorScheme = [SxUtilities ThemeColor];
    swarningFCAlert.darkTheme = [SxUtilities isDark];
    swarningFCAlert.detachButtons = YES;
    [swarningFCAlert makeAlertTypeWarning];
    [swarningFCAlert showAlertInView:[UIViewController spxoverlay_topMostController]
                        withTitle:@"Scanner Error"
                     withSubtitle:@"Please enable Pokemon Scanner first."
                  withCustomImage:nil
              withDoneButtonTitle:@"Dismiss"
                       andButtons:nil];
}

- (void)enableBerryError
{
    swarningFCAlert = [[FCAlertView alloc] init];
    swarningFCAlert.tag = 0004;
    swarningFCAlert.delegate = self;
    swarningFCAlert.colorScheme = [SxUtilities ThemeColor];
    swarningFCAlert.darkTheme = [SxUtilities isDark];
    swarningFCAlert.detachButtons = YES;
    [swarningFCAlert makeAlertTypeWarning];
    [swarningFCAlert showAlertInView:[UIViewController spxoverlay_topMostController]
                        withTitle:@"Scanner Error"
                     withSubtitle:@"Please enable Catch When Found first."
                  withCustomImage:nil
              withDoneButtonTitle:@"Dismiss"
                       andButtons:nil];
}

- (void)FCAlertDoneButtonClicked:(FCAlertView *)alertView
{
    if (alertView.tag == 0002)
    {
        [Preferences setAutoCatch:NO];
        [Preferences setAutoSpin:NO];
        [Preferences setEnablePokemonScanner:YES];
    }
    else if (alertView.tag == 0003)
    {
        [self.catcherSwitch setOn:NO animated:YES];
    }
    else if (alertView.tag == 0004)
    {
        [self.berrySwitch setOn:NO animated:YES];
    }
}

- (void) FCAlertView:(FCAlertView *)alertView clickedButtonIndex:(NSInteger)index buttonTitle:(NSString *)title {
    if (alertView.tag == 0002) {
        if ([title isEqualToString:@"Cancel"] == YES) {
            [self.scannerSwitch setOn:NO animated:YES];
        }
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return 3;
            break;
        case 1:
            return 5;
        default:
            return 0;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    switch (indexPath.section) {
        case 0:
            switch (indexPath.row) {
                case 0:
                {
                    [cell.textLabel setText:@"Enable Pokemon Scanner"];
                    cell.accessoryView = self.scannerSwitch;
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                }
                    break;
                case 1:
                {
                    [cell.textLabel setText:@"Catch When Found"];
                    cell.accessoryView = self.catcherSwitch;
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    if ([SxUtilities isLegendaryPokemon] == NO) {
                        self.catcherSwitch.enabled = NO;
                        cell.userInteractionEnabled = NO;
                        cell.textLabel.enabled = NO;
                    }
                }
                    break;
                case 2:
                {
                    [cell.textLabel setText:@"Feed Berry First"];
                    cell.accessoryView = self.berrySwitch;
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    if ([SxUtilities isLegendaryPokemon] == NO) {
                        self.berrySwitch.enabled = NO;
                        cell.userInteractionEnabled = NO;
                        cell.textLabel.enabled = NO;
                    }
                }
                    break;
                default:
                    break;
            }
            break;
        case 1:
        {
            switch (indexPath.row) {
                case 0:
                {
                    BOOL sc_shiny = [[self.scannerPrefs objectForKey:@"scan_shiny"] boolValue];
                    if (sc_shiny) {
                        cell.accessoryType = UITableViewCellAccessoryCheckmark;
                        cell.tintColor = [SxUtilities ThemeColor];
                    }
                    else
                    {
                        cell.accessoryType = UITableViewCellAccessoryNone;
                    }
                }
                    break;
                case 1:
                {
                    BOOL sc_hundo = [[self.scannerPrefs objectForKey:@"scan_hundo"] boolValue];
                    if (sc_hundo) {
                        cell.accessoryType = UITableViewCellAccessoryCheckmark;
                        cell.tintColor = [SxUtilities ThemeColor];
                    }
                    else
                    {
                        cell.accessoryType = UITableViewCellAccessoryNone;
                    }
                }
                    break;
                case 2:
                {
                    BOOL sc_nundo = [[self.scannerPrefs objectForKey:@"scan_nundo"] boolValue];
                    if (sc_nundo) {
                        cell.accessoryType = UITableViewCellAccessoryCheckmark;
                        cell.tintColor = [SxUtilities ThemeColor];
                    }
                    else
                    {
                        cell.accessoryType = UITableViewCellAccessoryNone;
                    }
                }
                    break;
                case 3:
                {
                    BOOL sc_tnrat = [[self.scannerPrefs objectForKey:@"scan_tinyrat"] boolValue];
                    if (sc_tnrat) {
                        cell.accessoryType = UITableViewCellAccessoryCheckmark;
                        cell.tintColor = [SxUtilities ThemeColor];
                    }
                    else
                    {
                        cell.accessoryType = UITableViewCellAccessoryNone;
                    }
                }
                    break;
                case 4:
                {
                    BOOL sc_bgkarp = [[self.scannerPrefs objectForKey:@"scan_bigkarp"] boolValue];
                    if (sc_bgkarp) {
                        cell.accessoryType = UITableViewCellAccessoryCheckmark;
                        cell.tintColor = [SxUtilities ThemeColor];
                    }
                    else
                    {
                        cell.accessoryType = UITableViewCellAccessoryNone;
                    }
                }
                    break;
                default:
                    break;
            }
        }
            break;
        default:
            break;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
            break;
        case 1:
        {
            switch (indexPath.row) {
                case 0:
                {
                    BOOL sc_shiny = [[self.scannerPrefs objectForKey:@"scan_shiny"] boolValue];
                    [self.scannerPrefs setObject:@(!sc_shiny) forKey:@"scan_shiny"];
                    [Preferences setScannerPrefs:self.scannerPrefs];
                }
                    break;
                case 1:
                {
                    BOOL sc_hundo = [[self.scannerPrefs objectForKey:@"scan_hundo"] boolValue];
                    [self.scannerPrefs setObject:@(!sc_hundo) forKey:@"scan_hundo"];
                    [Preferences setScannerPrefs:self.scannerPrefs];
                }
                    break;
                case 2:
                {
                    BOOL sc_nundo = [[self.scannerPrefs objectForKey:@"scan_nundo"] boolValue];
                    [self.scannerPrefs setObject:@(!sc_nundo) forKey:@"scan_nundo"];
                    [Preferences setScannerPrefs:self.scannerPrefs];
                }
                    break;
                case 3:
                {
                    BOOL sc_tnrat = [[self.scannerPrefs objectForKey:@"scan_tinyrat"] boolValue];
                    [self.scannerPrefs setObject:@(!sc_tnrat) forKey:@"scan_tinyrat"];
                    [Preferences setScannerPrefs:self.scannerPrefs];
                }
                    break;
                case 4:
                {
                    BOOL sc_bgkarp = [[self.scannerPrefs objectForKey:@"scan_bigkarp"] boolValue];
                    [self.scannerPrefs setObject:@(!sc_bgkarp) forKey:@"scan_bigkarp"];
                    [Preferences setScannerPrefs:self.scannerPrefs];
                }
                    break;
                default:
                    break;
            }
        }
            break;
        default:
            break;
    }
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    [self.tableView reloadData];
}

@end

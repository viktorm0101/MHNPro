//
//  EventPlacesController.m
//  igoprotest
//
//  Created by MADHATTER on 9/13/22.
//

#import "EventPlacesController.h"
#import "IGP_PlacesManager.h"
#import "EventPlacesCell.h"
#import "SpooferxManager.h"
#import "IGP_NotificationsManager.h"

@interface EventPlacesController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UITableView *placesList;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *doneBarButton;
@property (nonatomic, strong) NSMutableDictionary *hotSpots;
@property (nonatomic, strong) NSArray *spotsKeys;

- (IBAction)dismiss:(id)sender;

@end

@implementation EventPlacesController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.hotSpots = [[NSMutableDictionary alloc] init];
    [self setupUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
//    [Preferences setPlacesListIndex:1];
    [[SpooferxManager sharedManager] hideControls];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self loadHotspots];
    });
}

- (void)dismiss:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        [[SpooferxManager sharedManager] showControls];
    }];
}

- (void)setupUI
{
    self.navigationController.navigationBar.tintColor = [SxUtilities ThemeColor];
    self.placesList.contentInset = UIEdgeInsetsMake(0.0f, 0.0f, CGRectGetHeight(self.tabBarController.tabBar.frame), 0.0f);
    self.placesList.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    //[self loadHotspots];
}

//- (void)loadHotspots
//{
//    self.hotSpots = [[PlacesManager2 GetEventPlaces] mutableCopy];
//    self.spotsKeys = [self.hotSpots allKeys];
//    self.spotsKeys = [[self.spotsKeys copy] sortedArrayUsingComparator:^NSComparisonResult(NSString *a, NSString *b)
//                    {
//        NSDictionary *ad = [self.hotSpots objectForKey:a];
//        NSNumber *adsrn = [ad objectForKey:@"srno"];
//        NSDictionary *bd = [self.hotSpots objectForKey:b];
//        NSNumber *bdsrn = [bd objectForKey:@"srno"];
//        return ([adsrn intValue] > [bdsrn intValue]);
//    }];
//    [self.placesList reloadData];
//}

- (void)loadHotspots
{
    self.hotSpots = [[PlacesManager2 GetEventPlaces] mutableCopy];
    self.spotsKeys = [self.hotSpots allKeys];
    self.spotsKeys = [[self.spotsKeys copy] sortedArrayUsingComparator:^NSComparisonResult(NSString *a, NSString *b)
                    {
        NSString *bad = [self.hotSpots objectForKey:a];
        NSData *edata = [[NSData alloc] initWithBase64EncodedString:bad options:0];
        NSData *ddata = [SxUtilities decryptData:edata];
        NSDictionary *ad = [NSJSONSerialization JSONObjectWithData:ddata options:kNilOptions error:nil];
        NSNumber *adsrn = [ad objectForKey:@"srno"];
        
        NSString *badb = [self.hotSpots objectForKey:b];
        NSData *edatab = [[NSData alloc] initWithBase64EncodedString:badb options:0];
        NSData *ddatab = [SxUtilities decryptData:edatab];
        NSDictionary *bd = [NSJSONSerialization JSONObjectWithData:ddatab options:kNilOptions error:nil];
        NSNumber *bdsrn = [bd objectForKey:@"srno"];
        return ([adsrn intValue] > [bdsrn intValue]);
    }];
    
    [self.placesList reloadData];
}

//- (NSDictionary*)placeAtIndexPath:(NSIndexPath*)indexPath
//{
//    NSString *key = [self.spotsKeys objectAtIndex:indexPath.row];
//    return [self.hotSpots objectForKey:key];
//}

- (NSDictionary*)placeAtIndexPath:(NSIndexPath*)indexPath
{
    NSString *key = [self.spotsKeys objectAtIndex:indexPath.row];
    NSString *bad = [self.hotSpots objectForKey:key];
    if([bad length] > 0) {
        NSData *edata = [[NSData alloc] initWithBase64EncodedString:bad options:0];
        NSData *ddata = [SxUtilities decryptData:edata];
        NSDictionary *ad = [NSJSONSerialization JSONObjectWithData:ddata options:kNilOptions error:nil];
        return ad;
    }
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.spotsKeys count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EventPlacesCell *cell = [self.placesList dequeueReusableCellWithIdentifier:@"EventPlacesCell" forIndexPath:indexPath];
    NSDictionary *place = [self placeAtIndexPath:indexPath];
    NSString *key = [self.spotsKeys objectAtIndex:indexPath.row];
    [cell setupPlace:place key:key];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 85;
}
//if (Preferences.disableTeleDia == NO) {
//} else {
//    
//}
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    NSString *key = [self.spotsKeys objectAtIndex:indexPath.row];
//    [self ShowTeleportOptionsForKey:key];
//    [self.placesList deselectRowAtIndexPath:indexPath animated:YES];
//}
//
//- (void)ShowTeleportOptionsForKey:(NSString*)key
//{
//    NSDictionary *place = [self.hotSpots objectForKey:key];
//    CLLocationCoordinate2D destination = [SxUtilities CLLocationCoordinate2DMakeNormal:[place[@"latitude"] doubleValue] lon:[place[@"longitude"] doubleValue]];
//    NSString *destistring = [NSString stringWithFormat:@"%f,%f",destination.latitude, destination.longitude];
//    NSString *cdstring = [SxUtilities cooldownStringForDestination:destination];
//    NSString *msg = [NSString stringWithFormat:@"Location: %@\n%@",destistring,cdstring];
//    UIAlertController *alert = [UIAlertController
//                                alertControllerWithTitle:@"Teleport"
//                                message:msg
//                                preferredStyle:UIAlertControllerStyleAlert];
//    alert.view.tintColor = [SxUtilities ThemeColor];
//    UIAlertAction *teleport_action = [UIAlertAction actionWithTitle:@"Teleport to Location" style:UIAlertActionStyleDefault
//                                                            handler:^(UIAlertAction * action)
//                                      {
//        [self dismissViewControllerAnimated:YES completion:^{
//            [[SpooferxManager sharedManager] showControls];
//            [[SpooferxManager sharedManager] teleportTo:destination];
//        }];
//    }];
//
//    UIAlertAction* walk_action = [UIAlertAction actionWithTitle:@"Walk to Location" style:UIAlertActionStyleDefault
//                                                        handler:^(UIAlertAction * action)
//                                  {
//        [self dismissViewControllerAnimated:YES completion:^{
//            [[SpooferxManager sharedManager] showControls];
//            [[SpooferxManager sharedManager] walkToLocation:destination];
//        }];
//    }];
//
//    UIAlertAction *copy_action = [UIAlertAction actionWithTitle:@"Copy Coordinates" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//        UIPasteboard *thePasteboard = [UIPasteboard generalPasteboard];
//        [thePasteboard setString:destistring];
//        [[SpooferxManager sharedManager] presentTextNotification:@"Coordinates copied to clipboard" line2:@""];
//    }];
//
//    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive
//                                                   handler:^(UIAlertAction * action)
//                             {
//    }];
//
//    [alert addAction:teleport_action];
//    [alert addAction:walk_action];
//    [alert addAction:copy_action];
//    [alert addAction:cancel];
//
//    UIViewController *topController = [UIViewController spxoverlay_topMostController];
//    [topController presentViewController:alert
//                                animated:YES
//                              completion:nil];
//}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *place = [self placeAtIndexPath:indexPath];
    [self ShowTeleportOptionsForKey:place];
    [self.placesList deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)ShowTeleportOptionsForKey:(NSDictionary*)place
{
    CLLocationCoordinate2D destination = [SxUtilities CLLocationCoordinate2DMakeNormal:[place[@"latitude"] doubleValue] lon:[place[@"longitude"] doubleValue]];
    NSString *destistring = [NSString stringWithFormat:@"%f,%f",destination.latitude, destination.longitude];
    NSString *cdstring = [SxUtilities cooldownStringForDestination:destination];
    NSString *msg = [NSString stringWithFormat:@"Location: %@\n%@",destistring,cdstring];
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@"Teleport"
                                message:msg
                                preferredStyle:UIAlertControllerStyleAlert];
    alert.view.tintColor = [SxUtilities ThemeColor];
    UIAlertAction *teleport_action = [UIAlertAction actionWithTitle:@"Teleport to Location" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action)
                                      {
        [self dismissViewControllerAnimated:YES completion:^{
            [[SpooferxManager sharedManager] showControls];
            [[SpooferxManager sharedManager] teleportTo:destination];
        }];
    }];
    
    UIAlertAction* walk_action = [UIAlertAction actionWithTitle:@"Walk to Location" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
        [self dismissViewControllerAnimated:YES completion:^{
            [[SpooferxManager sharedManager] showControls];
            [[SpooferxManager sharedManager] walkToLocation:destination];
        }];
    }];
    
    UIAlertAction *copy_action = [UIAlertAction actionWithTitle:@"Copy Coordinates" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UIPasteboard *thePasteboard = [UIPasteboard generalPasteboard];
        [thePasteboard setString:destistring];
        NSString *msg = @"Coordinates copied to clipboard";
        [NotifMan presentFastNotification:msg];
        //[[SpooferxManager sharedManager] presentTextNotification:@"Coordinates copied to clipboard" line2:@""];
    }];
    
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive
                                                   handler:^(UIAlertAction * action)
                             {
    }];
    
    [alert addAction:teleport_action];
    double distance = [SxUtilities distanceBetweenLocations:destination];
    if(distance < 10000)
    {
        [alert addAction:walk_action];
    }
    [alert addAction:copy_action];
    [alert addAction:cancel];
    
    UIViewController *topController = [UIViewController spxoverlay_topMostController];
    [topController presentViewController:alert
                                animated:YES
                              completion:nil];
}

@end

//
//  LoadingView.h
//  toonsnow
//
//  Created by spooferx on 20/05/17.
//  Copyright © 2017 spooferx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingView : UIView
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityView;

@end

//
//  SettingsHeader.h
//  pokextest
//
//  Created by c0pyn1nja on 25/09/21
//  Copyright © 2021 iphonecake.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SettingsViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface SettingsHeader : UIView

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UIButton *activateButton;
@property (weak, nonatomic) IBOutlet UIButton *getKeyButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
@property (weak, nonatomic) IBOutlet UIView *backdropView;
@property (strong, nonatomic) SettingsViewController *parentV;

- (instancetype)initWithFrame:(CGRect)frame;

- (IBAction)activateSpooferx:(id)sender;
- (IBAction)getKey:(id)sender;

- (void)enableActivateButton:(NSString*)title;
- (void)disableActivateButton:(NSString*)title;
- (void)enableKeyButton:(NSString*)title;
- (void)disableKeyButton:(NSString*)title;

@end

NS_ASSUME_NONNULL_END

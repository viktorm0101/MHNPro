//
//  ImportsViewController.h
//  pokextest
//
//  Created by MADHATTER on 1/4/23.
//  Copyright © 2023 iphonecake.com. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ImportsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextView *importTextView;
@property (weak, nonatomic) IBOutlet UIButton *importButton;

- (IBAction)importAction:(id)sender;

@end

NS_ASSUME_NONNULL_END

//
//  UserPlacesController.m
//  igoprotest
//
//  Created by MADHATTER on 9/12/22.
//

#import "UserPlacesController.h"
#import "IGP_PlacesManager.h"
#import "UserPlacesCell.h"
#import "FCAlertView.h"
#import "SpooferxManager.h"
#import "IGP_NotificationsManager.h"

@interface UserPlacesController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UIBarButtonItem *doneBarButton;
@property (nonatomic, strong) NSMutableDictionary *hotSpots;
@property (nonatomic, strong) NSArray *spotsKeys;

@property (nonatomic, strong) FCAlertView *renameFCAlert;

- (IBAction)dismiss:(id)sender;

@end

@implementation UserPlacesController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.hotSpots = [[NSMutableDictionary alloc] init];
    self.editingCells = [[NSMutableArray alloc] init];
    [self setupUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [[SpooferxManager sharedManager] hideControls];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ReloadSpots" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadHotspots)
                                                 name:@"ReloadSpots"
                                               object:nil];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self loadHotspots];
    });
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [self.editingCells removeAllObjects];
}

- (void)dismiss:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        [[SpooferxManager sharedManager] showControls];
    }];
}

- (void)setupUI
{
    self.navigationController.navigationBar.tintColor = [SxUtilities ThemeColor];
    self.placesList.contentInset = UIEdgeInsetsMake(0.0f, 0.0f, CGRectGetHeight(self.tabBarController.tabBar.frame), 0.0f);
    self.placesList.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    //[self loadHotspots];
}

- (void)loadHotspots
{
    self.hotSpots = [[PlacesManager2 GetUserPlaces] mutableCopy];
    self.spotsKeys = [self.hotSpots allKeys];
    self.spotsKeys = [[self.spotsKeys copy] sortedArrayUsingComparator:^NSComparisonResult(NSString *a, NSString *b)
                      {
        NSDictionary *ad = [self.hotSpots objectForKey:a];
        NSDictionary *bd = [self.hotSpots objectForKey:b];
        if([ad objectForKey:@"savetime"] != nil &&  [bd objectForKey:@"savetime"] != nil)
        {
            NSNumber *adsrn = [ad objectForKey:@"savetime"];
            NSNumber *bdsrn = [bd objectForKey:@"savetime"];
            return ([adsrn doubleValue] > [bdsrn doubleValue]);
        }
        else
        {
            NSDictionary *ad = [self.hotSpots objectForKey:a];
            NSNumber *adsrn = [ad objectForKey:@"srno"];
            NSDictionary *bd = [self.hotSpots objectForKey:b];
            NSNumber *bdsrn = [bd objectForKey:@"srno"];
            return ([adsrn intValue] > [bdsrn intValue]);
        }
    }];
    
    [self.placesList reloadData];
}

- (NSDictionary*)placeAtIndexPath:(NSIndexPath*)indexPath
{
    NSString *key = [self.spotsKeys objectAtIndex:indexPath.row];
    return [self.hotSpots objectForKey:key];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.spotsKeys count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserPlacesCell *cell = [self.placesList dequeueReusableCellWithIdentifier:@"UserPlacesCell" forIndexPath:indexPath];
    NSDictionary *place = [self placeAtIndexPath:indexPath];
    NSString *key = [self.spotsKeys objectAtIndex:indexPath.row];
    [cell setupPlace:place key:key path:indexPath];
    cell.parentController = self;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 85;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *key = [self.spotsKeys objectAtIndex:indexPath.row];
    [self ShowTeleportOptionsForKey:key];
    [self.placesList deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(editingStyle == UITableViewCellEditingStyleDelete)
    {
        NSString *key = [self.spotsKeys objectAtIndex:indexPath.row];
        [PlacesManager2 RemoveUserPlaceWithKey:key];
    }
}

- (void)ShowRenameAlertWithKey:(NSString*)key
{
    self.renameFCAlert = [[FCAlertView alloc] init];
    self.renameFCAlert.colorScheme = [SxUtilities ThemeColor];
    self.renameFCAlert.darkTheme = [SxUtilities isDark];
    self.renameFCAlert.detachButtons = YES;
    NSDictionary *place = [self.hotSpots objectForKey:key];
    UITextField *emailField = [[UITextField alloc] init];
    emailField.autocorrectionType = UITextAutocorrectionTypeNo;
    emailField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    emailField.keyboardType = UIKeyboardTypeEmailAddress;
    emailField.text = place[@"nickname"];
    
    [self.renameFCAlert addTextFieldWithCustomTextField:emailField andPlaceholder:@"" andTextReturnBlock:^(NSString *text) {
        emailField.text = text;
    }];
    
    [self.renameFCAlert showAlertInView:[UIViewController spxoverlay_topMostController]
                              withTitle:@"Rename Location"
                           withSubtitle:@"Please enter nickname for this location"
                        withCustomImage:[AssetsManager imageForResource:@"feather-pen-mini"]
                    withDoneButtonTitle:@"Rename"
                             andButtons:@[@"Cancel"]];
    
    [self.renameFCAlert doneActionBlock:^{
        if (emailField.text.length > 0) {
            [PlacesManager2 RenameUserPlaceWithKey:key toNickname:emailField.text];
        } else {
            emailField.text = place[@"nickname"];
        }
    }];
}

- (void)ShowTeleportOptionsForKey:(NSString*)key
{
    NSDictionary *place = [self.hotSpots objectForKey:key];
    CLLocationCoordinate2D destination = [SxUtilities CLLocationCoordinate2DMakeNormal:[place[@"latitude"] doubleValue] lon:[place[@"longitude"] doubleValue]];
    NSString *destistring = [NSString stringWithFormat:@"%f,%f",destination.latitude, destination.longitude];
    NSString *cdstring = [SxUtilities cooldownStringForDestination:destination];
    NSString *msg = [NSString stringWithFormat:@"Location: %@\n%@",destistring,cdstring];
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@"Teleport"
                                message:msg
                                preferredStyle:UIAlertControllerStyleAlert];
    alert.view.tintColor = [SxUtilities ThemeColor];
    UIAlertAction *teleport_action = [UIAlertAction actionWithTitle:@"Teleport to Location" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action)
                                      {
        [self dismissViewControllerAnimated:YES completion:^{
            [[SpooferxManager sharedManager] showControls];
            [[SpooferxManager sharedManager] teleportTo:destination];
        }];
    }];
    
    UIAlertAction *rename_action = [UIAlertAction actionWithTitle:@"Rename Location" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self ShowRenameAlertWithKey:key];
    }];
    
    UIAlertAction* walk_action = [UIAlertAction actionWithTitle:@"Walk to Location" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
        [self dismissViewControllerAnimated:YES completion:^{
            [[SpooferxManager sharedManager] showControls];
            [[SpooferxManager sharedManager] walkToLocation:destination];
        }];
    }];
    
    UIAlertAction *copy_action = [UIAlertAction actionWithTitle:@"Copy Coordinates" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UIPasteboard *thePasteboard = [UIPasteboard generalPasteboard];
        [thePasteboard setString:destistring];
        NSString *msg = @"Coordinates copied to clipboard";
        [NotifMan presentFastNotification:msg];
    }];
    
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive
                                                   handler:^(UIAlertAction * action)
                             {
    }];
    
    [alert addAction:teleport_action];
    [alert addAction:rename_action];
    [alert addAction:walk_action];
    [alert addAction:copy_action];
    [alert addAction:cancel];
    
    UIViewController *topController = [UIViewController spxoverlay_topMostController];
    [topController presentViewController:alert
                                animated:YES
                              completion:nil];
}

- (void)tableView:(UITableView *)tableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.editingCells addObject:indexPath];
}

- (void)tableView:(UITableView *)tableView didEndEditingRowAtIndexPath:(NSIndexPath *)indexPath {
     [self.editingCells removeObject:indexPath];
}

@end

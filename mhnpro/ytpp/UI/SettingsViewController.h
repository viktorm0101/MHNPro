//
//  SettingsViewController.h
//  spooferxsl
//
//  Created by spooferx on 10/27/20
//  
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SettingsViewController : UITableViewController

- (void)prepareForKeyNew;
- (void)showResetKeyNew;
- (void)showOptionsForWeb;
- (void)showOptionsForWeb2;

@end

NS_ASSUME_NONNULL_END

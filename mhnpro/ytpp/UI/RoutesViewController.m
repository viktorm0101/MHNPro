//
//  RoutesViewController.m
//  SDWebImage
//
//  Created by spooferx on 28/12/20
//  Copyright © 2020 Dailymotion. All rights reserved.
//

#import "RoutesViewController.h"
#import "SpooferxManager.h"
#import "LoadingView.h"
#import "RpcManager.h"
#import "RoutesManager.h"
#import <AFNetworking.h>

@interface RoutesViewController () <UISearchControllerDelegate, UISearchBarDelegate, UISearchResultsUpdating, UITextViewDelegate,UITextPasteDelegate >

@property (nonatomic, strong) IBOutlet UITableView *routesList;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *doneBarButton;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *filterBarButton;
@property (nonatomic, strong) NSMutableArray *spx_routesArray;
@property (nonatomic, strong) NSMutableArray *spx_userroutesArray;
@property (nonatomic, strong) UITextView *gpxView;
@property (strong, nonatomic) UISearchController *searchController;
@property (strong, nonatomic) NSMutableArray *preSearchResults;
@property (strong, nonatomic) NSMutableArray *userSearchResults;
@property (nonatomic) UIAlertController *routeSaveAlert;
@property (nonatomic, strong) NSMutableArray *editingCells;

- (IBAction)dismiss:(id)sender;
- (IBAction)showFilter:(id)sender;

@end

@implementation RoutesViewController
{
    LoadingView *loading;
}

- (IBAction)dismiss:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

- (void)dismissWithRoute:(NSString*)route
{
    if (self.searchController.active == YES) {
        [self.searchController.searchBar resignFirstResponder];
        self.searchController.active = NO;
    }
    
    [self dismissViewControllerAnimated:YES completion:^{
        [[SpooferxManager sharedManager] startAutoWalkTo:route drawn:NO];
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RoutesDetails" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(getRoutes)
                                                 name:@"RoutesDetails"
                                               object:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    [self getRoutes];
}

- (void)setupUI
{
    self.navigationController.navigationBar.tintColor = [SxUtilities ThemeColor];
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.obscuresBackgroundDuringPresentation = NO;
    self.searchController.delegate = self;
    self.searchController.searchBar.delegate = self;
    [self.searchController.searchBar sizeToFit];
    self.searchController.searchBar.backgroundColor = [UIColor systemGroupedBackgroundColor];
    self.searchController.hidesNavigationBarDuringPresentation = NO;
    self.searchController.searchBar.searchBarStyle = UISearchBarStyleMinimal;
    self.searchController.searchBar.tintColor = [SxUtilities ThemeColor];
    self.navigationItem.searchController = self.searchController;
    self.navigationItem.hidesSearchBarWhenScrolling = false;
    
    UIImage *close = [AssetsManager imageForResource:@"multiply-symbol-mini"];
    [self.doneBarButton setImage:close];
    
    //UIImage *filter = [ResourcesManager imageForResource:@"filter"];
    //[self.filterBarButton setImage:filter];
    self.filterBarButton.width = 0.01;
    self.filterBarButton.enabled = NO;
    self.filterBarButton.tintColor = [UIColor clearColor];
    
    self.spx_routesArray = [[NSMutableArray alloc] init];
    self.spx_userroutesArray = [[NSMutableArray alloc] init];
    self.preSearchResults = [[NSMutableArray alloc] init];
    self.userSearchResults = [[NSMutableArray alloc] init];
}

- (IBAction)showFilter:(id)sender
{
    
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    NSString *searchString = searchController.searchBar.text;
    if ([searchString length] == 0) {
        //_ListArray=_TempArray;
        DBG(@"No Length");
    }
    else{
        // strip out all the leading and trailing spaces
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            //Background Thread
            NSString *strippedString = [[searchString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] lowercaseString];
            self.preSearchResults = [[self.spx_routesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(country CONTAINS[c] %@ or city CONTAINS[c] %@ or area CONTAINS[c] %@)",strippedString,strippedString,strippedString]] mutableCopy];
            self.userSearchResults = [[self.spx_userroutesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(rname CONTAINS[c] %@)",strippedString]] mutableCopy];
            dispatch_async(dispatch_get_main_queue(), ^(void){
                //Run UI Updates
                [self.routesList reloadData];
            });
        });
    }
}

- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope
{
    [self updateSearchResultsForSearchController:self.searchController];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self getRoutes];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.searchController setActive:NO];
}

- (void)getRoutes
{
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //Background Thread
        [self.spx_routesArray removeAllObjects];
        [self.spx_userroutesArray removeAllObjects];
        [self.preSearchResults removeAllObjects];
        [self.userSearchResults removeAllObjects];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            //Run UI Updates
            [self.routesList reloadData];
        });
    });

    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //Background Thread
        NSDictionary *routesd = [RoutesMan getRoutes];
        self.spx_userroutesArray = [routesd.allValues mutableCopy];
        NSArray *routes = [RoutesMan getRemoteRoutes];
        self.spx_routesArray = [routes mutableCopy];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            //Run UI Updates
            [self.routesList reloadData];
        });
    });
}

- (NSDictionary*)placeAtIndexPath:(NSIndexPath*)indexPath
{
    if(indexPath.section == 1) {
        if(self.searchController.isActive)
        {
            return [self.userSearchResults objectAtIndex:indexPath.row];
        }
        
        return [self.spx_userroutesArray objectAtIndex:indexPath.row];
    }
    else if (indexPath.section == 2) {
        if(self.searchController.isActive)
        {
            return [self.preSearchResults objectAtIndex:indexPath.row];
        }
        return [self.spx_routesArray objectAtIndex:indexPath.row];
    }
    
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([SxUtilities isLegendaryPokemon] == YES) {
        return 3;
    }
    else
    {
        return 2;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
        {
            return 2;
        }
            break;
        case 1:
        {
            if(self.searchController.isActive == YES)
            {
                return self.userSearchResults.count;
            }
            
            return self.spx_userroutesArray.count;
        }
            break;
        case 2:
        {
            if(self.searchController.isActive == YES)
            {
                return self.preSearchResults.count;
            }
            
            return self.spx_routesArray.count;
        }
            break;
        default:
            return 0;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BasicCell"];
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"BasicCell"];
            }
            
            [cell.textLabel setFont:[UIFont fontWithName:@"AvenirNext-DemiBold" size:16.0]];
            [cell.textLabel setTextColor:[UIColor labelColor]];
            
            switch (indexPath.row) {
                case 0:
                    cell.textLabel.text = @"Import Route";
                    return cell;
                    break;
                case 1:
                    cell.textLabel.text = @"Draw Route";
                    return cell;
                    break;
                default:
                    return nil;
                    break;
            }
        }
            break;
        case 1:
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RouteCell" forIndexPath:indexPath];
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"RouteCell"];
            }
            
            NSDictionary *route = [self placeAtIndexPath:indexPath];
            NSString *routestr = route[@"route"];
            double rd = [SxUtilities routeDistance:routestr];
            NSString *rdString = [SxUtilities distanceString:rd];
            double rdTime = rd/[SxUtilities currentSpeed];
            int rdMins = rdTime/60;
            NSString *routeName = route[@"rname"];
            NSNumber *rcnt = [route objectForKey:@"stopcount"];
            int stopCount = [rcnt intValue];
            NSString *routeSubTitle = [NSString stringWithFormat:@"%d Stops, D: %@, T: %d mins",stopCount,rdString,rdMins];
            [cell.textLabel setText:routeName];
            [cell.textLabel setFont:[UIFont fontWithName:@"AvenirNext-DemiBold" size:16.0]];
            [cell.textLabel setTextColor:[UIColor labelColor]];
            [cell.detailTextLabel setText:routeSubTitle];
            [cell.detailTextLabel setFont:[UIFont fontWithName:@"AvenirNext-Medium" size:14.0]];
            [cell.detailTextLabel setTextColor:[SxUtilities ThemeColor]];
            return cell;
        }
            break;
        case 2:
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RouteCell" forIndexPath:indexPath];
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"RouteCell"];
            }
            
            NSDictionary *gp = [self placeAtIndexPath:indexPath];
            NSString *cc  = gp[@"cc"];
            NSString *emoji = [NSLocale  emojiFlagForISOCountryCode:cc];
            NSString *city = gp[@"city"];
            city = [city stringByAppendingFormat:@", %@",gp[@"country"]];
            NSString *area = gp[@"area"];
            NSString *title = [NSString stringWithFormat:@"%@ - %@",emoji, area];
            cell.textLabel.text = title;
            [cell.textLabel setFont:[UIFont fontWithName:@"AvenirNext-DemiBold" size:16.0]];
            [cell.textLabel setTextColor:[UIColor labelColor]];
            cell.detailTextLabel.text = city;
            [cell.detailTextLabel setFont:[UIFont fontWithName:@"AvenirNext-Medium" size:14.0]];
            [cell.detailTextLabel setTextColor:[SxUtilities ThemeColor]];
            return cell;
        }
            break;
        default:
            return nil;
            break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
        {
            switch (indexPath.row) {
                case 0:
                {
                    [self ImportRoute];
                }
                    break;
                case 1:
                {
                }
                    break;
                default:
                    break;
            }
        }
            break;
        case 1:
        {
            [self GetUserMadeRoute:indexPath];
        }
            break;
        case 2:
        {
            [self GetPremadeFile:indexPath];
        }
            break;
        default:
            break;
    }
    
    [self.routesList deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case 0: return @"Options";
        case 1:
        {
            if(self.searchController.isActive)
            {
                return [NSString stringWithFormat:@"User Routes - %ld",self.userSearchResults.count];
            }
            
            return [NSString stringWithFormat:@"User Routes - %ld",self.spx_userroutesArray.count];
        }
            break;
        case 2:
        {
            if(self.searchController.isActive)
            {
                return [NSString stringWithFormat:@"Premade Routes - %ld",self.preSearchResults.count];
            }
            return [NSString stringWithFormat:@"Premade Routes - %ld",self.spx_routesArray.count];
        }
            break;
        default:
            return @"";
            break;
    }
//    if ([SxUtilities isLegendaryPokemon] == YES) {
//        switch (section) {
//            case 0: return @"Options";
//            case 1:
//            {
//                if(self.searchController.isActive)
//                {
//                    return [NSString stringWithFormat:@"User Routes - %ld",self.userSearchResults.count];
//                }
//
//                return [NSString stringWithFormat:@"User Routes - %ld",self.spx_userroutesArray.count];
//            }
//                break;
//            case 2:
//            {
//                if(self.searchController.isActive)
//                {
//                    return [NSString stringWithFormat:@"Premade Routes - %ld",self.preSearchResults.count];
//                }
//                return [NSString stringWithFormat:@"Premade Routes - %ld",self.spx_routesArray.count];
//            }
//                break;
//            default:
//                return @"";
//                break;
//        }
//    }
//    else
//    {
//        switch (section) {
//            case 0:
//            {
//                if(self.searchController.isActive)
//                {
//                    return [NSString stringWithFormat:@"User Routes - %ld",self.userSearchResults.count];
//                }
//
//                return [NSString stringWithFormat:@"User Routes - %ld",self.spx_userroutesArray.count];
//            }
//                break;
//            case 1:
//            {
//                if(self.searchController.isActive)
//                {
//                    return [NSString stringWithFormat:@"Premade Routes - %ld",self.preSearchResults.count];
//                }
//                return [NSString stringWithFormat:@"Premade Routes - %ld",self.spx_routesArray.count];
//            }
//                break;
//            default:
//                return @"";
//                break;
//        }
//    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 1)
    {
        if(editingStyle == UITableViewCellEditingStyleDelete)
        {
            NSDictionary *routed = [self.spx_userroutesArray objectAtIndex:indexPath.row];
            NSString *routeKey = routed[@"routekey"];
            BOOL deleted = [RoutesMan deleteRouteForKey:routeKey];
            if (deleted == YES) {
                [self getRoutes];
            }
        }
    }
}

- (void)tableView:(UITableView *)tableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.editingCells addObject:indexPath];
}

- (void)tableView:(UITableView *)tableView didEndEditingRowAtIndexPath:(NSIndexPath *)indexPath {
     [self.editingCells removeObject:indexPath];
}

- (void)GetUserMadeRoute:(NSIndexPath*)indexPath
{
    NSDictionary *routed = [self.spx_userroutesArray objectAtIndex:indexPath.row];
    NSString *route = routed[@"route"];
    BOOL validgpx = [SxUtilities validGpx:route];
    if (validgpx == YES) {
        int count = [SxUtilities stopCount:route];
        double rdistance = [SxUtilities routeDistance:route];
        NSString *rdststr = [SxUtilities distanceString:rdistance];
        double rdTime = rdistance/[SxUtilities currentSpeed];
        int rdMins = rdTime/60;
        NSString *msg = [NSString stringWithFormat:@"User Route with %d stops, walk distance %@ is selected. Walking time will be ~%d mins, would you like to start Auto Walk?",count,rdststr,rdMins];
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"User Route" message:msg preferredStyle:UIAlertControllerStyleAlert];
        alert.view.tintColor = [SxUtilities ThemeColor];
        UIAlertAction *walk_action = [UIAlertAction actionWithTitle:@"Walk Route" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self dismissWithRoute:route];
        }];
        
        UIAlertAction *cancel_action = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        
        [alert addAction:walk_action];
        [alert addAction:cancel_action];
        
        [[UIViewController spxoverlay_topMostController] presentViewController:alert animated:YES completion:^{
            
        }];
    }
    else
    {
        NSString *msg = [NSString stringWithFormat:@"Invalid Gpx Route, Please contact staff with String: %@",route];
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Route Error" message:msg preferredStyle:UIAlertControllerStyleAlert];
        alert.view.tintColor = [SxUtilities ThemeColor];
        UIAlertAction *cancel_action = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alert addAction:cancel_action];
        
        [[UIViewController spxoverlay_topMostController] presentViewController:alert animated:YES completion:^{
            
        }];
    }
}

- (void)GetPremadeFile:(NSIndexPath*)indexPath
{
    NSDictionary *dic = [self placeAtIndexPath:indexPath];
    NSString *file = dic[@"name"];
    NSString *area = dic[@"area"];
    NSString *city = dic[@"city"];
    NSString *title = [NSString stringWithFormat:@"%@,%@",area,city];
    NSString *host = @"https://routes.igopro.dev/gpx/routes/";
    NSString *link = [host stringByAppendingString:file];
    link = [link stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSError *routeError;
    NSString *route = [NSString stringWithContentsOfURL:[NSURL URLWithString:link] encoding:NSUTF8StringEncoding error:&routeError];
    if (routeError == nil)
    {
        BOOL validgpx = [SxUtilities validGpx:route];
        if (validgpx == YES) {
            int count = [SxUtilities stopCount:route];
            double rdistance = [SxUtilities routeDistance:route];
            NSString *rdststr = [SxUtilities distanceString:rdistance];
            double rdTime = rdistance/[SxUtilities currentSpeed];
            int rdMins = rdTime/60;
            NSString *msg = [NSString stringWithFormat:@"%@ route with %d stops, walk distance %@ is selected. Walking time will be ~%d mins, would you like to start Auto Walk?",title,count,rdststr,rdMins];
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
            alert.view.tintColor = [SxUtilities ThemeColor];
            UIAlertAction *walk_action = [UIAlertAction actionWithTitle:@"Walk Route" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self dismissWithRoute:route];
            }];
            
            UIAlertAction *cancel_action = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            }];
            
            [alert addAction:walk_action];
            [alert addAction:cancel_action];
            
            [[UIViewController spxoverlay_topMostController] presentViewController:alert animated:YES completion:^{
                
            }];
        }
        else
        {
            NSString *msg = [NSString stringWithFormat:@"Invalid Gpx Route, Please contact staff with name: %@",file];
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Route Error" message:msg preferredStyle:UIAlertControllerStyleAlert];
            alert.view.tintColor = [SxUtilities ThemeColor];
            UIAlertAction *cancel_action = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            }];
            [alert addAction:cancel_action];
            
            [[UIViewController spxoverlay_topMostController] presentViewController:alert animated:YES completion:^{
                
            }];
        }
    }
    else
    {
        NSString *msg = [NSString stringWithFormat:@"%@",routeError.debugDescription];
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:msg preferredStyle:UIAlertControllerStyleAlert];
        alert.view.tintColor = [SxUtilities ThemeColor];
        UIAlertAction *cancel_action = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        
        [alert addAction:cancel_action];
        
        [[UIViewController spxoverlay_topMostController] presentViewController:alert animated:YES completion:^{
            
        }];
    }
}

- (void)ImportRoute
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Route File"
                                                                             message:@"\n\n\n\n\n\n\n\n"
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okay = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                 handler:^(UIAlertAction * action) {
        //Do Some action here
        UITextView *txt = (UITextView*)[alertController.view viewWithTag:4];
        NSString *route = [NSString stringWithFormat:@"%@",txt.text];
        route = [route stringByRemovingPercentEncoding];
        BOOL validgpx = [SxUtilities validGpx:route];
        if (validgpx == YES)
        {
            int count = [SxUtilities stopCount:route];
            double rdistance = [SxUtilities routeDistance:route];
            NSString *rdststr = [SxUtilities distanceString:rdistance];
            double rdTime = rdistance/[SxUtilities currentSpeed];
            int rdMins = rdTime/60;
            NSString *msg = [NSString stringWithFormat:@"Imported Route with %d stops, walk distance %@ is selected. Walking time will be ~%d mins, would you like to start Auto Walk?",count,rdststr,rdMins];
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Imported Route" message:msg preferredStyle:UIAlertControllerStyleAlert];
            alert.view.tintColor = [SxUtilities ThemeColor];
            UIAlertAction *walk_action = [UIAlertAction actionWithTitle:@"Walk Route" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull actiona) {
                [self dismissWithRoute:route];
            }];
            
            UIAlertAction *save_action = [UIAlertAction actionWithTitle:@"Save Route" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull actiona) {
                [self saveImportedRoute:route];
            }];
            
            UIAlertAction *cancel_action = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull actiona) {
            }];
            
            [alert addAction:walk_action];
            [alert addAction:save_action];
            [alert addAction:cancel_action];
            
            [[UIViewController spxoverlay_topMostController] presentViewController:alert animated:YES completion:^{
            }];
        }
        else
        {
            NSString *msg = [NSString stringWithFormat:@"Invalid Gpx Route -> %@",route];
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Route Error" message:msg preferredStyle:UIAlertControllerStyleAlert];
            alert.view.tintColor = [SxUtilities ThemeColor];
            UIAlertAction *cancel_action = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action1) {
                
            }];

            [alert addAction:cancel_action];
            
            [[UIViewController spxoverlay_topMostController] presentViewController:alert animated:YES completion:^{
            }];
        }
    }];
    
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    [cancel setValue:[UIColor redColor] forKey:@"titleTextColor"];
    [alertController addAction:okay];
    [alertController addAction:cancel];
    
    alertController.view.autoresizesSubviews = YES;
    self.gpxView = [[UITextView alloc] initWithFrame:CGRectZero];
    self.gpxView.translatesAutoresizingMaskIntoConstraints = NO;
    self.gpxView.editable = YES;
    self.gpxView.dataDetectorTypes = UIDataDetectorTypeAll;
    self.gpxView.text = @"paste contents here from .gpx file";
    self.gpxView.textColor = [UIColor secondaryLabelColor];
    self.gpxView.delegate = self;
    self.gpxView.userInteractionEnabled = YES;
    self.gpxView.backgroundColor = [UIColor secondarySystemGroupedBackgroundColor];
    self.gpxView.scrollEnabled = YES;
    self.gpxView.tag = 4;
    self.gpxView.returnKeyType = UIReturnKeyDone;
    self.gpxView.keyboardType = UIKeyboardTypeDefault;
    
    NSLayoutConstraint *leadConstraint = [NSLayoutConstraint constraintWithItem:alertController.view attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.gpxView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:-8.0];
    NSLayoutConstraint *trailConstraint = [NSLayoutConstraint constraintWithItem:alertController.view attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.gpxView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:8.0];
    
    NSLayoutConstraint *topConstraint = [NSLayoutConstraint constraintWithItem:alertController.view attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.gpxView attribute:NSLayoutAttributeTop multiplier:1.0 constant:-64.0];
    NSLayoutConstraint *bottomConstraint = [NSLayoutConstraint constraintWithItem:alertController.view attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.gpxView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:64.0];
    [alertController.view addSubview:self.gpxView];
    [NSLayoutConstraint activateConstraints:@[leadConstraint, trailConstraint, topConstraint, bottomConstraint]];
    
    [[UIViewController spxoverlay_topMostController] presentViewController:alertController animated:YES completion:^{
        
    }];
}

-(void)textFieldDidChangeCoord:(UITextField*)field {
    NSString *msg = field.text;
    NSMutableCharacterSet *_alnum = [NSMutableCharacterSet characterSetWithCharactersInString:@"_"];
    [_alnum formUnionWithCharacterSet:[NSCharacterSet alphanumericCharacterSet]];
    NSCharacterSet *setToKeep = [_alnum invertedSet];
    NSString *newString =
    [[msg componentsSeparatedByCharactersInSet:setToKeep]
     componentsJoinedByString:@""];
    msg = [NSString stringWithFormat:@"%@",newString];
    self.routeSaveAlert.message = msg;
}

- (void)saveImportedRoute:(NSString*)route
{
    UIAlertController *save_alert = [UIAlertController alertControllerWithTitle:@"Title For Route" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    save_alert.view.tintColor = [SxUtilities ThemeColor];
    
    UIAlertAction *save_action = [UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *name = save_alert.textFields[0].text;
        [self saveRoute:route name:name];
    }];
    
    UIAlertAction *cancel_action = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [save_alert addAction:save_action];
    [save_alert addAction:cancel_action];
    
    [save_alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Name For Route";
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 11.0)
        {
            [textField setPasteDelegate:self];
        }
        
        [textField addTarget:self action:@selector(textFieldDidChangeCoord:) forControlEvents:UIControlEventEditingChanged];
    }];
    
    [[UIViewController spxoverlay_topMostController] presentViewController:save_alert animated:YES completion:^{ 
    }];
}

- (void)saveRoute:(NSString*)route name:(NSString*)name
{
    BOOL saved = [RoutesMan saveRoute:route name:name];
    if (saved == YES) {
        [self getRoutes];
        BOOL validgpx = [SxUtilities validGpx:route];
        if (validgpx == YES) {
            int count = [SxUtilities stopCount:route];
            NSString *msg = [NSString stringWithFormat:@"User Route with %d stops detected. Would you like to start Auto Walk?",count];
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"User Route" message:msg preferredStyle:UIAlertControllerStyleAlert];
            alert.view.tintColor = [SxUtilities ThemeColor];
            
            UIAlertAction *walk_action = [UIAlertAction actionWithTitle:@"Walk Route" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self dismissWithRoute:route];
            }];
            
            UIAlertAction *cancel_action = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            }];
            
            [alert addAction:walk_action];
            [alert addAction:cancel_action];
            
            [[UIViewController spxoverlay_topMostController] presentViewController:alert animated:YES completion:^{
                
            }];
        }
        else
        {
            NSString *msg = @"Invalid Gpx Route, Please contact staff.";
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Route Error" message:msg preferredStyle:UIAlertControllerStyleAlert];
            alert.view.tintColor = [SxUtilities ThemeColor];
            
            UIAlertAction *cancel_action = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            }];

            [alert addAction:cancel_action];
            
            [[UIViewController spxoverlay_topMostController] presentViewController:alert animated:YES completion:^{
                
            }];
        }
    }
    else
    {
        NSString *msg = @"Route Can not be saved";
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Save Error" message:msg preferredStyle:UIAlertControllerStyleAlert];
        alert.view.tintColor = [SxUtilities ThemeColor];
        UIAlertAction *cancel_action = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        
        [alert addAction:cancel_action];
        
        [[UIViewController spxoverlay_topMostController] presentViewController:alert animated:YES completion:^{
            
        }];
    }
}

-(void)textPasteConfigurationSupporting:(id<UITextPasteConfigurationSupporting>)textPasteConfigurationSupporting transformPasteItem:(id<UITextPasteItem>)item API_AVAILABLE(ios(11.0)) {
    [self paste:textPasteConfigurationSupporting];
}

//generic paste action handler
- (void)paste:(id)sender {
    UIPasteboard *gpBoard = [UIPasteboard generalPasteboard];
    if (@available(iOS 10.0, *)) {
        if ([gpBoard hasStrings]) {
            id<UITextPasteConfigurationSupporting> pasteItem = (id<UITextPasteConfigurationSupporting>)sender;
            if ([pasteItem isKindOfClass:[UITextView class]]) {
                UITextView* myTextView = (UITextView*)pasteItem;
                [myTextView insertText:[gpBoard string]];
            }
            else if ([pasteItem isKindOfClass:[UITextField class]])
            {
                UITextField *txtfield = (UITextField*)sender;
                [txtfield insertText:[gpBoard string]];
            }
        }
    } else {
        // Fallback on earlier versions
    }
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@"paste contents here from .gpx file"]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor]; //optional
    }
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@""]) {
        textView.text = @"paste contents here from .gpx file";
        textView.textColor = [UIColor secondaryLabelColor]; //optional
    }
    [textView resignFirstResponder];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    NSRange resultRange = [text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet] options:NSBackwardsSearch];
    if ([text length] == 1 && resultRange.location != NSNotFound) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

@end

//
//  JoystickController.h
//  spooferxsl
//
//  Created by spooferx on 11/15/19.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, JoystickControlDirection)
{
    JoystickControlDirectionUp     = 1 << 0,
    JoystickControlDirectionDown   = 1 << 1,
    JoystickControlDirectionLeft   = 1 << 2,
    JoystickControlDirectionRight  = 1 << 3,
};

@protocol JoystickControllerDelegate <NSObject>

@optional
- (void)joyStickDidMoveOffsetX:(CGFloat)x offsetY:(CGFloat)y;

@end

@interface JoystickController : UIControl

@property (nonatomic,weak) id<JoystickControllerDelegate> moveDelegate;
@property (nonatomic,assign) BOOL inMove;

@property (readonly, nonatomic) JoystickControlDirection direction;
@property (readonly, nonatomic) CGPoint directionVector;
@property (assign, nonatomic) BOOL isDragging;

- (id)initWithFrame:(CGRect)frame;

@end



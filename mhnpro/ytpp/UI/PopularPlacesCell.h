//
//  PopularPlacesCell.h
//  igoprotest
//
//  Created by MADHATTER on 9/12/22.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PopularPlacesCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *countryFlag;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *clockLabel;
@property (nonatomic, weak) IBOutlet UILabel *extraLabel;

- (void)setupPlace:(NSDictionary*)place key:(NSString*)key;
- (void)SetCurrentDateTime;

@end

NS_ASSUME_NONNULL_END

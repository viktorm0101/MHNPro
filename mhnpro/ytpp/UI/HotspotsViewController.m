//
//  HotspotsViewController.m
//  spooferxsl
//
//  Created by spooferx on 10/27/20
//  
//

#import "HotspotsViewController.h"
#import "SpooferxManager.h"
#import "LoadingView.h"

#import "HotspotsManager.h"

@interface HotspotsViewController () <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UITextPasteDelegate>

@property (nonatomic, weak) IBOutlet UIBarButtonItem *doneBarButton;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *flyBarButton;

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic) UIAlertController *teleportAlert;
@property (nonatomic, strong) NSMutableArray *user_placesArray;
@property (nonatomic, strong) NSMutableArray *spx_placesArray;
@property (nonatomic, strong) NSMutableArray *spx_nestsArray;

- (IBAction)dismiss:(id)sender;
- (IBAction)flyAction:(id)sender;

@end

@implementation HotspotsViewController 
{
    LoadingView *loading;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ReloadSpots" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadPlaces)
                                                 name:@"ReloadSpots"
                                               object:nil];
}
- (void)ShowDeprecationAlert
{
    NSString *msg = @"This Hotspots module will be removed soon. \nPlease manually transfer or save all your Hotspots to new module.\nOnce module is removed you will loose access to your saved hotspots.";
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Warning" message:msg preferredStyle:UIAlertControllerStyleAlert];
    
    alert.view.tintColor = [SxUtilities ThemeColor];
    UIAlertAction *dismiss_action = [UIAlertAction actionWithTitle:@"Understood" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alert addAction:dismiss_action];
    dispatch_async(dispatch_get_main_queue(), ^(void){
        [[UIViewController spxoverlay_topMostController] presentViewController:alert animated:YES completion:nil];
    });
    
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    [self loadPlaces];
}

- (void)loadTable
{
    [self hideLoading];
    [self.tableView reloadData];
}

- (IBAction)dismiss:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        [[SpooferxManager sharedManager] showControls];
    }];
}

- (void)dismissWithTeleport:(CLLocationCoordinate2D)location
{
    [self dismissViewControllerAnimated:YES completion:^{
        [[SpooferxManager sharedManager] showControls];
        [[SpooferxManager sharedManager] teleportTo:location];
    }];
}

- (void)dismissWithWalk:(CLLocationCoordinate2D)location
{
    [self dismissViewControllerAnimated:YES completion:^{
        [[SpooferxManager sharedManager] showControls];
        [[SpooferxManager sharedManager] walkToLocation:location];
    }];
}

- (IBAction)flyAction:(id)sender
{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@"Go To Location?"
                                message:@"Example: 34.019455,-118.49119"
                                preferredStyle:UIAlertControllerStyleAlert];
    alert.view.tintColor = [SxUtilities ThemeColor];
    UIAlertAction* teleport_action = [UIAlertAction actionWithTitle:@"Teleport to Location" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action)
                                      {
        NSString *coords = alert.textFields[0].text;
        BOOL valid = [SxUtilities coordinatesValid:coords];
        if (valid == YES) {
            CLLocationCoordinate2D cords = [SxUtilities coordsFromString:coords];
            [self dismissWithTeleport:cords];
        }
    }];
    
    UIAlertAction* walk_action = [UIAlertAction actionWithTitle:@"Walk to Location" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
        NSString *coords = alert.textFields[0].text;
        BOOL valid = [SxUtilities coordinatesValid:coords];
        if (valid == YES) {
            CLLocationCoordinate2D cords = [SxUtilities coordsFromString:coords];
            [self dismissWithWalk:cords];
        }
    }];
    
    UIAlertAction *save_action = [UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                  {
        NSString *coords = alert.textFields[0].text;;
        BOOL valid = [SxUtilities coordinatesValid:coords];
        if (valid == YES) {
            CLLocationCoordinate2D cords = [SxUtilities coordsFromString:coords];
            [[HotspotsManager sharedManager] add_userPlace:cords];
        }
    }];
    
    
    UIAlertAction* cancel_action = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                          handler:^(UIAlertAction * action)
                                    {
    }];
    
    [alert addAction:teleport_action];
    [alert addAction:walk_action];
    [alert addAction:save_action];
    [alert addAction:cancel_action];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Latitude,Longitude";
        textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 11.0)
        {
            [textField setPasteDelegate:self];
        }
        [textField addTarget:self action:@selector(textFieldDidChangeCoord:) forControlEvents:UIControlEventEditingChanged];
        
    }];
    
    self.teleportAlert = alert;
    dispatch_async(dispatch_get_main_queue(), ^(void){
        [[UIViewController spxoverlay_topMostController] presentViewController:alert animated:YES completion:nil];
    });
}

- (void)selectionAlert:(NSIndexPath*)indexPath
{
    NSString *cords;
    NSDictionary *place = [self placeAtIndexPath:indexPath];
    if (indexPath.section == 0) {
        cords = [NSString stringWithFormat:@"%f,%f",[place[@"latitude"] doubleValue],[place[@"longitude"] doubleValue]];
    }
    else if (indexPath.section == 1)
    {
        cords = [NSString stringWithFormat:@"%f,%f",[place[@"Lat"] doubleValue],[place[@"Lon"] doubleValue]];
    }
    
    NSString *msg;
    double distance;
    BOOL valid = [SxUtilities coordinatesValid:cords];
    if (valid == YES) {
        CLLocationCoordinate2D coords = [SxUtilities coordsFromString:cords];
        distance = [SxUtilities distanceBetweenLocations:coords];
        //distance = distance/1000;
        NSString *dststr = [SxUtilities distanceString:distance];
        double cdd = [SxUtilities cdDistanceBetweenLocations:coords];
        cdd = cdd/1000;
        int min = [SxUtilities getRemainingCD:cdd];
        msg = [NSString stringWithFormat:@"Teleport to: %@?\nD - %@, CD - %d mins",cords,dststr, min];
    }
    else
    {
        msg = [NSString stringWithFormat:@"Invalid Coordinates %@",cords];
    }
    
    UIAlertController *alert  = [UIAlertController alertControllerWithTitle:@"Go To Location?" message:msg preferredStyle:UIAlertControllerStyleActionSheet];
    alert.view.tintColor = [SxUtilities ThemeColor];
    
    UIAlertAction *teleport_action = [UIAlertAction actionWithTitle:@"Teleport to Location" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (valid == YES) {
            CLLocationCoordinate2D coords = [SxUtilities coordsFromString:cords];
            [self dismissWithTeleport:coords];
        }
    }];
    
    UIAlertAction *walk_action = [UIAlertAction actionWithTitle:@"Walk to Location" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (valid == YES) {
            CLLocationCoordinate2D coords = [SxUtilities coordsFromString:cords];
            [self dismissWithWalk:coords];
        }
    }];
    
    UIAlertAction *copy_action = [UIAlertAction actionWithTitle:@"Copy Coordinates" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *coords = [NSString stringWithFormat:@"%@",cords];
        UIPasteboard *thePasteboard = [UIPasteboard generalPasteboard];
        [thePasteboard setString:coords];
    }];
    
    UIAlertAction *delete_action = [UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [[HotspotsManager sharedManager] remove_userPlace:place];
    }];
    
    UIAlertAction *cancel_action = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alert addAction:teleport_action];
    [alert addAction:walk_action];
    [alert addAction:copy_action];
    if (indexPath.section == 0) {
       [alert addAction:delete_action];
    }
    [alert addAction:cancel_action];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        [alert setModalPresentationStyle:UIModalPresentationPopover];
        UIPopoverPresentationController *popper = [alert popoverPresentationController];
        popper.sourceView = cell;
        popper.sourceRect = CGRectMake(cell.frame.size.width/2, cell.frame.size.height/2, 1, 1);
    }
    
    [[UIViewController spxoverlay_topMostController] presentViewController:alert animated:YES completion:^{
        
    }];
}

- (void)setupUI
{
    self.navigationController.navigationBar.tintColor = [SxUtilities ThemeColor];
    
    UIImage *close = [AssetsManager imageForResource:@"multiply-symbol-mini"];
    [self.doneBarButton setImage:close];
    
    UIImage *fly = [AssetsManager imageForResource:@"compass-direction-7"];
    [self.flyBarButton setImage:fly];
    self.flyBarButton.width = 0.01;
    self.flyBarButton.enabled = NO;
    self.flyBarButton.tintColor = [UIColor clearColor];
    
    self.user_placesArray = [[NSMutableArray alloc] init];
    self.spx_placesArray = [[NSMutableArray alloc] init];
    [self ShowDeprecationAlert];
//    self.spx_nestsArray = [[NSMutableArray alloc] init];
}

- (void)showLoading
{
    loading = [[[AssetsManager resourcesBundle] loadNibNamed:@"LoadingView" owner:self options:nil] objectAtIndex:0];
    loading.tag = 0005;
    loading.frame = CGRectMake((self.view.frame.size.width/2)-40.0, (self.view.frame.size.height/2)-25.0, 80.0, 50.0);
    loading.center = self.view.center;
    [self.view addSubview:loading];
}

- (void)hideLoading
{
    loading = [self.view viewWithTag:0005];
    [loading removeFromSuperview];
    loading = nil;
    
    for (UIView *view in self.tableView.subviews) {
        if ([view isKindOfClass:[LoadingView class]]) {
            LoadingView *lod = (LoadingView*)view;
            [lod removeFromSuperview];
            lod = nil;
        }
    }
}

- (void)loadPlaces
{
    [self showLoading];
    self.user_placesArray = [[HotspotsManager sharedManager] get_userPlaces];
    self.spx_placesArray = [[HotspotsManager sharedManager] get_spxPlaces];
//    self.spx_nestsArray = [[HotspotsManager sharedManager] get_spxNests];
    [self loadTable];
}

- (id)placeAtIndexPath:(NSIndexPath*)indexPath
{
    if (indexPath.section == 0) {
        return [self.user_placesArray objectAtIndex:indexPath.row];
    }
    else
    {
        return [self.spx_placesArray objectAtIndex:indexPath.row];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return self.user_placesArray.count;
    }
    else
    {
        return self.spx_placesArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"hotspotCell" forIndexPath:indexPath];
    
    NSString *emoji;
    NSString *title;
    NSString *coordinates;
    
    if (indexPath.section == 0) {
        NSDictionary *place = [self placeAtIndexPath:indexPath];
        emoji = [NSLocale  emojiFlagForISOCountryCode:place[@"iso2code"]];
        title = [NSString stringWithFormat:@"%@ - %@, %@",emoji,place[@"name"],place[@"city"]];
        coordinates = [NSString stringWithFormat:@"%f,%f",[place[@"latitude"] doubleValue],[place[@"longitude"] doubleValue]];
        cell.textLabel.text = title;
        cell.detailTextLabel.text = coordinates;
        cell.imageView.image = nil;
    }
    else if (indexPath.section == 1)
    {
        NSDictionary *place = [self placeAtIndexPath:indexPath];
        emoji = [NSLocale  emojiFlagForISOCountryCode:place[@"CountryCode"]];
        title = [NSString stringWithFormat:@"%@ - %@, %@",emoji,place[@"Name"],place[@"CountryName"]];
        coordinates = [NSString stringWithFormat:@"%f,%f",[place[@"Lat"] doubleValue],[place[@"Lon"] doubleValue]];
        cell.textLabel.text = title;
        cell.detailTextLabel.text = coordinates;
        cell.imageView.image = nil;
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self selectionAlert:indexPath];
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return @"Favourites";
    }
    else
    {
        return @"Hotspots";
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textPasteConfigurationSupporting:(id<UITextPasteConfigurationSupporting>)textPasteConfigurationSupporting transformPasteItem:(id<UITextPasteItem>)item API_AVAILABLE(ios(11.0)) {
    [self paste:textPasteConfigurationSupporting];
}

- (void)paste:(id)sender {
    UIPasteboard *gpBoard = [UIPasteboard generalPasteboard];
    if (@available(iOS 10.0, *)) {
        if ([gpBoard hasStrings])
        {
            NSCharacterSet *setToRemove = [NSCharacterSet characterSetWithCharactersInString:@"0123456789.,-"];
            NSCharacterSet *setToKeep = [setToRemove invertedSet];
            NSString *newString = [[[gpBoard string] componentsSeparatedByCharactersInSet:setToKeep] componentsJoinedByString:@""];
            [self.teleportAlert.textFields[0] insertText:newString];
        }
    } else {
        // Fallback on earlier versions
    }
}

#pragma mark - TextField Delegate

-(void)textFieldDidChangeCoord:(UITextField*)field {
    
    @try
    {
        NSString *msg;
        BOOL valid = [SxUtilities coordinatesValid:field.text];
        if (valid == YES) {
            CLLocationCoordinate2D cords = [SxUtilities coordsFromString:field.text];
            double distance = [SxUtilities distanceBetweenLocations:cords];
            //distance = distance/1000;
            NSString *dststr = [SxUtilities distanceString:distance];
            double cdd = [SxUtilities cdDistanceBetweenLocations:cords];
            cdd = cdd/1000;
            int min = [SxUtilities getRemainingCD:cdd];
            msg = [NSString stringWithFormat:@"Teleport to: %@?\nD - %@, CD - %d mins",field.text,dststr, min];
        }
        else
        {
            msg = [NSString stringWithFormat:@"Invalid Coordinates %@",field.text];
        }
        
        self.teleportAlert.message = msg;
        
    } @catch (NSException *exception) {
        
        self.teleportAlert.message = @"";
    }
}

@end

//
//  SettingsHeader.m
//  pokextest
//
//  Created by c0pyn1nja on 25/09/21
//  Copyright © 2021 iphonecake.com. All rights reserved.
//

#import "SettingsHeader.h"
#import "RpcManager.h"
#import "SettingsViewController.h"

@implementation SettingsHeader

@synthesize iconImageView;
@synthesize titleLabel;
@synthesize versionLabel;
@synthesize activateButton;
@synthesize getKeyButton;
@synthesize backdropView;

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        NSArray *xibs = [[AssetsManager resourcesBundle] loadNibNamed:@"SettingsHeader" owner:self options:nil];
        self = [xibs objectAtIndex:0];
        self.frame = frame;
        self.backgroundColor = [UIColor secondarySystemGroupedBackgroundColor];
        self.backdropView.backgroundColor = [UIColor secondarySystemGroupedBackgroundColor];
        self.backdropView.layer.cornerRadius = 10;
        self.backdropView.layer.masksToBounds = YES;
    }
    
    return self;
}

- (void)enableActivateButton:(NSString*)title
{
    [self.activateButton setEnabled:YES];
    [self.activateButton setTitle:title forState:UIControlStateNormal];
    [self.activateButton setTitleColor:[SxUtilities ThemeColor] forState:UIControlStateNormal];
    //[self.activateButton.titleLabel setFont:[UIFont systemFontOfSize:18 weight:UIFontWeightSemibold]];
}

- (void)disableActivateButton:(NSString*)title
{
    [self.activateButton setEnabled:YES];
    [self.activateButton setTitle:title forState:UIControlStateNormal];
    [self.activateButton setTitleColor:[SxUtilities ThemeColor] forState:UIControlStateNormal];
}

- (void)enableKeyButton:(NSString*)title
{
    [self.getKeyButton setEnabled:YES];
    [self.getKeyButton setTitle:title forState:UIControlStateNormal];
    [self.getKeyButton setTitleColor:[UIColor labelColor] forState:UIControlStateNormal];
}

- (void)disableKeyButton:(NSString*)title
{
    [self.getKeyButton setEnabled:NO];
    [self.getKeyButton setTitle:title forState:UIControlStateNormal];
    [self.getKeyButton setTitleColor:[UIColor labelColor] forState:UIControlStateNormal];
}

- (UIViewController *)viewController {
    UIResponder *responder = self;
    while (![responder isKindOfClass:[UIViewController class]]) {
        responder = [responder nextResponder];
        if (nil == responder) {
            break;
        }
    }
    return (UIViewController *)responder;
}

- (IBAction)activateSpooferx:(id)sender
{
    if(self.parentV == nil)
    {
        self.parentV = (SettingsViewController*)[self viewController];
    }
    
    if ([SxUtilities isLegendaryPokemon] == YES) {
        [self.parentV showResetKeyNew];
    }
    else
    {
        [self.parentV prepareForKeyNew];
    }
}

- (IBAction)getKey:(id)sender
{
    if(self.parentV == nil)
    {
        self.parentV = (SettingsViewController*)[self viewController];
    }
    
    if ([SxUtilities isLegendaryPokemon] == NO) {
        [self.parentV showOptionsForWeb2];
    }
}

@end

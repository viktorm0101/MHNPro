//
//  LoadingView.m
//  toonsnow
//
//  Created by spooferx on 20/05/17.
//  Copyright © 2017 spooferx. All rights reserved.
//

#import "LoadingView.h"

@implementation LoadingView
@synthesize activityView;

- (void)startActivity
{
    [self.activityView startAnimating];
}

- (void)stopActivity
{
    [self.activityView stopAnimating];
}

@end

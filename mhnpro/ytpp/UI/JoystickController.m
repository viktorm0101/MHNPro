//
//  JoystickController.m
//  spooferxsl
//
//  Created by spooferx on 11/15/19.
//

#import "JoystickController.h"

@interface JoystickController() {
    CALayer *buttonImage;
    CFTimeInterval lastMove;
}

@property (readwrite, nonatomic) JoystickControlDirection direction;
@property (assign, nonatomic) CGSize deadZone;
@property (strong, nonatomic) UIImageView *backgroundImageView;
@property (nonatomic, strong) UITapGestureRecognizer *gestureRecognizerTripleTap;
@property (assign, nonatomic) CGRect deadZoneRect;

@end

@implementation JoystickController

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        //_backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 80, 80)];
        _backgroundImageView = [[UIImageView alloc] initWithFrame:self.bounds];
        _backgroundImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        [self addSubview:_backgroundImageView];
        
        buttonImage = [[CALayer alloc] init];
        buttonImage.frame = CGRectMake(0, 0, self.frame.size.width/2.2, self.frame.size.width/2.2);
        buttonImage.contents = (id)[AssetsManager imageForResource:@"J-Button"].CGImage;
        buttonImage.anchorPoint = CGPointMake(0.5, 0.5);
        buttonImage.actions = @{@"position": [NSNull null]};
        buttonImage.position = CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2);
        [self.layer addSublayer:buttonImage];
        self.backgroundImageView.image = [AssetsManager imageForResource:@"J-Base"];
        self.deadZone = CGSizeMake(self.frame.size.width/3, self.frame.size.height/3);
        self.direction = JoystickControlDirectionDown;
        
        _directionVector = CGPointMake(0.0, 0.0);
    }
    
    return self;
}

- (JoystickControlDirection)directionForTouch:(UITouch *)touch
{
    CGPoint loc = [touch locationInView:self];
    JoystickControlDirection direction = 0;
    
    if (loc.x > (self.bounds.size.width + self.deadZone.width)/2) direction |= JoystickControlDirectionRight;
    else if (loc.x < (self.bounds.size.width - self.deadZone.width)/2) direction |= JoystickControlDirectionLeft;
    if (loc.y > (self.bounds.size.height + self.deadZone.height)/2) direction |= JoystickControlDirectionDown;
    else if (loc.y < (self.bounds.size.height - self.deadZone.height)/2) direction |= JoystickControlDirectionUp;
    
    return direction;
}

- (BOOL)beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
    self.direction = [self directionForTouch:touch];
    [self sendActionsForControlEvents:UIControlEventValueChanged];
    CGPoint loc = [touch locationInView:self];
    buttonImage.position = loc;
    lastMove = CACurrentMediaTime();
    return YES;
}

- (BOOL)continueTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
    self.direction = [self directionForTouch:touch];
    
    [self sendActionsForControlEvents:UIControlEventValueChanged];
    
    CGPoint loc = [touch locationInView:self];
    loc.x -= self.bounds.size.width/2;
    loc.y -= self.bounds.size.height/2;
    
    double radius = sqrt(loc.x*loc.x+loc.y*loc.y);
    
    double maxRadius = self.bounds.size.width * 0.45;
    
    if (radius > maxRadius) {
        double angle = atan(loc.y/loc.x);
        if (loc.x < 0) angle += M_PI;
        loc.x = maxRadius * cos(angle);
        loc.y = maxRadius * sin(angle);
    }
    _directionVector = loc;
    
    if (!self.inMove) {
        self.inMove = YES;
    }
    
    if ([self.moveDelegate respondsToSelector:@selector(joyStickDidMoveOffsetX:offsetY:)]) {
        [self.moveDelegate joyStickDidMoveOffsetX:_directionVector.x offsetY:_directionVector.y];
    }
    
    loc.x += self.bounds.size.width/2;
    loc.y += self.bounds.size.height/2;
    
    if (CACurrentMediaTime() - lastMove > 0.033) {
        buttonImage.position = loc;
        lastMove = CACurrentMediaTime();
    }
    
    return YES;
}

- (void)endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
    self.direction = 0;
    [self sendActionsForControlEvents:UIControlEventValueChanged];
    buttonImage.position = CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2);
    _directionVector = CGPointMake(0.0, 0.0);
    self.inMove = NO;
}


@end

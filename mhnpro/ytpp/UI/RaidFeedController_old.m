//
//  RaidFeedController.m
//  SDWebImage
//
//  Created by c0pyn1nja on 20/03/21
//  Copyright © 2021 Dailymotion. All rights reserved.
//

#import "RaidFeedController.h"
#import "SpooferxManager.h"
#import "RpcManager.h"
#import "RaidFeedCell.h"
#import "LoadingView.h"
#import "IGP_ActivityManager.h"

@interface RaidFeedController () <UISearchControllerDelegate, UISearchBarDelegate, UISearchResultsUpdating>


@property (nonatomic, weak) IBOutlet UITableView *listView;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *sortBarButton;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *filterBarButton;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *doneBarButton;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *refreshBarButton;

@property (nonatomic, strong) NSMutableArray *raidsArray;
@property (nonatomic, strong) NSMutableArray *filteredRaidsArray;
@property (nonatomic, strong) NSDictionary *sorts;
@property (nonatomic, strong) NSDictionary *filters;
@property (nonatomic, strong) NSString *currentSort;
@property (nonatomic, strong) NSString *currentFilter;

@property (strong, nonatomic) UISearchController *searchController;
@property (strong, nonatomic) NSMutableArray *searchResults;

- (IBAction)dismiss:(id)sender;
- (IBAction)showFilter:(id)sender;
- (IBAction)showSort:(id)sender;
- (IBAction)refreshData:(id)sender;

@end

@implementation RaidFeedController
{
    LoadingView *loading;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"BattleDetails" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(processData:)
                                                 name:@"BattleDetails"
                                               object:nil];
    
    self.raidsArray = [[NSMutableArray alloc] init];
    self.filteredRaidsArray = [[NSMutableArray alloc] init];
    self.searchResults =  [[NSMutableArray alloc] init];
    
    [self loadData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"BattleDetails" object:nil];
    [self hideLoading];
}

- (NSDictionary*)getfiltersForRaid
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:@(2500) forKey:@"limit"];
    return parameters;
}

- (void)loadData
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.raidsArray removeAllObjects];
        [self.filteredRaidsArray removeAllObjects];
        [self.searchResults removeAllObjects];
        [self.listView reloadData];
    });
    
    [self showLoading];
    NSDictionary *params = [self getfiltersForRaid];
    [RpcManager getNewBattlePoints:params];
}

- (IBAction)refreshData:(id)sender
{
    [self loadData];
}

- (void)showLoading
{
    CGSize displaySize = [[UIScreen mainScreen] bounds].size;
    loading = [[[AssetsManager resourcesBundle] loadNibNamed:@"LoadingView" owner:self options:nil] objectAtIndex:0];
    loading.tag = 0005;
    loading.frame = CGRectMake((displaySize.width/2)-40.0, (displaySize.height/2)-25.0, 80.0, 50.0);
    loading.center = self.view.center;
    [self.view addSubview:loading];
}

- (void)hideLoading
{
    loading = [self.view viewWithTag:0005];
    [loading removeFromSuperview];
    loading = nil;
    
    for (UIView *view in self.listView.subviews) {
        if ([view isKindOfClass:[LoadingView class]]) {
            LoadingView *lod = (LoadingView*)view;
            [lod removeFromSuperview];
            lod = nil;
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupInterface];
}

- (void)setupInterface
{
    self.navigationController.navigationBar.tintColor = [SxUtilities ThemeColor];
    self.title = @"Raids Feed";
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.obscuresBackgroundDuringPresentation = NO;
    self.searchController.delegate = self;
    self.searchController.searchBar.delegate = self;
    [self.searchController.searchBar sizeToFit];
    self.searchController.searchBar.backgroundColor = [UIColor systemGroupedBackgroundColor];
    self.searchController.hidesNavigationBarDuringPresentation = NO;
    self.searchController.searchBar.searchBarStyle = UISearchBarStyleMinimal;
    self.searchController.searchBar.tintColor = [SxUtilities ThemeColor];
    self.searchController.searchBar.returnKeyType = UIReturnKeyDone;
    self.navigationItem.searchController = self.searchController;
    self.navigationItem.hidesSearchBarWhenScrolling = false;
    
    UIImage *close = [AssetsManager imageForResource:@"multiply-symbol-mini"];
    [self.doneBarButton setImage:close];
    UIImage *sort = [AssetsManager imageForResource:@"sort"];
    [self.sortBarButton setImage:sort];
    UIImage *filter = [AssetsManager imageForResource:@"filter"];
    [self.filterBarButton setImage:filter];
    
    self.listView.contentInset = UIEdgeInsetsMake(0.0f, 0.0f, CGRectGetHeight(self.tabBarController.tabBar.frame), 0.0f);
    self.listView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.sorts = @{@"1":@"all",@"2":@"level",@"3":@"start time",@"4":@"end time",@"5":@"distance"};
    self.filters = @{@"1":@"all",@"2":@"eggs",@"3":@"on going",@"4":@"level 1 raid",@"5":@"level 3 raid",@"6":@"legendary raid",@"7":@"mega raid",@"8":@"mega5 raid",@"9":@"ultra raid",@"10":@"elite raid",@"11":@"possible shiny"};
    [Preferences setGlobal_list_index:1];
    NSArray *sf = [[Preferences raidfeedfs] componentsSeparatedByString:@","];
    self.currentSort = [NSString stringWithFormat:@"%@",[sf objectAtIndex:0]];
    if ([self.currentSort length] == 0) {
        self.currentSort = @"1";
    }
    
    self.currentFilter = [NSString stringWithFormat:@"%@",[sf objectAtIndex:1]];
    if ([self.currentFilter length] == 0) {
        self.currentFilter = @"1";
    }
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    NSString *searchString = searchController.searchBar.text;
    if (!searchString.length) {
        //_ListArray=_TempArray;
    }
    else{
        // strip out all the leading and trailing spaces
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            //Background Thread
            NSString *strippedString = [[searchString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] lowercaseString];
            self.searchResults = [[NSMutableArray alloc] init];
            for (RaidFeed *mon in self.raidsArray)
            {
                NSString *name = @"";
                NSDate *current = [NSDate date];
                NSDate *battleDate = [NSDate dateWithTimeIntervalSince1970:mon.raidBattleTimestamp.doubleValue];
                if ([current isEarlierThan:battleDate]) {
                    name = [SxUtilities titleForRaidEgg:mon.raidLevel.intValue];
                }
                else if ([current isLaterThan:battleDate])
                {
                    NSString *nam = [SxUtilities nameForPokemon2:mon.raidPokemonId.intValue form:(PokemonDisplayProto_Form)mon.raidPokemonForm.intValue];
                    name = [nam stringByAppendingString:@" Raid"];
                }
                
                if ([name.lowercaseString containsString:strippedString])
                {
                    [self.searchResults addObject:mon];
                }
            }
            
            [self.filteredRaidsArray removeAllObjects];
            self.filteredRaidsArray = [self.searchResults mutableCopy];
            [self sortData];
        });
    }
    
}

- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope
{
    [self updateSearchResultsForSearchController:self.searchController];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self loadData];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.searchController setActive:NO];
}

- (void)processData:(NSNotification*)notif
{
    if (notif.userInfo[@"battlepoints"] != nil) {
        GRKConcurrentMutableDictionary *dict = notif.userInfo[@"battlepoints"];
        [self.raidsArray addObjectsFromArray:[dict nonConcurrentDictionary].allValues];
        [self filterData];
    }
    else if (notif.userInfo[@"battleerror"] != nil)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hideLoading];
            self.title = [NSString stringWithFormat:@"Raids (%lu)",self.filteredRaidsArray.count];
            [self.listView reloadData];
        });
    }
}

- (IBAction)dismiss:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

- (void)dismissWithTeleport:(RaidFeed*)mon
{
    [self dismissViewControllerAnimated:YES completion:^{
        CLLocationCoordinate2D coords = [SxUtilities CLLocationCoordinate2DMakeNormal:mon.latitude.doubleValue lon:mon.longitude.doubleValue];
        [[SpooferxManager sharedManager] teleportTo:coords];
        [ActivityManager SaveRaid:mon];
    }];
}

- (void)filterData
{
    self.filteredRaidsArray = [self.raidsArray mutableCopy];
    
    if ([self.currentFilter isEqualToString:@"1"] == YES) {
        
    }
    
    if ([self.currentFilter isEqualToString:@"2"] == YES) {
        NSMutableArray *f = [self.raidsArray mutableCopy];
        for (RaidFeed *p in self.raidsArray) {
            NSDate *current = [NSDate date];
            NSDate *battleDate = [NSDate dateWithTimeIntervalSince1970:p.raidBattleTimestamp.doubleValue];
            if ([current isLaterThan:battleDate] == YES) {
                [f removeObject:p];
            }
        }
        self.filteredRaidsArray = [f mutableCopy];
        f = nil;
    }
    
    if ([self.currentFilter isEqualToString:@"3"] == YES) {
        NSMutableArray *f = [self.raidsArray mutableCopy];
        for (RaidFeed *p in self.raidsArray) {
            NSDate *current = [NSDate date];
            NSDate *battleDate = [NSDate dateWithTimeIntervalSince1970:p.raidBattleTimestamp.doubleValue];
            if ([current isEarlierThan:battleDate] == YES) {
                [f removeObject:p];
            }
        }
        self.filteredRaidsArray = [f mutableCopy];
        f = nil;
    }
    
    
    if ([self.currentFilter isEqualToString:@"4"] == YES) {
        NSMutableArray *f = [self.raidsArray mutableCopy];
        for (RaidFeed *p in self.raidsArray) {
            if (p.raidLevel.intValue != 1) {
                [f removeObject:p];
            }
        }
        self.filteredRaidsArray = [f mutableCopy];
        f = nil;
    }
    
    if ([self.currentFilter isEqualToString:@"5"] == YES) {
        NSMutableArray *f = [self.raidsArray mutableCopy];
        for (RaidFeed *p in self.raidsArray) {
            if (p.raidLevel.intValue != 3) {
                [f removeObject:p];
            }
        }
        self.filteredRaidsArray = [f mutableCopy];
        f = nil;
    }
    
    if ([self.currentFilter isEqualToString:@"6"] == YES) {
        NSMutableArray *f = [self.raidsArray mutableCopy];
        for (RaidFeed *p in self.raidsArray) {
            if (p.raidLevel.intValue != 5) {
                [f removeObject:p];
            }
        }
        self.filteredRaidsArray = [f mutableCopy];
        f = nil;
    }
    
    if ([self.currentFilter isEqualToString:@"7"] == YES) {
        NSMutableArray *f = [self.raidsArray mutableCopy];
        for (RaidFeed *p in self.raidsArray) {
            if (p.raidLevel.intValue != 6) {
                [f removeObject:p];
            }
        }
        self.filteredRaidsArray = [f mutableCopy];
        f = nil;
    }
    
    if ([self.currentFilter isEqualToString:@"8"] == YES) {
        NSMutableArray *f = [self.raidsArray mutableCopy];
        for (RaidFeed *p in self.raidsArray) {
            if (p.raidLevel.intValue != 7) {
                [f removeObject:p];
            }
        }
        self.filteredRaidsArray = [f mutableCopy];
        f = nil;
    }
    
    if ([self.currentFilter isEqualToString:@"9"] == YES) {
        NSMutableArray *f = [self.raidsArray mutableCopy];
        for (RaidFeed *p in self.raidsArray) {
            if (p.raidLevel.intValue != 8) {
                [f removeObject:p];
            }
        }
        self.filteredRaidsArray = [f mutableCopy];
        f = nil;
    }
    
    if ([self.currentFilter isEqualToString:@"10"] == YES) {
        NSMutableArray *f = [self.raidsArray mutableCopy];
        for (RaidFeed *p in self.raidsArray) {
            if (p.raidLevel.intValue != 9) {
                [f removeObject:p];
            }
        }
        self.filteredRaidsArray = [f mutableCopy];
        f = nil;
    }
    
    if ([self.currentFilter isEqualToString:@"11"] == YES) {
        NSMutableArray *f = [self.raidsArray mutableCopy];
        for (RaidFeed *p in self.raidsArray) {
            if (p.posshiny == false) {
                [f removeObject:p];
            }
        }
        self.filteredRaidsArray = [f mutableCopy];
        f = nil;
    }
    
    [self sortData];
}

- (void)sortData
{
    if ([self.currentSort isEqualToString:@"1"] == YES)
    {
        
    }
    
    if ([self.currentSort isEqualToString:@"2"] == YES)
    {
        self.filteredRaidsArray = [[self.filteredRaidsArray sortedArrayUsingComparator:^NSComparisonResult(RaidFeed *a, RaidFeed *b)
                                    {
            if (a.raidLevel.intValue == b.raidLevel.intValue)
            {
                return a.raidBattleTimestamp.doubleValue < b.raidBattleTimestamp.doubleValue;
            }
            return a.raidLevel.intValue < b.raidLevel.intValue;
        }] mutableCopy];
    }
    
    if ([self.currentSort isEqualToString:@"3"] == YES)
    {
        self.filteredRaidsArray = [[self.filteredRaidsArray sortedArrayUsingComparator:^NSComparisonResult(RaidFeed *a, RaidFeed *b)
                                    {
            return a.raidBattleTimestamp.doubleValue > b.raidBattleTimestamp.doubleValue;
        }] mutableCopy];
    }
    
    if ([self.currentSort isEqualToString:@"4"] == YES)
    {
        self.filteredRaidsArray = [[self.filteredRaidsArray sortedArrayUsingComparator:^NSComparisonResult(RaidFeed *a, RaidFeed *b)
                                    {
            return a.raidEndTimestamp.doubleValue < b.raidEndTimestamp.doubleValue;
        }] mutableCopy];
    }
    
    if ([self.currentSort isEqualToString:@"5"] == YES)
    {
        self.filteredRaidsArray = [[self.filteredRaidsArray sortedArrayUsingComparator:^NSComparisonResult(RaidFeed *a, RaidFeed *b)
                                    {
            double d1 = [SxUtilities distanceBetweenLocations:CLLocationCoordinate2DMake([a.latitude doubleValue], [a.longitude doubleValue])];
            double d2 = [SxUtilities distanceBetweenLocations:CLLocationCoordinate2DMake([b.latitude doubleValue], [b.longitude doubleValue])];
            return d1 > d2;
        }] mutableCopy];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self hideLoading];
        self.title = [NSString stringWithFormat:@"Raids (%lu)",self.filteredRaidsArray.count];
        [self.listView reloadData];
    });
}

- (IBAction)showFilter:(id)sender
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Filter" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    alert.view.tintColor = [SxUtilities ThemeColor];
    
    NSArray *filterKeys = [[self.filters allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    for (NSString *key in filterKeys)
    {
        NSString *title = [[self.filters objectForKey:key] capitalizedString];
        
        UIAlertAction *aaction = [UIAlertAction actionWithTitle:title style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            self.currentFilter = key;
            NSString *sf = [NSString stringWithFormat:@"%@,%@",self.currentSort,self.currentFilter];
            [Preferences setRaidfeedfs:sf];
            [self filterData];
        }];
        
        if ([key isEqualToString:self.currentFilter] == YES) {
            [aaction setValue:[UIColor colorWithRed:0.94 green:0.52 blue:0.28 alpha:1.00] forKey:@"imageTintColor"];
            [aaction setValue:@true forKey:@"checked"];
        }
        
        [alert addAction:aaction];
    }
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        self.currentFilter = @"1";
        NSString *sf = [NSString stringWithFormat:@"%@,%@",self.currentSort,self.currentFilter];
        [Preferences setRaidfeedfs:sf];
        [self filterData];
    }];
    
    [cancel setValue:[UIColor redColor] forKey:@"titleTextColor"];
    [alert addAction:cancel];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        [alert setModalPresentationStyle:UIModalPresentationPopover];
        UIPopoverPresentationController *popper = [alert popoverPresentationController];
        popper.barButtonItem = self.filterBarButton;
        popper.permittedArrowDirections = UIPopoverArrowDirectionUp;
    }
    
    [[UIViewController spxoverlay_topMostController] presentViewController:alert animated:YES completion:nil];
}

- (IBAction)showSort:(id)sender
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Sort" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    alert.view.tintColor = [SxUtilities ThemeColor];
    
    NSArray *sortKeys = [[self.sorts allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    for (NSString *key in sortKeys) {
        NSString *title = [[self.sorts objectForKey:key] capitalizedString];
        UIAlertAction *aaction = [UIAlertAction actionWithTitle:title style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            self.currentSort = key;
            NSString *sf = [NSString stringWithFormat:@"%@,%@",self.currentSort,self.currentFilter];
            [Preferences setRaidfeedfs:sf];
            [self filterData];
            
        }];
        
        if ([key isEqualToString:self.currentSort] == YES) {
            [aaction setValue:[SxUtilities ThemeColor] forKey:@"imageTintColor"];
            [aaction setValue:@true forKey:@"checked"];
        }
        
        [alert addAction:aaction];
    }
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        self.currentSort = @"5";
        NSString *sf = [NSString stringWithFormat:@"%@,%@",self.currentSort,self.currentFilter];
        [Preferences setRaidfeedfs:sf];
        [self filterData];
        
    }];
    [cancel setValue:[UIColor redColor] forKey:@"titleTextColor"];
    
    [alert addAction:cancel];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        [alert setModalPresentationStyle:UIModalPresentationPopover];
        UIPopoverPresentationController *popper = [alert popoverPresentationController];
        popper.barButtonItem = self.sortBarButton;
        popper.permittedArrowDirections = UIPopoverArrowDirectionUp;
    }
    
    [[UIViewController spxoverlay_topMostController] presentViewController:alert animated:YES completion:nil];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.filteredRaidsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RaidFeedCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RaidFeedCell" forIndexPath:indexPath];
    RaidFeed *mon = [self.filteredRaidsArray objectAtIndex:indexPath.row];
    [cell setupRaidPokemon:mon];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self showCellActions:indexPath];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)showCellActions:(NSIndexPath*)indexPath
{
    RaidFeed *p = [self.filteredRaidsArray objectAtIndex:indexPath.row];
    if (Preferences.disableTeleDia == NO) {
        CLLocationCoordinate2D desti = [SxUtilities CLLocationCoordinate2DMakeNormal:p.latitude.doubleValue lon:p.longitude.doubleValue];
        NSString *cdstr = [SxUtilities cooldownStringForDestination:desti];
        NSString *msg = [NSString stringWithFormat:@"Go to Location %f,%f\r\n%@",desti.latitude,desti.longitude,cdstr];
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Go To Location?" message:msg preferredStyle:UIAlertControllerStyleAlert];
        alert.view.tintColor = [SxUtilities ThemeColor];
        
        UIAlertAction *teleport_action = [UIAlertAction actionWithTitle:@"Teleport to Location" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self dismissWithTeleport:p];
        }];
        
        UIAlertAction *copy_action = [UIAlertAction actionWithTitle:@"Copy Coordinates" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            NSString *coords = [NSString stringWithFormat:@"%f,%f",desti.latitude, desti.longitude];
            UIPasteboard *thePasteboard = [UIPasteboard generalPasteboard];
            [thePasteboard setString:coords];
        }];
        
        UIAlertAction *cancel_action = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        
        [alert addAction:teleport_action];
        [alert addAction:copy_action];
        [alert addAction:cancel_action];
        
        [[UIViewController spxoverlay_topMostController] presentViewController:alert animated:YES completion:^{
        }];
    }
    else
    {
        [self dismissWithTeleport:p];
    }
}


@end

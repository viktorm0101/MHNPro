//
//  SettingsViewController.m
//  spooferxsl
//
//  Created by spooferx on 10/27/20
//  
//

#import "SettingsViewController.h"
#import "SpooferxManager.h"
#import "RpcManager.h"
#import "RoutesManager.h"
#import "SettingsHeader.h"
#import "IGP_PlacesManager.h"
#import "IGP_LocationSimulator.h"
#import "IGP_NotificationsManager.h"

#import <ZTGCDTimerManager.h>

@interface SettingsViewController () <UITextPasteDelegate, FCAlertViewDelegate>

@property (nonatomic, weak) IBOutlet UIBarButtonItem *doneBarButton;

@property (nonatomic, strong) UISwitch *spoofSwitch;
@property (nonatomic, strong) UISwitch *joystickSwitch;
@property (nonatomic, strong) UISwitch *tapToWalkSwitch;
@property (nonatomic, strong) UISwitch *tapToTeleportSwitch;

@property (nonatomic, strong) SettingsHeader *settingsH;
@property (nonatomic, strong) UIAlertController *speedAlert;
@property (nonatomic, strong) UIAlertController *licenseAlert;
@property (nonatomic, strong) NSString *readableLocation;

@property (nonatomic, strong) NSString *fakeLocationString;
@property (nonatomic, strong) NSString *fakeLocationTime;
@property (nonatomic, strong) NSString *cooldownTime;
@property (nonatomic, strong) UIColor *cooldownColor;

@property (nonatomic, strong) FCAlertView *licenseFCAlert;
@property (nonatomic, strong) FCAlertView *progressFCAlert;
@property (nonatomic, strong) FCAlertView *successFCAlert;
@property (nonatomic, strong) FCAlertView *failFCAlert;
@property (nonatomic, strong) FCAlertView *warningFCAlert;


- (IBAction)dismiss:(id)sender;

@end

@implementation SettingsViewController
{
    UIActivityIndicatorView *activity;
    UIBarButtonItem *activityBarButton;
    NSString *licenseKey;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
        [self startFakelocationTimer];
        [self startCooldownTimer];
    });
    
}

#pragma mark - Timers

- (void)startFakelocationTimer
{
    [[ZTGCDTimerManager sharedInstance] scheduleGCDTimerWithName:@"com.pmgo.spx.fakeloc_timer" interval:1 queue:dispatch_get_main_queue() repeats:YES option:CancelPreviousTimerAction action:^{
        [self getFakeLocationDetails];
    }];
}

- (void)stopFakelocationTimer
{
    [[ZTGCDTimerManager sharedInstance] cancelTimerWithName:@"com.pmgo.spx.fakeloc_timer"];
}

- (void)startCooldownTimer
{
    [[ZTGCDTimerManager sharedInstance] scheduleGCDTimerWithName:@"com.spx.igp.cooldown_timer" interval:1 queue:dispatch_get_main_queue() repeats:YES option:CancelPreviousTimerAction action:^{
        [self setupCooldownTimers];
    }];
}

- (void)stopCooldownTimer
{
    [[ZTGCDTimerManager sharedInstance] cancelTimerWithName:@"com.pmgo.spx.cooldown_timer"];
}

- (NSString*)getLocationAddresss
{
    NSString *locality = @"";
    if ([Preferences.fakePlacemark.locality length] > 0) {
        locality = [NSString stringWithFormat:@"%@, ",Preferences.fakePlacemark.locality];
    }
    
    NSString *adminArea = @"";
    if ([Preferences.fakePlacemark.administrativeArea length] > 0) {
        adminArea = [NSString stringWithFormat:@"%@, ",Preferences.fakePlacemark.administrativeArea];
    }
    
    NSString *country = @"";
    if ([Preferences.fakePlacemark.country length] > 0) {
        country = Preferences.fakePlacemark.country;
    }
    
    return [NSString stringWithFormat:@"%@%@%@",locality,adminArea,country];
}


- (void)getFakeLocationDetails
{
    self.fakeLocationString = [self getLocationAddresss];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEE, MMM d, yyyy - h:mm:ss a"];
    [dateFormatter setTimeZone:Preferences.fakePlacemark.timeZone];
    self.fakeLocationTime = [dateFormatter stringFromDate:[NSDate date]];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    cell.detailTextLabel.text = self.fakeLocationTime;
    NSArray *indpaths = @[indexPath];
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:indpaths withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView endUpdates];
}

- (void)setupCooldownTimers
{
    CLLocationCoordinate2D destination = [SxUtilities CLLocationCoordinate2DMakeNormal:Preferences.fakeLatitude lon:Preferences.fakeLongitude];
    int tr  = [SxUtilities GetRemainingCDForTimer:destination];
    if (tr > 0)
    {
        NSUInteger h = tr / 3600;
        NSUInteger m = (tr / 60) % 60;
        NSUInteger s = tr % 60;
        self.cooldownTime = [NSString stringWithFormat:@"%lu:%02lu:%02lu", (unsigned long)h, (unsigned long)m, (unsigned long)s];
        self.cooldownColor = [UIColor redColor];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        cell.detailTextLabel.text = self.cooldownTime;
        NSArray *indpaths = @[indexPath];
        [self.tableView beginUpdates];
        [self.tableView reloadRowsAtIndexPaths:indpaths withRowAnimation:UITableViewRowAnimationNone];
        [self.tableView endUpdates];
    }
    else
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        cell.detailTextLabel.text = @"00:00:00";
        self.cooldownColor = [UIColor greenColor];
        NSArray *indpaths = @[indexPath];
        [self.tableView beginUpdates];
        [self.tableView reloadRowsAtIndexPaths:indpaths withRowAnimation:UITableViewRowAnimationNone];
        [self.tableView endUpdates];
        [self stopCooldownTimer];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MetadataReceived" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(proccessMeta)
                                                 name:@"MetadataReceived"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"LoginStatus" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadSView:)
                                                 name:@"LoginStatus"
                                               object:nil];
}

- (void)reloadTableView
{
    [self.tableView reloadData];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    if (self.tableView.tableHeaderView == nil) {
        CGSize displaySize = [[UIScreen mainScreen] bounds].size;
        self.settingsH = [[SettingsHeader alloc] initWithFrame:CGRectMake(0, 0, displaySize.width, 250)];
        self.tableView.tableHeaderView = self.settingsH;
        [self setupSettingsHeaderView];
    }
}

- (void)setupSettingsHeaderView {
    [self.settingsH.iconImageView setContentMode:UIViewContentModeScaleAspectFit];
    self.settingsH.iconImageView.layer.masksToBounds = true;
    self.settingsH.iconImageView.layer.cornerRadius = 8;
    [self.settingsH.iconImageView setImage:AssetsManager.sppLogo];
    [self.settingsH.titleLabel setTextColor:[SxUtilities ThemeColor]];
    [self.settingsH.titleLabel setText:@"MHNPro"];
    self.settingsH.parentV = self;
    
    NSString *versionTxt = [NSString stringWithFormat:@"%@-%@",[SxUtilities getAppVersion],[SxUtilities getTweakVersion]];
    [self.settingsH.versionLabel setText:versionTxt];
    [self.settingsH.versionLabel setTextColor:[UIColor labelColor]];
    
    if ([SxUtilities isLegendaryPokemon] == YES) {
        NSString *vytext = [NSString stringWithFormat:@"Expires on %@",[Preferences uservalidity]];
        NSString *actTitle = [NSString stringWithFormat:@"%@",[Preferences useremail]];
        [self.settingsH disableActivateButton:actTitle];
        [self.settingsH disableKeyButton:vytext];
    }
    else
    {
        [self.settingsH enableActivateButton:@"Activate MHNPro"];
        [self.settingsH enableKeyButton:@"Dont have key? Get Your Key!"];
    }
}

- (void)proccessMeta
{
    [self setupSettingsHeaderView];
    [self setupUI];
    [[SpooferxManager sharedManager] reloadMenu];
}


- (IBAction)dismiss:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        [self stopFakelocationTimer];
        [self stopCooldownTimer];
    }];
}


- (void)setupUI
{
    self.navigationController.navigationBar.tintColor = [SxUtilities ThemeColor];
    self.navigationController.navigationBar.backItem.title = @"Back";
    self.title = @"SpooferPro Settings";
    if (self.readableLocation.length < 6) {
        self.readableLocation = [NSString stringWithFormat:@"%f,%f",Preferences.fakeLatitude,Preferences.fakeLongitude];
    }
    
    self.spoofSwitch = [[UISwitch alloc] init];
    self.spoofSwitch.onTintColor = [SxUtilities ThemeColor];
    [self.spoofSwitch addTarget:self action:@selector(spoofSwitchToggled:) forControlEvents:UIControlEventTouchUpInside];
    [self.spoofSwitch setOn:Preferences.enableFakeLocation];
    
    self.joystickSwitch = [[UISwitch alloc] init];
    self.joystickSwitch.onTintColor = [SxUtilities ThemeColor];
    [self.joystickSwitch addTarget:self action:@selector(joystickSwitchToggled:) forControlEvents:UIControlEventTouchUpInside];
    [self.joystickSwitch setOn:Preferences.showJoystick];
    
    self.tapToWalkSwitch = [[UISwitch alloc] init];
    self.tapToWalkSwitch.onTintColor = [SxUtilities ThemeColor];
    [self.tapToWalkSwitch addTarget:self action:@selector(tapToWalkSwitchToggled:) forControlEvents:UIControlEventTouchUpInside];
    [self.tapToWalkSwitch setOn:Preferences.enableTapToWalk];
    
    self.tapToTeleportSwitch = [[UISwitch alloc] init];
    self.tapToTeleportSwitch.onTintColor = [SxUtilities ThemeColor];
    [self.tapToTeleportSwitch addTarget:self action:@selector(tapToTeleportSwitchToggled:) forControlEvents:UIControlEventTouchUpInside];
    [self.tapToTeleportSwitch setOn:Preferences.enableTapToTeleport];
    
    self.fakeLocationString = @"";
    self.fakeLocationTime = @"";
    self.cooldownTime = @"00:00:00";
    self.cooldownColor = [SxUtilities ThemeColor];
    
    [self reloadTableView];
}

#pragma mark - Free Sart

- (void)spoofSwitchToggled:(UISwitch*)sender {
    UISwitch *lnswitch = (UISwitch*)sender;
    BOOL isON = lnswitch.on ? YES : NO;
    [Preferences setEnableFakeLocation:isON];
    if (isON == NO) {
        [[SpooferxManager sharedManager] hideJoystick];
        [LocationSimulator StopWalkToLocationDirect];
        [LocationSimulator ResetFakeLocation];
        
        [Preferences.fakeLocationManager stopUpdatingLocation];
        [Preferences.fakeLocationManager startUpdatingLocation];
    } else {
        //[[SpooferxManager sharedManager] showJoystick];
    }
    
    [[SpooferxManager sharedManager] reloadMenu];
}

- (void)joystickSwitchToggled:(UISwitch*)sender {
    UISwitch *lnswitch = (UISwitch*)sender;
    BOOL isON = lnswitch.on ? YES : NO;
    [Preferences setShowJoystick:isON];
    if (isON == YES) {
        [[SpooferxManager sharedManager] showJoystick];
    } else {
        [[SpooferxManager sharedManager] hideJoystick];
    }
}

- (void)tapToWalkSwitchToggled:(UISwitch*)sender {
    UISwitch *lnswitch = (UISwitch*)sender;
    BOOL isON = lnswitch.on ? YES : NO;
    if(isON == YES) {
        if(Preferences.enableTapToTeleport == YES) {
            [Preferences setEnableTapToTeleport:NO];
            [self.tapToTeleportSwitch setOn:NO animated:YES];
        }
    }
    
    [Preferences setEnableTapToWalk:isON];
}

- (void)tapToTeleportSwitchToggled:(UISwitch*)sender {
    if ([SxUtilities isLegendaryPokemon] == YES) {
        UISwitch *lnswitch = (UISwitch*)sender;
        BOOL isON = lnswitch.on ? YES : NO;
        if(isON == YES) {
            if(Preferences.enableTapToWalk == YES) {
                [Preferences setEnableTapToWalk:NO];
                [self.tapToWalkSwitch setOn:NO animated:YES];
            }
        }
        
        [Preferences setEnableTapToTeleport:isON];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return 2;
            break;
        case 1:
            return 4;
            break;
        case 2:
            return 2;
            break;
        default:
            return 0;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    
    switch (indexPath.section) {
        case 0:
            switch (indexPath.row) {
                case 0:
                {
                    cell.textLabel.text = self.fakeLocationString;
                    cell.detailTextLabel.text = self.fakeLocationTime;
                }
                    break;
                case 1:
                {
                    cell.textLabel.text = @"User Speed";
                    NSString *sp = [NSString stringWithFormat:@"%.1f Km/h",Preferences.userSpeed];
                    cell.detailTextLabel.text = sp;
                }
                    break;
                default:
                    break;
            }
            break;
        case 1:
            switch (indexPath.row) {
                case 0:
                    if ([SxUtilities isLegendaryPokemon] == NO) {
                        self.spoofSwitch.enabled = NO;
                        cell.userInteractionEnabled = NO;
                        cell.textLabel.enabled = NO;
                    }
                    cell.accessoryView = self.spoofSwitch;
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    break;
                case 1:
                    if ([SxUtilities isLegendaryPokemon] == NO) {
                        self.joystickSwitch.enabled = NO;
                        cell.userInteractionEnabled = NO;
                        cell.textLabel.enabled = NO;
                    }
                    cell.accessoryView = self.joystickSwitch;
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    break;
                case 2:
                    if ([SxUtilities isLegendaryPokemon] == NO) {
                        self.tapToWalkSwitch.enabled = NO;
                        cell.userInteractionEnabled = NO;
                        cell.textLabel.enabled = NO;
                    }
                    cell.accessoryView = self.tapToWalkSwitch;
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    break;
                case 3:
                    if ([SxUtilities isLegendaryPokemon] == NO) {
                        self.tapToTeleportSwitch.enabled = NO;
                        cell.userInteractionEnabled = NO;
                        cell.textLabel.enabled = NO;
                    }
                    cell.accessoryView = self.tapToTeleportSwitch;
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    break;
                default:
                    break;
            }
            break;
        case 2:
            switch (indexPath.row) {
                case 0:
                    cell.textLabel.text = @"Official SpooferPro Website";
                    [cell.imageView setImage:[AssetsManager imageForResource:@"website"]];
                    break;
                case 1:
                    cell.textLabel.text = @"SpooferPro Discord";
                    [cell.imageView setImage:[AssetsManager imageForResource:@"discord"]];
                    break;
                default:
                    break;
            }
            break;
        default:
            break;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
            switch (indexPath.row) {
                case 0:
                {
                    [self showUserLocationAction];
                }
                    break;
                case 1:
                {
                    [self showSpeedControl];
                }
                    break;
                default:
                    break;
            }
            break;
        case 1:
            switch (indexPath.row) {
                case 0:
                    break;
                case 1:
                    break;
                case 2:
                    break;
                case 3:
                    break;
                default:
                    break;
            }
            break;
        case 2:
            switch (indexPath.row) {
                case 0:
                {
                    [self showOptionsForWeb];
                }
                    break;
                case 1:
                {
                    [self showOptionsForSpxDiscord];
                }
                    break;
                default:
                    break;
            }
            break;
        default:
            break;
    }
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)showUserLocationAction
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"User Location" message:@"Options for User Location" preferredStyle:UIAlertControllerStyleActionSheet];
    alert.view.tintColor = [SxUtilities ThemeColor];
    
    UIAlertAction *copy_action = [UIAlertAction actionWithTitle:@"Copy Coordinates" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *coords = [NSString stringWithFormat:@"%f,%f",Preferences.fakeLatitude, Preferences.fakeLongitude];
        UIPasteboard *thePasteboard = [UIPasteboard generalPasteboard];
        [thePasteboard setString:coords];
        NSString *msg = @"Coordinates copied to clipboard";
        [NotifMan presentFastNotification:msg];
    }];
    
    UIAlertAction *save_action = [UIAlertAction actionWithTitle:@"Save Location" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //NSString *coords = [NSString stringWithFormat:@"%f,%f",Preferences.fakeLatitude, Preferences.fakeLongitude];
        CLLocationCoordinate2D coords = [SxUtilities CLLocationCoordinate2DMakeNormal:Preferences.fakeLatitude lon:Preferences.fakeLongitude];
        [PlacesManager2 StoreUserLocation:coords];
    }];
    
    UIAlertAction *cancel_action = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alert addAction:copy_action];
    [alert addAction:save_action];
    [alert addAction:cancel_action];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        NSIndexPath *cellPath = [NSIndexPath indexPathForRow:0 inSection:1];
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:cellPath];
        [alert setModalPresentationStyle:UIModalPresentationPopover];
        UIPopoverPresentationController *popper = [alert popoverPresentationController];
        popper.sourceView = cell;
        popper.sourceRect = CGRectMake(cell.frame.size.width/2, cell.frame.size.height/2, 1, 1);
    }
    
    [[UIViewController spxoverlay_topMostController] presentViewController:alert animated:YES completion:nil];
}


- (void)showSpeedControl
{
    NSString *speedmsg =[NSString stringWithFormat:@"Current speed is %.1f Km/h",Preferences.userSpeed];
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@"Custom Speed"
                                message:speedmsg
                                preferredStyle:UIAlertControllerStyleAlert];
    alert.view.tintColor = [SxUtilities ThemeColor];
    UIAlertAction* done_action = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
        NSString *coords = alert.textFields[0].text;
        [self validateSpeed:coords];
    }];
    
    
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction * action)
                             {
    }];
    
    [done_action setValue:[SxUtilities ThemeColor] forKey:@"titleTextColor"];
    
    [alert addAction:done_action];
    [alert addAction:cancel];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Example: 3.6 Km/h";
        textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        textField.tag = 103;
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 11.0)
        {
            [textField setPasteDelegate:self];
        }
        [textField addTarget:self action:@selector(textFieldDidChangeCoord:) forControlEvents:UIControlEventEditingChanged];
    }];
    
    self.speedAlert = alert;
    [[UIViewController spxoverlay_topMostController] presentViewController:alert animated:YES completion:nil];
}

- (void)showOptionsForWeb2
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Official Website" message:@"https://spooferpro.com" preferredStyle:UIAlertControllerStyleActionSheet];
    alert.view.tintColor = [SxUtilities ThemeColor];
    
    UIAlertAction *visit_action = [UIAlertAction actionWithTitle:@"Visit Website" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *web = @"https://spooferpro.com";
        NSURL *url = [NSURL URLWithString:web];
        [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
    }];
    
    UIAlertAction *copy_action = [UIAlertAction actionWithTitle:@"Copy Website URL" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *coords = @"https://spooferpro.com";
        UIPasteboard *thePasteboard = [UIPasteboard generalPasteboard];
        [thePasteboard setString:coords];
    }];
    
    UIAlertAction *cancel_action = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    
    [alert addAction:visit_action];
    [alert addAction:copy_action];
    [alert addAction:cancel_action];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        [alert setModalPresentationStyle:UIModalPresentationPopover];
        UIPopoverPresentationController *popper = [alert popoverPresentationController];
        popper.sourceView = self.settingsH.getKeyButton;
        popper.sourceRect = CGRectMake(self.settingsH.getKeyButton.frame.size.width/2, self.settingsH.getKeyButton.frame.size.height/2, 1, 1);
    }
    
    [[UIViewController spxoverlay_topMostController] presentViewController:alert animated:YES completion:nil];
}

- (void)showOptionsForWeb
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Official Website" message:@"https://spooferpro.com" preferredStyle:UIAlertControllerStyleActionSheet];
    alert.view.tintColor = [SxUtilities ThemeColor];
    
    UIAlertAction *visit_action = [UIAlertAction actionWithTitle:@"Visit Website" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *web = @"https://spooferpro.com";
        NSURL *url = [NSURL URLWithString:web];
        [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
    }];
    
    UIAlertAction *copy_action = [UIAlertAction actionWithTitle:@"Copy Website URL" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *coords = @"https://spooferpro.com";
        UIPasteboard *thePasteboard = [UIPasteboard generalPasteboard];
        [thePasteboard setString:coords];
    }];
    
    UIAlertAction *cancel_action = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    
    [alert addAction:visit_action];
    [alert addAction:copy_action];
    [alert addAction:cancel_action];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        NSIndexPath *cellPath = [NSIndexPath indexPathForRow:0 inSection:5];
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:cellPath];
        [alert setModalPresentationStyle:UIModalPresentationPopover];
        UIPopoverPresentationController *popper = [alert popoverPresentationController];
        popper.sourceView = cell;
        popper.sourceRect = CGRectMake(cell.frame.size.width/2, cell.frame.size.height/2, 1, 1);
    }
    
    [[UIViewController spxoverlay_topMostController] presentViewController:alert animated:YES completion:nil];
}

- (void)showOptionsForSpxDiscord
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"SpooferPro Discord" message:@"https://discord.gg/52HpPM4T2p" preferredStyle:UIAlertControllerStyleActionSheet];
    alert.view.tintColor = [SxUtilities ThemeColor];
    
    UIAlertAction *visit_action = [UIAlertAction actionWithTitle:@"Visit Discord" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *web = @"https://discord.gg/52HpPM4T2p";
        NSURL *url = [NSURL URLWithString:web];
        [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
    }];
    
    UIAlertAction *copy_action = [UIAlertAction actionWithTitle:@"Copy Discord URL" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *coords = @"https://discord.gg/52HpPM4T2p";
        UIPasteboard *thePasteboard = [UIPasteboard generalPasteboard];
        [thePasteboard setString:coords];
    }];
    
    UIAlertAction *cancel_action = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alert addAction:visit_action];
    [alert addAction:copy_action];
    [alert addAction:cancel_action];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        NSIndexPath *cellPath = [NSIndexPath indexPathForRow:1 inSection:5];
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:cellPath];
        [alert setModalPresentationStyle:UIModalPresentationPopover];
        UIPopoverPresentationController *popper = [alert popoverPresentationController];
        popper.sourceView = cell;
        popper.sourceRect = CGRectMake(cell.frame.size.width/2, cell.frame.size.height/2, 1, 1);
    }
    
    [[UIViewController spxoverlay_topMostController] presentViewController:alert animated:YES completion:nil];
}


-(void)textPasteConfigurationSupporting:(id<UITextPasteConfigurationSupporting>)textPasteConfigurationSupporting transformPasteItem:(id<UITextPasteItem>)item API_AVAILABLE(ios(11.0)) {
    [self paste:textPasteConfigurationSupporting];
}

- (void)paste:(id)sender {
    UIPasteboard *gpBoard = [UIPasteboard generalPasteboard];
    if (@available(iOS 10.0, *)) {
        if ([gpBoard hasStrings] == YES)
        {
            UITextField *txtfield = (UITextField*)sender;
            if (txtfield.tag == 102) {
                NSCharacterSet *setToRemove =
                [NSCharacterSet alphanumericCharacterSet];
                NSCharacterSet *setToKeep = [setToRemove invertedSet];
                NSString *newString =
                [[[gpBoard string] componentsSeparatedByCharactersInSet:setToKeep]
                 componentsJoinedByString:@""];
                [self.licenseAlert.textFields[0] insertText:newString];
            }
            else if (txtfield.tag == 103)
            {
                NSCharacterSet *setToRemove =
                [NSCharacterSet characterSetWithCharactersInString:@"0123456789."];
                NSCharacterSet *setToKeep = [setToRemove invertedSet];
                NSString *newString =
                [[[gpBoard string] componentsSeparatedByCharactersInSet:setToKeep]
                 componentsJoinedByString:@""];
                [self.speedAlert.textFields[0] insertText:newString];
            }
        }
    } else {
        // Fallback on earlier versions
    }
}

-(void)textFieldDidChangeCoord:(UITextField*)field {
    
    @try {
        NSString *usp = field.text;
        NSString* trimusp = [usp stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        double uspd = [trimusp doubleValue];
        NSString *msg;
        if (uspd > 0 && uspd < 120) {
            msg = [NSString stringWithFormat:@"New speed is %.1f Km/h",uspd];
        }
        else
        {
            msg = @"Abnormal Speed Detected, Please use proper speed.";
            field.text = @"";
        }
        
        self.speedAlert.message = msg;
        
    } @catch (NSException *exception) {
        
        self.speedAlert.message = [NSString stringWithFormat:@"%@",exception.description];
    }
}

- (void)validateSpeed:(NSString*)str
{
    NSString* trimusp = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    double uspd = [trimusp doubleValue];
    if (uspd > 0 && uspd < 120) {
        [Preferences setUserSpeed:uspd];
        [self.tableView reloadData];
    }
}

- (void)prepareForKeyNew
{
    self.licenseFCAlert = [[FCAlertView alloc] init];
    self.licenseFCAlert.tag = 0001;
    self.licenseFCAlert.delegate = self;
    self.licenseFCAlert.darkTheme = [SxUtilities isDark];
    self.licenseFCAlert.colorScheme = [SxUtilities ThemeColor];
    self.licenseFCAlert.detachButtons = YES;
    
    UITextField *emailField = [[UITextField alloc] init];
    emailField.autocorrectionType = UITextAutocorrectionTypeNo;
    emailField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    emailField.keyboardType = UIKeyboardTypeEmailAddress;
    emailField.backgroundColor = [UIColor systemBackgroundColor];
    
    [self.licenseFCAlert addTextFieldWithCustomTextField:emailField andPlaceholder:@"Enter MHNPro Key" andTextReturnBlock:^(NSString *text) {
        self->licenseKey = text;
    }];
    
    [self.licenseFCAlert showAlertInView:[UIViewController spxoverlay_topMostController]
                          withTitle:@"MHNPro Key"
                       withSubtitle:@"Please enter valid MHNPro key."
                    withCustomImage:[AssetsManager imageForResource:@"rosette"]
                withDoneButtonTitle:@"Activate"
                         andButtons:@[@"Cancel"]];
    
    [self.licenseFCAlert doneActionBlock:^{
        if (self->licenseKey.length > 0) {
            NSCharacterSet *allowedSet = [NSCharacterSet alphanumericCharacterSet];
            NSCharacterSet *forbiddenSet = [allowedSet invertedSet];
            NSRange r = [self->licenseKey rangeOfCharacterFromSet:forbiddenSet];
            if (r.location != NSNotFound) {
                [self loadSView:nil];
            }
            else
            {
                self.progressFCAlert = [[FCAlertView alloc] init];
                self.progressFCAlert.delegate = self;
                self.progressFCAlert.detachButtons = YES;
                self.progressFCAlert.tag = 0003;
                self.progressFCAlert.darkTheme = [SxUtilities isDark];
                [self.progressFCAlert makeAlertTypeProgress];
                self.progressFCAlert.hideAllButtons = YES;
                [self.progressFCAlert showAlertInView:self
                                       withTitle:@"MHNPro Key"
                                    withSubtitle:@"Checking MHNPro Key.."
                                 withCustomImage:nil
                             withDoneButtonTitle:nil
                                      andButtons:nil];
                [RpcManager copyMetaKeyToKeychain:self->licenseKey];
            }
        }
        else
        {
            [self loadSView:nil];
        }
    }];
}

- (void)showResetKeyNew
{
    self.warningFCAlert = [[FCAlertView alloc] init];
    self.warningFCAlert.delegate = self;
    self.warningFCAlert.darkTheme = [SxUtilities isDark];
    self.warningFCAlert.colorScheme = [SxUtilities ThemeColor];
    self.warningFCAlert.detachButtons = YES;
    [self.warningFCAlert makeAlertTypeCaution];
    [self.warningFCAlert showAlertInView:[UIViewController spxoverlay_topMostController]
                          withTitle:@"Reset MHNPro"
                       withSubtitle:@"This will reset the Activation and Disable Pro Features. Are you sure?"
                    withCustomImage:nil
                withDoneButtonTitle:@"Reset!"
                         andButtons:@[@"Cancel"]];
    
    [self.warningFCAlert doneActionBlock:^{
        [RpcManager resetMetadatG];
    }];
}

- (void)loadSView:(NSNotification*)notif {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *login = notif.userInfo[@"login"];
        
        if (self.progressFCAlert.tag == 0003) {
            [self.progressFCAlert dismissAlertView];
            self.progressFCAlert.tag = 101;
        }
        
        if ([login isEqualToString:@"success"] == YES)
        {
            self.successFCAlert = [[FCAlertView alloc] init];
            self.successFCAlert.delegate = self;
            [self.successFCAlert makeAlertTypeSuccess];
            self.successFCAlert.detachButtons = YES;
            self.successFCAlert.darkTheme = [SxUtilities isDark];
            [self.successFCAlert showAlertInView:[UIViewController spxoverlay_topMostController]
                                        withTitle:@"Activation Successful"
                                     withSubtitle:@"Thank you for supporting SpooferPro, enjoy Pro features!"
                                  withCustomImage:nil
                              withDoneButtonTitle:nil
                                       andButtons:nil];
            [self proccessMeta];
        }
        else
        {
            self.failFCAlert = [[FCAlertView alloc] init];
            self.failFCAlert.delegate = self;
            self.failFCAlert.detachButtons = YES;
            [self.failFCAlert makeAlertTypeWarning];
            self.failFCAlert.darkTheme = [SxUtilities isDark];
            [self.failFCAlert showAlertInView:[UIViewController spxoverlay_topMostController]
                                     withTitle:@"Activation Failed"
                                  withSubtitle:@"Incorrect Activation Key or Server not reachable. Please try again."
                               withCustomImage:nil
                           withDoneButtonTitle:nil
                                    andButtons:nil];
            [self proccessMeta];
        }
    });
}


@end

//
//  PopularPlacesCell.m
//  igoprotest
//
//  Created by MADHATTER on 9/12/22.
//

#import "PopularPlacesCell.h"
#import "ZTGCDTimerManager.h"

@interface PopularPlacesCell ()

@property (nonatomic, strong) NSDictionary *currentPlace;
@property (nonatomic, strong) NSString *placeKey;
@property (nonatomic, strong) NSDateFormatter *formatter;
@property (nonatomic, strong) NSTimeZone *currentzone;

@end

@implementation PopularPlacesCell

- (void)prepareForReuse
{
    [super prepareForReuse];
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
}

- (void)startFakelocationTimer
{
    NSString *timerKey = [NSString stringWithFormat:@"com.pmgo.spx.%@",self.placeKey];
    [[ZTGCDTimerManager sharedInstance] scheduleGCDTimerWithName:timerKey interval:1 queue:dispatch_get_main_queue() repeats:YES option:CancelPreviousTimerAction action:^{
        [self SetCurrentDateTime];
    }];
}

- (void)stopFakelocationTimer
{
    NSString *timerKey = [NSString stringWithFormat:@"com.pmgo.spx.%@",self.placeKey];
    [[ZTGCDTimerManager sharedInstance] cancelTimerWithName:timerKey];
}

- (void)SetCurrentDateTime
{
    //[dateFormatter setDateFormat:@"EEE, MMM d, yyyy - h:mm:ss a"];
    [self.formatter setDateFormat:@"MMM d, h:mm:ss a"];
    [self.formatter setTimeZone:self.currentzone];
    self.extraLabel.textColor = [SxUtilities ThemeColor];
    self.extraLabel.text = [self.formatter stringFromDate:[NSDate date]];
//    NSIndexPath *indexPath = [(UITableView *)self.superview indexPathForCell: self];
//    if (indexPath) {
//        [(UITableView *)self.superview reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
//    }
}

- (void)setupPlace:(NSDictionary*)place key:(NSString*)key
{
    self.currentPlace = place;
    [self.titleLabel setText:@""];
    [self.clockLabel setText:@""];
    [self.extraLabel setText:@""];
    self.placeKey = key;
    NSNumber *scrno = place[@"srno"];
    NSString *title = place[@"nickname"];
    title = [NSString stringWithFormat:@"#%@ - %@",scrno,title];
    NSNumber *lat = place[@"latitude"];
    NSNumber *lng = place[@"longitude"];
    self.formatter = [[NSDateFormatter alloc] init];
    self.currentzone = [NSTimeZone timeZoneWithName:self.currentPlace[@"timezone"]];
    CLLocationCoordinate2D destination = [SxUtilities CLLocationCoordinate2DMakeNormal:lat.doubleValue lon:lng.doubleValue];
    NSString *cdstring = [SxUtilities cooldownStringForDestination:destination];
    NSString *extra = [NSString stringWithFormat:@"%@, %@",place[@"country"],cdstring];
    NSString *imagename = [NSString stringWithFormat:@"%@.png",place[@"iso2code"]];
    [self.titleLabel setText:title];
    [self SetCurrentDateTime];
    self.countryFlag.image = [UIImage imageNamed:imagename inBundle:[AssetsManager countriesBundle]];
    [self.clockLabel setText:extra];
    self.clockLabel.textColor = [UIColor secondaryLabelColor];
    [self startFakelocationTimer];
}


- (void)dealloc
{
    [self stopFakelocationTimer];
}

@end

//
//  IGP_ApplicationSwizzler.m
//  SDWebImage
//
//  Created by MADHATTER on 4/20/22.
//  Copyright © 2022 Dailymotion. All rights reserved.
//

#import "IGP_ApplicationSwizzler.h"
#import <IGMethodHook.h>
#import "SpooferxManager.h"

@implementation IGP_ApplicationSwizzler

+(void)load
{
    [[NSNotificationCenter defaultCenter] addObserver:[IGP_ApplicationSwizzler sharedSwizzler]
                                             selector:@selector(appDidFinishLaunching:)
                                                 name:UIApplicationDidFinishLaunchingNotification object:nil];
}

+ (IGP_ApplicationSwizzler *)sharedSwizzler
{
    static IGP_ApplicationSwizzler * sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init
{
    if (self = [super init])
    {
    }
    return self;
}

- (void)appDidFinishLaunching:(NSNotification *)notification {
//    NSURL *url = notification.userInfo[UIApplicationLaunchOptionsURLKey];
//    DBG(@"URL -> %@",url);
    [IGP_ApplicationSwizzler hookAppDelegateUrlHandlers];
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
    [[IQKeyboardManager sharedManager] setEnable:NO];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [[PreferencesManager sharedManager] loadDefaults];
    });
//    if (url) {
//        [ApplicationSwizzler hookAppDelegateUrlHandlers];
//    }
}

static void uncaughtExceptionHandler(NSException *exception)
{
    DBG(@"App Crash:\n%@", exception);
    DBG(@"Stack Trace:\n%@", [exception callStackSymbols]);
}

// MARK: - Hook URL Handling

static BOOL (*originalAppDelegateURLHandlerWithOptions)(id, SEL, UIApplication *, NSURL *, NSDictionary *) = nil;

+ (BOOL)hookAppDelegateUrlHandlers {
    static dispatch_once_t onceToken;
    static BOOL hooked = FALSE;
    dispatch_once(&onceToken, ^{
        hooked = [self hookAppDelegateUrlHandlerWithOptions];
#ifdef DEBUG
        //DBG(@"hooked: %d", hooked);
#endif
        hooked = hooked || [self hookAppDelegateUrlHandlerWithSourceApplication];
#ifdef DEBUG
        //DBG(@"hooked: %d", hooked);
#endif
        hooked = hooked || [self hookAppDelegateUrlHandler];
#ifdef DEBUG
        //DBG(@"hooked: %d", hooked);
#endif
    });

    return hooked;
}

+ (BOOL)hookAppDelegateUrlHandlerWithOptions {
    id delegate = [UIApplication sharedApplication].delegate;
    Class delegateClass = [delegate class];
    
    SEL sel = @selector(application:openURL:options:);
    SEL localSelector = @selector(hook_application:openURL:options:);
    Method method = class_getInstanceMethod(self.class, localSelector);
    const char *typeEncoding = method_getTypeEncoding(method);
    IMP imp = class_getMethodImplementation(self.class, localSelector);
    
    if (!imp) {
#ifdef DEBUG
        DBG(@"Couldn't get hook implementation");
#endif
        return FALSE;
    }
    
    return [IGMethodHook hookClass:delegateClass selector:sel with:imp
       storeOriginalImplementation:(IMP _Nonnull * _Nullable)&originalAppDelegateURLHandlerWithOptions
                      typeEncoding:typeEncoding];
}

- (BOOL)hook_application:(UIApplication *)app
            openURL:(NSURL *)url
                 options:(NSDictionary<UIApplicationOpenURLOptionsKey, id> *)options {
    DBG(@"URL -> %@",url);
//    DBG(@"Options -> %@",options.description);
//    if ([self handleHookedUrl:url]) {
//        DBG(@"IGAUTH: %@ handled", NSStringFromSelector(_cmd));
//        return TRUE;
//    }
//
    if ([url.pathExtension isEqualToString:@"json"] == YES) {
        [[SpooferxManager sharedManager] processConfigFile:url];
    }
    
    if ([url.absoluteString hasPrefix:@"pokemongo"] == YES || [url.absoluteString hasPrefix:@"spooferpro"] == YES)
    {
        NSString *query = [url query];
        NSArray *componenets = [query componentsSeparatedByString:@"="];
        NSString *scheme = [componenets objectAtIndex:0];
        if ([scheme isEqualToString:@"spprotele"])
        {
            NSString *location = [componenets objectAtIndex:1];
            if (Preferences.mapLoaded == YES) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [[SpooferxManager sharedManager] jumpFromURL:location];
                });
            }
            else
            {
                [[NSUserDefaults standardUserDefaults] setObject:location forKey:@"spprotele"];
            }
        }
    }
    
    if (originalAppDelegateURLHandlerWithOptions) {
        DBG(@"Passing %@ to original handler", NSStringFromSelector(_cmd));
        return originalAppDelegateURLHandlerWithOptions(self, _cmd, app, url, options);
    } else {
        DBG(@"%@ missing original handler!", NSStringFromSelector(_cmd));
    }
    
    return TRUE;
}

static BOOL (*originalAppDelegateURLHandlerWithSourceApplication)(id, SEL, UIApplication *, NSURL *, NSString *, id) = nil;

+ (BOOL)hookAppDelegateUrlHandlerWithSourceApplication {
    id delegate = [UIApplication sharedApplication].delegate;
    Class delegateClass = [delegate class];
    
    SEL sel = @selector(application:openURL:sourceApplication:annotation:);
    SEL localSelector = @selector(hook_application:openURL:sourceApplication:annotation:);
    Method method = class_getInstanceMethod(self.class, localSelector);
    const char *typeEncoding = method_getTypeEncoding(method);
    IMP imp = class_getMethodImplementation(self.class, localSelector);
    
    if (!imp) {
        DBG(@"IGAuth: Couldn't get hook implementation");
        return FALSE;
    }
    
    return [IGMethodHook hookClass:delegateClass selector:sel with:imp
       storeOriginalImplementation:(IMP _Nonnull * _Nullable)&originalAppDelegateURLHandlerWithSourceApplication
                      typeEncoding:typeEncoding];
}

- (BOOL)hook_application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
//    if ([self handleHookedUrl:url]) {
//        DBG(@"IGAUTH: %@ handled", NSStringFromSelector(_cmd));
//        return TRUE;
//    }
    
    if ([url.pathExtension isEqualToString:@"json"] == YES) {
        [[SpooferxManager sharedManager] processConfigFile:url];
    }
    
    if ([url.absoluteString hasPrefix:@"pokemongo"] == YES || [url.absoluteString hasPrefix:@"spooferpro"] == YES)
    {
        NSString *query = [url query];
        NSArray *componenets = [query componentsSeparatedByString:@"="];
        NSString *scheme = [componenets objectAtIndex:0];
        if ([scheme isEqualToString:@"spprotele"])
        {
            NSString *location = [componenets objectAtIndex:1];
            if (Preferences.mapLoaded == YES) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [[SpooferxManager sharedManager] jumpFromURL:location];
                });
            }
            else
            {
                [[NSUserDefaults standardUserDefaults] setObject:location forKey:@"spprotele"];
            }
        }
    }
    
    if (originalAppDelegateURLHandlerWithSourceApplication) {
        DBG(@"Passing %@ to original handler", NSStringFromSelector(_cmd));
        return originalAppDelegateURLHandlerWithSourceApplication(self, _cmd, application,
                                                                  url, sourceApplication, annotation);
    } else {
        DBG(@"%@ missing original handler!", NSStringFromSelector(_cmd));
    }
    
    return TRUE;
}

static BOOL (*originalAppDelegateURLHandler)(id, SEL, UIApplication *, NSURL *) = nil;

+ (BOOL)hookAppDelegateUrlHandler {
    id delegate = [UIApplication sharedApplication].delegate;
    Class delegateClass = [delegate class];
    
    SEL sel = @selector(application:handleOpenURL:);
    SEL localSelector = @selector(hook_application:handleOpenURL:);
    Method method = class_getInstanceMethod(self.class, localSelector);
    const char *typeEncoding = method_getTypeEncoding(method);
    IMP imp = class_getMethodImplementation(self.class, localSelector);
    
    if (!imp) {
        DBG(@"Couldn't get hook implementation");
        return FALSE;
    }
    
    return [IGMethodHook hookClass:delegateClass selector:sel with:imp
       storeOriginalImplementation:(IMP _Nonnull * _Nullable)&originalAppDelegateURLHandler
                      typeEncoding:typeEncoding];
}

- (BOOL)hook_application:(UIApplication *)application
                 handleOpenURL:(NSURL *)url {
//    if ([self handleHookedUrl:url]) {
//        DBG(@"IGAUTH: %@ handled", NSStringFromSelector(_cmd));
//        return TRUE;
//    }
    if ([url.pathExtension isEqualToString:@"json"] == YES) {
        [[SpooferxManager sharedManager] processConfigFile:url];
    }
    
    if ([url.absoluteString hasPrefix:@"pokemongo"] == YES || [url.absoluteString hasPrefix:@"spooferpro"] == YES)
    {
        NSString *query = [url query];
        NSArray *componenets = [query componentsSeparatedByString:@"="];
        NSString *scheme = [componenets objectAtIndex:0];
        if ([scheme isEqualToString:@"spprotele"])
        {
            NSString *location = [componenets objectAtIndex:1];
            if (Preferences.mapLoaded == YES) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [[SpooferxManager sharedManager] jumpFromURL:location];
                });
            }
            else
            {
                [[NSUserDefaults standardUserDefaults] setObject:location forKey:@"spprotele"];
            }
        }
    }
    
    if (originalAppDelegateURLHandler) {
        DBGB(@"IGAUTH: Passing %@ to original handler", NSStringFromSelector(_cmd));
        return originalAppDelegateURLHandler(self, _cmd, application, url);
    } else {
        DBGB(@"IGAUTH: %@ missing original handler!", NSStringFromSelector(_cmd));
    }
    
    return TRUE;
}


@end

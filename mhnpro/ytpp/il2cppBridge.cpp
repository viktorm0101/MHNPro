//
//  il2cppBridge.cpp
//  spooferxsl
//
//  Created by spooferx on 4/8/20.
//

#include "codegen/il2cpp-codegen.h"

struct  IntPtr_t
{
public:
    // System.Void* System.IntPtr::m_value
    void* ___m_value_0;
    
public:
    inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
    inline void* get_m_value_0() const { return ___m_value_0; }
    inline void** get_address_of_m_value_0() { return &___m_value_0; }
    inline void set_m_value_0(void* value)
    {
        ___m_value_0 = value;
    }
};

struct il2Int32  : public Il2CppArray //struct Int32U5BU5D_t3030399641  : public Il2CppArray
{
public:
    ALIGN_FIELD (8) int32_t m_Items[1];
};

struct il2ReturnValue //struct RetVal
{
public:
    bool isModified;
    IntPtr_t pointer;
};

struct il2ByteArray  : public Il2CppArray //struct ByteU5BU5D_t3397334013  : public Il2CppArray
{
public:
    ALIGN_FIELD(8) uint8_t m_Items[1];
    int length;
public:
    inline uint8_t GetAt(il2cpp_array_size_t index) const
    {
        return m_Items[index];
    }
    inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
    {
        return m_Items + index;
    }
    inline void SetAt(il2cpp_array_size_t index, int32_t value)
    {
        m_Items[index] = value;
    }
    inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
    {
        return m_Items[index];
    }
    inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
    {
        return m_Items + index;
    }
    inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
    {
        m_Items[index] = value;
    }
};

struct  il2ByteString  : public Il2CppObject //struct  ByteString  : public Il2CppObject
{
    ALIGN_FIELD(8) il2ByteArray* ___bytes_1;
    int length;
};

struct il2ByteStringArray : public Il2CppArray //struct ByteStringArray : public Il2CppArray
{
    ALIGN_FIELD (8) il2ByteString* m_Items[1];
};

struct  il2RepeatedField  : public Il2CppObject //struct  RepeatedField  : public Il2CppObject
{
    il2ByteStringArray* ___array_1;
    int32_t ___count_2;
};

struct Il2CppClass_1 {
    Il2CppImage* image;
    void* gc_desc;
    const char* name;
    const char* namespaze;
    Il2CppType byval_arg;
    Il2CppType this_arg;
    Il2CppClass* element_class;
    Il2CppClass* castClass;
    Il2CppClass* declaringType;
    Il2CppClass* parent;
    void *generic_class;
    void* typeDefinition;
    void* interopData;
    Il2CppClass* klass;
    void* fields;
    void* events;
    void* properties;
    //void* methods;
    const MethodInfo** methods;
    Il2CppClass** nestedTypes;
    Il2CppClass** implementedInterfaces;
    void* interfaceOffsets;
};

struct Il2CppClass_2 {
    void* rgctx_data;
    Il2CppClass** typeHierarchy;
    uint32_t initializationExceptionGCHandle;
    uint32_t cctor_started;
    uint32_t cctor_finished;
    uint64_t cctor_thread;
    int32_t genericContainerIndex;
    uint32_t instance_size;
    uint32_t actualSize;
    uint32_t element_size;
    int32_t native_size;
    uint32_t static_fields_size;
    uint32_t thread_static_fields_size;
    int32_t thread_static_fields_offset;
    uint32_t flags;
    uint32_t token;
    uint16_t method_count;
    uint16_t property_count;
    uint16_t field_count;
    uint16_t event_count;
    uint16_t nested_type_count;
    uint16_t vtable_count;
    uint16_t interfaces_count;
    uint16_t interface_offsets_count;
    uint8_t typeHierarchyDepth;
    uint8_t genericRecursionDepth;
    uint8_t rank;
    uint8_t minimumAlignment;
    uint8_t naturalAligment;
    uint8_t packingSize;
    uint8_t bitflags1;
    uint8_t bitflags2;
};

struct Il2CppClass_IG
{
    Il2CppClass_1 _1;
    void* static_fields;
    Il2CppClass_2 _2;
    VirtualInvokeData vtable[255];
};

struct Il2CppObject_IG
{
    Il2CppClass_IG *klass;
    void *monitor;
};

struct System_Byte_array {
    Il2CppObject obj;
    Il2CppArrayBounds *bounds;
    uintptr_t max_length;
    uint8_t m_Items[65535];
};

struct CodedInputStream_O {
    Il2CppClass_IG *klass;
    void *monitor;
    System_Byte_array* buffer;
    int32_t bufferSize;
    int32_t bufferSizeAfterLimit;
    int32_t bufferPos;
    Il2CppObject_IG* input;
    uint32_t lastTag;
    uint32_t nextTag;
    bool hasNextTag;
    int32_t totalBytesRetired;
    int32_t currentLimit;
    int32_t recursionDepth;
    int32_t recursionLimit;
    int32_t sizeLimit;
};

struct Il2ActionResponse_CD : public Il2CppObject {
public:
    int32_t _rpcId_0;
    int32_t _rpcStatus_1;
    CodedInputStream_O *_CodedInputStream;

public:
    inline int32_t get_rpcId_0() const {return _rpcId_0;}
    inline int32_t* get_address_of_rpcId_0() { return &_rpcId_0; }
    inline void set_rpcId_0(int32_t value)
    {
        _rpcId_0 = value;
    }
    inline int32_t get_rpcStatus_1() const { return _rpcStatus_1; }
    inline int32_t* get_address_of_rpcStatus_1() { return &_rpcStatus_1; }
    inline void set_rpcStatus_1(int32_t value)
    {
        _rpcStatus_1 = value;
    }
    inline CodedInputStream_O * get_CodedInputStream() {return _CodedInputStream;}
    inline CodedInputStream_O ** get_address_of_CodedInputStream(){return &_CodedInputStream;}
    inline void set_CodedInputStream(CodedInputStream_O *value)
    {
        _CodedInputStream = value;
    }
};

struct il2ByteArray_k  : public Il2CppArray
{
public:
    ALIGN_FIELD(8) uint8_t m_Items[1];
    int length;

    inline uint8_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
    inline uint8_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
    inline void SetAt(il2cpp_array_size_t index, int32_t value) { m_Items[index] = value; }
    inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const { return m_Items[index]; }
    inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index) { return m_Items + index; }
    inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value) { m_Items[index] = value; }
};

struct  il2ByteString_k  : public Il2CppObject
{
public:
    ALIGN_FIELD(8) il2ByteArray_k* m_bytes;
    int length;
};

struct il2ByteStringArray_k : public Il2CppArray
{
public:
    ALIGN_FIELD (8) il2ByteString_k* m_Items[1];
};

struct  il2RepeatedField_k  : public Il2CppObject
{
public:
    il2ByteStringArray_k* m_Array;
    int32_t Count;
};

struct il2ActionResponse_k: public Il2CppObject
{
public:
    int32_t _RpcId;
    int32_t _RpcStatus;
    il2ByteString_k *_CodedInputStream;

public:
    inline int32_t get_rpcId_0() const { return _RpcId; }
    inline int32_t* get_address_of_rpcId_0() { return &_RpcId; }
    inline void set_rpcId_0(int32_t value) { _RpcId = value; }

    inline int32_t get_rpcStatus() const { return _RpcStatus; }
    inline int32_t* get_address_of_rpcStatus() { return &_RpcStatus; }
    inline void set_rpcStatus(int32_t value) { _RpcStatus = value; }

    inline il2ByteString_k * get_CodedInputStream() {return _CodedInputStream;}
    inline il2ByteString_k ** get_address_of_CodedInputStream(){return &_CodedInputStream;}
    inline void set_CodedInputStream(il2ByteString_k *value) { _CodedInputStream = value; }
};

struct LatLng
{
    double Latitude; // 0x0
    double Longitude; // 0x8
};

struct Vector2 {
    float x;
    float y;
};

struct System_String_o {
    Il2CppClass *klass;
    void *monitor;
    int32_t length;
    uint16_t start_char;
};

struct UnityEngine_Resolution_Fields {
    int32_t m_Width;
    int32_t m_Height;
    int32_t m_RefreshRate;
};

struct UnityEngine_Resolution_o {
    UnityEngine_Resolution_Fields fields;
};

struct LookAtCamera_Fields {
    struct LatLng Target;
    float Heading;
    float Tilt;
    float Range;
};

struct LookAtCamera_o {
    LookAtCamera_Fields fields;
};

//
//  IGP_Loader.m
//  spooferxsl
//
//  Created by spooferx on 9/17/20
//  
//

#import "IGP_Loader.h"
#import <mach-o/dyld.h>
#import <dlfcn.h>
#import "IGP_FunctionPointers.h"
#import "IGP_HooksManager.h"
#import "fishhook.h"

#include "RandomStrings_public.h"
#include <stdexcept>
#include <string>

#include <assert.h>
#include <stdbool.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/sysctl.h>


#define REQUEST_HIDE_FILES "com.spooferpro.hide_files"

@implementation IGP_Loader

extern "C" CFNotificationCenterRef CFNotificationCenterGetDistributedCenter(void);

static bool AmIBeingDebugged(void)
    // Returns true if the current process is being debugged (either
    // running under the debugger or has a debugger attached post facto).
{
    int                 junk;
    int                 mib[4];
    struct kinfo_proc   info;
    size_t              size;

    // Initialize the flags so that, if sysctl fails for some bizarre
    // reason, we get a predictable result.

    info.kp_proc.p_flag = 0;

    // Initialize mib, which tells sysctl the info we want, in this case
    // we're looking for information about a specific process ID.

    mib[0] = CTL_KERN;
    mib[1] = KERN_PROC;
    mib[2] = KERN_PROC_PID;
    mib[3] = getpid();

    // Call sysctl.

    size = sizeof(info);
    junk = sysctl(mib, sizeof(mib) / sizeof(*mib), &info, &size, NULL, 0);
    assert(junk == 0);

    // We're being debugged if the P_TRACED flag is set.

    return ( (info.kp_proc.p_flag & P_TRACED) != 0 );
}

static void BypassJailbreakChecks() {
    pid_t pid = getpid();
    DBG(@"Firing bypass for pid %d", pid);
    NSDictionary *info = @{
        @"Pid" : @(pid),
        @"Paths": @[
            @"/.bootstrapped_electra",
            @"/Applications/Anemone.app",
            @"/Applications/Cydia.app",
            @"/Applications/SafeMode.app",
            @"/Applications/Sileo.app",
            @"/Applications/Zebra.app",
            @"/Applications/vnodebypass.app/Info.plist",
            @"/Applications/vnodebypass.app/vnodebypass",
            @"/Library/Frameworks/CydiaSubstrate.framework",
            @"/Library/MobileSubstrate/DynamicLibraries/FlyJB.dylb",
            @"/Library/MobileSubstrate/MobileSubstrate.dylib",
            @"/Library/PreferenceBundles/LaunchInSafeMode.bundle",
            @"/Library/PreferenceLoader/Preferences/LaunchInSafeMode.plist",
            @"/Library/Themes",
            @"/Library/dpkg/info/com.inoahdev.launchinsafemode.list",
            @"/Library/dpkg/info/com.inoahdev.launchinsafemode.md5sums",
            @"/bin/bash",
            @"/bin/bunzip2",
            @"/bin/bzip2",
            @"/bin/cat",
            @"/bin/chgrp",
            @"/bin/chmod",
            @"/bin/chown",
            @"/bin/cp",
            @"/bin/grep",
            @"/bin/gzip",
            @"/bin/kill",
            @"/bin/ln",
            @"/bin/ls",
            @"/bin/mkdir",
            @"/bin/mv",
            @"/bin/sed",
            @"/bin/sh",
            @"/bin/su",
            @"/bin/tar",
            @"/binpack",
            @"/bootstrap",
            @"/chimera",
            @"/electra",
            @"/etc/apt",
            @"/etc/profile",
            @"/jb",
            @"/private/var/binpack",
            @"/private/var/checkra1n.dmg",
            @"/private/var/lib/apt",
            @"/tmp/vnodeMem.txt",
            @"/usr/bin/diff",
            @"/usr/bin/hostinfo",
            @"/usr/bin/killall",
            @"/usr/bin/passwd",
            @"/usr/bin/recache",
            @"/usr/bin/tar",
            @"/usr/bin/vnodebypass",
            @"/usr/bin/which",
            @"/usr/bin/xargs",
            @"/usr/lib/SBInject",
            @"/usr/lib/SBInject.dylib",
            @"/usr/lib/TweakInject",
            @"/usr/lib/TweakInject.dylib",
            @"/usr/lib/TweakInjectMapsCheck.dylib",
            @"/usr/lib/libjailbreak.dylib",
            @"/usr/lib/libsubstitute.0.dylib",
            @"/usr/lib/libsubstitute.dylib",
            @"/usr/lib/libsubstrate.dylib",
            @"/usr/lib/substitute-loader.dylib",
            @"/usr/libexec/sftp-server",
            @"/usr/sbin/sshd",
            @"/usr/share/terminfo",
            @"/usr/share/vnodebypass/hidePathList.plist",
            @"/var/MobileSoftwareUpdate/mnt1/fakevar",
            @"/var/mobile/Library/.sbinjectSafeMode",
            @"/var/mobile/Library/Preferences/jp.akusio.kernbypass-unofficial.plist",
            @"/var/mobile/Library/Preferences/jp.akusio.kernbypass.plist",
            @"/var/mobile/Library/Preferences/jp.akusio.kernbypass2.plist",
            @"/var/mobile/fakevar",
            @"/var/jb",
            @"/private/preboot",
            @"/var/jb/usr/lib/TweakInject"
        ]
    };
    
    CFNotificationCenterPostNotification(CFNotificationCenterGetDistributedCenter(), CFSTR(REQUEST_HIDE_FILES), NULL, (__bridge CFDictionaryRef)info, YES);
    DBG(@"Waiting for bypass to finish from pid: %d", pid);
    kill(pid, SIGSTOP);
}

void setupJBFlag ();
void setupJBFlag () {
    void *rplsym = dlsym(RTLD_DEFAULT, "image_added");
    Dl_info lib_info;
    int rc = dladdr(rplsym, &lib_info);
    if (rc != 0) {
        NSString *path = [NSString stringWithUTF8String:lib_info.dli_fname];
        DBG(@"path -> %@",path);
        if([path hasPrefix:@"/private/preboot"]) {
            Preferences.IsRootless = true;
            Preferences.IsJailbroken = false;
            DBG(@"Found Rootless");
        } else if([path hasPrefix:@"/var/jb/Library/MobileSubstrate/DynamicLibraries/"]){
            Preferences.IsRootless = true;
            Preferences.IsJailbroken = false;
            DBG(@"Found Rootless1");
        }else if ([path hasPrefix:@"/private/var/jb/Library/MobileSubstrate/DynamicLibraries/"]) {
            Preferences.IsRootless = true;
            Preferences.IsJailbroken = false;
            DBG(@"Found Rootless2");
        } else if ([path hasPrefix:@"/var/jb/usr/lib/TweakInject/mhnpro.dylib"]){
            Preferences.IsRootless = true;
            Preferences.IsJailbroken = false;
            DBG(@"Found Rootless3");
        }else if ([path hasPrefix:@"/Library/MobileSubstrate/DynamicLibraries/"]) {
            Preferences.IsJailbroken = true;
            Preferences.IsRootless = false;
            DBG(@"Found Rootful");
        } else if ([path hasPrefix:@"/usr/lib/TweakInject/mhnpro.dylib"]){
            Preferences.IsJailbroken = true;
            Preferences.IsRootless = false;
            DBG(@"Found Taurine");
            NSError *err;
            NSString *loadp = [DEST_PATH stringByAppendingString:@"loadpath.txt"];
            [path writeToFile:loadp atomically:YES encoding:NSUTF8StringEncoding error:&err];
            DBG(@"Write Err -> %@",err);
        } else {
            NSError *err;
            NSString *loadp = [DEST_PATH stringByAppendingString:@"loadpath.txt"];
            [path writeToFile:loadp atomically:YES encoding:NSUTF8StringEncoding error:&err];
            DBG(@"Write Err -> %@",err);
            Preferences.IsJailbroken = false;
            Preferences.IsRootless = false;
            DBG(@"Found Sideload");
        }
    } else {
        Preferences.IsJailbroken = false;
        Preferences.IsRootless = false;
        DBG(@"Found Sideload");
    }
}

static void loadGame() {
    NSString *path = [[[NSBundle mainBundle] bundlePath] stringByAppendingString:@"/Frameworks/NianticLabsPlugin.framework/7CLUydP3nIVBHBL.dylib"];
    void *libHandle = dlopen([path UTF8String], RTLD_NOW);
    
    if (libHandle != NULL) {
        void *run = dlsym(libHandle, BYPASS_INITIALIZER);
        if(run != nullptr) {
            void (*runner)(void) = (void (*)(void))run;
            runner();
        }
    } else {
        DBG(@"Could Not Load Bypass");
    }
}

extern "C" void image_added(const struct mach_header *mh,intptr_t slide);
void image_added(const struct mach_header *mh,intptr_t slide) {
    Dl_info image_info;
    int result = dladdr(mh, &image_info);
    if (result == 0) {
        DBG(@"Could not print info for mach_header:%p\n\n",mh);
        return;
    }
    
    const char *image_name = image_info.dli_fname;
    
    NSString *imgn = [NSString stringWithCString:image_name encoding:NSUTF8StringEncoding];
    if ([[imgn lastPathComponent] isEqualToString:@"UnityFramework"] == YES) {
        DBG(@"UnityFramework Loaded");
        
        if (Preferences.IsJailbroken == true){
            BypassJailbreakChecks();
        }
        
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            int imgcount = _dyld_image_count();
            for (int i = 0; i < imgcount; i++) {
                const char *imagename = _dyld_get_image_name(i);
                NSString *imgn1 = [NSString stringWithCString:imagename encoding:NSUTF8StringEncoding];
                if ([[imgn1 lastPathComponent] isEqualToString:@"UnityFramework"] == YES) {
                    Preferences.unityIndex = i;
                    break;
                }
            }
//
//            [IGPHooks enableHooks];
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                [IL2Functions SetUp];
//            });
        });
    }
    
    if ([[imgn lastPathComponent] isEqualToString:@"NianticLabsPlugin"] == YES) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            if(Preferences.IsJailbroken == false && Preferences.IsRootless == false) {
                DBG(@"NianticLabsPlugin Loaded");
                loadGame();
            }
//            else if (Preferences.IsJailbroken == true){
//                BypassJailbreakChecks();
//            }
        });
    }
}

+ (void)load {
    setupJBFlag();
    DBG(@"Registered for Image Notifications");
    _dyld_register_func_for_add_image(&image_added);
}


@end

//
//  DataManager.m
//  obfuscation
//
//  Created by c0pyn1nja on 6/11/20
//  Copyright © 2020 iphonecake.com. All rights reserved.
//

#import "DataManager.h"
#import <CommonCrypto/CommonCryptor.h>

char candies[] = {0x4f,0x54,0x5a,0x44,0x51,0x6a,0x4d,0x7a,0x4d,0x45,0x55,0x34,0x52,0x54,0x6b,0x77,0x4f,0x54,0x56,0x43,0x51,0x6a,0x49,0x35,0x4d,0x45,0x59,0x31,0x56,0x7a,0x45,0x79,0x4e,0x54,0x55,0x32,0x4d,0x7a,0x59,0x7a,0x4d,0x7a,0x6b,0x3d};//key

char stardust[] = {0x4e,0x45,0x55,0x30,0x51,0x54,0x67,0x33,0x4e,0x7a,0x42,0x45,0x4e,0x54,0x45,0x33,0x4d,0x45,0x56,0x46,0x51,0x51,0x3d,0x3d};//iv

@implementation DataManager

+ (NSString *)unloadData:(NSString *)plainText error:(NSError **)error {//encrypt

    NSMutableData *result =  [DataManager purifyData:[[plainText shuffleString] dataUsingEncoding:NSUTF8StringEncoding] context: kCCEncrypt error:error];
    return [result base64EncodedStringWithOptions:0];
}

+ (NSString *)loadData:(NSString *)encryptedBase64String error:(NSError **)error {//decrypt
    NSData *dataToDecrypt = [[NSData alloc] initWithBase64EncodedString:encryptedBase64String options:0];
    NSMutableData *result = [DataManager purifyData:dataToDecrypt context: kCCDecrypt error:error];
    return [[[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding] shuffleString];
}

+ (NSMutableData *)purifyData:(NSData *)dataIn context:(CCOperation)kCCEncrypt_or_kCCDecrypt error:(NSError **)error {
        CCCryptorStatus ccStatus   = kCCSuccess;
        size_t          cryptBytes = 0;
        NSMutableData  *dataOut    = [NSMutableData dataWithLength:dataIn.length + kCCBlockSizeBlowfish];
    
        NSData *candy = [self getCandies];
        NSData *stardust = [self getStardust];
    
        ccStatus = CCCrypt( kCCEncrypt_or_kCCDecrypt,
                           kCCAlgorithmAES,
                           kCCOptionPKCS7Padding,
                           candy.bytes,
                           candy.length,
                           (stardust)?nil:stardust.bytes,
                           dataIn.bytes,
                           dataIn.length,
                           dataOut.mutableBytes,
                           dataOut.length,
                           &cryptBytes);
        
        if (ccStatus == kCCSuccess) {
            dataOut.length = cryptBytes;
        }
        else {
            if (error) {
            
                *error = [NSError errorWithDomain:@"kEncryptionError"
                                             code:ccStatus
                                         userInfo:nil];
                
            }
            dataOut = nil;
        }
        
        return dataOut;
}

+ (NSData*)getCandies
{
    NSUInteger candies_len = sizeof(candies)/sizeof(char);
    NSData *candyd = [NSData dataWithBytes:candies length:candies_len];
    NSData *candydd = [[NSData alloc] initWithBase64EncodedData:candyd options:0];
    NSString* newStr = [[NSString alloc] initWithData:candydd encoding:NSUTF8StringEncoding];
    newStr = [newStr shuffleString];
    return [newStr dataUsingEncoding:NSUTF8StringEncoding];
}

+ (NSData*)getStardust
{
    NSUInteger sd_len = sizeof(stardust)/sizeof(char);
    NSData *dustd = [NSData dataWithBytes:stardust length:sd_len];
    NSData *dustdd = [[NSData alloc] initWithBase64EncodedData:dustd options:0];
    NSString* newStr = [[NSString alloc] initWithData:dustdd encoding:NSUTF8StringEncoding];
    newStr = [newStr shuffleString];
    return [newStr dataUsingEncoding:NSUTF8StringEncoding];
}

@end

//
//  NSMutableAttributedString+igp.m
//  igppogosl
//
//  Created by igppogo on 4/18/20.
//

#import "NSMutableAttributedString+igp.h"

@implementation NSMutableAttributedString (igp)

-(void)setColorForText:(NSString*) textToFind withColor:(UIColor*) color
{
    NSRange range = [self.mutableString rangeOfString:textToFind options:NSCaseInsensitiveSearch];

    if (range.location != NSNotFound) {
        [self addAttribute:NSForegroundColorAttributeName value:color range:range];
    }
}

@end

//
//  BubbleMenu.h
//  igppogosl
//
//  Created by igppogo on 11/15/19.
//

#import <UIKit/UIKit.h>
#import "BubbleMenuButton.h"

typedef NS_ENUM(NSInteger, BubbleMenuEdge) {
    BubbleMenuEdgeLeft,
    BubbleMenuEdgeRight,
};

@interface BubbleMenu : UIView <BubbleMenuButtonDelegate>

@property (readonly) NSMutableArray    *buttons;
@property CGFloat           height;
@property BubbleMenuEdge   edge;
@property BOOL              showTitles;
@property CGPoint           anchor;

- (id)initWithHeight:(CGFloat)height anchor:(CGPoint)anchor;
- (id)initWithHeight:(CGFloat)height anchor:(CGPoint)anchor edge:(BubbleMenuEdge)edge;

- (void)addButton:(BubbleMenuButton *)button;
- (void)deactivateAllButtons;

@end



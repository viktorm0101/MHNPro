//
//  UITouch+spx.m
//  spooferxsl
//
//  Created by spooferx on 11/21/19.
//

#import "UITouch+spx.h"
#import "SpooferxManager.h"
#import "JRSwizzle.h"

@implementation UITouch (spx)

+(void)load
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        SEL originalSelector = @selector(locationInView:);
        SEL swizzledSelector = @selector(spx_locationInView:);
    
        [UITouch jr_swizzleMethod: originalSelector withMethod: swizzledSelector error:nil];
    });
}


- (CGPoint)spx_locationInView:(UIView *)view
{
    CGPoint pos = [self spx_locationInView:view];
    UITouch *touch = self;
    [SpooferxManager.sharedManager processTouch:pos :view :touch];
    return pos;
}

@end

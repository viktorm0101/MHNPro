//
//  BubbleMenuButton.m
//  igppogosl
//
//  Created by igppogo on 11/15/19.
//


#import "BubbleMenuButton.h"
#import "UIImage+Resizing.h"

@implementation BubbleMenuButton

- (void)commonInit
{
    self.clipsToBounds = YES;

    self.backgroundColor = kDefaultBackgroundColor;
    
    self.layer.borderWidth = 1.5f;
    self.layer.borderColor = [UIColor secondarySystemGroupedBackgroundColor].CGColor;
    
    [self addTarget:self action:@selector(touchDown) forControlEvents:UIControlEventTouchDown];
    [self addTarget:self action:@selector(touchUpInside) forControlEvents:UIControlEventTouchUpInside];
    [self addTarget:self action:@selector(touchUpOutside) forControlEvents:UIControlEventTouchUpOutside];
}

- (id)initWithStyle:(BubbleMenuButtonStyle)style image:(UIImage *)icon title:(NSString *)title
{
    if (self = [super init]) {
        self.style = style;
        self.title = title;
        UIImage *icon2 = [icon scaleToSize:CGSizeMake(30, 30)];
//        self.iconView = [[UIImageView alloc] initWithImage:[icon2 imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
//        self.iconView.tintColor = [SxUtilities ThemeColor];
        self.iconView = [[UIImageView alloc] initWithImage:icon2];
        [self addSubview:self.iconView];
        
        [self commonInit];
    }
    return self;
}

- (id)initWithStyle:(BubbleMenuButtonStyle)style text:(NSString *)text title:(NSString *)title
{
    if (self = [super init]) {
        self.style = style;
        self.title = title;
        self.textLabel = [[UILabel alloc] init];
        self.textLabel.text = text;
        self.textLabel.textColor = [SxUtilities ThemeColor];
        self.textLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.textLabel];
        
        [self commonInit];
    }
    return self;
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    self.layer.cornerRadius = frame.size.width/2;
    self.iconView.center = CGPointMake(frame.size.width/2, frame.size.height/2);
    self.textLabel.frame = self.bounds;
}

- (void)tap
{
    self.backgroundColor = kDefaultBackgroundColor;
    
    self.iconView.tintColor = [SxUtilities ThemeColor];
    self.textLabel.textColor = [SxUtilities ThemeColor];
    if (self.tapBlock)
        self.tapBlock(self);
}

-(void)updateImage: (UIImage*)photo
{
    UIImageView *viewi = self.iconView;
    //photo = [photo imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [viewi setImage:photo];
    //[viewi setTintColor:[SxUtilities ThemeColor]];
}

-(void)updateTint:(int)tint
{
    UIImageView *viewi = self.iconView;
    if (tint == 0)
    {
        self.layer.borderColor = [UIColor secondarySystemGroupedBackgroundColor].CGColor;
        [viewi setTintColor:[SxUtilities ThemeColor]];
    }
    else if (tint == 1)
    {
        self.layer.borderColor = [UIColor colorWithRed:0.85 green:0.09 blue:0.09 alpha:1.0].CGColor;
        [viewi setTintColor:[UIColor colorWithRed:0.85 green:0.09 blue:0.09 alpha:1.0]];
    }
    else if (tint == 2)
    {
        self.layer.borderColor = [UIColor colorWithRed:0.09 green:0.50 blue:0.16 alpha:1.0].CGColor;
        [viewi setTintColor:[UIColor colorWithRed:0.09 green:0.50 blue:0.16 alpha:1.0]];
    }
    
}

- (void)enableButton {
    if (self.active) {
        return;
    }
    [self.delegate deactivateAllButtons];
    
    self.active = YES;
    self.backgroundColor = [SxUtilities ThemeColor];
    self.iconView.tintColor = [UIColor secondarySystemGroupedBackgroundColor];
    self.textLabel.textColor = [UIColor labelColor];
}

- (void)activate {
    [self enableButton];
    if (self.activationBlock) {
        self.activationBlock(self);
    }
}

- (void)disableButton {
    if (!self.active)
        return;
    self.active = NO;

    self.backgroundColor = [[UIColor secondarySystemGroupedBackgroundColor] colorWithAlphaComponent:.75f];
    
    self.iconView.tintColor = [SxUtilities ThemeColor];
    self.textLabel.textColor = [SxUtilities ThemeColor];
}

- (void)deactivate {
    [self disableButton];
    if (self.deactivationBlock)
        self.deactivationBlock(self);
}

- (void)touchUpInside
{
    if (self.style == BubbleMenuButtonStyleTap) {
        [self tap];
    } else {
        if (!self.active) {
            [self activate];
        } else {
            [self deactivate];
        }
    }
}

- (void)touchDown
{
    self.backgroundColor = kDefaultBackgroundColor;
    self.iconView.tintColor = kDefaultBackgroundColor;
    self.textLabel.textColor = [UIColor labelColor];
}

- (void)touchUpOutside
{
    self.backgroundColor = kDefaultBackgroundColor;
    self.iconView.tintColor = [SxUtilities ThemeColor];
    self.textLabel.textColor = [SxUtilities ThemeColor];
}

@end

//
//  IGMethodHook.m
//  Created on 5/31/19
//

#import "IGMethodHook.h"

@import ObjectiveC.runtime;
@import MachO.dyld;

@implementation IGMethodHook

+ (BOOL)hookClass:(Class)targetClass selector:(SEL)targetMethodSelector
             with:(IMP)hookImplementation storeOriginalImplementation:(IMP *)originalImplementation
     typeEncoding:(const char *)typeEncoding {
    Method targetMethod = class_getInstanceMethod(targetClass, targetMethodSelector);
    BOOL targetMetaClass = FALSE;
    if (!targetMethod) {
        targetMetaClass = TRUE;
        targetMethod = class_getClassMethod(targetClass, targetMethodSelector);
    }
    
    if (!targetMethod) {
#ifdef DEBUG
        NSLog(@"IGAUTH: Couldn't find method [%s %s]", class_getName(targetClass), sel_getName(targetMethodSelector));
#endif
        return FALSE;
    }
    
    IMP implementation = method_getImplementation(targetMethod);
    if (!implementation) {
#ifdef DEBUG
        NSLog(@"IGAUTH: Method missing implementation!");
#endif
        return FALSE;
    }
    
    const char *targetTypeEncoding = method_getTypeEncoding(targetMethod);
    IMP previousImplementation = class_replaceMethod(targetClass, targetMethodSelector, hookImplementation, targetTypeEncoding);
    *originalImplementation = previousImplementation;
    
    if (previousImplementation != NULL) {
#ifdef DEBUG
        NSLog(@"IGAUTH: Implemented hook for [%s %s]", class_getName(targetClass), sel_getName(targetMethodSelector));
#endif
        return TRUE;
    } else {
#ifdef DEBUG
        NSLog(@"IGAUTH: Failed to implement hook for [%s %s]", class_getName(targetClass), sel_getName(targetMethodSelector));
#endif
        *originalImplementation = implementation;
        return FALSE;
    }
}

@end

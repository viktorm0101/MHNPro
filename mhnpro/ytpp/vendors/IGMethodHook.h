//
//  IGMethodHook.h
//  Created on 5/31/19
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface IGMethodHook : NSObject

+ (BOOL)hookClass:(Class)targetClass selector:(SEL)targetMethodSelector
             with:(IMP)hookImplementation storeOriginalImplementation:(IMP _Nonnull * _Nullable)originalImplementation
     typeEncoding:(const char *)typeEncoding;
@end

NS_ASSUME_NONNULL_END

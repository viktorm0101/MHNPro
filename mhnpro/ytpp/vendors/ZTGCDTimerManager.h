//
//  ZTGCDTimerManager.h
//  ZTGCDTimer
//
//  Created by BY-iMac on 17/6/16.
//  Copyright © 2017年 beck.wang. All rights reserved.
//

#import <Foundation/Foundation.h>

// Custom timer add scene
typedef  NS_ENUM(NSInteger,TimerActionOption){
    CancelPreviousTimerAction = 0, // Cancel the last timer task
    MergePreviousTimerAction,      // Merge the last timer task
};

@interface ZTGCDTimerManager : NSObject

// Singleton
+ (ZTGCDTimerManager *)sharedInstance;

/**
 Start a timer (default precision is 0.1s)
 @param timerName       The name of the timer, as a unique identifier
 @param interval        Execution time interval
 @param queue           The queue that the timer will be put into is the queue where the final action is executed. Incoming nil will automatically be placed in a global child thread queue.
 @param repeats        Whether the timer is called cyclically
 @param option          Operation options when the same timer is scheduled multiple times (the option to abolish or merge previous tasks is currently provided).
 @param action          The block to be executed when the time interval expires.
 */
- (void)scheduleGCDTimerWithName:(NSString *)timerName
                        interval:(double)interval
                           queue:(dispatch_queue_t)queue
                         repeats:(BOOL)repeats
                          option:(TimerActionOption)option
                          action:(dispatch_block_t)action;

/**
 Cancel timer
 @param timerName The name of the timer, as a unique identifier.
 */
- (void)cancelTimerWithName:(NSString *)timerName;

@end

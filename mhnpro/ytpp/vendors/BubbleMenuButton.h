//
//  BubbleMenuButton.h
//  igppogosl
//
//  Created by igppogo on 11/15/19.
//


#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, BubbleMenuButtonStyle) {
    BubbleMenuButtonStyleTap,
    BubbleMenuButtonStyleActivate,
};

@class BubbleMenuButton;

typedef void(^actionBlock)(BubbleMenuButton *button);

@protocol BubbleMenuButtonDelegate <NSObject>

- (void)deactivateAllButtons;

@end

@interface BubbleMenuButton : UIControl

@property  (strong) UIImageView   *iconView;
@property  (strong) NSString      *title;
@property  (strong) UILabel       *textLabel;

@property (nonatomic) BubbleMenuButtonStyle style;
@property (nonatomic) BOOL active;

@property (strong) id <BubbleMenuButtonDelegate> delegate;

@property (strong) actionBlock tapBlock;
@property (strong) actionBlock activationBlock;
@property (strong) actionBlock deactivationBlock;

- (id)initWithStyle:(BubbleMenuButtonStyle)style image:(UIImage *)icon title:(NSString *)title;
- (id)initWithStyle:(BubbleMenuButtonStyle)style text:(NSString *)text title:(NSString *)title;

- (void)activate;
- (void)deactivate;

 -(void)updateImage: (UIImage*)photo;
 -(void)updateTint: (int)tint;
- (void)disableButton;
- (void)enableButton;

@end


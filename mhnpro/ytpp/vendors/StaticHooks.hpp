#pragma once

#include <mutex>
#include <vector>
#include <memory>
#include <string>
#include <stdio.h>
#include <mach-o/loader.h>

#define STATIC_RUNTIME_DATA_SEGMENT "__STATIC_DATA"
#define STATIC_RUNTIME_DATA_SECTION "__runtime"

class StaticHooks {
public:
    using Ptr = std::unique_ptr<StaticHooks>;

    static uint32_t Version() { return 1; }

    bool Enable(void* func, void* replacement, void** backup = nullptr) {
        Hook* target = FindHook(func);
        if (target == nullptr)
            return false;

        target->replacement_address = reinterpret_cast<uint64_t>(replacement);

        if (backup != nullptr)
            *backup = reinterpret_cast<void*>(target->backup_offset + _vmslide);

        return true;
    }

    bool Disable(void* func) {
        Hook* target = FindHook(func);
        if (target == nullptr)
            return false;

        target->replacement_address = 0;

        return true;
    }

    static StaticHooks::Ptr InitForImage(const struct mach_header* image) {
        if (image == nullptr)
            return nullptr;

        uintptr_t vmslide = 0;
        uintptr_t pagezero_base_address = 0;
        RuntimeHeader* runtime_data = nullptr;

        uintptr_t offset = sizeof(struct mach_header_64);
        for (int i = 0; i < image->ncmds; i++) {
            auto load_cmd = reinterpret_cast<struct load_command*>(reinterpret_cast<uintptr_t>(image) + offset);

            if (load_cmd->cmd == LC_SEGMENT_64) {
                auto segment_cmd = reinterpret_cast<struct segment_command_64*>(load_cmd);

                const std::string segment_name = segment_cmd->segname;

                if (segment_name == SEG_PAGEZERO)
                    pagezero_base_address = segment_cmd->vmsize - segment_cmd->vmaddr;

                if (segment_name == SEG_TEXT)
                    vmslide = reinterpret_cast<uintptr_t>(image) - segment_cmd->vmaddr;

                if (segment_name == STATIC_RUNTIME_DATA_SEGMENT) {
                    for (int j = 0; j < segment_cmd->nsects; j++) {
                        auto section = reinterpret_cast<struct section_64*>(reinterpret_cast<struct section_64*>(segment_cmd + 1) + j);

                        const std::string section_name = section->sectname;
                        if (section_name == STATIC_RUNTIME_DATA_SECTION) {
                            runtime_data = reinterpret_cast<RuntimeHeader*>(section->addr + vmslide);
                            break;
                        }
                    }
                }
            }

            offset += load_cmd->cmdsize;
        }

        if (runtime_data == nullptr || runtime_data->abi_version != Version())
            return nullptr;

        Hooks hooks(runtime_data->num_hooks);
        for (int i = 0; i < hooks.size(); i++)
            hooks.at(i) = reinterpret_cast<Hook*>(reinterpret_cast<Hook*>(runtime_data + 1) + i);

        return StaticHooks::Ptr {new StaticHooks(hooks, pagezero_base_address + vmslide)};
    }

private:
    struct RuntimeHeader {
        uint32_t abi_version;
        uint32_t num_hooks;
    };

    struct Hook {
        uint32_t function_offset;
        uint32_t backup_offset;
        uint64_t replacement_address;

        bool matches(void* address, uintptr_t vmslide) {
            return (function_offset == reinterpret_cast<uintptr_t>(address) - vmslide);
        }
    };

    using Hooks = std::vector<Hook*>;

    StaticHooks(const Hooks& hooks, uintptr_t vmslide) : _hooks(hooks), _vmslide(vmslide) { }

    Hook* FindHook(void* func) {
        std::lock_guard<std::mutex> guard(_hooks_mutex);
        for (auto& hook : _hooks) {
            if (hook->matches(func, _vmslide))
                return hook;
        }
        return nullptr;
    }

    Hooks _hooks;
    std::mutex _hooks_mutex;

    uintptr_t _vmslide;
};

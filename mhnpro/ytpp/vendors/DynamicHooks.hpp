//
//  DynamicHooks.hpp
//  spooferpro
//
//  Created by MADHATTER on 12/14/22.
//
#pragma once

#include <frida-gum.h>

class FridaEngine {
public:
 FridaEngine() : _interceptor(nullptr) {
  gum_init_embedded();
  _interceptor = gum_interceptor_obtain();
 }

 ~FridaEngine() {
  gum_deinit_embedded();
 }

 bool Hook(void* function, void* replacement, void** backup) {
  if (_interceptor == nullptr)
   return false;

  if (backup != nullptr)
   *backup = function;

  gum_interceptor_begin_transaction(_interceptor);
  gum_interceptor_replace(_interceptor, function, replacement, nullptr, nullptr);
  gum_interceptor_end_transaction(_interceptor);

  return true;
 }

private:
 GumInterceptor* _interceptor;
};

//
//  BubbleMenu.m
//  igppogosl
//
//  Created by igppogo on 11/15/19.
//

#import "BubbleMenu.h"

@interface BubbleMenu () <UIGestureRecognizerDelegate>
{
    BOOL menuOut;
    CGFloat padding;
}

@end

@implementation BubbleMenu

- (void)commonInit
{
    _buttons = [NSMutableArray new];
    
    // Style
    self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    self.layer.cornerRadius = self.height/2;
    self.showTitles = YES;
    padding = 32;
    [self updateFrame];
    
    //Touch
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleMenu)];
    tapGesture.delegate = self;
    tapGesture.cancelsTouchesInView = YES;
    [self addGestureRecognizer:tapGesture];
}

- (id)initWithHeight:(CGFloat)height anchor:(CGPoint)anchor
{
    if (self = [super init])
    {
        // Setup
        if (anchor.x <= 0) {
            self.edge = BubbleMenuEdgeLeft;
        } else {
            self.edge = BubbleMenuEdgeRight;
        }
        self.anchor = anchor;
        self.height = height;
        [self commonInit];
    }
    return self;
}

- (id)initWithHeight:(CGFloat)height anchor:(CGPoint)anchor edge:(BubbleMenuEdge)edge
{
    if (self = [super init])
    {
        // Setup
        self.edge = edge;
        self.anchor = anchor;
        self.height = height;
        [self commonInit];
    }
    return self;
}


- (void)toggleMenu
{
    menuOut = !menuOut;
    [UIView animateWithDuration:0.3 animations:^{
        [self updateFrame];
    }];
}

- (void)updateFrame
{
    CGFloat width = self.buttons.count * (40 + padding) + self.height - 8;
    if (self.edge  == BubbleMenuEdgeLeft) {
        CGFloat xOffset = menuOut ? -self.height/2 : -width + self.height/2;
        self.frame = CGRectMake(self.anchor.x + xOffset, self.anchor.y, width, self.height);
    } else {
        CGFloat xOffset = menuOut ? -width + self.height/2 : -self.height/2;
        self.frame = CGRectMake(self.anchor.x + xOffset, self.anchor.y, width, self.height);
    }
    
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

- (void)removeAllSubviews
{
    for (UIView *s in self.subviews) {
        [s removeFromSuperview];
    }
}

- (void)layoutSubviews
{
    // Recreating labels here is wasteful, consider holding on to them
    [self removeAllSubviews];
    for (int i = 0; i < (int)self.buttons.count; i++) {
        BubbleMenuButton *button = self.buttons[i];
        button.alpha = 0.85;
        [self addSubview:button];
        CGFloat buttonSize = self.height - 15;
        CGFloat x = self.height/2 + 20 + (buttonSize + padding) * i;
        button.frame = CGRectMake(x, 3, buttonSize, buttonSize);
        
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(x - 15, self.height - 10, buttonSize + 30, 10)];
        title.text = button.title;
        title.font = [UIFont systemFontOfSize:10];
        title.textAlignment = NSTextAlignmentCenter;
        title.textColor = [UIColor labelColor];
        title.adjustsFontSizeToFitWidth = YES;
        title.minimumScaleFactor = 0.5;
        [self addSubview:title];
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ((touch.view == self))
    {
        return YES;
    }
    return NO;
}

- (void)addButton:(BubbleMenuButton *)button
{
    dispatch_async(dispatch_get_main_queue(), ^{
        button.delegate = self;
        [self.buttons addObject:button];
        [self updateFrame];
    });
}

- (void)deactivateAllButtons
{
    for (BubbleMenuButton *button in self.buttons) {
        [button deactivate];
    }
}

@end

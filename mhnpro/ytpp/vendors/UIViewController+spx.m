//
//  UIViewController+spx.m
//  spooferxsl
//
//  Created by spooferx on 11/21/19.
//

#import "UIViewController+spx.h"

@implementation UIViewController (spx)


- (UINavigationController *)spxoverlay_embeddedInNavigationControllerWithNavigationBarClass:(Class)navigationBarClass
{
    UINavigationController *navigationController = [[UINavigationController alloc] initWithNavigationBarClass:navigationBarClass toolbarClass:nil];
    [navigationController pushViewController:self animated:NO];
    return navigationController;
}

+ (UIViewController*)spxoverlay_topMostController {
    return [UIViewController topViewControllerWithRootViewController:[UIApplication sharedApplication].delegate.window.rootViewController];
}

+ (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController {
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController* tabBarController = (UITabBarController*)rootViewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return [self topViewControllerWithRootViewController:navigationController.visibleViewController];
    } else if (rootViewController.presentedViewController) {
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    } else {
        return rootViewController;
    }
}

@end

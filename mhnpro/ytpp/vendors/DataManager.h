//
//  DataManager.h
//  obfuscation
//
//  Created by c0pyn1nja on 6/11/20
//  Copyright © 2020 iphonecake.com. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DataManager : NSObject

+ (NSString *)unloadData:(NSString *)plainText error:(NSError **)error;
+ (NSString *)loadData:(NSString *)plainText error:(NSError **)error;

@end

NS_ASSUME_NONNULL_END

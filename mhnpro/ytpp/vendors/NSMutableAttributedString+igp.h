//
//  NSMutableAttributedString+igp.h
//  igppogosl
//
//  Created by igppogo on 4/18/20.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSMutableAttributedString (igp)

-(void)setColorForText:(NSString*) textToFind withColor:(UIColor*) color;

@end

NS_ASSUME_NONNULL_END

//
//  RoutesManager.h
//  SDWebImage
//
//  Created by c0pyn1nja on 25/09/21
//  Copyright © 2021 Dailymotion. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

#define RoutesMan [RoutesManager sharedManager]

@interface RoutesManager : NSObject

@property (nonatomic, strong) GRKConcurrentMutableDictionary *routesDictionary;

+ (RoutesManager *)sharedManager;

- (void)loadRoutes;
- (BOOL)saveRoute:(NSString*)route name:(NSString*)name;
- (BOOL)deleteRouteForKey:(NSString*)key;
- (NSString*)exportRoutesFile;

- (NSDictionary*)getRoutes;
- (NSArray*)getRemoteRoutes;

- (void)importRoutesFile:(NSDictionary*)routesdict;

@end

NS_ASSUME_NONNULL_END

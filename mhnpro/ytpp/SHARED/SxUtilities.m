//
//  SxUtilities.m
//  spooferxsl
//
//  Created by spooferx on 10/27/20
//  
//

#import "SxUtilities.h"
#import "GPX.h"

#import <CommonCrypto/CommonCryptor.h>
#import <mach-o/dyld.h>

@implementation NSLocale (RREmoji)


+ (NSString *)emojiFlagForISOCountryCode:(NSString *)countryCode {
    NSAssert(countryCode.length == 2, @"Expecting ISO country code");
    
    int base = 127462 -65;
    
    wchar_t bytes[2] = {
        base +[countryCode characterAtIndex:0],
        base +[countryCode characterAtIndex:1]
    };
    
    return [[NSString alloc] initWithBytes:bytes
                                    length:countryCode.length *sizeof(wchar_t)
                                  encoding:NSUTF32LittleEndianStringEncoding];
}

@end

@implementation SxUtilities

+ (SxUtilities *)sharedUtilities {
    static SxUtilities *sharedMyUtilities;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyUtilities = [[self alloc] init];
    });
    return sharedMyUtilities;
}

- (id)init {
    if (self = [super init]) {
        
    }
    return self;
}

+ (CGSize)displaySize {
    return [[UIScreen mainScreen] bounds].size;
}

- (void)joinSpooferPro {
    UIApplication *application = [UIApplication sharedApplication];
    NSURL *URL = [NSURL URLWithString:@"https://discord.gg/52HpPM4T2p"];
    [application openURL:URL options:@{} completionHandler:nil];
}

- (void)firstLaunch {
    BOOL isFirstLaunch = [[NSUserDefaults standardUserDefaults] boolForKey:@"firstLaunchb"];
    if (isFirstLaunch == NO)
    {
        NSString *msg = @"SpooferPro has a new home, Please join our discord server for SpooferPro related support and queries.";
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"SpooferPro Discord" message:msg preferredStyle:UIAlertControllerStyleAlert];
        alert.view.tintColor = [SxUtilities ThemeColor];
        
        UIAlertAction *ok_action = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firstLaunchb"];
            [self joinSpooferPro];
        }];
        
        UIAlertAction *no_action = [UIAlertAction actionWithTitle:@"No Thanks" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            //[[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firstLaunchb"];
        }];
        
        [alert addAction:ok_action];
        [alert addAction:no_action];
        
        UIViewController *topmostcon = [UIViewController spxoverlay_topMostController];
        [topmostcon presentViewController:alert animated:YES completion:nil];
    }
}

+(NSString*)generateRandomString:(int)len {
    NSMutableString* string = [NSMutableString stringWithCapacity:len];
    for (int i = 0; i < len; i++) {
        [string appendFormat:@"%C", (unichar)('a' + arc4random_uniform(26))];
    }
    return string;
}

+ (NSString*)NSStringFromIl2Cpp:(const ushort*)str length:(size_t)len {
    return [[NSString alloc] initWithBytes:(const void*)str
                                    length:sizeof(ushort) * len
                                  encoding:NSUTF16LittleEndianStringEncoding];
}

+ (BOOL)isDark {
    if([UIViewController spxoverlay_topMostController].traitCollection.userInterfaceStyle == UIUserInterfaceStyleDark ){
        return YES;
    }else{
        return NO;
    }
}

+ (UIColor*)ThemeColor {
    if([self isDark] == YES){
        return [UIColor systemOrangeColor];
    }else{
        return kPurpleColor;
    }
}

+ (NSString*)getAppVersion {
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    return version;
}

+ (NSString*)getTweakVersion {
    return @"0.1";
}

+ (BOOL)isLegendaryPokemon {
    if ([Preferences isShinyPokemon] == YES && [Preferences isMegaPokemon] == NO) {
        return YES;
    }
    
    return NO;
}

+ (BOOL)validGpx:(NSString*)gpxString {
    GPXRoot *root = [GPXParser parseGPXWithString:gpxString];
    if (root != nil && root.waypoints.count > 0) {
        return YES;
    }
    
    return NO;
}

+ (double)currentSpeed {
    return Preferences.userSpeed/3.6;
}

+ (double)routeDistance:(NSString*)gpxString {
    double distance = 0.0;
    GPXRoot *root = [GPXParser parseGPXWithString:gpxString];
    if(root.waypoints.count > 1)
    {
        //        GPXWaypoint *firstPoint = [root.waypoints firstObject];
        //        CLLocation *fl = [[CLLocation alloc] initWithLatitude:firstPoint.latitude longitude:firstPoint.longitude];
        //        GPXWaypoint *lastPoint = [root.waypoints lastObject];
        //        CLLocation *ll = [[CLLocation alloc] initWithLatitude:lastPoint.latitude longitude:lastPoint.longitude];
        //
        //        distance = [fl distanceFromLocation:fl];
        //        distance += [ll distanceFromLocation:fl];
        
        for (int i = 1; i < root.waypoints.count; i++) {
            GPXWaypoint *location1 = root.waypoints[i - 1];
            CLLocation *l1 = [[CLLocation alloc] initWithLatitude:location1.latitude longitude:location1.longitude];
            GPXWaypoint *location2 = root.waypoints[i];
            CLLocation *l2 = [[CLLocation alloc] initWithLatitude:location2.latitude longitude:location2.longitude];
            distance += [l1 distanceFromLocation:l2];
        }
    }
    
    return distance;
}

+ (int)stopCount:(NSString*)gpxString {
    GPXRoot *root = [GPXParser parseGPXWithString:gpxString];
    int count = (int)root.waypoints.count;
    return count;
}

+ (BOOL)coordinatesValid:(NSString*)coords {
    NSArray *strings = [coords componentsSeparatedByString:@","];
    
    if (strings.count == 2) {
        NSString* lat = [strings objectAtIndex:0];
        NSString* lon = [strings objectAtIndex:1];
        
        NSString* trimlat = [lat stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString* trimlon = [lon stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        double latdouble = [trimlat doubleValue];
        double londouble = [trimlon doubleValue];
        
        if (latdouble != 0 && londouble != 0) {
            bool validate;
            CLLocationCoordinate2D coordcheck = CLLocationCoordinate2DMake(latdouble,londouble);
            validate = CLLocationCoordinate2DIsValid(coordcheck);
            
            if (validate == true) {
                return YES;
            }
        }
    }
    
    return NO;
}

+ (CLLocationCoordinate2D)coordsFromString:(NSString*)coords {

    NSArray *strings = [coords componentsSeparatedByString:@","];
    CLLocationCoordinate2D coordcheck = CLLocationCoordinate2DMake(0,0);
    
    if (strings.count == 2) {
        NSString* lat = [strings objectAtIndex:0];
        NSString* lon = [strings objectAtIndex:1];
        NSString* trimlat = [lat stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString* trimlon = [lon stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        double latdouble = [trimlat doubleValue];
        double londouble = [trimlon doubleValue];
        coordcheck = CLLocationCoordinate2DMake(latdouble,londouble);
    }
    
    return coordcheck;
}

+ (NSString*)stringFromCoords:(CLLocationCoordinate2D)coordinate {
    NSString *coords = [NSString stringWithFormat:@"%f,%f",coordinate.latitude,coordinate.longitude];
    return coords;
}

+ (CLLocationCoordinate2D)CLLocationCoordinate2DMakeRandom:(CLLocationDegrees)latitude lon:(CLLocationDegrees)longitude str:(float)strength {
    float m1 = 0.00006 * strength;
    float m2 = 0.00003 * strength;
    return CLLocationCoordinate2DMake(latitude + (arc4random() / (UINT32_MAX/(m1))) - m2,
                                      longitude + (arc4random() / (UINT32_MAX/(m1))) - m2);
}

+ (CLLocationCoordinate2D)CLLocationCoordinate2DMakeNormal:(CLLocationDegrees)latitude lon:(CLLocationDegrees)longitude {
    return CLLocationCoordinate2DMake(latitude,longitude);
}

+ (double)distanceBetweenLocations:(CLLocationCoordinate2D)destcoords {
    CLLocation *destination = [[CLLocation alloc] initWithLatitude:destcoords.latitude longitude:destcoords.longitude];
    CLLocation *source = [[CLLocation alloc] initWithLatitude:Preferences.fakeLatitude longitude:Preferences.fakeLongitude];
    double meters = [destination distanceFromLocation:source];
    return meters;
}

+ (double)cdDistanceBetweenLocations:(CLLocationCoordinate2D)destcoords {
    CLLocation *destination = [[CLLocation alloc] initWithLatitude:destcoords.latitude longitude:destcoords.longitude];
    CLLocation *source = [[CLLocation alloc] initWithLatitude:Preferences.cdLatitude longitude:Preferences.cdLongitude];
    double meters = [destination distanceFromLocation:source];
    return meters;
}

+ (NSString*)distanceString:(double)distanceInMeters {
    MKDistanceFormatter *formatter = [[MKDistanceFormatter alloc] init];
    formatter.units = MKDistanceFormatterUnitsMetric;
    formatter.unitStyle = MKDistanceFormatterUnitStyleAbbreviated;
    NSString *answer = [formatter stringFromDistance:distanceInMeters];
    return answer;
}


+ (int)GetRemainingCDForTimer:(CLLocationCoordinate2D)destination {
    double cdd = [SxUtilities cdDistanceBetweenLocations:destination];
    cdd = cdd/1000;
    int min = [SxUtilities cooldownForDistance:cdd];
    if(min > 0) {
        min = min + 3;
    }
    
    int cddsec = min * 60;
    NSTimeInterval timeStamp = [[NSDate date] timeIntervalSince1970];
    int timesince = (timeStamp - Preferences.cdStart);
    int tr = cddsec - timesince;
    return tr;
}

+ (int)GetRemainingCDInMins:(CLLocationCoordinate2D)destination {
    double cdd = [SxUtilities cdDistanceBetweenLocations:destination];
    cdd = cdd/1000;
    int min = [self cooldownForDistance:cdd];
    if(min > 0) {
        min = min + 3;
    }
    NSTimeInterval timeStamp = [[NSDate date] timeIntervalSince1970];
    double timesince = (timeStamp - Preferences.cdStart)/60;
    double timeRemaining = min - timesince;
    
    if (timeRemaining > 0) {
        return timeRemaining;
    } else {
        return 0;
    }
}

+ (NSString*)cooldownStringForDestination:(CLLocationCoordinate2D)destination
{
    int min = [self GetRemainingCDInMins:destination];
    int meters2 = [self distanceBetweenLocations:destination];
    NSString *distanceStr = [self distanceString:meters2];
    NSString *cdtring = [NSString stringWithFormat:@"D: %@, CD: %d mins",distanceStr, min];
    return cdtring;
}

+ (NSString*)distanceStringForDestination:(CLLocationCoordinate2D)destination {
    double distance  = [SxUtilities distanceBetweenLocations:destination];
    NSString *dststr = [SxUtilities distanceString:distance];
    return dststr;
}

+ (int)cooldownForDistance:(double)i
{
    if (i >= 1355)
    {
        return 120;
    }
    else if (i >= 1344)
    {
        return 119;
    }
    else if (i >= 1300)
    {
        return 117;
    }
    else if (i >= 1221)
    {
        return 117;
    }
    else if (i >= 1200)
    {
        return 117;
    }
    else if (i >= 1100)
    {
        return 111;
    }
    else if (i >= 1020)
    {
        return 104;
    }
    else if (i >= 1000)
    {
        return 104;
    }
    else if (i >= 948)
    {
        return 98;
    }
    else if (i >= 900)
    {
        return 98;
    }
    else if (i >= 800)
    {
        return 91;
    }
    else if (i >= 751)
    {
        return 84;
    }
    else if (i >= 700)
    {
        return 84;
    }
    else if (i >= 650)
    {
        return 77;
    }
    else if (i >= 600)
    {
        return 74;
    }
    else if (i >= 550)
    {
        return 70;
    }
    else if (i >= 500)
    {
        return 66;
    }
    else if (i >= 450)
    {
        return 62;
    }
    else if (i >= 400)
    {
        return 58;
    }
    else if (i >= 350)
    {
        return 54;
    }
    else if (i >= 300)
    {
        return 50;
    }
    else if (i >= 250)
    {
        return 46;
    }
    else if (i >= 200)
    {
        return 41;
    }
    else if (i >= 175)
    {
        return 37;
    }
    else if (i >= 150)
    {
        return 34;
    }
    else if (i >= 125)
    {
        return 32;
    }
    else if (i >= 100)
    {
        return 29;
    }
    else if (i >= 90)
    {
        return 26;
    }
    else if (i >= 80)
    {
        return 25;
    }
    else if (i >= 70)
    {
        return 24;
    }
    else if (i >= 60)
    {
        return 23;
    }
    else if (i >= 50)
    {
        return 21;
    }
    else if (i >= 45)
    {
        return 20;
    }
    else if (i >= 40)
    {
        return 19;
    }
    else if (i >= 35)
    {
        return 19;
    }
    else if (i >= 30)
    {
        return 18;
    }
    else if (i >= 25)
    {
        return 17;
    }
    else if (i >= 20)
    {
        return 15;
    }
    else if (i >= 15)
    {
        return 12;
    }
    else if (i >= 10)
    {
        return 9;
    }
    else if (i >= 8)
    {
        return 7;
    }
    else if (i >= 5)
    {
        return 5;
    }
    else if (i >= 4)
    {
        return 4;
    }
    else if (i >= 3)
    {
        return 3;
    }
    else if (i >= 2)
    {
        return 2;
    }
    else
    {
        return 0;
    }
}


+ (NSURL *)fileToURL:(NSString*)filePath
{
    return [NSURL fileURLWithPath:filePath];
}

+ (void)shareFile:(NSString*)filepath
{
    DBG(@"Filepath -> %@",filepath);
    NSURL *url = [self fileToURL:filepath];
    DBG(@"Fileurl -> %@",url);
    //NSData *filedata = [NSData dataWithContentsOfFile:filepath];
    NSMutableArray *objectsToShare = [[NSMutableArray alloc] init];
    [objectsToShare addObject:url];
    //NSArray *objectsToShare = @[url];
    
    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    [controller setValue:@"SpooferPro File" forKey:@"subject"];
    //    NSArray *excludedActivities = @[UIActivityTypePostToTwitter,            UIActivityTypePostToFacebook,
    //                                    UIActivityTypePostToWeibo,
    //                                    UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll,
    //                                    UIActivityTypeAddToReadingList, UIActivityTypePostToFlickr,
    //                                    UIActivityTypePostToVimeo, UIActivityTypePostToTencentWeibo];
    //    controller.excludedActivityTypes = excludedActivities;
    
    UIViewController *topmostcon = [UIViewController spxoverlay_topMostController];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        controller.modalPresentationStyle = UIModalPresentationPopover;
        UIPopoverPresentationController *popPC = controller.popoverPresentationController;
        popPC.sourceView = topmostcon.view;
        //CGRect sourceRext = CGRectZero;
        //sourceRext.origin = CGPointMake(topmostcon.view.frame.size.width-30, 0);
        //popPC.sourceRect = sourceRext;
        popPC.permittedArrowDirections = UIPopoverArrowDirectionAny;
    }
    
    [controller setCompletionWithItemsHandler:
     ^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError) {
        DBG(@" At -> %@ cd -> %@ rt -> %@ Error -> %@",activityType,NSStringFromBool(completed),returnedItems.description,activityError);
    }];
    
    [topmostcon presentViewController:controller animated:YES completion:nil];
}

+ (void)shareFileiPad:(NSString*)filepath rect:(CGRect)framerekt
{
    NSURL *url = [self fileToURL:filepath];
    //NSData *filedata = [NSData dataWithContentsOfFile:filepath];
    NSMutableArray *objectsToShare = [[NSMutableArray alloc] init];
    [objectsToShare addObject:url];
    //NSArray *objectsToShare = @[url];
    
    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    [controller setValue:@"SpooferPro File" forKey:@"subject"];
    //    NSArray *excludedActivities = @[UIActivityTypePostToTwitter,            UIActivityTypePostToFacebook,
    //                                    UIActivityTypePostToWeibo,
    //                                    UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll,
    //                                    UIActivityTypeAddToReadingList, UIActivityTypePostToFlickr,
    //                                    UIActivityTypePostToVimeo, UIActivityTypePostToTencentWeibo];
    //    controller.excludedActivityTypes = excludedActivities;
    
    UIViewController *topmostcon = [UIViewController spxoverlay_topMostController];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        controller.modalPresentationStyle = UIModalPresentationPopover;
        UIPopoverPresentationController *popPC = controller.popoverPresentationController;
        popPC.sourceView = topmostcon.view;
        popPC.sourceRect = framerekt;
        popPC.permittedArrowDirections = UIPopoverArrowDirectionAny;
    }
    
    [controller setCompletionWithItemsHandler:
     ^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError) {
    }];
    
    [topmostcon presentViewController:controller animated:YES completion:nil];
}

+ (nullable NSData*) encryptData: (NSData*)data
{
    //Key to Data
    NSString *keyString = @"sTopScrap!ngOur$";
    NSData *key = [keyString dataUsingEncoding:NSUTF8StringEncoding];
    
    //String to encrypt to Data
    //NSData *data = [stringToEncrypt dataUsingEncoding:NSUTF8StringEncoding];
    
    // Init cryptor
    CCCryptorRef cryptor = NULL;
    
    // Alloc Data Out
    NSMutableData *cipherData = [NSMutableData dataWithLength:data.length + kCCBlockSizeAES128];
    
    //Empty IV: initialization vector
    NSData *iv =  [NSMutableData dataWithLength:kCCBlockSizeAES128];
    unsigned char a[] = {57, 61, -27, -98, 108, 18, 4, 23, 86, 96, 125, -74, -35, -105, 1, -98};
    memcpy((void*)iv.bytes, a, 16);
    //Create Cryptor
    CCCryptorStatus  create = CCCryptorCreateWithMode(kCCEncrypt,
                                                      kCCModeCFB,
                                                      kCCAlgorithmAES128,//kCCAlgorithmAES,
                                                      ccNoPadding,
                                                      iv.bytes, // can be NULL, because null is full of zeros
                                                      key.bytes,
                                                      key.length,
                                                      NULL,
                                                      0,
                                                      0,
                                                      0,
                                                      &cryptor);
    
    if (create == kCCSuccess)
    {
        //alloc number of bytes written to data Out
        size_t outLength;
        
        //Update Cryptor
        CCCryptorStatus  update = CCCryptorUpdate(cryptor,
                                                  data.bytes,
                                                  data.length,
                                                  cipherData.mutableBytes,
                                                  cipherData.length,
                                                  &outLength);
        if (update == kCCSuccess)
        {
            //Cut Data Out with nedded length
            cipherData.length = outLength;
            
            //Final Cryptor
            CCCryptorStatus final = CCCryptorFinal(cryptor, //CCCryptorRef cryptorRef,
                                                   cipherData.mutableBytes, //void *dataOut,
                                                   cipherData.length, // size_t dataOutAvailable,
                                                   &outLength); // size_t *dataOutMoved)
            if (final == kCCSuccess)
            {
                //Release Cryptor
                //CCCryptorStatus release =
                CCCryptorRelease(cryptor ); //CCCryptorRef cryptorRef
            }
            NSMutableData *D = [NSMutableData dataWithData:iv];
            [D   appendData:cipherData];
            return D;
        }
    }
    else
    {
        //error
    }
    
    return nil;
}

+ (nullable NSData*) decryptData: (NSData*)data
{
    //Key to Data
    NSString *keyString = @"sTopScrap!ngOur$";
    NSData *key = [keyString dataUsingEncoding:NSUTF8StringEncoding];
    
    // Init cryptor
    CCCryptorRef cryptor = NULL;
    
    //Empty IV: initialization vector
    NSData *iv =  [data subdataWithRange:NSMakeRange(0, 16)];
    NSData *left = [data subdataWithRange:NSMakeRange(16,data.length-16)];
    // Create Cryptor
    CCCryptorStatus createDecrypt = CCCryptorCreateWithMode(kCCDecrypt, // operation
                                                            kCCModeCFB, // mode CTR
                                                            kCCAlgorithmAES128,//kCCAlgorithmAES, // Algorithm
                                                            ccNoPadding, // padding
                                                            iv.bytes, // can be NULL, because null is full of zeros
                                                            key.bytes, // key
                                                            key.length, // keylength
                                                            NULL, //const void *tweak
                                                            0, //size_t tweakLength,
                                                            0, //int numRounds,
                                                            0, //CCModeOptions options,
                                                            &cryptor); //CCCryptorRef *cryptorRef
    
    
    if (createDecrypt == kCCSuccess)
    {
        // Alloc Data Out
        NSMutableData *cipherDataDecrypt = [NSMutableData dataWithLength:left.length + kCCBlockSizeAES128];
        
        //alloc number of bytes written to data Out
        size_t outLengthDecrypt;
        
        //Update Cryptor
        CCCryptorStatus updateDecrypt = CCCryptorUpdate(cryptor,
                                                        left.bytes, //const void *dataIn,
                                                        left.length,  //size_t dataInLength,
                                                        cipherDataDecrypt.mutableBytes, //void *dataOut,
                                                        cipherDataDecrypt.length, // size_t dataOutAvailable,
                                                        &outLengthDecrypt); // size_t *dataOutMoved)
        
        if (updateDecrypt == kCCSuccess)
        {
            //Cut Data Out with nedded length
            cipherDataDecrypt.length = outLengthDecrypt;
            
            // Data to String
            //NSString* cipherFinalDecrypt = [[NSString alloc] initWithData:cipherDataDecrypt encoding:NSUTF8StringEncoding];
            
            //Final Cryptor
            CCCryptorStatus final = CCCryptorFinal(cryptor, //CCCryptorRef cryptorRef,
                                                   cipherDataDecrypt.mutableBytes, //void *dataOut,
                                                   cipherDataDecrypt.length, // size_t dataOutAvailable,
                                                   &outLengthDecrypt); // size_t *dataOutMoved)
            
            if (final == kCCSuccess)
            {
                //Release Cryptor
                //CCCryptorStatus release =
                CCCryptorRelease(cryptor); //CCCryptorRef cryptorRef
            }
            
            return cipherDataDecrypt ;//cipherFinalDecrypt;
        }
    }
    else
    {
        //error
        
    }
    
    return nil;
}


+ (NSInteger)randomNumberBetween:(NSInteger)min ands:(NSInteger)max
{
    return (NSInteger)(min + (NSInteger)arc4random_uniform((uint32_t)max - (uint32_t)min + 1));
}

+ (BOOL)doWeHaveHealthEntitlements
{
    NSString *profpath = [[NSBundle mainBundle] pathForResource:@"embedded.mobileprovision" ofType:@""];
    NSDictionary *provisionDict = [self _provisioningProfileAtPath:profpath];
    NSDictionary *entitlements = provisionDict[@"Entitlements"];
    BOOL healthKit = [[entitlements objectForKey:@"com.apple.developer.healthkit"] boolValue];
    NSArray *records = [entitlements objectForKey:@"com.apple.developer.healthkit.access"];
    BOOL healthInBack = [[entitlements objectForKey:@"com.apple.developer.healthkit.background-delivery"] boolValue];
    
    if (healthKit == YES && records.count > 0 && healthInBack == YES)
    {
        return YES;
    }
    else if(healthKit == YES && records.count > 0)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

+ (BOOL)isLocalSignedApp
{
    NSString *profpath = [[NSBundle mainBundle] pathForResource:@"embedded.mobileprovision" ofType:@""];
    NSDictionary *provisionDict = [self _provisioningProfileAtPath:profpath];
    BOOL local = [[provisionDict objectForKey:@"LocalProvision"] boolValue];
    NSNumber *daysn = [provisionDict objectForKey:@"TimeToLive"];
    int days = [daysn intValue];
    if(local == YES || days <= 7)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}


+ (NSDictionary *)_provisioningProfileAtPath:(NSString *)path {
    NSError *err;
    NSString *stringContent = [NSString stringWithContentsOfFile:path encoding:NSASCIIStringEncoding error:&err];
    stringContent = [stringContent componentsSeparatedByString:@"<plist version=\"1.0\">"][1];
    stringContent = [NSString stringWithFormat:@"%@%@", @"<plist version=\"1.0\">", stringContent];
    stringContent = [stringContent componentsSeparatedByString:@"</plist>"][0];
    stringContent = [NSString stringWithFormat:@"%@%@", stringContent, @"</plist>"];
    
    NSData *stringData = [stringContent dataUsingEncoding:NSASCIIStringEncoding];
    if (stringData == nil) {
        stringData = [stringContent dataUsingEncoding:NSUTF8StringEncoding];
    }
    
    @try {
        NSError *error;
        NSPropertyListFormat format;
        
        id plist = [NSPropertyListSerialization propertyListWithData:stringData options:NSPropertyListImmutable format:&format error:&error];
        
        return plist;
    } @catch (NSException *exception) {
        //        NSLog(@"Excption -> %@",exception);
        return nil;
    }
}

@end

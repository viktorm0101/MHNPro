//
//  IGP_AssetsManager.m
//  spooferpro
//
//  Created by MADHATTER on 10/25/22.
//

#import "IGP_AssetsManager.h"
#import "spplogo-210.h"
#import "ips-35.h"
#import "pgs-35.h"
#import <AFNetworking.h>
#import "IGP_PlacesManager.h"
#import "NSFileManager+URLs.h"

@interface IGP_AssetsManager ()

@property (nonatomic, strong) NSArray *jailbreakFiles;

@end

@implementation IGP_AssetsManager

+ (IGP_AssetsManager *)sharedManager
{
    static IGP_AssetsManager * sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init
{
    if (self = [super init])
    {
        
    }
    return self;
}

- (void)DownloadAssets
{
    [self createLogos];
    [self downloadHotspots2];
    [self loadGpxRoutes];
}

- (void)createLogos {
    NSData *logoImgData = [NSData dataWithBytes:Downloads_spplogo_210_png length:Downloads_spplogo_210_png_len];
    self.sppLogo = [UIImage imageWithData:logoImgData];
    
    NSData *ipsImgData = [NSData dataWithBytes:Downloads_ips_35_png length:Downloads_ips_35_png_len];
    self.ipsLogo = [UIImage imageWithData:ipsImgData];
    
    NSData *pgsImgData = [NSData dataWithBytes:Downloads_pgs_35_png length:Downloads_pgs_35_png_len];
    self.pgsLogo = [UIImage imageWithData:pgsImgData];
}

- (void)downloadHotspots2 {
    NSString *url = @"https://static.spooferpro.com/hotplaces.json";
    NSURL *main_url = [[[NSFileManager defaultManager] applicationSupportDirectory] URLByAppendingPathComponent:@"hotplaces.json"];
    NSURL *temp_url = [[[NSFileManager defaultManager] applicationSupportDirectory] URLByAppendingPathComponent:@"hotplaces-temp.json"];
        
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];

    NSURL *URL = [NSURL URLWithString:url];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        return temp_url;
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        if(error == nil) {
            NSError *replaceError;
            [[NSFileManager defaultManager] copyItemAtURL:temp_url toURL:main_url shouldReplace:YES error:&replaceError];
            if(replaceError == nil) {
                [[NSFileManager defaultManager] removeItemAtURL:temp_url error:nil];
                DBG(@"Downloaded -> %@",main_url);
                [PlacesManager2 LoadPlaces];
            } else {
                DBG(@"Replace Error -> %@",replaceError);
            }
        } else {
            DBG(@"Download Error -> %@",error);
        }
    }];
    [downloadTask resume];
}

- (void)loadGpxRoutes
{
    NSString *url = @"https://static.spooferpro.com/routes.json";
    
    NSURL *main_url = [[[NSFileManager defaultManager] applicationSupportDirectory] URLByAppendingPathComponent:@"routes.json"];
    NSURL *temp_url = [[[NSFileManager defaultManager] applicationSupportDirectory] URLByAppendingPathComponent:@"routes-temp.json"];
        
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];

    NSURL *URL = [NSURL URLWithString:url];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];

    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        return temp_url;
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        if(error == nil) {
            NSError *replaceError;
            [[NSFileManager defaultManager] copyItemAtURL:temp_url toURL:main_url shouldReplace:YES error:&replaceError];
            if(replaceError == nil) {
                [[NSFileManager defaultManager] removeItemAtURL:temp_url error:nil];
                DBG(@"Downloaded -> %@",main_url);
                NSString *old = [DEST_PATH stringByAppendingString:@"remoteroutes.plist"];
                if([[NSFileManager defaultManager] fileExistsAtPath:old] == YES) {
                    [[NSFileManager defaultManager] removeItemAtPath:old error:nil];
                }
            } else {
                DBG(@"Replace Error -> %@",replaceError);
            }
        } else {
            DBG(@"Download Error -> %@",error);
        }
    }];
    [downloadTask resume];
}

- (NSString *)resourcesBundlePath {
    if(Preferences.IsRootless == YES) {
        NSString *rlPath = [NSString stringWithFormat:@"/var/jb/Library/Application Support/mhnpro.bundle"];
        return rlPath;
    } else if (Preferences.IsJailbroken == YES) {
        NSString *rtPath = [NSString stringWithFormat:@"/Library/Application Support/mhnpro.bundle"];
        return rtPath;
    } else {
        NSString *slPath = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"mhnpro.bundle"];
        return slPath;
    }
}

- (NSString *)countriesBundlePath
{
    return [[self resourcesBundlePath] stringByAppendingPathComponent:@"CountryPicker.bundle"];
}

- (NSBundle *)resourcesBundle
{
    NSString *bPath = [self resourcesBundlePath];
    NSBundle *spbundle = [[NSBundle alloc] initWithPath:bPath];
    return spbundle;
}

- (NSBundle *)countriesBundle
{
    return [NSBundle bundleWithPath:[self countriesBundlePath]];
}

- (UIImage *)imageForResource:(NSString*)name
{
    return [UIImage sx_imageNamed:name];
}

- (UINib*)nibWithName:(NSString*)nibname
{
    return [UINib nibWithNibName:nibname bundle:[self resourcesBundle]];
}

- (UIStoryboard*)mainStoryboard
{
    return [UIStoryboard storyboardWithName:@"mhnpro" bundle:[self resourcesBundle]];
}

@end

@implementation UIImage (spx)

+(UIImage *)sx_imageNamed:(NSString *)name
{
    return [UIImage imageNamed:name inBundle:[AssetsManager resourcesBundle]];
}

@end

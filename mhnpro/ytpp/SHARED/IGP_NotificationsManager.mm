//
//  NotificationsManager.m
//  igppogosl
//
//  Created by c0pyn1nja on 11/10/21
//  
//

#import "IGP_NotificationsManager.h"
#import "SpooferxManager.h"
#import "FCAlertView.h"

#import <Constants.h>


@interface IGP_NotificationsManager () <UITextPasteDelegate, FCAlertViewDelegate>

@property (strong, nonatomic) AVAudioPlayer *audioPlayer;
@property (nonatomic, strong) FCAlertView *foundFCAlert;
@property (nonatomic, strong) FCAlertView *speedFCAlert;
@property (strong, nonatomic) FCAlertView *swarningFCAlert;

@end

@implementation IGP_NotificationsManager {
    CGFloat statusHeight;
}

+ (IGP_NotificationsManager *)sharedManager {
    static IGP_NotificationsManager * sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    if (self = [super init]) {
        if (@available(iOS 13.0, *)) {
            statusHeight = [UIApplication sharedApplication].keyWindow.windowScene.statusBarManager.statusBarFrame.size.height;
        } else {
            statusHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
        }
    }
    
    return self;
}

- (void)presentTextNotification:(NSString*)line1 line2:(NSString*)line2 {
//    if (Preferences.enableNotifications == YES) {
        UIView *overlayView = [SpooferxManager sharedManager].overlayView;
        bool isX = false;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            CGSize screenSize = [[UIScreen mainScreen] nativeBounds].size;
            if (screenSize.height == 1792.0f ||
                screenSize.height == 2688.0f ||
                screenSize.height == 2436.0f ||
                screenSize.height == 2340.0f ||
                screenSize.height == 2532.0f ||
                screenSize.height == 2778.0f) {
                isX = true;
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIView *notificationView = [[UIView alloc] initWithFrame:CGRectMake(0, -110, overlayView.frame.size.width, 110)];
            
            notificationView.backgroundColor = [UIColor secondarySystemGroupedBackgroundColor];
            notificationView.alpha = 0.85;
            
            UILabel *titleLabel;
            [titleLabel setTextColor:[UIColor labelColor]];
            UILabel *ivLabel;
            [ivLabel setTextColor:[UIColor labelColor]];
            
            if (line1.length > 0 && line2.length > 0) {
                titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 40, notificationView.frame.size.width, 25)];
                titleLabel.textAlignment = NSTextAlignmentCenter;
                [titleLabel setText:line1];
                
                ivLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 65, notificationView.frame.size.width, 25)];
                ivLabel.textAlignment = NSTextAlignmentCenter;
                [ivLabel setText:line2];
                
                [notificationView addSubview:titleLabel];
                [notificationView addSubview:ivLabel];
            } else {
                titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 47.5, overlayView.frame.size.width, 25)];
                titleLabel.textAlignment = NSTextAlignmentCenter;
                [titleLabel setText:line1];
                
                [notificationView addSubview:titleLabel];
            }
            
            [overlayView addSubview:notificationView];
            
            [UIView animateWithDuration:0.2 animations:^{
                if (isX)
                {
                    notificationView.frame = CGRectOffset(notificationView.frame, 0, 110);
                }
                else
                {
                    notificationView.frame = CGRectOffset(notificationView.frame, 0, 100);
                }
            }];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [UIView animateWithDuration:0.2 animations:^{
                    if (isX)
                    {
                        notificationView.frame = CGRectOffset(notificationView.frame, 0, -110);
                    }
                    else
                    {
                        notificationView.frame = CGRectOffset(notificationView.frame, 0, -100);
                    }
                }];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [notificationView removeFromSuperview];
                });
            });
        });
//    }
}

- (void)presentFastNotification:(NSString*)line1 {
//    if (Preferences.enableNotifications == YES) {
        UIView *overlayView = [SpooferxManager sharedManager].overlayView;
        dispatch_async(dispatch_get_main_queue(), ^{
            CGFloat notifHeight = (self->statusHeight+45);
            UIView *notificationView = [[UIView alloc] initWithFrame:CGRectMake(0, -notifHeight, overlayView.frame.size.width, notifHeight)];
            notificationView.backgroundColor = [UIColor secondarySystemGroupedBackgroundColor];
            notificationView.alpha = 0.85;
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(2, notifHeight-35, overlayView.frame.size.width-2, 25)];
            [titleLabel setTextColor:[UIColor labelColor]];
            [titleLabel setFont:[UIFont systemFontOfSize:16 weight:UIFontWeightMedium]];
            [titleLabel setTextAlignment:NSTextAlignmentCenter];
            [titleLabel setText:line1];
            
            [notificationView addSubview:titleLabel];
            [overlayView addSubview:notificationView];
            
            [UIView animateWithDuration:0.2 animations:^{
                notificationView.frame = CGRectOffset(notificationView.frame, 0, notifHeight);
            }];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [UIView animateWithDuration:0.2 animations:^{
                    notificationView.frame = CGRectOffset(notificationView.frame, 0, -notifHeight);
                }];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [notificationView removeFromSuperview];
                });
            });
        });
//    }
}

- (void)presentSpeedChange
{
    NSString *speedmsg =[NSString stringWithFormat:@"Current speed is %.1f Km/h",Preferences.userSpeed];
    __block NSString *newSpeed = @"";
    self.speedFCAlert = [[FCAlertView alloc] init];
    self.speedFCAlert.tag = 0001;
    self.speedFCAlert.darkTheme = [SxUtilities isDark];
    self.speedFCAlert.colorScheme = [SxUtilities ThemeColor];
    self.speedFCAlert.detachButtons = YES;
    
    UITextField *speedField = [[UITextField alloc] init];
    speedField.autocorrectionType = UITextAutocorrectionTypeNo;
    speedField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    speedField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    speedField.backgroundColor = [UIColor secondarySystemGroupedBackgroundColor];
    speedField.tag = 103;
    [speedField setPasteDelegate:self];
    [speedField addTarget:self action:@selector(textFieldDidChangeCoord:) forControlEvents:UIControlEventEditingChanged];
    
    [self.speedFCAlert addTextFieldWithCustomTextField:speedField andPlaceholder:@"Example: 10.0 Km/h" andTextReturnBlock:^(NSString *text) {
        newSpeed = text;
    }];
    
    [self.speedFCAlert showAlertInView:[UIViewController spxoverlay_topMostController]
                          withTitle:@"Custom Speed"
                       withSubtitle:speedmsg
                    withCustomImage:[AssetsManager imageForResource:@"man-running"]
                withDoneButtonTitle:@"Done"
                         andButtons:@[@"Cancel"]];
    
    [self.speedFCAlert doneActionBlock:^{
        [self validateSpeed:newSpeed];
    }];
}

- (void)textPasteConfigurationSupporting:(id<UITextPasteConfigurationSupporting>)textPasteConfigurationSupporting transformPasteItem:(id<UITextPasteItem>)item API_AVAILABLE(ios(11.0)) {
    [self paste:textPasteConfigurationSupporting];
}

- (void)paste:(id)sender {
    UIPasteboard *gpBoard = [UIPasteboard generalPasteboard];
    if ([gpBoard hasStrings] == YES)
    {
        UITextField *txtfield = (UITextField*)sender;
        if (txtfield.tag == 103)
        {
            NSCharacterSet *setToRemove =
            [NSCharacterSet characterSetWithCharactersInString:@"0123456789."];
            NSCharacterSet *setToKeep = [setToRemove invertedSet];
            NSString *newString =
            [[[gpBoard string] componentsSeparatedByCharactersInSet:setToKeep]
             componentsJoinedByString:@""];
            [self.speedFCAlert.textField insertText:newString];
        }
    }
}

-(void)textFieldDidChangeCoord:(UITextField*)field {
    NSString *usp = field.text;
    NSString* trimusp = [usp stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    double uspd = [trimusp doubleValue];
    NSString *msg;
    if (uspd > 0 && uspd < 120) {
        msg = [NSString stringWithFormat:@"New speed is %.1f Km/h",uspd];
    }
    else
    {
        msg = @"Abnormal Speed Detected, Please use proper speed.";
        field.text = @"";
    }
    
    self.speedFCAlert.subTitle = msg;
}

- (void)validateSpeed:(NSString*)str {
    NSString* trimusp = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    double uspd = [trimusp doubleValue];
    if (uspd > 0 && uspd < 120) {
        [Preferences setUserSpeed:uspd];
    }
}


@end


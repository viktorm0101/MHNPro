//
//  ProxyDelegate.m
//  igppogosl
//
//  Created by c0pyn1nja on 7/7/20
//  
//

#import "ProxyDelegate.h"
#import <objc/runtime.h>
#import "SpooferxManager.h"

@interface CLLocationManager (MockingLocation)

- (void)custom_startUpdatingLocation;
- (void)custom_stopUpdatingLocation;
- (void)custom_setDelegate:(id<CLLocationManagerDelegate>)delegate;

@end

CLLocation *realLocation;
float x = 40.764892;
float y = -73.972669;
float xreal = 0;
float yreal = 0;
CLLocation *lastLocation;
CFTimeInterval lastLocationUpdate;

void spoof_updateLocations(id cls, SEL selector, CLLocationManager *locationManager, NSArray *locations);
void spoof_updateLocations(id cls, SEL selector, CLLocationManager *locationManager, NSArray *locations) {
    if (Preferences.fakeDelegate == nil) {
        Preferences.fakeDelegate = cls;
    }
    
    if (Preferences.fakeLocationManager == nil) {
        Preferences.fakeLocationManager = locationManager;
    }
    
    if (locations.count > 0) {
        realLocation = [locations lastObject];
        if (realLocation != nil) {
            if (Preferences.realLocationSet == NO) {
                Preferences.realLocationSet = YES;
                [Preferences setRealLatitude:realLocation.coordinate.latitude];
                [Preferences setRealLongitude:realLocation.coordinate.longitude];
            }
            
            if (Preferences.fakeLatitude == 0.0 || Preferences.fakeLongitude == 0.0)
            {
                [Preferences setFakeLatitude:realLocation.coordinate.latitude];
                [Preferences setFakeLongitude:realLocation.coordinate.longitude];
            }
        }
        else
        {
            realLocation = [[CLLocation alloc] initWithLatitude:Preferences.realLatitude longitude:Preferences.realLongitude];
        }
    }
    
    if (Preferences.enableFakeLocation == YES) {
        CLLocation *spoofedLocation = [[CLLocation alloc] initWithCoordinate:CLLocationCoordinate2DMake(Preferences.fakeLatitude, Preferences.fakeLongitude)
                                                                    altitude: [realLocation altitude]
                                                          horizontalAccuracy: [realLocation horizontalAccuracy]
                                                            verticalAccuracy: [realLocation verticalAccuracy]
                                                                      course: [realLocation course]
                                                                       speed: [realLocation speed]
                                                                   timestamp: [NSDate date]];
        
        if ([cls respondsToSelector:@selector(locationManager:oldDidUpdateLocations:)]){
            [cls performSelector:@selector(locationManager:oldDidUpdateLocations:) withObject:locationManager withObject:@[spoofedLocation]];
        }
    }
    else
    {
        [Preferences setRealLatitude:realLocation.coordinate.latitude];
        [Preferences setRealLongitude:realLocation.coordinate.longitude];
        
        [Preferences setFakeLatitude:realLocation.coordinate.latitude];
        [Preferences setFakeLongitude:realLocation.coordinate.longitude];
        
        if ([cls respondsToSelector:@selector(locationManager:oldDidUpdateLocations:)] == YES){
            [cls performSelector:@selector(locationManager:oldDidUpdateLocations:) withObject:locationManager withObject:locations];
        }
    }
    
    if (Preferences.fakePlacemark == nil) {
        CLGeocoder *reverseGeocoder = [[CLGeocoder alloc] init];
        CLLocation *loc = [[CLLocation alloc] initWithLatitude:Preferences.fakeLatitude longitude:Preferences.fakeLongitude];
        [reverseGeocoder reverseGeocodeLocation:loc completionHandler:^(NSArray *placemarks, NSError *error)
         {
            if (error == nil) {
                CLPlacemark *myPlacemark = [placemarks firstObject];
                if (myPlacemark != nil) {
                    Preferences.fakePlacemark = myPlacemark;
                }
            }
        }];
    }
}

@implementation CLLocationManager (MockingLocation)

static IMP old_updateLocationsMethod = NULL;
typedef void (*OriginalImpType)(id self, SEL selector);
static OriginalImpType originalImp;
static OriginalImpType originalImpStop;
static OriginalImpType originalImpDelegate;

-(void)custom_setDelegate:(id<CLLocationManagerDelegate>)delegate
{
    NSString *name = NSStringFromClass([delegate class]);
    if ([name isEqualToString:@"NIAIosLocationManager"] == YES)
    {
        if (Preferences.fakeDelegateSet == NO) {
            old_updateLocationsMethod = class_replaceMethod(object_getClass(delegate), @selector(locationManager:didUpdateLocations:),(IMP)spoof_updateLocations, "@:@@");
            class_addMethod(object_getClass(delegate),@selector(locationManager:oldDidUpdateLocations:), old_updateLocationsMethod, "@:@@");
            Preferences.fakeDelegateSet = YES;
            
            if ([SpooferxManager sharedManager].overlayView == nil) {
                [[SpooferxManager sharedManager] setupWithUnity:[[[UIApplication sharedApplication] delegate] window]];
                //[AutoBot startQueueTimer];
            }
        }
    }
    
    return [self custom_setDelegate:delegate];
}


+ (void) load
{
    Method m1 = class_getInstanceMethod(self, @selector(startUpdatingLocation));
    Method m2 = class_getInstanceMethod(self, @selector(custom_startUpdatingLocation));
    originalImp = (OriginalImpType)method_getImplementation(m1);
    Preferences.originalImp = originalImp;
    method_exchangeImplementations(m1, m2);
    
    Method s1 = class_getInstanceMethod(self, @selector(stopUpdatingLocation));
    Method s2 = class_getInstanceMethod(self, @selector(custom_stopUpdatingLocation));
    originalImpStop = (OriginalImpType)method_getImplementation(s1);
    Preferences.originalImpStop = originalImpStop;
    method_exchangeImplementations(s1, s2);
    
    Method d1 = class_getInstanceMethod(self, @selector(setDelegate:));
    Method d2 = class_getInstanceMethod(self, @selector(custom_setDelegate:));
    originalImpDelegate = (OriginalImpType)method_getImplementation(d1);
    Preferences.originalImpDelegate = originalImpDelegate;
    method_exchangeImplementations(d1, d2);
}

-(void)custom_stopUpdatingLocation
{
    originalImpStop(self, @selector(stopUpdatingLocation));
}

-(void)custom_startUpdatingLocation
{
    originalImp(self, @selector(startUpdatingLocation));
}

@end

@implementation CLLocation (Swizzle)

id thisClass;

+ (void) load {
    
    thisClass = self;
    Method m1 = class_getInstanceMethod(self, @selector(coordinate));
    Method m2 = class_getInstanceMethod(self, @selector(coordinate_));
    method_exchangeImplementations(m1, m2);
}

-(CLLocationAccuracy) horizontalAccuracy_ {
    
    CLLocationAccuracy pos = [self horizontalAccuracy_];
    
    return pos;
}

-(CLLocationDistance) altitude_ {
    
    CLLocationDistance pos = [self altitude_];
    return pos;
    
}

-(CLLocationSpeed) speed_ {
    
    CLLocationSpeed pos = [self speed_];
    
    if (lastLocation) {
        
        CLLocation *l = [[CLLocation alloc] initWithLatitude:x longitude:y];
        CLLocationDistance d = [l distanceFromLocation:lastLocation];
        
        
        pos = d / (CACurrentMediaTime() - lastLocationUpdate);
    }
    
    lastLocation = [[CLLocation alloc] initWithLatitude:x longitude:y];
    lastLocationUpdate = CACurrentMediaTime();
    
    return pos;
}

- (CLLocationCoordinate2D) coordinate_ {
    
    CLLocationCoordinate2D pos = [self coordinate_];
    if (Preferences.cllocationChange == YES) {
        if (Preferences.enableFakeLocation == YES)
        {
            if (Preferences.fakeLatitude == 0)
            {
                Preferences.fakeLatitude = pos.latitude;
                Preferences.fakeLongitude = pos.longitude;
                
                return pos;
            }
            else
            {
                
                return CLLocationCoordinate2DMake(Preferences.fakeLatitude, Preferences.fakeLongitude);
            }
        }
        else
        {
            if(Preferences.fakeLatitude == 0)
            {
                Preferences.fakeLatitude = pos.latitude;
                Preferences.fakeLongitude = pos.longitude;
            }
            
            Preferences.realLatitude = pos.latitude;
            Preferences.realLongitude = pos.longitude;
            
            return pos;
        }
    }
    else
    {
        return pos;
    }
}

@end

@implementation ProxyDelegate

@end

//
//  SpooferxManager.h
//  spooferxsl
//
//  Created by spooferx on 10/26/20
//  
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, UserState) {
    UserStateStill,
    UserStateWalking,
    UserStatePatroling,
    UserStateWalkingToLocation,
    UserStateSniping,
    UserStateRouting
};

@interface SpooferxManager : NSObject

@property (nonatomic, strong) UIView *overlayView;
@property (nonatomic, assign) BOOL interfaceHidden;
@property (nonatomic, assign) UserState currentState;

+ (SpooferxManager *)sharedManager;

- (void)setupWithUnity:(UIView*)view;
- (void)reloadMenu;
- (void)startAutoWalkTo:(NSString*)route drawn:(BOOL)drawn;
- (void)teleportTo:(CLLocationCoordinate2D)location;
- (void)walkToLocation:(CLLocationCoordinate2D)location;
- (void)tapToWalkLat:(double)lat Lng:(double)lng;
- (void)showJoystick;
- (void)hideJoystick;
- (void)showControls;
- (void)hideControls;

- (void)jumpFromURL:(NSString*)coords;

- (void)presentTextNotification:(NSString*)line1 line2:(NSString*)line2;
- (void)processConfigFile:(NSURL*)url;

- (void)pauseAutoWalk;
- (void)resumeAutoWalk;

- (void)jumpToLocation:(CLLocationCoordinate2D)location;
- (void)enableGpxButton;
- (void)disableGpxButton;
- (void)enablePatrolButton;
- (void)disablePatrolButton;

- (void)ShowTeleportOptions:(CLLocationCoordinate2D)destination;


@end

NS_ASSUME_NONNULL_END

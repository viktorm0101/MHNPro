//
//  HotspotsManager.h
//  SDWebImage
//
//  Created by spooferx on 12/3/20
//  Copyright © 2020 Dailymotion. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HotspotsManager : NSObject

+ (HotspotsManager *)sharedManager;

- (void)loadPlaces;
- (void)savePlaces;
- (void)loadNests;

- (NSMutableArray*)get_userPlaces;
- (NSMutableArray*)get_spxPlaces;
- (NSMutableArray*)get_spxNests;

- (void)add_userPlace:(CLLocationCoordinate2D)coords;
- (void)remove_userPlace:(NSDictionary*)place;

- (NSString*)exportUserHotspots;
- (void)importHotspotsFile:(NSDictionary*)spotsdict;

@end

NS_ASSUME_NONNULL_END

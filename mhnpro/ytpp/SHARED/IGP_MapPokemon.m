//
//  IGP_MapPokemon.m
//  igppogosl
//
//  Created by c0pyn1nja on 03/02/21
//  
//

#import "IGP_MapPokemon.h"
#import "NearbyDataManager.h"
#import "IGP_InventoryManager.h"

@implementation IGP_MapPokemon

- (IGP_MapPokemon*)initWithWildPokemon:(WildPokemonProto*)wwpokemon
{
    if (self = [super init])
    {
        if (wwpokemon.hasPokemon)
        {
            self.hasWildPokemon = YES;
            self.mlatitude = wwpokemon.latitude;
            self.mlongitude = wwpokemon.longitude;
            CLLocationCoordinate2D destination = [SxUtilities CLLocationCoordinate2DMakeNormal:wwpokemon.latitude lon:wwpokemon.longitude];
            double distance = [SxUtilities distanceBetweenLocations:destination];
            self.mdistanceInMeters = distance;
            self.mencounterId = wwpokemon.encounterId;
            self.mpokemonId = wwpokemon.pokemon.pokemonId;
            self.isPokemonCaptured = [Inventory isPokemonCaptured:self.mpokemonId];
            self.pokemonName = [SxUtilities nameForPokemon2:wwpokemon.pokemon.pokemonId form:wwpokemon.pokemon.pokemonDisplay.form];
            self.type1 = [SxUtilities type1ForPokemon2:wwpokemon.pokemon.pokemonId form:wwpokemon.pokemon.pokemonDisplay.form];
            self.type2 = [SxUtilities type2ForPokemon2:wwpokemon.pokemon.pokemonId form:wwpokemon.pokemon.pokemonDisplay.form];
            self.genderString = @"";
            if (wwpokemon.pokemon.pokemonDisplay.gender == 1) {
                self.genderString = @" - ♂";
            }
            else if (wwpokemon.pokemon.pokemonDisplay.gender == 2)
            {
                self.genderString = @" - ♀";
            }
            self.wpokemon = wwpokemon;
            
            return self;
        }
    }
    
    return nil;
}

- (IGP_MapPokemon*)initWithNearbyPokemon:(NearbyPokemonProto*)nnpokemon
{
    if (self = [super init])
    {
        PokemonFortProto *mfort = [NearbyMan GetFortForFortId:nnpokemon.fortId];
        if (mfort != nil)
        {
            self.hasNearbyPokemon = YES;
            self.mdistanceInMeters = nnpokemon.distanceMeters;
            CLLocationCoordinate2D randomAroundFort = [SxUtilities CLLocationCoordinate2DMakeRandom:mfort.latitude lon:mfort.longitude str:2];
            self.mlatitude = randomAroundFort.latitude;
            self.mlongitude = randomAroundFort.longitude;
            self.mpokemonId = nnpokemon.pokedexNumber;
            self.isPokemonCaptured = [Inventory isPokemonCaptured:self.mpokemonId];
            self.mencounterId = nnpokemon.encounterId;
            self.pokemonName = [SxUtilities nameForPokemon2:nnpokemon.pokedexNumber form:nnpokemon.pokemonDisplay.form];
            self.type1 = [SxUtilities type1ForPokemon2:nnpokemon.pokedexNumber form:nnpokemon.pokemonDisplay.form];
            self.type2 = [SxUtilities type2ForPokemon2:nnpokemon.pokedexNumber form:nnpokemon.pokemonDisplay.form];
            self.genderString = @"";
            if (nnpokemon.pokemonDisplay.gender == 1) {
                self.genderString = @" - ♂";
            }
            else if (nnpokemon.pokemonDisplay.gender == 2)
            {
                self.genderString = @" - ♀";
            }
            self.npokemon = nnpokemon;
            return self;
        }
    }
    
    return nil;
}

//- (PokemonFortProto*)GetFortForMon:(NearbyPokemonProto*)pokemon
//{
//    PokemonFortProto *fort = [NearbyMan.gmoForts objectForKey:pokemon.fortId];
//    return fort;
//}

@end

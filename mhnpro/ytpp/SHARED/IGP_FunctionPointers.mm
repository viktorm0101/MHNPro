//
//  IGP_FunctionPointers.m
//  igppogosl
//
//  Created by igppogo on 5/13/20.
//

#import "IGP_FunctionPointers.h"
#import "Constants.h"
#include <dlfcn.h>

struct Il2CppClass_IG;
struct Il2CppObject_IG;

typedef Il2CppDomain *    (* il2cpp_domain_get_f)           ();
typedef Il2CppImage*      (* il2cpp_get_corlib_f)   ();
typedef Il2CppAssembly*   (* il2cpp_domain_assembly_open_f) (Il2CppDomain* domain, const char* name);
typedef Il2CppImage*      (* il2cpp_assembly_get_image_f)   (Il2CppAssembly* assembly);
typedef Il2CppClass_IG*   (* il2cpp_class_from_name_f)   (const Il2CppImage * image, const char* namespaze, const char *name);
typedef Il2CppObject_IG*  (* il2cpp_object_new_f)        (const Il2CppClass_IG * klass);
typedef Il2CppObject_IG*     (* il2cpp_runtime_invoke_f)       (MethodInfo* method, void* obj, void** params, Il2CppObject* exec);
typedef Il2CppString*     (* il2cpp_string_new_f)           (const char* str);
typedef uint32_t    (* il2cpp_method_get_param_count_f)     (const MethodInfo *method);
typedef MethodInfo* (* il2cpp_class_get_method_from_name_f) (Il2CppClass_IG *klass, const char* name, int argsCount);
typedef Il2CppThread* (* il2cpp_thread_attach_f) (Il2CppDomain* domain);

static il2cpp_domain_get_f            ig_il2cpp_domain_get;
static il2cpp_assembly_get_image_f    ig_il2cpp_assembly_get_image;
static il2cpp_class_from_name_f       ig_il2cpp_class_from_name;
static il2cpp_object_new_f            ig_il2cpp_object_new;
static il2cpp_domain_assembly_open_f  ig_il2cpp_domain_assembly_open;
static il2cpp_runtime_invoke_f        ig_il2cpp_runtime_invoke;
//static il2cpp_get_corlib_f            ig_il2cpp_get_corlib;
static il2cpp_string_new_f            ig_il2cpp_string_new;
static il2cpp_class_get_method_from_name_f  ig_il2cpp_class_get_method_from_name;
static il2cpp_method_get_param_count_f  ig_il2cpp_method_get_param_count;
static il2cpp_thread_attach_f         ig_il2cpp_thread_attach;

Il2CppDomain *dom;

@interface IGP_FunctionPointers ()

@end

@implementation IGP_FunctionPointers

+ (IGP_FunctionPointers *)sharedManager
{
    static IGP_FunctionPointers * sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

-(id)init
{
    if (self = [super init])
    {
    }
    
    return self;
}

- (void)SetUp
{
    ig_il2cpp_domain_get = (il2cpp_domain_get_f)dlsym(RTLD_DEFAULT, "il2cpp_domain_get");
    //DBG(@"current il2cpp_domain_get: %p",ig_il2cpp_domain_get);
    ig_il2cpp_domain_assembly_open = (il2cpp_domain_assembly_open_f)dlsym(RTLD_DEFAULT, "il2cpp_domain_assembly_open");
    //DBG(@"current il2cpp_domain_assembly_open: %p",ig_il2cpp_domain_assembly_open);
    ig_il2cpp_assembly_get_image = (il2cpp_assembly_get_image_f)dlsym(RTLD_DEFAULT, "il2cpp_assembly_get_image");
    //DBG(@"current ig_il2cpp_assembly_get_image: %p",ig_il2cpp_assembly_get_image);
    ig_il2cpp_object_new = (il2cpp_object_new_f)dlsym(RTLD_DEFAULT, "il2cpp_object_new");
    //DBG(@"current il2cpp_object_new: %p",ig_il2cpp_object_new);
    ig_il2cpp_class_from_name = (il2cpp_class_from_name_f)dlsym(RTLD_DEFAULT, "il2cpp_class_from_name");
    //DBG(@"current il2cpp_class_from_name: %p",ig_il2cpp_class_from_name);
    ig_il2cpp_class_get_method_from_name = (il2cpp_class_get_method_from_name_f)dlsym(RTLD_DEFAULT, "il2cpp_class_get_method_from_name");
    ig_il2cpp_method_get_param_count = (il2cpp_method_get_param_count_f)dlsym(RTLD_DEFAULT, "il2cpp_method_get_param_count");
    //DBG(@"current il2cpp_class_from_name: %p",ig_il2cpp_class_from_name);
    ig_il2cpp_runtime_invoke = (il2cpp_runtime_invoke_f)dlsym(RTLD_DEFAULT, "il2cpp_runtime_invoke");
    //DBG(@"current il2cpp_runtime_invoke: %p",ig_il2cpp_runtime_invoke);
    ig_il2cpp_string_new = (il2cpp_string_new_f)dlsym(RTLD_DEFAULT, "il2cpp_string_new");
    //DBG(@"current ig_il2cpp_string_new: %p",ig_il2cpp_string_new);
    ig_il2cpp_thread_attach = (il2cpp_thread_attach_f)dlsym(RTLD_DEFAULT, "il2cpp_thread_attach");
    dom = ig_il2cpp_domain_get();
    ig_il2cpp_thread_attach(dom);
    DBG(@"IL2Functions Ready");
//    void *ep = [self GetEncounterProto];
//    DBG(@"GetEncounterProto -> %p",ep);
//    void *cpp = [self GetCatchPokemonProto];
//    DBG(@"GetCatchPokemonProto -> %p",cpp);
//    void *arpp = [self GetARPlusValuesProto];
//    DBG(@"GetARPlusValuesProto -> %p",arpp);
//    void *fsp = [self GetFortSearchProto];
//    DBG(@"GetFortSearchProto -> %p",fsp);
//    Il2CppString *ilstr = (Il2CppString*)[self GetIl2String:"C0PYN1NJA"];
//    DBG(@"Il2CppString -> %p",ilstr);
//    NSString *strfilstr = [Utilities NSStringFromIl2Cpp:(const ushort*)ilstr->chars length:ilstr->length];
//    DBG(@"Il2CppString -> %@",strfilstr);
}

- (void*)GetIl2StringFromNSString:(NSString*)str
{
    void *ilstr = ig_il2cpp_string_new([str UTF8String]);
    if(ilstr == nullptr)
    {
        ilstr = [self GetIl2StringFromNSStringFallback:str];
    }
    return ilstr;
}

- (void*)GetIl2StringFromNSStringFallback:(NSString*)str
{
    const char* strch = [str cStringUsingEncoding:NSUTF16LittleEndianStringEncoding];
    Il2CppString *quid = SS_Ctor(strch);
    return quid;
}

- (NSString*)GetNSStringFromIl2String:(void*)str {
    if(str != nullptr) {
        Il2CppString *str1 = (Il2CppString*)str;
        return [SxUtilities NSStringFromIl2Cpp:(const ushort*)str1->chars length:str1->length];
    }
    
    return @"";
}


@end


//
//  SpooferxManager.m
//  spooferxsl
//
//  Created by spooferx on 10/26/20
//  
//

#import "SpooferxManager.h"

#import "HotspotsManager.h"
#import "RpcManager.h"
#import "GPX.h"

#import "DWBubbleMenuButton.h"
#import "BubbleMenu.h"
#import "RoutesManager.h"
#import "IGP_PlacesManager.h"
#import "IGP_LocationSimulator.h"
#import "IGP_NotificationsManager.h"

CGSize displaySize;
CGPoint touchPoint;
UIView *touchView;
UITouch *touchTouch;

BubbleMenuButton *patrolButton;
BubbleMenuButton *gpxButton;

@interface DWBubbleMenuButton ()
@property (nonatomic, assign) CGRect originFrame;
@end

@interface SpooferxManager () <UITabBarControllerDelegate,UITextPasteDelegate>

@property (nonatomic, strong) JoystickController *joyStick;
@property (nonatomic, assign) BOOL autoWalkPaused;
@property (nonatomic, assign) BOOL walkToPaused;
@property (nonatomic, strong) UIStoryboard *story;
@property (nonatomic, strong) DWBubbleMenuButton *bubbleMenuButtons;
@property (nonatomic, strong) NSMutableArray *buttonsArray;
@property (nonatomic, strong) UIAlertController *speedAlert;
@property (nonatomic, strong) UIAlertController *teleportAlert;
@property (nonatomic, strong) UILongPressGestureRecognizer *longPress;


@end

@implementation SpooferxManager {
    
}

+ (SpooferxManager *)sharedManager
{
    static SpooferxManager * sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
        
    });
    return sharedMyManager;
}

-(id)init
{
    if (self = [super init])
    {
        self.interfaceHidden = YES;
        displaySize = [[UIScreen mainScreen] bounds].size;
        self.buttonsArray = [[NSMutableArray alloc] init];
        self.story = [AssetsManager mainStoryboard];
        [self setupControls];
    }
    
    return self;
}

- (void)setupControls
{
    if (self.joyStick == nil)
    {
        CGPoint joystickOrigin = CGPointFromString(Preferences.joystickFrame);
        if (joystickOrigin.x < 10 || joystickOrigin.x > displaySize.width-10) {
            joystickOrigin = CGPointMake(displaySize.width-125, displaySize.height-225);
        }
        
        if (joystickOrigin.y < 64 || joystickOrigin.y > displaySize.height-64) {
            joystickOrigin = CGPointMake(displaySize.width-125, displaySize.height-225);
        }
        NSString *joystickOriginFrame = NSStringFromCGPoint(joystickOrigin);
        [Preferences setJoystickFrame:joystickOriginFrame];
        self.joyStick =  [LocationSimulator joyStick];
    }
    
    self.longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    [self.longPress setMinimumPressDuration:2.0];
    
    [self addObservers];
}

- (BOOL)isBuddyPokemon
{
    return [SxUtilities isLegendaryPokemon];
}

- (BOOL)isLegendaryPokemon
{
    return [Preferences enableFakeLocation];
}

#pragma mark KVO

- (void)addObservers
{
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    
    [audioSession setActive:YES error:nil];
    [audioSession addObserver:self
                   forKeyPath:@"outputVolume"
                      options:0
                      context:nil];
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    DBG(@" =%@ =%@ =: %@", keyPath, object, change);
    id newValue = [change objectForKey:NSKeyValueChangeNewKey];
    DBG(@" new value=: %@", newValue);
    
    if ([keyPath isEqual:@"outputVolume"] == YES)
    {
        if (self.interfaceHidden == YES) {
            [self showControls];
        }
        else
        {
            [self hideControls];
        }
    }
}

- (void)showControls
{
    [UIView animateWithDuration:0.3 animations:^{
        [self showMenu];
        [self showJoystick];
    }];
    
    self.interfaceHidden = NO;
}

- (void)hideControls
{
    [UIView animateWithDuration:0.3 animations:^{
        [self hideMenu];
        [self hideJoystick];
    }];
    
    self.interfaceHidden = YES;
}

#pragma mark - BubbleMenu
- (void)setupButtons
{
    BubbleMenuButton *settings = [[BubbleMenuButton alloc] initWithStyle:BubbleMenuButtonStyleTap
                                                                   image:[AssetsManager imageForResource:@"settings"]
                                                                   title:@"Settings"];
    settings.tapBlock = ^(BubbleMenuButton *button)
    {
        [self showSettingsController];
    };
    
    BubbleMenuButton *tohomeButton = [[BubbleMenuButton alloc] initWithStyle:BubbleMenuButtonStyleTap
                                                                       image:[AssetsManager imageForResource:@"gohome"]
                                                                       title:@"ToHome"];
    tohomeButton.tapBlock = ^(BubbleMenuButton *button)
    {
        [self showToHomeController];
    };
    
    BubbleMenuButton *speed_change = [[BubbleMenuButton alloc] initWithStyle:BubbleMenuButtonStyleTap
                                                                       image:[AssetsManager imageForResource:@"speed"]
                                                                       title:@"Speed Change"];
    speed_change.tapBlock = ^(BubbleMenuButton *button)
    {
        [self changeWalkingSpeed];
    };
    
    BubbleMenuButton *hotspots = [[BubbleMenuButton alloc] initWithStyle:BubbleMenuButtonStyleTap
                                                                   image:[AssetsManager imageForResource:@"gps"]
                                                                   title:@"Hotspots"];
    hotspots.tapBlock = ^(BubbleMenuButton *button)
    {
        [self showHotspotsController];
    };
    
    
    gpxButton = [[BubbleMenuButton alloc] initWithStyle:BubbleMenuButtonStyleActivate
                                                  image:[AssetsManager imageForResource:@"route"]
                                                  title:@"Routes"];
    gpxButton.activationBlock = ^(BubbleMenuButton *button)
    {
        [self showRoutesController];
        [gpxButton disableButton];
    };
    gpxButton.deactivationBlock = ^(BubbleMenuButton *button)
    {
        [self stopAutoWalk];
    };
    
    
    if (self.buttonsArray.count > 0) {
        [self.buttonsArray removeAllObjects];
    }
    
    if (displaySize.height <= 667.0) {
        [self.buttonsArray addObject:settings];
        
        if ([self isLegendaryPokemon] == YES) {
            [self.buttonsArray addObject:speed_change];
            [self.buttonsArray addObject:hotspots];
            [self.buttonsArray addObject:gpxButton];
        }
    }
    else
    {
        [self.buttonsArray addObject:settings];
        
        if ([self isLegendaryPokemon] == YES) {
            [self.buttonsArray addObject:tohomeButton];
            [self.buttonsArray addObject:speed_change];
            [self.buttonsArray addObject:hotspots];
            [self.buttonsArray addObject:gpxButton];
        }
    }
    
    for (UIButton *button in self.buttonsArray) {
        button.frame = CGRectMake(0.f, 0.f, 40.0f, 40.f);
    }
    
    [self setupMenu];
}

- (void)setupMenu
{
    if (self.bubbleMenuButtons != nil) {
        [self.bubbleMenuButtons removeFromSuperview];
        self.bubbleMenuButtons = nil;
    }
    
    CGPoint savedMenuOrigin = CGPointFromString(Preferences.menuFrame);
    if (savedMenuOrigin.x == 0)
    {
        savedMenuOrigin = CGPointFromString(@"{10,64}");
    }
    
    if (savedMenuOrigin.x < 0 || savedMenuOrigin.y < 64) {
        savedMenuOrigin = CGPointMake(10, 64);
        Preferences.menuFrame = NSStringFromCGPoint(savedMenuOrigin);
    }
    
    if (savedMenuOrigin.x > displaySize.width - 40 || savedMenuOrigin.y > displaySize.height - 40) {
        savedMenuOrigin = CGPointMake(10, 64);
        Preferences.menuFrame = NSStringFromCGPoint(savedMenuOrigin);
    }
    
    self.bubbleMenuButtons = [[DWBubbleMenuButton alloc] initWithFrame:CGRectMake(savedMenuOrigin.x, savedMenuOrigin.y,40,40)
                                                    expansionDirection:DirectionDown];
    
    UIImageView *homeimage = [[UIImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, 40.f, 40.f)];
    homeimage.image = AssetsManager.sppLogo;
    homeimage.contentMode = UIViewContentModeScaleAspectFit;
    homeimage.layer.borderWidth = 1.5f;
    homeimage.layer.borderColor = [UIColor secondarySystemGroupedBackgroundColor].CGColor;
    homeimage.layer.cornerRadius = homeimage.frame.size.height / 2.f;
    homeimage.center = CGPointMake(homeimage.frame.size.width/2, homeimage.frame.size.height/2);
    homeimage.tintColor = [SxUtilities ThemeColor];
    homeimage.backgroundColor = [[UIColor secondarySystemGroupedBackgroundColor] colorWithAlphaComponent:.75f];
    homeimage.clipsToBounds = YES;
    
    self.bubbleMenuButtons.homeButtonView = homeimage;
    
    self.bubbleMenuButtons.collapseAfterSelection = NO;
    self.bubbleMenuButtons.buttonSpacing = 10.0f;
    
    [self.bubbleMenuButtons enableDragging];
    [self.bubbleMenuButtons setCagingArea:CGRectMake(10, 64, self.overlayView.frame.size.width - 10, self.overlayView.frame.size.height - 100)];
    
    self.bubbleMenuButtons.draggingEndedBlock = ^void (UIView * menuButton)
    {
        DWBubbleMenuButton *button = (DWBubbleMenuButton *)menuButton;
        CGRect newFrame = button.originFrame;
        newFrame.origin.x = button.frame.origin.x;
        newFrame.origin.y = button.frame.origin.y;
        
        button.originFrame = newFrame;
        
        NSString *originMenuFrame = NSStringFromCGPoint(button.frame.origin);
        [Preferences setMenuFrame:originMenuFrame];
    };
    
    if (self.currentState == UserStatePatroling) {
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [patrolButton enableButton];
        });
    }
    
    [self.bubbleMenuButtons addButtons:self.buttonsArray];
    [self.overlayView addSubview:self.bubbleMenuButtons];
}

- (void)showMenu
{
    self.bubbleMenuButtons.alpha = 1.0;
}

- (void)hideMenu
{
    self.bubbleMenuButtons.alpha = 0.0;
}

- (void)enableGpxButton
{
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //Background Thread
        dispatch_async(dispatch_get_main_queue(), ^(void){
            //Run UI Updates
            [gpxButton enableButton];
        });
    });
}

- (void)disableGpxButton
{
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //Background Thread
        dispatch_async(dispatch_get_main_queue(), ^(void){
            //Run UI Updates
            [gpxButton disableButton];
        });
    });
}

- (void)enablePatrolButton
{
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //Background Thread
        dispatch_async(dispatch_get_main_queue(), ^(void){
            //Run UI Updates
            [patrolButton enableButton];
        });
    });
}

- (void)disablePatrolButton
{
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //Background Thread
        dispatch_async(dispatch_get_main_queue(), ^(void){
            //Run UI Updates
            [patrolButton disableButton];
        });
    });
}

#pragma mark - JoyStick

- (void)showJoystick
{
    if (Preferences.showJoystick == YES && [self isLegendaryPokemon] == YES) {
        self.joyStick.alpha = 1.0;
    }
}

- (void)hideJoystick
{
    self.joyStick.alpha = 0.0;
}

#pragma mark - Touch

- (void)setupWithUnity:(UIView*)view {
    if(view) {
        self.overlayView = view;
        
        [self setupButtons];
        [self.overlayView addSubview:self.joyStick];
        [self hideJoystick];
        [self.overlayView addGestureRecognizer:self.longPress];
        
        [RpcManager downloadSpawnPoints];
    }
}

- (void)reloadMenu
{
    if (self.overlayView != nil) {
        [self setupButtons];
    }
}

- (void)showSettingsController
{
    UINavigationController *nav = [self.story instantiateViewControllerWithIdentifier:@"SPP_SettingsViewController_NAV"];
    nav.navigationBar.translucent = NO;
    nav.modalPresentationStyle = UIModalPresentationFullScreen;
    [[UIViewController spxoverlay_topMostController] presentViewController:nav animated:YES completion:nil];
}

- (void)showToHomeController
{
    CLLocationCoordinate2D destination = [SxUtilities CLLocationCoordinate2DMakeNormal:Preferences.realLatitude lon:Preferences.realLongitude];
    NSString *cdstr = [SxUtilities cooldownStringForDestination:destination];
    
    NSString *msg;
    msg = [NSString stringWithFormat:@"Teleport to Real Location:\n %f,%f\n%@",Preferences.realLatitude,Preferences.realLongitude,cdstr];
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@"Real Location"
                                message:msg
                                preferredStyle:UIAlertControllerStyleAlert];
    alert.view.tintColor = [SxUtilities ThemeColor];
    UIAlertAction* teleport_action = [UIAlertAction actionWithTitle:@"Teleport to Location" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action)
                                      {
        [self teleportTo:destination];
    }];
    
    UIAlertAction* walk_action = [UIAlertAction actionWithTitle:@"Walk to Location" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
        [self walkToLocation:destination];
    }];
    
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction * action)
                             {
    }];
    
    [alert addAction:teleport_action];
    [alert addAction:walk_action];
    [alert addAction:cancel];
    
    dispatch_async(dispatch_get_main_queue(), ^(void){
        UIViewController *topController = [UIViewController spxoverlay_topMostController];
        [topController presentViewController:alert
                                    animated:YES
                                  completion:nil];
    });
}


- (void)validateSpeed:(NSString*)str
{
    NSString* trimusp = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    double uspd = [trimusp doubleValue];
    if (uspd > 0 && uspd < 120) {
        [Preferences setUserSpeed:uspd];
    }
}

- (void)changeWalkingSpeed
{
    NSString *speedmsg =[NSString stringWithFormat:@"Current speed is %.1f Km/h",Preferences.userSpeed];
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@"Custom Speed"
                                message:speedmsg
                                preferredStyle:UIAlertControllerStyleAlert];
    alert.view.tintColor = [SxUtilities ThemeColor];
    UIAlertAction* done_action = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
        NSString *coords = alert.textFields[0].text;
        [self validateSpeed:coords];
    }];
    
    
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction * action)
                             {
    }];
    
    [done_action setValue:[SxUtilities ThemeColor] forKey:@"titleTextColor"];
    
    [alert addAction:done_action];
    [alert addAction:cancel];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Example: 3.6 Km/h";
        textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        textField.tag = 100;
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 11.0)
        {
            [textField setPasteDelegate:self];
        }
        [textField addTarget:self action:@selector(textFieldDidChangeSpeed:) forControlEvents:UIControlEventEditingChanged];
    }];
    
    self.speedAlert = alert;
    [[UIViewController spxoverlay_topMostController] presentViewController:alert animated:YES completion:nil];
}

- (void)showHotspotsController
{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@"Go To Location?"
                                message:@"Example: 34.019455,-118.49119"
                                preferredStyle:UIAlertControllerStyleAlert];
    alert.view.tintColor = [SxUtilities ThemeColor];
    UIAlertAction* teleport_action = [UIAlertAction actionWithTitle:@"Teleport to Location" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action)
                                      {
        NSString *coords = alert.textFields[0].text;
        BOOL valid = [SxUtilities coordinatesValid:coords];
        if (valid == YES) {
            CLLocationCoordinate2D cords = [SxUtilities coordsFromString:coords];
            [self teleportTo:cords];
        }
    }];
    
    UIAlertAction* walk_action = [UIAlertAction actionWithTitle:@"Walk to Location" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
        NSString *coords = alert.textFields[0].text;
        BOOL valid = [SxUtilities coordinatesValid:coords];
        if (valid == YES) {
            CLLocationCoordinate2D cords = [SxUtilities coordsFromString:coords];
            [self walkToLocation:cords];
        }
    }];
    
    UIAlertAction *save_action = [UIAlertAction actionWithTitle:@"Save Location" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                  {
        NSString *coords = alert.textFields[0].text;
        BOOL valid = [SxUtilities coordinatesValid:coords];
        if (valid == YES) {
            CLLocationCoordinate2D cords = [SxUtilities coordsFromString:coords];
            [PlacesManager2 StoreUserLocation:cords];
        }
    }];
    
    UIAlertAction *hotspots = [UIAlertAction actionWithTitle:@"Show Hotspots" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self showPopularPlacesController];
    }];
    
    
    UIAlertAction* cancel_action = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                          handler:^(UIAlertAction * action)
                                    {
    }];
    
    [alert addAction:teleport_action];
    [alert addAction:walk_action];
    [alert addAction:save_action];
    [alert addAction:hotspots];
    [alert addAction:cancel_action];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Latitude,Longitude";
        textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        textField.tag = 101;
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 11.0)
        {
            [textField setPasteDelegate:self];
        }
        [textField addTarget:self action:@selector(textFieldDidChangeCoord:) forControlEvents:UIControlEventEditingChanged];
        
    }];
    
    self.teleportAlert = alert;
    dispatch_async(dispatch_get_main_queue(), ^(void){
        [[UIViewController spxoverlay_topMostController] presentViewController:alert animated:YES completion:nil];
    });
}

- (void)showPopularPlacesController
{
    UIStoryboard *story = [AssetsManager mainStoryboard];
    
    UINavigationController *nav1 = [story instantiateViewControllerWithIdentifier:@"PopularPlacesController_NAV"];
    nav1.tabBarItem.image = [AssetsManager imageForResource:@"map-pin-area-multiple"];
    nav1.tabBarItem.title = @"Popular";
    
    UINavigationController *nav2 = [story instantiateViewControllerWithIdentifier:@"EventPlacesController_NAV"];
    nav2.tabBarItem.image = [AssetsManager imageForResource:@"map-pin-area-multiple"];
    nav2.tabBarItem.title = @"Events";
    
    UINavigationController *nav3 = [story instantiateViewControllerWithIdentifier:@"UserPlacesController_NAV"];
    nav3.tabBarItem.image = [AssetsManager imageForResource:@"map-pin-area-multiple"];
    nav3.tabBarItem.title = @"User";
    
    UITabBarController *tabbarcon = [[UITabBarController alloc] initWithNibName:nil bundle:nil];
    
    tabbarcon.delegate = self;
    tabbarcon.viewControllers = [[NSArray alloc] initWithObjects:nav1, nav2, nav3, nil];
    tabbarcon.view.autoresizingMask=(UIViewAutoresizingFlexibleHeight);
    [tabbarcon.tabBar setTintColor:[SxUtilities ThemeColor]];
    tabbarcon.modalPresentationStyle = UIModalPresentationFullScreen;
    UIViewController *topController = [UIViewController spxoverlay_topMostController];
    [topController presentViewController:tabbarcon
                                animated:YES
                              completion:nil];
}

- (void)showRoutesController
{
    NSString *route = [[NSUserDefaults standardUserDefaults] objectForKey:@"SavedUnfinishedRoute"];
    UIAlertController *routeAlert = [UIAlertController alertControllerWithTitle:@"Route Options" message:@"Would you like to Resume, Generate or Select New route?" preferredStyle:UIAlertControllerStyleAlert];
    routeAlert.view.tintColor = [SxUtilities ThemeColor];
    UIAlertAction *resume = [UIAlertAction actionWithTitle:@"Resume Route" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self startAutoWalkTo:route drawn:NO];
    }];
    
    UIAlertAction *select = [UIAlertAction actionWithTitle:@"Select Route" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UINavigationController *nav1 = [self.story instantiateViewControllerWithIdentifier:@"SPP_RoutesViewController_NAV"];
        nav1.navigationBar.translucent = NO;
        nav1.modalPresentationStyle = UIModalPresentationFullScreen;
        [[UIViewController spxoverlay_topMostController] presentViewController:nav1 animated:YES completion:nil];
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    
    if([route length] > 0)
    {
        [routeAlert addAction:resume];
    }
    
    [routeAlert addAction:select];
    [routeAlert addAction:cancel];
    [[UIViewController spxoverlay_topMostController] presentViewController:routeAlert animated:YES completion:nil];
}

#pragma mark - Location Loops

- (void)tapToWalkLat:(double)lat Lng:(double)lng
{
    CLLocationCoordinate2D mapPoint = CLLocationCoordinate2DMake(lat,lng);
    [LocationSimulator WalkToLocationDirect:mapPoint];
}

- (void)walkToLocation:(CLLocationCoordinate2D)location
{
    [LocationSimulator WalkToLocationDirect:location];
}

- (void)teleportTo:(CLLocationCoordinate2D)location
{
    if (self.autoWalkPaused == NO) {
        [self stopAutoWalk];
    }
    
    [LocationSimulator TeleportToLocationNew:location];
}

- (void)jumpToLocation:(CLLocationCoordinate2D)location
{
    [LocationSimulator JumpToLocationNew:location];
}


- (void)jumpFromURL:(NSString*)coords
{
    bool validate = [SxUtilities coordinatesValid:coords];
    if (validate == YES)
    {
        CLLocationCoordinate2D destination = [SxUtilities coordsFromString:coords];
        NSString *cdstr = [SxUtilities cooldownStringForDestination:destination];
        NSString *msg = [NSString stringWithFormat:@"Go to Location %f,%f?\r\n%@",destination.latitude,destination.longitude,cdstr];
        
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            //Background Thread
            dispatch_async(dispatch_get_main_queue(), ^(void){
                //Run UI Updates
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Go To Location?" message:msg preferredStyle:UIAlertControllerStyleAlert];
                alert.view.tintColor = [SxUtilities ThemeColor];
                UIAlertAction *teleport = [UIAlertAction actionWithTitle:@"Teleport to Location" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [self teleportTo:destination];
                }];
                
                UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                    [alert dismissViewControllerAnimated:NO completion:nil];
                }];
                [teleport setValue:[SxUtilities ThemeColor] forKey:@"titleTextColor"];
                [alert addAction:teleport];
                [alert addAction:cancel];
                
                UIViewController *topController = [UIViewController spxoverlay_topMostController];
                [topController presentViewController:alert
                                            animated:YES
                                          completion:nil];
            });
        });
        
    }
    else
    {
        NSString *msg = [NSString stringWithFormat:@"Please check coordinates: %@",coords];
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            //Background Thread
            dispatch_async(dispatch_get_main_queue(), ^(void){
                //Run UI Updates
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Inavlid Coordinates" message:msg preferredStyle:UIAlertControllerStyleAlert];
                alert.view.tintColor = [SxUtilities ThemeColor];
                UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                    [alert dismissViewControllerAnimated:NO completion:nil];
                }];
                
                [alert addAction:cancel];
                
                UIViewController *topController = [UIViewController spxoverlay_topMostController];
                [topController presentViewController:alert
                                            animated:YES
                                          completion:nil];
            });
        });
    }
}

- (void)stopAutoWalk
{
    if (LocationSimulator.isDirectWalking == YES) {
        [LocationSimulator StopWalkToLocationDirect];
    }
}

- (void)textPasteConfigurationSupporting:(id<UITextPasteConfigurationSupporting>)textPasteConfigurationSupporting transformPasteItem:(id<UITextPasteItem>)item API_AVAILABLE(ios(11.0)) {
    [self paste:textPasteConfigurationSupporting];
}

- (void)paste:(id)sender {
    UIPasteboard *gpBoard = [UIPasteboard generalPasteboard];
    if ([gpBoard hasStrings])
    {
        UITextField *txtfield = (UITextField*)sender;
        if (txtfield.tag == 100) {
            NSCharacterSet *setToRemove =
            [NSCharacterSet characterSetWithCharactersInString:@"0123456789."];
            NSCharacterSet *setToKeep = [setToRemove invertedSet];
            NSString *newString =
            [[[gpBoard string] componentsSeparatedByCharactersInSet:setToKeep]
             componentsJoinedByString:@""];
            [self.speedAlert.textFields[0] insertText:newString];
        }
        else if (txtfield.tag == 101)
        {
            NSCharacterSet *setToRemove = [NSCharacterSet characterSetWithCharactersInString:@"0123456789.,-"];
            NSCharacterSet *setToKeep = [setToRemove invertedSet];
            NSString *newString = [[[gpBoard string] componentsSeparatedByCharactersInSet:setToKeep] componentsJoinedByString:@""];
            [self.teleportAlert.textFields[0] insertText:newString];
        }
    }
}

-(void)textFieldDidChangeSpeed:(UITextField*)field {
    
    @try {
        NSString *usp = field.text;
        NSString* trimusp = [usp stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        double uspd = [trimusp doubleValue];
        NSString *msg;
        if (uspd > 0 && uspd < 120) {
            msg = [NSString stringWithFormat:@"New speed is %.1f Km/h",uspd];
        }
        else
        {
            msg = @"Abnormal Speed Detected, Please use proper speed.";
            field.text = @"";
        }
        
        self.speedAlert.message = msg;
        
    } @catch (NSException *exception) {
        
        self.speedAlert.message = [NSString stringWithFormat:@"%@",exception.description];
    }
}

-(void)textFieldDidChangeCoord:(UITextField*)field {
    
    @try
    {
        NSString *msg;
        BOOL valid = [SxUtilities coordinatesValid:field.text];
        if (valid == YES) {
            CLLocationCoordinate2D cords = [SxUtilities coordsFromString:field.text];
            NSString *cdstr = [SxUtilities cooldownStringForDestination:cords];
            msg = [NSString stringWithFormat:@"Teleport to: %@?\n%@",field.text,cdstr];
        }
        else
        {
            msg = [NSString stringWithFormat:@"Invalid Coordinates %@",field.text];
        }
        
        self.teleportAlert.message = msg;
        
    } @catch (NSException *exception) {
        
        self.teleportAlert.message = @"";
    }
}

- (void)presentTextNotification:(NSString*)line1 line2:(NSString*)line2
{
    //    if(Preferences.enableNotifications == YES) {
    bool isX = false;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        CGSize screenSize = [[UIScreen mainScreen] nativeBounds].size;
        if (screenSize.height == 1792.0f ||
            screenSize.height == 2340.0f ||
            screenSize.height == 2436.0f ||
            screenSize.height == 2532.0f ||
            screenSize.height == 2556.0f ||
            screenSize.height == 2688.0f ||
            screenSize.height == 2778.0f ||
            screenSize.height == 2796.0f) {
            isX = true;
        }
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        UIView *notificationView = [[UIView alloc] initWithFrame:CGRectMake(0, -125, self.overlayView.frame.size.width, 110)];
        
        notificationView.backgroundColor = [UIColor secondarySystemGroupedBackgroundColor];
        notificationView.alpha = 0.85;
        
        UILabel *titleLabel;
        [titleLabel setTextColor:[UIColor labelColor]];
        UILabel *ivLabel;
        [ivLabel setTextColor:[UIColor labelColor]];
        
        if (line1.length > 0 && line2.length > 0) {
            titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 50, notificationView.frame.size.width, 25)];
            titleLabel.textAlignment = NSTextAlignmentCenter;
            [titleLabel setText:line1];
            
            ivLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 75, notificationView.frame.size.width, 25)];
            ivLabel.textAlignment = NSTextAlignmentCenter;
            [ivLabel setText:line2];
            
            [notificationView addSubview:titleLabel];
            [notificationView addSubview:ivLabel];
        } else {
            titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 47.5, self.overlayView.frame.size.width, 25)];
            titleLabel.textAlignment = NSTextAlignmentCenter;
            [titleLabel setText:line1];
            
            [notificationView addSubview:titleLabel];
        }
        
        [self.overlayView addSubview:notificationView];
        
        [UIView animateWithDuration:0.3 animations:^{
            if (isX == true) {
                notificationView.frame = CGRectOffset(notificationView.frame, 0, 125);
            } else {
                notificationView.frame = CGRectOffset(notificationView.frame, 0, 110);
            }
        }];
        
        int disapp = 1000;
        if ([line1 containsString:@"Captured"] || [line1 containsString:@"Ran Away"] || [line1 containsString:@"Feeding "]) {
            disapp = 1000;
        }
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(disapp * NSEC_PER_MSEC)), dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:0.3 animations:^{
                if (isX == true)
                {
                    notificationView.frame = CGRectOffset(notificationView.frame, 0, -125);
                }
                else
                {
                    notificationView.frame = CGRectOffset(notificationView.frame, 0, -100);
                }
            }];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [notificationView removeFromSuperview];
            });
        });
    });
    //   }
}
//REGEX USE
//NSString *myRegex = @"[A-Z0-9a-z_]*";
//NSPredicate *myTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", myRegex];
//NSString *string = nameField.text;
//BOOL valid = [myTest evaluateWithObject:string];


- (void)processConfigFile:(NSURL*)url
{
    
}

- (void)pauseAutoWalk
{
    [LocationSimulator PauseWalk];
}

- (void)resumeAutoWalk
{
    [LocationSimulator ResumeWalk];
}

//-(void)processTouch:(CGPoint)touchLocation :(UIView*)view :(UITouch*)touch
//{
//    touchPoint = touchLocation;
//    DBG(@"Touch Point -> %@",NSStringFromCGPoint(touchPoint));
//    touchView = view;
//    touchTouch = touch;
//}
//
//-(void)processTouch2:(CGPoint)touchLocation :(UIView*)view
//{
//    touchPoint = touchLocation;
//    touchView = view;
//}

-  (void)handleLongPress:(UILongPressGestureRecognizer*)sender {
    if (sender.state == UIGestureRecognizerStateEnded) {
        //DBG(@"UIGestureRecognizerStateEnded");
        //Do Whatever You want on End of Gesture
    }
    else if (sender.state == UIGestureRecognizerStateBegan){
        //DBG(@"UIGestureRecognizerStateBegan.");
        if(Preferences.showJoystick == YES && self.joyStick.alpha == 1.0)
        {
            CGPoint tpoint = [sender locationInView:sender.view];
            [UIView animateWithDuration:0.3 animations:^{
                [self hideJoystick];
                [self.joyStick removeFromSuperview];
                self.joyStick = [LocationSimulator joyStickForPoint:CGPointMake(tpoint.x-40, tpoint.y-40)];
                NSString *joystickOriginFrame = NSStringFromCGPoint(self.joyStick.frame.origin);
                [Preferences setJoystickFrame:joystickOriginFrame];
                [self.overlayView addSubview:self.joyStick];
                [self showJoystick];
            }];
        }
    }
}

- (void)ShowTeleportOptions:(CLLocationCoordinate2D)destination {
    
    //    if(Preferences.disableTeleDia == YES) {
    //        [self teleportTo:destination];
    //    } else {
    NSString *destistring = [NSString stringWithFormat:@"%f,%f",destination.latitude, destination.longitude];
    NSString *cdstring = [SxUtilities cooldownStringForDestination:destination];
    NSString *msg = [NSString stringWithFormat:@"Location: %@\n%@",destistring,cdstring];
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@"Teleport"
                                message:msg
                                preferredStyle:UIAlertControllerStyleAlert];
    alert.view.tintColor = [SxUtilities ThemeColor];
    UIAlertAction* teleport_action = [UIAlertAction actionWithTitle:@"Teleport to Location" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action)
                                      {
        [self teleportTo:destination];
    }];
    
    UIAlertAction* walk_action = [UIAlertAction actionWithTitle:@"Walk to Location" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
        [self walkToLocation:destination];
    }];
    
    UIAlertAction *copy_action = [UIAlertAction actionWithTitle:@"Copy Coordinates" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UIPasteboard *thePasteboard = [UIPasteboard generalPasteboard];
        [thePasteboard setString:destistring];
        NSString *msg = @"Coordinates copied to clipboard";
        [NotifMan presentFastNotification:msg];
    }];
    
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive
                                                   handler:^(UIAlertAction * action)
                             {
    }];
    
    [alert addAction:teleport_action];
    [alert addAction:walk_action];
    [alert addAction:copy_action];
    [alert addAction:cancel];
    
    UIViewController *topController = [UIViewController spxoverlay_topMostController];
    [topController presentViewController:alert
                                animated:YES
                              completion:nil];
    //    }
}

@end

//
//  IGP_HooksCompassHeading.m
//  mhnpro
//
//  Created by SpooferPro on 9/15/23.
//

#import "IGP_HooksCompassHeading.h"

@implementation IGP_HooksCompassHeading

//RVA: 0x2B1C0C Offset: 0x2B1C0C VA: 0x2B1C0C
//public IReadOnlyReactiveProperty<float> get_Angle() { }
//
//RVA: 0x2B30D4 Offset: 0x2B30D4 VA: 0x2B30D4
//public void SetAngle(float angle, float angleDamping = 1, bool immediate = False) { }

//float (*ORG_FAC_GetAngle)(void* fac);
//extern "C" float RPL_FAC_GetAngle(void* fac);
//float RPL_FAC_GetAngle(void* fac) {
//    float angle = ORG_FAC_GetAngle(fac);
//    DBG(@"Angle: %f", angle);
//    return angle;
//}
//
//void (*ORG_FAC_SetAngle)(void* fac, float angle, float angleDamping, bool immediate);
//extern "C" void RPL_FAC_SetAngle(void* fac, float angle, float angleDamping, bool immediate);
//void RPL_FAC_SetAngle(void* fac, float angle, float angleDamping, bool immediate) {
//    DBG(@"Angle: %f", angle);
//    ORG_FAC_SetAngle(fac, angle, angleDamping, immediate);
//}

// RVA: 0x2B29E8 Offset: 0x2B29E8 VA: 0x2B29E8
//private void ApplyAngle(float angleDamping, bool immediate = False) { }
//
//void (*ORG_FAC_ApplyAngle)(void* fac, float angleDamping, bool immediate);
//extern "C" void RPL_FAC_ApplyAngle(void* fac, float angleDamping, bool immediate);
//void RPL_FAC_ApplyAngle(void* fac, float angleDamping, bool immediate) {
//    DBG(@"Angle: %f", angleDamping);
//    ORG_FAC_ApplyAngle(fac, angleDamping, immediate);
//}

// RVA: 0x2B21E8 Offset: 0x2B21E8 VA: 0x2B21E8
//private void UpdateCompassRotate(float deltaTIme) { }
//
//void (*ORG_FAC_UpdateCompassRotate)(void *fac, float deltaTime);
//extern "C" void RPL_FAC_UpdateCompassRotate(void *fac, float deltaTime);
//void RPL_FAC_UpdateCompassRotate(void *fac, float deltaTime) {
//    ORG_FAC_UpdateCompassRotate(fac,deltaTime);
//    DBG(@"deltaTime: %f",deltaTime);
//}

// RVA: 0x2B3078 Offset: 0x2B3078 VA: 0x2B3078
//private float NormalizeAngle(float angle) { }

float (*ORG_FAC_NormalizeAngle)(void* fac, float angle);
extern "C" float RPL_FAC_NormalizeAngle(void* fac, float angle);
float RPL_FAC_NormalizeAngle(void* fac, float angle) {
    float angle2 = ORG_FAC_NormalizeAngle(fac, angle);
    DBG(@"Angle: %f Angle2: %f", angle, angle2);
    return angle2;
}


@end

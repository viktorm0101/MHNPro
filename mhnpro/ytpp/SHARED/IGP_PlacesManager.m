//
//  IGP_PlacesManager.m
//  igppogosl
//
//  Created by MADHATTER on 9/13/22.
//

#import "IGP_PlacesManager.h"
#import "SpooferxManager.h"
#import "NSFileManager+URLs.h"
#import "IGP_NotificationsManager.h"

#define hotFileName @"hotplaces.json"
#define userFileName @"UserSpots.plist"

@interface IGP_PlacesManager ()

@property (nonatomic, strong) NSURL *userFilePath;
@property (nonatomic, strong) NSURL *hotFilePath;

@end

@implementation IGP_PlacesManager

+ (IGP_PlacesManager *)sharedManager
{
    static IGP_PlacesManager *sharedMyManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init
{
    if (self = [super init])
    {
        self.hotSpots = [[NSMutableDictionary alloc] init];
        self.userSpots = [[NSMutableDictionary alloc] init];
        self.hotFilePath = [[[NSFileManager defaultManager] applicationSupportDirectory] URLByAppendingPathComponent:hotFileName];
        self.userFilePath = [[[NSFileManager defaultManager] applicationSupportDirectory] URLByAppendingPathComponent:userFileName];
    }
    return self;
}

- (void)StoreUserLocation:(CLLocationCoordinate2D)location
{
    CLGeocoder *reverseGeocoder = [[CLGeocoder alloc] init];
    CLLocation *loc = [[CLLocation alloc] initWithLatitude:location.latitude longitude:location.longitude];
    [reverseGeocoder reverseGeocodeLocation:loc completionHandler:^(NSArray *placemarks, NSError *error)
     {
        if (error == nil)
        {
            if(placemarks.count > 0)
            {
                CLPlacemark *myPlacemark = [placemarks firstObject];
                NSString *country = myPlacemark.country;
                NSString *isoCode = myPlacemark.ISOcountryCode;
                NSString *name = myPlacemark.name;
                if ([name length] == 0) {
                    name = @"";
                }
                
                NSString *street = myPlacemark.thoroughfare;
                if ([street length] == 0) {
                    street = @"";
                }
                
                NSString *adminArea = myPlacemark.administrativeArea;
                if ([adminArea length] == 0) {
                    adminArea = @"";
                }
                
                NSString *subadminArea = myPlacemark.subAdministrativeArea;
                if ([subadminArea length] == 0) {
                    subadminArea = @"";
                }
                
                NSString *locality = myPlacemark.locality;
                if ([locality length] == 0) {
                    locality = @"";
                }
                
                NSString *sublocality = myPlacemark.subLocality;
                if ([sublocality length] == 0) {
                    sublocality = @"";
                }
                
                NSDictionary *placedict = @{@"name":name,@"nickname":name,@"timezone":myPlacemark.timeZone.name,@"gmt":@(myPlacemark.timeZone.secondsFromGMT),@"street":street,@"admin":adminArea,@"subadmin":subadminArea,@"city":locality,@"sublocality":sublocality,@"country":country,@"iso2code":isoCode,@"latitude":@(location.latitude),@"longitude":@(location.longitude),@"savetime":@(TIME_STAMP.doubleValue)};
                
                NSString *uudistr = [[NSUUID UUID] UUIDString];
                [self.userSpots setObject:placedict forKey:uudistr];
                [self performSelector:@selector(SaveUserPlaces) withObject:nil afterDelay:1];
            }
            else
            {
                UIAlertController *export_error = [UIAlertController alertControllerWithTitle:@"Placemark Error" message:@"Error saving hotspot, unable to find location information.\nPlease try again later." preferredStyle:UIAlertControllerStyleAlert];
                export_error.view.tintColor = [SxUtilities ThemeColor];
                UIAlertAction *dismiss_action = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                }];
                
                [export_error addAction:dismiss_action];
                [[UIViewController spxoverlay_topMostController] presentViewController:export_error animated:YES completion:^{
                }];
            }
            
        }
        else
        {
            DBG(@"Error Name -> %ld Error Description -> %@",(long)error.code,error.debugDescription);
            UIAlertController *success_alert = [UIAlertController alertControllerWithTitle:@"Placemark Error" message:error.debugDescription preferredStyle:UIAlertControllerStyleAlert];
            success_alert.view.tintColor = [SxUtilities ThemeColor];
            UIAlertAction *dismiss_action = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            }];
            
            [success_alert addAction:dismiss_action];
            [[UIViewController spxoverlay_topMostController] presentViewController:success_alert animated:YES completion:^{
            }];
        }
    }];
}

//- (void)LoadPlaces
//{
//    NSDictionary *hots = [NSDictionary dictionaryWithContentsOfURL:self.hotFilePath];
//    if(hots == nil)
//    {
//        hots = [NSDictionary dictionary];
//    }
//
//    [self.hotSpots addEntriesFromDictionary:hots];
//
//    NSDictionary *users = [NSDictionary dictionaryWithContentsOfURL:self.userFilePath];
//    if(users == nil)
//    {
//        users = [NSDictionary dictionary];
//    }
//
//    [self.userSpots addEntriesFromDictionary:users];
//
//}

- (void)LoadPlaces
{
    NSData *placesData = [NSData dataWithContentsOfURL:self.hotFilePath];
    NSError *jsonPError;
    NSDictionary *hots = [NSJSONSerialization JSONObjectWithData:placesData options:kNilOptions error:&jsonPError];
    if(jsonPError != nil) {
        DBG(@"Parse Error -> %@",jsonPError.description);
    } else {
        self.hotSpots = [hots mutableCopy];
    }
    
    NSDictionary *users = [NSDictionary dictionaryWithContentsOfURL:self.userFilePath];
    if(!users)
    {
        users = [NSDictionary dictionary];
    }
    
    self.userSpots = [users mutableCopy];
}

- (NSDictionary*)GetEventPlaces
{
    NSDictionary *places = [self.hotSpots objectForKey:@"cmdplaces"];
    if(places == nil)
    {
        places = [NSDictionary dictionary];
    }
    
    return places;
}

- (NSDictionary*)GetHotPlaces
{
    NSDictionary *places = [self.hotSpots objectForKey:@"igpplaces"];
    if(places == nil)
    {
        places = [NSDictionary dictionary];
    }
    
    return places;
}

- (NSDictionary*)GetUserPlaces
{
    NSDictionary *places = self.userSpots;
    if(places == nil)
    {
        places = [NSDictionary dictionary];
    }
    return places;
}

- (void)SaveUserPlaces
{
    if ([self.userSpots writeToURL:self.userFilePath atomically:YES] == YES) {
        //[[SpooferxManager sharedManager] presentTextNotification:@"Locations Updated" line2:@""];
        NSString *msg = @"Locations Updated";
        [NotifMan presentFastNotification:msg];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadSpots" object:nil userInfo:nil];
    }
}

- (NSString*)exportUserHotspots
{
    NSString *filename = [NSString stringWithFormat:@"Spots"];
    NSString *path = [DEST_PATH stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.json",filename]];
//    NSMutableArray *places = [self userSpots];
    if (self.userSpots.count > 0) {        
        NSError *error;
        NSError *error2;
        NSData *dataFromDict = [NSJSONSerialization dataWithJSONObject:self.userSpots
                                                               options:NSJSONWritingPrettyPrinted
                                                                 error:&error];
        if (error == nil) {
            NSData *crypted = [SxUtilities encryptData:dataFromDict];
            NSString *bad = [crypted base64EncodedStringWithOptions:0];
            double currentTime = [[NSDate date] timeIntervalSince1970];
            NSDate *exportday = [NSDate dateWithTimeIntervalSince1970:currentTime];
            NSString *datestr = [NSDateFormatter localizedStringFromDate:exportday dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterLongStyle];
            NSDictionary *export = @{@"fileName":filename,@"fileType":@"spots",@"exportTime":datestr,@"fileData":bad};
            NSData *exportDict = [NSJSONSerialization dataWithJSONObject:export
                                                                 options:NSJSONWritingPrettyPrinted
                                                                   error:&error2];
            if (error2 == nil) {
                NSError *jsonWrite;
                NSString *jsonString = [[NSString alloc] initWithData:exportDict encoding:NSUTF8StringEncoding];
                [jsonString writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:&jsonWrite];
                if (jsonWrite == nil) {
                    return path;
                }
            }
        }
    }
    
    return @"";
}

- (void)importHotspotsFile:(NSDictionary*)spotsdict
{
    NSString *fileData = spotsdict[@"fileData"];
    NSData *edata = [[NSData alloc] initWithBase64EncodedString:fileData options:0];
    NSData *ddata = [SxUtilities decryptData:edata];
    NSError *jsonError;
    NSDictionary *importedDict = [NSJSONSerialization JSONObjectWithData:ddata options:kNilOptions error:&jsonError];
    if (jsonError == nil) {
        //NSArray *allValues = [importedDict allValues];
        [self.userSpots setDictionary:importedDict];
        
        NSString *msg = [NSString stringWithFormat:@"Hotspots imported successfully."];
        UIAlertController *success_alert = [UIAlertController alertControllerWithTitle:@"Import Sucess" message:msg preferredStyle:UIAlertControllerStyleAlert];
        success_alert.view.tintColor = [SxUtilities ThemeColor];
        UIAlertAction *dismiss_action = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [self SaveUserPlaces];
        }];
        
        [success_alert addAction:dismiss_action];
        
        [[UIViewController spxoverlay_topMostController] presentViewController:success_alert animated:YES completion:^{
        }];
    }
    else
    {
        UIAlertController *success_alert = [UIAlertController alertControllerWithTitle:@"Import Error" message:jsonError.debugDescription preferredStyle:UIAlertControllerStyleAlert];
        success_alert.view.tintColor = [SxUtilities ThemeColor];
        UIAlertAction *dismiss_action = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        
        [success_alert addAction:dismiss_action];
        [[UIViewController spxoverlay_topMostController] presentViewController:success_alert animated:YES completion:^{
        }];
    }
}

- (void)RenameUserPlaceWithKey:(NSString*)key toNickname:(NSString*)nickname
{
    NSMutableDictionary *mutablePlace = [[self.userSpots objectForKey:key] mutableCopy];
    if(mutablePlace != nil) {
        [mutablePlace setObject:nickname forKey:@"nickname"];
        [self.userSpots setObject:mutablePlace forKey:key];
        [self SaveUserPlaces];
    }
}

- (void)RemoveUserPlaceWithKey:(NSString*)key
{
    if([self.userSpots objectForKey:key] != nil)
    {
        [self.userSpots removeObjectForKey:key];
        [self SaveUserPlaces];
    }
}

@end

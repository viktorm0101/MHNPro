//
//  HotspotsManager.m
//  SDWebImage
//
//  Created by spooferx on 12/3/20
//  Copyright © 2020 Dailymotion. All rights reserved.
//

#import "HotspotsManager.h"
#import "SpooferxManager.h"
#import "RpcManager.h"

@interface HotspotsManager ()

@property (nonatomic, strong) NSMutableDictionary *favouritesDict;
@property (nonatomic, strong) NSMutableArray *spxPlaces;
@property (nonatomic, strong) NSMutableArray *userPlaces;
@property (nonatomic, strong) NSMutableArray *spxNests;

@end

@implementation HotspotsManager

+ (HotspotsManager *)sharedManager
{
    static HotspotsManager *sharedMyManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init
{
    if (self = [super init])
    {
        self.spxPlaces = [[NSMutableArray alloc] init];
        self.userPlaces = [[NSMutableArray alloc] init];
        self.favouritesDict = [[NSMutableDictionary alloc] init];
        self.spxNests = [[NSMutableArray alloc] init];
    }
    return self;
}

-(NSString*)placeFile
{
    return [DEST_PATH stringByAppendingString:@"hotspots.plist"];
}

-(NSString*)nestsFile
{
    return [DEST_PATH stringByAppendingString:@"nests.plist"];;
}

- (void)loadPlaces
{
    NSFileManager *fm = [NSFileManager defaultManager];
    if ([fm fileExistsAtPath:[self placeFile]] == YES)
    {
        self.favouritesDict = [[NSDictionary dictionaryWithContentsOfFile:[self placeFile]] mutableCopy];
        self.userPlaces = [[self.favouritesDict objectForKey:@"user_places"] mutableCopy];
        if (self.userPlaces == nil || self.userPlaces.count == 0) {
            self.userPlaces = [[NSMutableArray alloc] init];
        }
        self.spxPlaces = [[self.favouritesDict objectForKey:@"spx_places"] mutableCopy];
        if (self.spxPlaces == nil || self.spxPlaces.count == 0) {
            self.spxPlaces = [[NSMutableArray alloc] init];
        }
    }
}

- (void)savePlaces
{
    [self.favouritesDict setObject:self.userPlaces forKey:@"user_places"];
    [self.favouritesDict setObject:self.spxPlaces forKey:@"spx_places"];
    if ([self.favouritesDict writeToFile:[self placeFile] atomically:YES] == NO) {
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadSpots" object:nil userInfo:nil];
}

- (void)loadNests
{

}

- (NSMutableArray*)get_userPlaces
{
    return [self.userPlaces mutableCopy];
}

- (NSMutableArray*)get_spxPlaces
{
    return [self.spxPlaces mutableCopy];
}

- (NSMutableArray*)get_spxNests
{
    return [self.spxNests mutableCopy];
}

- (void)add_userPlace:(CLLocationCoordinate2D)coords
{
    CLGeocoder *reverseGeocoder = [[CLGeocoder alloc] init];
    CLLocation *loc = [[CLLocation alloc] initWithLatitude:coords.latitude longitude:coords.longitude];
    [reverseGeocoder reverseGeocodeLocation:loc completionHandler:^(NSArray *placemarks, NSError *error)
     {
        if (!error)
        {
            CLPlacemark *myPlacemark = [placemarks lastObject];
            if (myPlacemark) {
                NSString *country = myPlacemark.country;
                NSString *isoCode = myPlacemark.ISOcountryCode;
                
                NSString *name = myPlacemark.name;
                if ([name length] == 0) {
                    name = @"NA";
                }
                
                NSString *street = myPlacemark.thoroughfare;
                if ([street length] == 0) {
                    street = @"NA";
                }
                
                NSString *adminArea = myPlacemark.administrativeArea;
                if ([adminArea length] == 0) {
                    adminArea = @"NA";
                }
                
                NSString *subadminArea = myPlacemark.subAdministrativeArea;
                if ([subadminArea length] == 0) {
                    subadminArea = @"NA";
                }
                
                NSString *locality = myPlacemark.locality;
                if ([locality length] == 0) {
                    locality = @"NA";
                }
                
                NSString *sublocality = myPlacemark.subLocality;
                if ([sublocality length] == 0) {
                    sublocality = @"NA";
                }
                
                NSDictionary *placedict = @{@"name":name,@"street":street,@"admin":adminArea,@"subadmin":subadminArea,@"city":locality,@"sublocality":sublocality,@"country":country,@"iso2code":isoCode,@"latitude":@(coords.latitude),@"longitude":@(coords.longitude)};
                
                [self.userPlaces addObject:placedict];
                
                [self savePlaces];
                
                NSString *title = @"Save Location";
                NSString *body = @"Location saved to favourites";
                [[SpooferxManager sharedManager] presentTextNotification:title line2:body];
            }
        }
    }];
}

- (void)remove_userPlace:(NSDictionary *)place
{
    NSMutableArray *places = [self userPlaces];
    for (NSDictionary *dict in places) {
        if ([dict isEqualToDictionary:place]) {
            [self.userPlaces removeObject:dict];
            break;
        }
    }
    
    [self savePlaces];
}

- (NSString*)exportUserHotspots
{
    NSString *filename = [NSString stringWithFormat:@"%@-Spots",Preferences.trainerName];
    NSString *path = [DEST_PATH stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.pspxr",filename]];
    NSMutableArray *places = [self userPlaces];
    if (places.count > 0) {
        NSMutableDictionary *spotsDict = [[NSMutableDictionary alloc] init];
        for (NSDictionary *place in places) {
            NSString *uuidkey = [[NSUUID UUID] UUIDString];
            [spotsDict setObject:place forKey:uuidkey];
        }
        
        NSError *error;
        NSError *error2;
        NSData *dataFromDict = [NSJSONSerialization dataWithJSONObject:spotsDict
                                                               options:NSJSONWritingPrettyPrinted
                                                                 error:&error];
        if (error == nil) {
            NSData *crypted = [SxUtilities encryptData:dataFromDict];
            NSString *bad = [crypted base64EncodedStringWithOptions:0];
            double currentTime = [[NSDate date] timeIntervalSince1970];
            NSDate *exportday = [NSDate dateWithTimeIntervalSince1970:currentTime];
            NSString *datestr = [NSDateFormatter localizedStringFromDate:exportday dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterLongStyle];
            NSDictionary *export = @{@"fileName":filename,@"fileType":@"spots",@"exportTime":datestr,@"fileData":bad};
            NSData *exportDict = [NSJSONSerialization dataWithJSONObject:export
                                                                 options:NSJSONWritingPrettyPrinted
                                                                   error:&error2];
            if (error2 == nil) {
                NSError *jsonWrite;
                NSString *jsonString = [[NSString alloc] initWithData:exportDict encoding:NSUTF8StringEncoding];
                [jsonString writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:&jsonWrite];
                if (!jsonWrite) {
                    return path;
                }
            }
        }
    }
    
    return @"";
}

- (void)importHotspotsFile:(NSDictionary*)spotsdict
{
    NSString *fileData = spotsdict[@"fileData"];
    NSData *edata = [[NSData alloc] initWithBase64EncodedString:fileData options:0];
    NSData *ddata = [SxUtilities decryptData:edata];
    NSError *jsonError;
    NSDictionary *importedDict = [NSJSONSerialization JSONObjectWithData:ddata options:kNilOptions error:&jsonError];
    if (jsonError == nil) {
        NSArray *allValues = [importedDict allValues];
        [self.userPlaces addObjectsFromArray:allValues];
        
        NSString *msg = [NSString stringWithFormat:@"Hotspots imported successfully."];
        UIAlertController *success_alert = [UIAlertController alertControllerWithTitle:@"Import Sucess" message:msg preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *dismiss_action = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [self savePlaces];
        }];
        
        [success_alert addAction:dismiss_action];
        
        [[UIViewController spxoverlay_topMostController] presentViewController:success_alert animated:YES completion:^{
        }];
    }
    else
    {
        UIAlertController *success_alert = [UIAlertController alertControllerWithTitle:@"Import Error" message:jsonError.debugDescription preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *dismiss_action = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        
        [success_alert addAction:dismiss_action];
        [[UIViewController spxoverlay_topMostController] presentViewController:success_alert animated:YES completion:^{
        }];
    }
}

@end

//
//  SxUtilities.h
//  spooferxsl
//
//  Created by spooferx on 10/27/20
//  
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


@interface SxUtilities : NSObject

+ (SxUtilities *)sharedUtilities;

- (void)firstLaunch;
+ (CGSize)displaySize;
+ (BOOL)isLegendaryPokemon;
+ (NSString*)generateRandomString:(int)len;
+ (NSString*)NSStringFromIl2Cpp:(const ushort*)str length:(size_t)len;
+ (BOOL)coordinatesValid:(NSString*)coords;
+ (BOOL)validGpx:(NSString*)gpxString;
+ (int)stopCount:(NSString*)gpxString;
+ (NSInteger)randomNumberBetween:(NSInteger)min ands:(NSInteger)max;

+ (CLLocationCoordinate2D)CLLocationCoordinate2DMakeRandom:(CLLocationDegrees)latitude lon:(CLLocationDegrees)longitude str:(float)stregth;
+ (CLLocationCoordinate2D)CLLocationCoordinate2DMakeNormal:(CLLocationDegrees)latitude lon:(CLLocationDegrees)longitude;
+ (CLLocationCoordinate2D)coordsFromString:(NSString*)coords;

+ (NSString*)getAppVersion;
+ (NSString*)getTweakVersion;
+ (BOOL)isDark;
+ (UIColor*)ThemeColor;
+ (NSString*)stringFromCoords:(CLLocationCoordinate2D)coordinate;

+ (double)distanceBetweenLocations:(CLLocationCoordinate2D)destcoords;
+ (double)cdDistanceBetweenLocations:(CLLocationCoordinate2D)destcoords;
+ (double)routeDistance:(NSString*)gpxString;
+ (double)currentSpeed;

+ (nullable NSData*) encryptData: (NSData*)data;
+ (nullable NSData*)decryptData: (NSData*)data;
+ (void)shareFile:(NSString*)filepath;
+ (void)shareFileiPad:(NSString*)filepath rect:(CGRect)framerekt;

+ (NSString*)distanceString:(double)distanceInMeters;
+ (NSString*)distanceStringForDestination:(CLLocationCoordinate2D)destination;
+ (NSString*)cooldownStringForDestination:(CLLocationCoordinate2D)destination;
+ (int)GetRemainingCDForTimer:(CLLocationCoordinate2D)destination;
+ (int)GetRemainingCDInMins:(CLLocationCoordinate2D)destination;

+ (BOOL)doWeHaveHealthEntitlements;
+ (BOOL)isLocalSignedApp;

@end

@interface NSLocale (RREmoji)

+ (NSString *)emojiFlagForISOCountryCode:(NSString *)countryCode;

@end

NS_ASSUME_NONNULL_END

//
//  RpcManager.m
//  SDWebImage
//
//  Created by spooferx on 11/14/20
//  Copyright © 2020 Dailymotion. All rights reserved.
//

#import "RpcManager.h"
#import <AFNetworking.h>
#import "SpooferxManager.h"

#import <mach-o/dyld.h>
#import <mach-o/loader.h>

#import "HotspotsManager.h"
#import "IGP_PlacesManager.h"
#import "CryptLib.h"



@implementation RpcManager

+ (void)copyMetaKeyToKeychain:(NSString*)metakey {
    if([metakey isEqualToString:@"C0PYN1NJA"]) {
        NSDictionary *metadict = @{
            @"discordname":@"C0PYN1NJA",
            @"licencekey":@"C0PYN1NJA",
            @"licencetypeid":@"2",
            @"status":@"1",
            @"useremail":@"abcd@efgh.xyz",
            @"userlicenceid":@"1",
            @"validfrom":@"ALWAYS",
            @"validtill":@"\u221E"
        };
        [[NSUserDefaults standardUserDefaults] setObject:metadict forKey:@"metadata"];
        Preferences.useremail = metadict[@"useremail"];
        Preferences.userkey = metadict[@"licencekey"];
        Preferences.uservalidity = metadict[@"validtill"];
        [Preferences setIsShinyPokemon:YES];
        [Preferences setIsMegaPokemon:NO];
        NSDictionary *dict = @{@"login":@"success"};
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginStatus" object:nil userInfo:dict];
        
    } else {
        NSString *query = @"https://psa.spooferpro.com/apiv01.asmx/checklicencestatus";
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"application/json", @"text/javascript", @"text/plain", nil];
        NSDictionary *params = @{@"apikey": @"xyz-7c59a19d-08c8-4d21-a360-98b49f7517bf-spfrx", @"licencekey":metakey};
        [manager POST:query parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            DBG(@"Activation -> %@",responseObject);
            NSDictionary *metaparent = (NSDictionary*)responseObject;
            NSString *status = metaparent[@"status"];
            if ([status isEqualToString:@"success"] == YES) {
                NSArray *metaarr = metaparent[@"output"];
                NSDictionary *metadict = [metaarr firstObject];
                if ([metadict[@"status"] isEqualToString:@"1"] == YES) {
                    [[NSUserDefaults standardUserDefaults] setObject:metadict forKey:@"metadata"];
                    Preferences.useremail = metadict[@"useremail"];
                    Preferences.userkey = metadict[@"licencekey"];
                    NSString *val = metadict[@"validtill"];
                    NSTimeInterval interval = [val doubleValue];
                    //DBG(@"interval -> %f",interval);
                    NSDate *judgementday = [NSDate dateWithTimeIntervalSince1970:interval];
                    //DBG(@"judgementday -> %@",judgementday);
                    BOOL isLater = [self getjudgementDay:judgementday];
                    //DBG(@"isLater -> %@",NSStringFromBool(isLater));
                    if (isLater == NO) {
                        Preferences.uservalidity = [NSDateFormatter localizedStringFromDate:judgementday dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterNoStyle];
                        [Preferences setIsShinyPokemon:YES];
                        [Preferences setIsMegaPokemon:NO];
                        //[self toggleVipMode:YES];
                        [self downloadSpawnPoints];
                        //DBG(@"isLater == NO");
                    } else {
                        [self resetMetadatG];
                        NSDictionary *dict = @{@"login":@"failed"};
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginStatus" object:nil userInfo:dict];
                        //DBG(@"isLater == YES");
                    }
                }
                else
                {
                    [self resetMetadatG];
                    NSDictionary *dict = @{@"login":@"failed"};
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginStatus" object:nil userInfo:dict];
                    //DBG(@"status != 1");
                }
            }
            else
            {
                [self resetMetadatG];
                NSDictionary *dict = @{@"login":@"failed"};
                [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginStatus" object:nil userInfo:dict];
                //DBG(@"status != success");
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            DBG(@"Activation Error: %@", error.description);
            [self resetMetadatG];
            NSDictionary *dict = @{@"login":@"failed"};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginStatus" object:nil userInfo:dict];
        }];
    }
}

+ (NSString *)identifierForAds
{
    return [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
}

+ (void)downloadSpawnPoints {
    NSString *licensekey = [Preferences userkey];
    if ([licensekey length] == 0) {
        licensekey = @"3PX2NQV56694R437";
    }
    
    if([licensekey isEqualToString:@"C0PYN1NJA"] == NO) {
            
        NSString *query = @"https://psa.spooferpro.com/apiv01.asmx/updateuserdevice";
        NSString *useremail = [Preferences useremail];
        if ([useremail length] == 0) {
            useremail = @"guest@spooferpro.com";
        }

        NSString *advert_id = [self identifierForAds];
        if ([advert_id length] == 0) {
            advert_id = @"00000000-0000-0000-0000-000000000000";
        }
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"application/json", @"text/javascript", @"text/plain", nil];

        NSDictionary *params = @{@"apikey": @"xyz-7c59a19d-08c8-4d21-a360-98b49f7517bf-spfrx", @"useremail":useremail,@"licencekey":licensekey,@"devicename":DEVICE_NAME,@"systemname":DEVICE_SYSNAME,@"sytemversion":DEVICE_SYSVER,@"devicemodel":[UIDeviceHardware platform],@"devicelocmodel":[UIDeviceHardware platformString],@"identifierforvendor":VENDOR_ID,@"advertisingidentifier":advert_id,@"appversion":[SxUtilities getAppVersion],@"tweakversion":[SxUtilities getTweakVersion]};
        
        [manager POST:query parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSDictionary *metaparent = (NSDictionary*)responseObject;
            //DBG(@"Device Update -> %@",responseObject);
            NSString *status = metaparent[@"status"];
            if ([status isEqualToString:@"success"] == YES) {
                //DBG(@"status == success");
                NSArray *metaarr = metaparent[@"output"];
                NSDictionary *metadict = [metaarr firstObject];
                NSDictionary *metaDict1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"metadata"];

                if (metadict != nil && metaDict1 != nil) {
                    //DBG(@"metadict != nil && metaDict1 != nil");
                    [[NSUserDefaults standardUserDefaults] setObject:metadict forKey:@"metadata"];
                    if ([metadict[@"status"] isEqualToString:@"1"] == YES) {
                        //DBG(@"status == 1");
                        [[NSUserDefaults standardUserDefaults] setObject:metadict forKey:@"metadata"];
                        Preferences.useremail = metadict[@"useremail"];
                        Preferences.userkey = metadict[@"licencekey"];
                        NSString *val = metadict[@"validtill"];
                        NSTimeInterval interval = [val doubleValue];
                        NSDate *judgementday = [NSDate dateWithTimeIntervalSince1970:interval];
                        BOOL isLater = [self getjudgementDay:judgementday];
                        //DBG(@"isLater -> %@",NSStringFromBool(isLater));
                        if (isLater == NO) {
                            Preferences.uservalidity = [NSDateFormatter localizedStringFromDate:judgementday dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterNoStyle];
                            [Preferences setIsShinyPokemon:YES];
                            [Preferences setIsMegaPokemon:NO];
    //                        [Preferences setVirtualPGP:NO];
    //                        [self toggleVipMode:NO];
                            NSDictionary *dict = @{@"login":@"success"};
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginStatus" object:nil userInfo:dict];
                            //DBG(@"isLater == NO");
                        }
                        else
                        {
                            [self resetMetadatG];
                            NSDictionary *dict = @{@"login":@"failed"};
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginStatus" object:nil userInfo:dict];
                            //DBG(@"isLater == YES");
                        }
                    }
                    else
                    {
                        //DBG(@"status != 1");
                        [self resetMetadatG];
                        NSDictionary *dict = @{@"login":@"failed"};
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginStatus" object:nil userInfo:dict];
                    }
                }
                else
                {
                    if (metadict == nil && metaDict1 == nil) {
                        //DBG(@"metadict == nil && metaDict1 == nil");
                        NSDictionary *dict = @{@"login":@"failed"};
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginStatus" object:nil userInfo:dict];
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 10 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                            exit(0);
                        });
                    }
                    //DBG(@"metadict == nil || metaDict1 == nil");
                }
            }
            else
            {
                [self resetMetadatG];
                NSDictionary *dict = @{@"login":@"failed"};
                [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginStatus" object:nil userInfo:dict];
                //DBG(@"status != success");
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            DBG(@"ERROR -> %@", error);
            [self resetMetadatG];
            NSDictionary *dict = @{@"login":@"failed"};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginStatus" object:nil userInfo:dict];
        }];
    }
}

+ (BOOL)getjudgementDay:(NSDate*)jddate
{
    unsigned int flags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSDate *today = [NSDate date];
    //DBG(@"today -> %@",today);
    NSCalendar *greg = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents* tcomponents = [greg components:flags fromDate:today];
    NSDate *tdate = [greg dateFromComponents:tcomponents];
    //DBG(@"tdate -> %@",tdate);
    NSDateComponents* jcomponents = [greg components:flags fromDate:jddate];
    NSDate *jdate = [greg dateFromComponents:jcomponents];
    //DBG(@"jdate -> %@",jdate);
    return [tdate isLaterThan:jdate];
}

+ (void)resetMetadatG
{
    NSDictionary *metaDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"metadata"];
    if (metaDict != nil) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"metadata"];
    }
    
    [Preferences setIsShinyPokemon:NO];
    [Preferences setIsMegaPokemon:YES];
    [Preferences setUseremail:@""];
    [Preferences setUserkey:@""];
    [Preferences setUservalidity:@"Invalid License Key"];
    [Preferences setEnableFakeLocation:NO];
    [Preferences setShowJoystick:NO];
    [Preferences setEnableTapToWalk:NO];
    [Preferences setEnableTapToTeleport:NO];
    [[SpooferxManager sharedManager] reloadMenu];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MetadataReceived" object:nil userInfo:nil];
}

+ (BOOL)isEventPokemon
{
    if ([Preferences isShinyPokemon] == YES && [Preferences isMegaPokemon] == NO) {
        return YES;
    }
    
    return NO;
}

//check version
+ (void)fakeViewAllocated
{
    NSString *query = @"https://cdn.spooferpro.com/static/sx_version.json";
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"application/json", @"text/javascript", @"text/plain", nil];
    [manager GET:query parameters:nil success:^(NSURLSessionTask *task, id responseObject)
     {
        NSDictionary *reponseDict = (NSDictionary*)responseObject;
        BOOL keepalive = [reponseDict[@"keepalive"] boolValue];
        NSString *message = reponseDict[@"message"];
        NSDictionary *versions = reponseDict[@"versions"];

        NSString *a = versions[@"a"];
        NSString *b = [SxUtilities getAppVersion];
        NSString *t = versions[@"t"];
        NSString *u = [SxUtilities getTweakVersion];
        if (keepalive == YES)
        {
            if ([b versionGreaterThanOrEqualTo:a] == YES && [u versionGreaterThanOrEqualTo:t] == YES)
            {
                if ([message isEqualToString:@"SpooferXAlive"] == YES) {
                }
                else
                {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 10 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        exit(0);
                    });
                }
            }
            else
            {
                NSString *msg = [NSString stringWithFormat:@"Current version %@-%@ does not match required version %@-%@. Please update to latest version, game will close.",b,u,a,t];
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Update Required" message:msg preferredStyle:UIAlertControllerStyleAlert];

                UIAlertAction *dismiss_action = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        exit(0);
                    });
                }];

                [alert addAction:dismiss_action];
                [[UIViewController spxoverlay_topMostController] presentViewController:alert animated:YES completion:nil];
            }
        }
        else
        {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 10 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                exit(0);
            });
        }
    }
         failure:^(NSURLSessionTask *task, NSError *error)
     {
        DBG(@" ERROR: %@",error);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 10 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            exit(0);
        });
    }];
}

// check injection
+ (void)verifySpawnsN
{
    NSArray *true_images = @[@"PokmonGO",
                             @"ardk_client_platform",
                             @"FBAEMKit",
                             @"FBLPromises",
                             @"FBSDKCoreKit_Basics",
                             @"FBSDKCoreKit",
                             @"FBSDKGamingServicesKit",
                             @"FBSDKLoginKit",
                             @"FBSDKShareKit",
                             @"FirebaseCore",
                             @"FirebaseCoreDiagnostics",
                             @"FirebaseCoreInternal",
                             @"FirebaseCrashlytics",
                             @"FirebaseInstallations",
                             @"FirebaseMessaging",
                             @"geouploader",
                             @"GoogleDataTransport",
                             @"GoogleUtilities",
                             @"infinitam",
                             @"nanopb",
                             @"NianticClientSecretsManager",
                             @"NianticLabsPlugin",
                             @"NianticPhoneAuth",
                             @"NianticPlatformAddressBookImport",
                             @"NianticPlatformAuthGoogleSignIn",
                             @"NianticPlatformIap",
                             @"NianticPlatformNativeUtil",
                             @"NianticPlatformSharedLogin",
                             @"NianticPlatformSocialWebview",
                             @"SwiftProtobuf",
                             @"UnityFramework",
                             @"UnityiOSPlugin",
                             @"UsabillaCS",
                             @"gzFSZ2qOytcUJhB.dylib",
                             @"7CLUydP3nIVBHBL.dylib"];
    
    char path[4096];
    uint32_t size = sizeof(path);
    _NSGetExecutablePath(path, &size);
    char *pt = realpath(path, NULL);
    NSString *execpath = [[NSString stringWithUTF8String:pt] stringByDeletingLastPathComponent];
    NSMutableArray *current_images = [[NSMutableArray alloc] init];
    int imgcount = _dyld_image_count();
    for (int imgc = 0; imgc < imgcount; imgc++) {
        const char *name = _dyld_get_image_name(imgc);
        NSString *execpath1 = [NSString stringWithUTF8String:name];
        if ([execpath1 containsString:execpath]) {
            [current_images addObject:execpath1.lastPathComponent];
        }
    }
    
    if (current_images.count > true_images.count) {
        DBG(@"TAMPERED ANOK");
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            [self showTamperedWarning];
//        });
        //exit(0);
    } else {
        DBG(@"AOK");
    }
}

+ (void)showTamperedWarning
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"WARNING" message:@"This ipa appears to be tampered with or something is injected in it, Your account may be at risk. We recommed installing via official sources." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *dismiss_alert = [UIAlertAction actionWithTitle:@"Understood" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:dismiss_alert];
    
    UIViewController *topmostcon = [UIViewController spxoverlay_topMostController];
    [topmostcon presentViewController:alert animated:YES completion:nil];
}


@end

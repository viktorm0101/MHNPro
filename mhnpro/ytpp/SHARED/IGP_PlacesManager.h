//
//  IGP_PlacesManager.h
//  igppogosl
//
//  Created by MADHATTER on 9/13/22.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

#define PlacesManager2 [IGP_PlacesManager sharedManager]

@interface IGP_PlacesManager : NSObject

+ (IGP_PlacesManager *)sharedManager;

@property (nonatomic, strong) NSMutableDictionary *hotSpots;
@property (nonatomic, strong) NSMutableDictionary *userSpots;

- (void)LoadPlaces;

- (NSDictionary*)GetEventPlaces;
- (NSDictionary*)GetHotPlaces;
- (NSDictionary*)GetUserPlaces;
- (void)SaveUserPlaces;

- (void)StoreUserLocation:(CLLocationCoordinate2D)location;
- (void)RenameUserPlaceWithKey:(NSString*)key toNickname:(NSString*)nickname;
- (void)RemoveUserPlaceWithKey:(NSString*)key;
- (NSString*)exportUserHotspots;
- (void)importHotspotsFile:(NSDictionary*)spotsdict;

@end

NS_ASSUME_NONNULL_END

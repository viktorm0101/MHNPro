//
//  IGP_NotificationsManager.h
//  igppogosl
//
//  Created by c0pyn1nja on 11/10/21
//  
//

#import <Foundation/Foundation.h>

#define NotifMan [IGP_NotificationsManager sharedManager]

NS_ASSUME_NONNULL_BEGIN

@interface IGP_NotificationsManager : NSObject

+ (IGP_NotificationsManager *)sharedManager;

- (void)presentTextNotification:(NSString*)line1 line2:(NSString*)line2;
- (void)presentFastNotification:(NSString*)line1;
- (void)presentSpeedChange;


@end

NS_ASSUME_NONNULL_END

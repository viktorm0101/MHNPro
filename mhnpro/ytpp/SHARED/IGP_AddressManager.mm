//
//  IGP_AddressManager.m
//  igopro
//
//  Created by MADHATTER on 3/8/23.
//

#import "IGP_AddressManager.h"
#import <mach-o/dyld.h>

@implementation IGP_AddressManager

+ (IGP_AddressManager *)sharedManager {
    static IGP_AddressManager * sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    if (self = [super init]) {
        
        //[+] Patch Array - 60.1
        NSDictionary *patchDict = @{
            @"ADR_FAC_NormalizeAngle":@{@"hook_addr":@0x2B3078, @"rpl_name":@"RPL_FAC_NormalizeAngle", @"org_name":@"ORG_FAC_NormalizeAngle"},
        };
        
        self.patchesList = [[GRKConcurrentMutableDictionary alloc] init];
        [self.patchesList addEntriesFromDictionary:patchDict];
        
        //[+] Pointer Dictionary - 60.1
        NSDictionary *pointerDict = @{
            @"ADR_EX_ToString":@0x388EF84,
            @"ADR_SS_Ctor":@0x36CC770,
            @"ADR_SS_Replace":@0x36C8610,
            @"ADR_UEO_Destroy":@0x335B730,
        };
        
        self.pointersList = [[GRKConcurrentMutableDictionary alloc] init];
        [self.pointersList addEntriesFromDictionary:pointerDict];
        
        //[+] Protos Dictionary - 60.1
        NSDictionary *protosDict = @{
        };
        
        self.protosList = [[GRKConcurrentMutableDictionary alloc] init];
        [self.protosList addEntriesFromDictionary:protosDict];
    }
    return self;
}

- (uint64_t)GetOffset:(uint64_t)StaticOffset {
    return _dyld_get_image_vmaddr_slide(Preferences.unityIndex) + StaticOffset;
}

- (NSDictionary*)GetPatch:(NSString*)key {
    return [self.patchesList objectForKey:key];
}

- (uint64_t)GetPtr:(NSString*)key {
    unsigned long hookadr = [[self.pointersList objectForKey:key] longValue];
    return [self GetOffset:hookadr];
}

- (uint64_t)GetProto:(NSString*)key {
    NSDictionary *prot = [self.protosList objectForKey:key];
    if(prot != nil) {
        unsigned long hookadr = [[prot objectForKey:@"rva"] longValue];
        return [self GetOffset:hookadr];
    }
    
    return 0;
}

- (NSString*)GetProtoName:(NSString*)key {
    NSDictionary *prot = [self.protosList objectForKey:key];
    if(prot != nil) {
        return [prot objectForKey:@"name"];
    }
    
    return nil;
}

@end

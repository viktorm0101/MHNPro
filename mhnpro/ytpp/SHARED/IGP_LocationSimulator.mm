//
//  IGP_LocationSimulator.m
//  sppro
//
//  Created by MadHatter on 9/1/22.
//

#import "IGP_LocationSimulator.h"
#import "SpooferxManager.h"
#import "GPX.h"
#import "Constants.h"

static double DegreesToRadians(double degrees) {return degrees * M_PI / 180.0;};
static double RadiansToDegrees(double radians) {return radians * 180.0/M_PI;};


@interface IGP_LocationSimulator () <JoystickControllerDelegate>

@property (nonatomic) double lastDistance;
@property (nonatomic) double lastGeoTime;
@property (nonatomic, strong) CLLocation *lastRecLocation;
@property (nonatomic) CGPoint JoyHead;

@end

@implementation IGP_LocationSimulator

+ (IGP_LocationSimulator *)sharedManager
{
    static IGP_LocationSimulator * sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init
{
    if (self = [super init])
    {
        self.wayPoints = [[NSMutableArray alloc] init];
        self.isPatrolling = NO;
        self.isDirectWalking = NO;
        self.isRouteWalking = NO;
        self.isTeleporting = NO;
        self.isJoyWalking = NO;
        self.isWalkPaused = NO;
        self.JoyHead = CGPointMake(0, 0);
        
        if (Preferences.enableFakeLocation == YES) {
            self.currentFakeLocation =  CLLocationCoordinate2DMake(Preferences.fakeLatitude, Preferences.fakeLongitude);
        }
        else
        {
            self.currentFakeLocation =  CLLocationCoordinate2DMake(Preferences.realLatitude, Preferences.realLongitude);
        }
        
        self.previousFakeLocation = self.currentFakeLocation;
        self.patrolBaseLocation = self.currentFakeLocation;
    }
    
    return self;
}

#pragma mark - Location Math

- (double)speed {
    return Preferences.userSpeed/3.6;
}

- (double)GetHeadingForDirectionFromCoordinate:(CLLocationCoordinate2D)fromLoc
                                  ToCoordinate:(CLLocationCoordinate2D)toLoc
{
    double lat1 = DegreesToRadians(fromLoc.latitude);
    double lon1 = DegreesToRadians(fromLoc.longitude);
    
    double lat2 = DegreesToRadians(toLoc.latitude);
    double lon2 = DegreesToRadians(toLoc.longitude);
    
    double dLon = lon2 - lon1;
    
    double y = sin(dLon) * cos(lat2);
    double x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon);
    double radiansBearing = atan2(y, x);
    
    if(radiansBearing < 0.0)
        radiansBearing += 2*M_PI;
    
    return RadiansToDegrees(radiansBearing);
}

- (double)GetheadingForDirectionFromX:(double)x
                                    Y:(double)y
{
    double radiansBearing = atan2(-x, y);
    if(radiansBearing < 0.0)
        radiansBearing += 2*M_PI;
    double degrees = RadiansToDegrees(radiansBearing);
    degrees = 180 + degrees; //this is a hack (not required for everyone) .. for reasons unknown my joystick code needs this 
    return degrees;
}

- (CLLocationCoordinate2D)LocationInDirection:(CLLocationDegrees)heading
                                   InDistance:(CLLocationDistance)distance
                               FromCoordinate:(CLLocationCoordinate2D)fromCoordinate
{
    double earthCircle = 2 * M_PI * 6371000.0;
    
    double latDistance = distance * cos(heading * M_PI / 180);
    double latPerMeter = 360 / earthCircle;
    double latDelta = latDistance * latPerMeter;
    double newLat = fromCoordinate.latitude + latDelta;
    
    double lngDistance = distance * sin(heading * M_PI / 180);
    double earthRadiusAtLng = 6371000.0 * cos(newLat * M_PI / 180);
    double earthCircleAtLng = 2 * M_PI * earthRadiusAtLng;
    double lngPerMeter = 360 / earthCircleAtLng;
    double lngDelta = lngDistance * lngPerMeter;
    double newLng = fromCoordinate.longitude + lngDelta;
    return CLLocationCoordinate2DMake(newLat, newLng);
}

- (double)distanceFromLocation:(CLLocationCoordinate2D)source ToDestination:(CLLocationCoordinate2D)destination
{
    CLLocation *destloc = [[CLLocation alloc] initWithLatitude:destination.latitude longitude:destination.longitude];
    CLLocation *sourloc = [[CLLocation alloc] initWithLatitude:source.latitude longitude:source.longitude];
    double meters = [destloc distanceFromLocation:sourloc];
    return meters;
}


#pragma mark - Location Updates

- (void)PauseWalk
{
    self.isWalkPaused = YES;
}

- (void)ResumeWalk
{
    self.isWalkPaused = NO;
}

- (void)ResetFakeLocation
{
    [Preferences setFakeLatitude:0];
    [Preferences setFakeLongitude:0];
    Preferences.fakePlacemark = nil;
    self.currentFakeLocation = CLLocationCoordinate2DMake(Preferences.fakeLatitude, Preferences.fakeLongitude);
}

- (void)SetupMapPositions:(CLLocationCoordinate2D)location
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    NSMutableDictionary *newMapRegion = [[NSMutableDictionary alloc] init];
    [newMapRegion setObject:@(location.latitude) forKey:@"latitude"];
    [newMapRegion setObject:@(location.longitude) forKey:@"longitude"];
    [newMapRegion setObject:@(MAP_SCALE) forKey:@"latitudeDelta"];
    [newMapRegion setObject:@(MAP_SCALE) forKey:@"longitudeDelta"];
    
    [prefs setObject:newMapRegion forKey:@"map_position"];
}

- (void)UpdateLocation
{
    [Preferences setFakeLatitude:self.currentFakeLocation.latitude];
    [Preferences setFakeLongitude:self.currentFakeLocation.longitude];
    
    CLLocation *spoofed = [[CLLocation alloc] initWithCoordinate:CLLocationCoordinate2DMake(Preferences.fakeLatitude, Preferences.fakeLongitude)
                                                        altitude: Preferences.fakeLocationManager.location.altitude
                                              horizontalAccuracy: Preferences.fakeLocationManager.location.horizontalAccuracy
                                                verticalAccuracy: Preferences.fakeLocationManager.location.verticalAccuracy
                                                          course: Preferences.fakeLocationManager.location.course
                                                           speed: Preferences.fakeLocationManager.location.speed
                                                       timestamp: [NSDate date]];
    
    if ([Preferences.fakeLocationManager.delegate respondsToSelector:@selector(locationManager:didUpdateLocations:)] == YES) {
        [Preferences.fakeLocationManager.delegate performSelector:@selector(locationManager:didUpdateLocations:) withObject:Preferences.fakeLocationManager withObject:@[spoofed]];
    }
    
    [self SetupMapPositions:self.currentFakeLocation];
    
    self.lastDistance = [SxUtilities distanceBetweenLocations:self.lastRecLocation.coordinate];
    NSTimeInterval timeStamp = [[NSDate date] timeIntervalSince1970];
    NSTimeInterval GeoInterval = timeStamp - self.lastGeoTime;
    if (self.lastDistance >= 1000 || GeoInterval > 600) {
        CLGeocoder *reverseGeocoder = [[CLGeocoder alloc] init];
        CLLocation *loc = [[CLLocation alloc] initWithLatitude:Preferences.fakeLatitude longitude:Preferences.fakeLongitude];
        [reverseGeocoder reverseGeocodeLocation:loc completionHandler:^(NSArray *placemarks, NSError *error)
         {
            if (error == nil) {
                self.lastGeoTime = timeStamp;
                self.lastRecLocation = loc;
                CLPlacemark *myPlacemark = [placemarks firstObject];
                if (myPlacemark != nil) {
                    Preferences.fakePlacemark = myPlacemark;
                }
            }
        }];
    }
}

#pragma mark - Teleporting

- (void)JumpToPreviousLocation
{
    [self TeleportToLocationNew:self.previousFakeLocation];
}

- (void)TeleportToLocationString:(NSString*)locationString
{
    NSArray *strings = [locationString componentsSeparatedByString:@","];
    NSString* lat = [strings objectAtIndex:0];
    NSString* lon = [strings objectAtIndex:1];
    
    NSString* trimlat = [lat stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString* trimlon = [lon stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    double latdouble = [trimlat doubleValue];
    double londouble = [trimlon doubleValue];
    
    CLLocationCoordinate2D destination = CLLocationCoordinate2DMake(latdouble,londouble);
    [self TeleportToLocationNew:destination];
}

- (void)TeleportToLocationNew:(CLLocationCoordinate2D)location
{
    if (self.isDirectWalking == YES) {
        [self StopWalkToLocationDirect];
    }
    
    self.isTeleporting = YES;
    bool validLocation = CLLocationCoordinate2DIsValid(location);
    if (validLocation == YES)
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"SavedUnfinishedRoute"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            double oldlatitude = self.currentFakeLocation.latitude;
            double oldlongitude = self.currentFakeLocation.longitude;
            
            self.previousFakeLocation = CLLocationCoordinate2DMake(oldlatitude, oldlongitude);
            self.currentFakeLocation = location;
            
            [self UpdateLocation];
        });
    }
    else
    {
        DBG(@"Invalid coord for teleport to %f,%f",location.latitude,location.longitude);
    }
    
    self.isTeleporting = NO;
}

- (void)JumpToLocationNew:(CLLocationCoordinate2D)location
{
    if (self.isDirectWalking == YES) {
        [self StopWalkToLocationDirect];
    }

    self.isTeleporting = YES;
    bool validLocation = CLLocationCoordinate2DIsValid(location);
    if (validLocation == YES)
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            double oldlatitude = self.currentFakeLocation.latitude;
            double oldlongitude = self.currentFakeLocation.longitude;
            
            self.previousFakeLocation = CLLocationCoordinate2DMake(oldlatitude, oldlongitude);
            self.currentFakeLocation = location;
            
            [self UpdateLocation];
        });
    }
    else
    {
        DBG(@"invalid coord for teleport to %f,%f",location.latitude,location.longitude);
    }
    
    self.isTeleporting = NO;
}

#pragma mark - Direct Walking

- (void)WalkToLocationDirect:(CLLocationCoordinate2D)location
{
    if (self.isDirectWalking == YES) {
        [self StopWalkToLocationDirect];
    }

    if (self.isDirectWalking == NO) {
        self.isDirectWalking = YES;
        [[SpooferxManager sharedManager] enableGpxButton];
    }
    //double kilometers_per_hour = 10.0;
    //self.currentSpeed = (kilometers_per_hour / 3.6);
    
    CLLocationCoordinate2D initial_coords = CLLocationCoordinate2DMake(Preferences.fakeLatitude, Preferences.fakeLongitude);
    self.destinationFakeLocation = location;
    
    int min_sleep_time = 1;
    int max_sleep_time = 2;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        CLLocationCoordinate2D current_coords = initial_coords;
        time_t last_loop_update = time(nullptr);
        
        while (self.isDirectWalking == YES) {
            if (self.isWalkPaused == NO) {
                self.currentHeading = [self GetHeadingForDirectionFromCoordinate:current_coords ToCoordinate:self.destinationFakeLocation];
                self.totalDistance = [self distanceFromLocation:current_coords ToDestination:self.destinationFakeLocation];
                self.timeToDestination = self.totalDistance/self.speed;
                //DBG(@"Walking %.1f km in %.1f mins",self.totalDistance/1000,self.timeToDestination/60);
                
                time_t now = time(nullptr);
                time_t time_delta = now - last_loop_update;
                
                double distance_delta = time_delta * self.speed;
                
                if (distance_delta > 0) {
                    bool final_round = false;
                    self.currentFakeLocation = current_coords;
                    self.previousFakeLocation = current_coords;
                    
                    if (distance_delta < self.totalDistance) {
                        self.currentFakeLocation = [self LocationInDirection:self.currentHeading InDistance:self.speed FromCoordinate:current_coords];
                    } else {
                        final_round = true;
                        self.currentFakeLocation = self.destinationFakeLocation;
                    }
                    
                    [self UpdateLocation];
                    
                    last_loop_update = now;
                    current_coords = self.currentFakeLocation;
                    
                    if (final_round == true)
                    {
                        [self StopWalkToLocationDirect];
                        break;
                    }
                }
                
                int sleep_time = (rand() % (max_sleep_time - min_sleep_time)) + min_sleep_time;
                sleep(sleep_time);
            }
            else
            {
                last_loop_update = time(nullptr);
                int sleep_time = (rand() % (max_sleep_time - min_sleep_time)) + min_sleep_time;
                sleep(sleep_time);
            }
        }
    });
}

- (void)StopWalkToLocationDirect
{
    self.isDirectWalking = NO;
    [[SpooferxManager sharedManager] disableGpxButton];
}

#pragma mark - JoyStick

- (JoystickController *)joyStick
{
    if (_joyStick == nil) {
        CGSize displaySize = [SxUtilities displaySize];
        CGPoint joystickOrigin = CGPointFromString(Preferences.joystickFrame);
        if (joystickOrigin.x < 10 || joystickOrigin.x > displaySize.width-10) {
            joystickOrigin = CGPointFromString(@"{275,605}");
        }
        
        if (joystickOrigin.y < 64 || joystickOrigin.y > displaySize.height-64) {
            joystickOrigin = CGPointFromString(@"{275,605}");
        }
        NSString *joystickOriginFrame = NSStringFromCGPoint(joystickOrigin);
        [Preferences setJoystickFrame:joystickOriginFrame];
        self.joyStick = [[JoystickController alloc] initWithFrame:CGRectMake(joystickOrigin.x, joystickOrigin.y, 80, 80)];
        self.joyStick.moveDelegate = self;
    }
    return _joyStick;
}

- (JoystickController *)joyStickForPoint:(CGPoint)point
{
    if (_joyStick != nil) {
        _joyStick = nil;
    }
    
    if (_joyStick == nil) {
        self.joyStick = [[JoystickController alloc] initWithFrame:CGRectMake(point.x, point.y, 80, 80)];
        self.joyStick.moveDelegate = self;
    }
    
    return _joyStick;
}

- (void)joyStickDidMoveOffsetX:(CGFloat)x offsetY:(CGFloat)y
{
    if (self.isDirectWalking == YES) {
        [self StopWalkToLocationDirect];
    }

    self.JoyHead = CGPointMake(x, y);
    
    if (self.isJoyWalking == NO) {
        
        self.isJoyWalking = YES;
        
        CLLocationCoordinate2D initial_coords = CLLocationCoordinate2DMake(Preferences.fakeLatitude, Preferences.fakeLongitude);
        
        int min_sleep_time = 1;
        int max_sleep_time = 2;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            CLLocationCoordinate2D current_coords = initial_coords;
            time_t last_loop_update = time(nullptr);
            
            while (self.joyStick.inMove == YES) {
                
                self.currentHeading = [self GetheadingForDirectionFromX:self.JoyHead.x Y:self.JoyHead.y];
                
                self.currentHeading = (self.currentHeading+Preferences.compassHeading);
                
                time_t now = time(nullptr);
                time_t time_delta = now - last_loop_update;
                double distance_delta = time_delta * self.speed;
                
                if (distance_delta > 0) {
                    self.currentFakeLocation = current_coords;
                    self.previousFakeLocation = current_coords;
                    
                    self.currentFakeLocation = [self LocationInDirection:self.currentHeading InDistance:self.speed FromCoordinate:current_coords];
                    [self UpdateLocation];
                    
                    last_loop_update = now;
                    current_coords = self.currentFakeLocation;
                }
                
                int sleep_time = (rand() % (max_sleep_time - min_sleep_time)) + min_sleep_time;
                sleep(sleep_time);
            }
            
            self.isJoyWalking = NO;
        });
    }
}

@end

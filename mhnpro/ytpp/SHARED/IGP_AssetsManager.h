//
//  IGP_AssetsManager.h
//  spooferpro
//
//  Created by MADHATTER on 10/25/22.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

#define AssetsManager [IGP_AssetsManager sharedManager]

@interface IGP_AssetsManager : NSObject

+ (IGP_AssetsManager *)sharedManager;

@property (nonatomic, strong) UIImage *sppLogo;
@property (nonatomic, strong) UIImage *ipsLogo;
@property (nonatomic, strong) UIImage *pgsLogo;

- (void)DownloadAssets;

- (NSString *)resourcesBundlePath;
- (NSString *)countriesBundlePath;

- (NSBundle *)resourcesBundle;
- (NSBundle *)countriesBundle;

- (UIImage *)imageForResource:(NSString*)name;

- (UINib*)nibWithName:(NSString*)nibname;
- (UIStoryboard*)mainStoryboard;

@end

@interface UIImage ()
+ (id)imageNamed:(id)arg1 inBundle:(id)arg2;
@end

@interface UIImage (spx)
+(UIImage *)sx_imageNamed:(NSString *)name;
@end

NS_ASSUME_NONNULL_END

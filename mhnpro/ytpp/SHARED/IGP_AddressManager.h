//
//  IGP_AddressManager.h
//  igopro
//
//  Created by MADHATTER on 3/8/23.
//

#import <Foundation/Foundation.h>

#define OffMan [IGP_AddressManager sharedManager]

NS_ASSUME_NONNULL_BEGIN

@interface IGP_AddressManager : NSObject

@property (nonatomic,strong) GRKConcurrentMutableDictionary *patchesList;
@property (nonatomic,strong) GRKConcurrentMutableDictionary *pointersList;
@property (nonatomic,strong) GRKConcurrentMutableDictionary *protosList;

+ (IGP_AddressManager *)sharedManager;
- (NSDictionary*)GetPatch:(NSString*)key;
- (uint64_t)GetPtr:(NSString*)key;
- (uint64_t)GetProto:(NSString*)key;
- (NSString*)GetProtoName:(NSString*)key;

@end

NS_ASSUME_NONNULL_END

//
//  RoutesManager.m
//  SDWebImage
//
//  Created by c0pyn1nja on 25/09/21
//  Copyright © 2021 Dailymotion. All rights reserved.
//

#import "RoutesManager.h"
#import "NSFileManager+URLs.h"
#import "GPX.h"

@interface RoutesManager ()

@property (nonatomic, strong) NSTimeZone *fakeTimeZone;
@property (nonatomic, strong) NSString *locname;
@property (nonatomic, strong) NSString *locstreet;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *adminarea;
@property (nonatomic, strong) NSString *locality;
@property (nonatomic, strong) NSString *subblocality;
@property (nonatomic, strong) NSString *ISOcode;

@end

@implementation RoutesManager

+ (RoutesManager *)sharedManager
{
    static RoutesManager * sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init
{
    if (self = [super init])
    {
        self.routesDictionary = [[GRKConcurrentMutableDictionary alloc] init];
    }
    return self;
}

- (NSString*)getRoutesFileName
{
    return @"Routes";
}

- (NSString*)getRoutesFilePath
{
    NSString *path = [DEST_PATH stringByAppendingString:[self getRoutesFileName]];
    return path;
}

- (void)loadRoutes
{
    NSDictionary *routes = [NSDictionary dictionaryWithContentsOfFile:[self getRoutesFilePath]];
    [self.routesDictionary setDictionary:routes];
}

- (BOOL)saveRoutes
{
    NSDictionary *routes = [self getRoutes];
    return [routes writeToFile:[self getRoutesFilePath] atomically:YES];
}

- (NSDictionary*)getRoutes
{
    return self.routesDictionary.nonConcurrentDictionary;
}

//- (NSArray*)getRemoteRoutes
//{
//    NSURL *path = [[[NSFileManager defaultManager] applicationSupportDirectory] URLByAppendingPathComponent:@"remoteroutes.plist"];
//    NSDictionary *routesdict = [NSDictionary dictionaryWithContentsOfURL:path];
//    NSArray *routes = routesdict[@"routeslist"];
//    return routes;
//}

- (NSArray*)getRemoteRoutes {
    NSURL *main_url = [[[NSFileManager defaultManager] applicationSupportDirectory] URLByAppendingPathComponent:@"routes.json"];
    NSData *iconsData = [NSData dataWithContentsOfURL:main_url];
    NSError *jsonPError;
    NSDictionary *routes = [NSJSONSerialization JSONObjectWithData:iconsData options:kNilOptions error:&jsonPError];
    if(jsonPError == nil)
    {
        NSString *bad = [routes objectForKey:@"routeslist"];
        if([bad length] > 0) {
            NSData *edata = [[NSData alloc] initWithBase64EncodedString:bad options:0];
            NSData *ddata = [SxUtilities decryptData:edata];
            NSError *jsonError;
            NSArray *routes = [NSJSONSerialization JSONObjectWithData:ddata options:kNilOptions error:&jsonError];
            if(jsonError == nil) {
                NSMutableArray *dictsArray = [[NSMutableArray alloc] init];
                if(routes.count > 0) {
                    for (NSString *key in routes)
                    {
                        NSString *cc = [key substringToIndex:2];
                        NSString *country = [[NSLocale systemLocale] displayNameForKey:NSLocaleCountryCode value:cc];
                        NSString *t1 = [key substringFromIndex:5];
                        NSString *t2 = [t1 substringToIndex:[t1 length]-4];
                        NSArray *c1 = [t2 componentsSeparatedByString:@" - "];
                        NSString *ct1 = [c1 firstObject];
                        NSString *ct2 = [c1 lastObject];
                        if (c1.count > 2) {
                            ct2 = [NSString stringWithFormat:@"%@ - %@",[c1 objectAtIndex:1],[c1 objectAtIndex:2]];
                        }
                        
                        NSDictionary *gp = @{@"cc":cc,@"city":ct1,@"area":ct2,@"name":key,@"country":country};
                        [dictsArray addObject:gp];
                    }
                }
                
                return dictsArray;
            }
            
            DBG(@"Decryption Error -> %@",jsonError.debugDescription);
        }
        
        return nil;
    }
    
    DBG(@"Routes Error -> %@",jsonPError.description);
    return nil;
}

- (BOOL)saveRoute:(NSString*)route name:(NSString*)name
{
    GPXRoot *root = [GPXParser parseGPXWithString:route];
    int count = (int)root.waypoints.count;
    NSString *key = [[NSUUID UUID] UUIDString];
    NSDictionary *routed = @{@"route":route,@"rname":name,@"stopcount":@(count),@"routekey":key};
    [self.routesDictionary setObject:routed forKey:key];
    return [self saveRoutes];
}

- (BOOL)deleteRouteForKey:(NSString*)key
{
    if ([self.routesDictionary objectForKey:key] != nil) {
        [self.routesDictionary removeObjectForKey:key];
        return [self saveRoutes];
    }
    
    return NO;
}

- (NSString*)exportRoutesFile
{
    NSError *error;
    NSError *error2;
    NSString *filename = [self getRoutesFileName];
    NSString *path = [DEST_PATH stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.json",filename]];
    
    NSDictionary *routes = [self getRoutes];
    if (routes.count > 0) {
        NSData *dataFromDict = [NSJSONSerialization dataWithJSONObject:routes
                                                               options:NSJSONWritingPrettyPrinted
                                                                 error:&error];
        if (error == nil) {
            NSData *crypted = [SxUtilities encryptData:dataFromDict];
            NSString *bad = [crypted base64EncodedStringWithOptions:0];
            double currentTime = [[NSDate date] timeIntervalSince1970];
            NSDate *exportday = [NSDate dateWithTimeIntervalSince1970:currentTime];
            NSString *datestr = [NSDateFormatter localizedStringFromDate:exportday dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterLongStyle];
            NSDictionary *export = @{@"fileName":filename,@"fileType":@"routes",@"exportTime":datestr,@"fileData":bad};
            NSData *exportDict = [NSJSONSerialization dataWithJSONObject:export
                                                                 options:NSJSONWritingPrettyPrinted
                                                                   error:&error2];
            if (error2 == nil) {
                NSError *jsonWrite;
                NSString *jsonString = [[NSString alloc] initWithData:exportDict encoding:NSUTF8StringEncoding];
                [jsonString writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:&jsonWrite];
                if (jsonWrite == nil) {
                    return path;
                }
            }
        }
    }
    
    return @"";
}

- (void)importRoutesFile:(NSDictionary*)routesdict
{
    NSString *fileData = routesdict[@"fileData"];
    NSData *edata = [[NSData alloc] initWithBase64EncodedString:fileData options:0];
    NSData *ddata = [SxUtilities decryptData:edata];
    NSError *jsonError;
    NSDictionary *importedDict = [NSJSONSerialization JSONObjectWithData:ddata options:kNilOptions error:&jsonError];
    if (jsonError == nil) {
        [self.routesDictionary addEntriesFromDictionary:importedDict];
        if ([self saveRoutes] == YES) {
            NSString *msg = [NSString stringWithFormat:@"Routes file imported successfully."];
            UIAlertController *success_alert = [UIAlertController alertControllerWithTitle:@"Import Sucess" message:msg preferredStyle:UIAlertControllerStyleAlert];
            success_alert.view.tintColor = [SxUtilities ThemeColor];
            UIAlertAction *dismiss_action = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                [self loadRoutes];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"RoutesReceived" object:nil userInfo:nil];
            }];
            
            [success_alert addAction:dismiss_action];
            
            [[UIViewController spxoverlay_topMostController] presentViewController:success_alert animated:YES completion:^{
            }];
        }
        else
        {
            UIAlertController *success_alert = [UIAlertController alertControllerWithTitle:@"Write Error" message:@"Failed to import routes file." preferredStyle:UIAlertControllerStyleAlert];
            success_alert.view.tintColor = [SxUtilities ThemeColor];
            UIAlertAction *dismiss_action = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            }];
            
            [success_alert addAction:dismiss_action];
            [[UIViewController spxoverlay_topMostController] presentViewController:success_alert animated:YES completion:^{
            }];
        }
    }
    else
    {
        UIAlertController *success_alert = [UIAlertController alertControllerWithTitle:@"Import Error" message:jsonError.debugDescription preferredStyle:UIAlertControllerStyleAlert];
        success_alert.view.tintColor = [SxUtilities ThemeColor];
        UIAlertAction *dismiss_action = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        
        [success_alert addAction:dismiss_action];
        [[UIViewController spxoverlay_topMostController] presentViewController:success_alert animated:YES completion:^{
        }];
    }
}

@end

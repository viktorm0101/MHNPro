//
//  RpcManager.h
//  SDWebImage
//
//  Created by spooferx on 11/14/20
//  Copyright © 2020 Dailymotion. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface RpcManager : NSObject

+ (void)downloadSpawnPoints;
+ (void)resetMetadatG;
+ (BOOL)isEventPokemon;
+ (void)fakeViewAllocated;
+ (void)verifySpawnsN;
+ (void)copyMetaKeyToKeychain:(NSString*)metakey;

@end

NS_ASSUME_NONNULL_END

//
//  PreferencesManager.h
//  spooferxsl
//
//  Created by spooferx on 10/26/20
//  
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>

NS_ASSUME_NONNULL_BEGIN
typedef void (*OriginalImpType)(id self, SEL selector);
#define Preferences [PreferencesManager sharedManager]

@interface PreferencesManager : NSObject

+ (PreferencesManager *)sharedManager;

@property (nonatomic, assign) OriginalImpType originalImp;
@property (nonatomic, assign) OriginalImpType originalImpStop;
@property (nonatomic, assign) OriginalImpType originalImpDelegate;

@property (nonatomic) double compassHeading;

@property (nonatomic) double fakeLatitude;
@property (nonatomic) double fakeLongitude;
@property (nonatomic) double realLatitude;
@property (nonatomic) double realLongitude;
@property (nonatomic) double userSpeed;

@property (nonatomic, assign) BOOL enableFakeLocation;
@property (nonatomic, assign) BOOL isShinyPokemon;
@property (nonatomic, assign) BOOL isMegaPokemon;
@property (nonatomic, assign) BOOL showJoystick;
@property (nonatomic, assign) BOOL mapLoaded;
@property (nonatomic, assign) BOOL mapVisible;
    
@property (nonatomic, strong) UIView *overlayView;
@property (nonatomic, strong) CLLocationManager *fakeLocationManager;
@property (nonatomic, strong) id<CLLocationManagerDelegate> fakeDelegate;
@property (nonatomic, strong, nullable) CLPlacemark *fakePlacemark;

@property (nonatomic, strong) NSString *useremail;
@property (nonatomic, strong) NSString *userkey;
@property (nonatomic, strong) NSString *uservalidity;
@property (nonatomic, strong) NSString *menuFrame;
@property (nonatomic, strong) NSString *joystickFrame;

@property (nonatomic, assign) int32_t unityIndex;

@property (nonatomic, assign) NSInteger cdStart;
@property (nonatomic) double cdLatitude;
@property (nonatomic) double cdLongitude;

- (void)loadDefaults;
- (void)updateCD;
- (void)updateCDTime;

@property (nonatomic, assign) BOOL cllocationChange;
@property (nonatomic, assign) BOOL fakeDelegateSet;
@property (nonatomic, assign) BOOL realLocationSet;
@property (nonatomic, assign) BOOL enableTapToWalk;
@property (nonatomic, assign) BOOL enableTapToTeleport;


@property (nonatomic, strong) NSArray *prokeys;
@property (nonatomic) double cameraHeading;

@property (nonatomic, assign) bool IsJailbroken;
@property (nonatomic, assign) bool IsRootless;

@end

NS_ASSUME_NONNULL_END

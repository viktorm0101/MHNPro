//
//  IGP_MapPokemon.h
//  igppogosl
//
//  Created by c0pyn1nja on 03/02/21
//  
//

#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN

@interface IGP_MapPokemon : NSObject

@property (nonatomic, readwrite) float mdistanceInMeters;
@property (nonatomic, assign) double mlatitude;
@property (nonatomic, assign) double mlongitude;
@property (nonatomic, readwrite) uint64_t mencounterId;
@property (nonatomic, assign) HoloPokemonId mpokemonId;

@property (nonatomic, assign) BOOL hasWildPokemon;
@property (nonatomic, assign) BOOL hasNearbyPokemon;
@property (nonatomic, assign) BOOL isPokemonEncountered;
@property (nonatomic, assign) BOOL isPokemonCaptured;

@property (nonatomic, strong) NSString *pokemonName;
@property (nonatomic, assign) HoloPokemonType type1;
@property (nonatomic, assign) HoloPokemonType type2;
@property (nonatomic, assign) NSString *genderString;

@property (nonatomic, strong) WildPokemonProto *wpokemon;
@property (nonatomic, strong) NearbyPokemonProto *npokemon;

- (IGP_MapPokemon*)initWithWildPokemon:(WildPokemonProto*)wwpokemon;
- (IGP_MapPokemon*)initWithNearbyPokemon:(NearbyPokemonProto*)nnpokemon;


@end

NS_ASSUME_NONNULL_END

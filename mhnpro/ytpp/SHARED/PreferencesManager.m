//
//  PreferencesManager.m
//  spooferxsl
//
//  Created by spooferx on 10/26/20
//  
//

#import "PreferencesManager.h"
#import "RpcManager.h"

@interface PreferencesManager ()

@end

@implementation PreferencesManager

+ (PreferencesManager *)sharedManager {
    static PreferencesManager * sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    if (self = [super init])
    {
        
    }
    
    return self;
}

- (void)loadDefaults {
    [AssetsManager DownloadAssets];
    
    self.prokeys = @[
        @"enable_tap_teleport",
    ];
    
    NSDictionary *defaults = [self prefsDict];
    [[NSUserDefaults standardUserDefaults] registerDefaults:defaults];
    [self processPolicy];
    self.cllocationChange = NO;
    self.realLocationSet = NO;
}


- (NSDictionary*)prefsDict
{
    CGSize displaySize = [[UIScreen mainScreen] bounds].size;
    CGPoint joystickOrigin = CGPointMake(displaySize.width-125, displaySize.height-225);
    NSString *jframe = NSStringFromCGPoint(joystickOrigin);
    NSMutableDictionary *prefs = [[NSMutableDictionary alloc] init];
    [prefs setValue:@(0) forKey:@"user_real_lat"];
    [prefs setValue:@(0) forKey:@"user_real_lon"];
    [prefs setValue:@(0) forKey:@"user_fake_lat"];
    [prefs setValue:@(0) forKey:@"user_fake_lon"];
    [prefs setValue:@(10.0) forKey:@"user_speed"];
    [prefs setValue:@(0) forKey:@"cd_start"];
    [prefs setValue:@(0) forKey:@"user_cd_lat"];
    [prefs setValue:@(0) forKey:@"user_cd_lon"];
    [prefs setValue:@(YES) forKey:@"enable_fake_location"];
    [prefs setValue:@(YES) forKey:@"show_joystick"];
    [prefs setObject:@(NO) forKey:@"enable_tap_walk"];
    [prefs setObject:@(NO) forKey:@"enable_tap_teleport"];
    [prefs setObject:@"{5,60}" forKey:@"menu_frame"];
    [prefs setObject:jframe forKey:@"joystick_frame"];
    
    return prefs;
}

- (void)updateCD
{
    CLLocationCoordinate2D destination = [SxUtilities CLLocationCoordinate2DMakeNormal:self.fakeLatitude lon:self.fakeLongitude];
    int mins = [SxUtilities GetRemainingCDInMins:destination];
    if(mins <= 0) {
        self.cdStart = [[NSDate date] timeIntervalSince1970];
        self.cdLatitude = self.fakeLatitude;
        self.cdLongitude = self.fakeLongitude;
    }
}

- (void)updateCDTime
{
    self.cdStart = [[NSDate date] timeIntervalSince1970];
//    self.cdLatitude = self.fakeLatitude;
//    self.cdLongitude = self.fakeLongitude;
//    //DBG(@"New CD Set");
}

- (BOOL)getjudgementDay:(NSDate*)jddate
{
    unsigned int flags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSDate *today = [NSDate date];
    NSCalendar *greg = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents* tcomponents = [greg components:flags fromDate:today];
    NSDate *tdate = [greg dateFromComponents:tcomponents];
    NSDateComponents* jcomponents = [greg components:flags fromDate:jddate];
    NSDate *jdate = [greg dateFromComponents:jcomponents];
    return [tdate isLaterThan:jdate];
}

- (void)processPolicy
{
    NSDictionary *metaDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"metadata"];
    if (metaDict != nil)
    {
        Preferences.useremail = metaDict[@"useremail"];
        Preferences.userkey = metaDict[@"licencekey"];
        NSString *val = metaDict[@"validtill"];
        NSString *stat = metaDict[@"status"];
        if ([stat isEqualToString:@"1"] == YES)
        {
            NSTimeInterval interval = [val doubleValue];
            NSDate *judgementday = [NSDate dateWithTimeIntervalSince1970:interval];
            BOOL later = [self getjudgementDay:judgementday];
            if (later == NO) {
                Preferences.uservalidity = [NSDateFormatter localizedStringFromDate:judgementday dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterNoStyle];
                [Preferences setIsShinyPokemon:YES];
                [Preferences setIsMegaPokemon:NO];
            }
            else
            {
                [RpcManager resetMetadatG];
            }
        }
        else
        {
            [RpcManager resetMetadatG];
        }
    }
    else
    {
        [RpcManager resetMetadatG];
    }
}

- (double)realLatitude
{
    return [[NSUserDefaults standardUserDefaults] doubleForKey:@"user_real_lat"];
}

- (void)setRealLatitude:(double)realLatitude
{
    [[NSUserDefaults standardUserDefaults] setDouble:realLatitude forKey:@"user_real_lat"];
}

- (double)realLongitude
{
    return [[NSUserDefaults standardUserDefaults] doubleForKey:@"user_real_lon"];
}

- (void)setRealLongitude:(double)realLongitude
{
    [[NSUserDefaults standardUserDefaults] setDouble:realLongitude forKey:@"user_real_lon"];
}

- (double)fakeLatitude
{
    return [[NSUserDefaults standardUserDefaults] doubleForKey:@"user_fake_lat"];
}

- (void)setFakeLatitude:(double)fakeLatitude
{
    [[NSUserDefaults standardUserDefaults] setDouble:fakeLatitude forKey:@"user_fake_lat"];
}

- (double)fakeLongitude
{
    return [[NSUserDefaults standardUserDefaults] doubleForKey:@"user_fake_lon"];
}

- (void)setFakeLongitude:(double)fakeLongitude
{
    [[NSUserDefaults standardUserDefaults] setDouble:fakeLongitude forKey:@"user_fake_lon"];
}

- (BOOL)enableFakeLocation
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"enable_fake_location"];
}

- (void)setEnableFakeLocation:(BOOL)enableFakeLocation
{
    [[NSUserDefaults standardUserDefaults] setBool:enableFakeLocation forKey:@"enable_fake_location"];
}

- (BOOL)showJoystick
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"show_joystick"];
}

- (void)setShowJoystick:(BOOL)showJoystick
{
    [[NSUserDefaults standardUserDefaults] setBool:showJoystick forKey:@"show_joystick"];
}

- (double)userSpeed
{
    return [[NSUserDefaults standardUserDefaults] doubleForKey:@"user_speed"];
}

- (void)setUserSpeed:(double)userSpeed
{
    [[NSUserDefaults standardUserDefaults] setDouble:userSpeed forKey:@"user_speed"];
}

- (NSInteger)cdStart
{
    NSString * key = @"cd_start";
    return [[NSUserDefaults standardUserDefaults] integerForKey:key];
}

- (void)setCdStart:(NSInteger)cdStart
{
    [[NSUserDefaults standardUserDefaults] setInteger:cdStart forKey:@"cd_start"];
}

- (double)cdLatitude
{
    return [[NSUserDefaults standardUserDefaults] doubleForKey:@"user_cd_lat"];
}

- (void)setCdLatitude:(double)cdLatitude
{
    [[NSUserDefaults standardUserDefaults] setDouble:cdLatitude forKey:@"user_cd_lat"];
}

- (double)cdLongitude
{
    return [[NSUserDefaults standardUserDefaults] doubleForKey:@"user_cd_lon"];
}

- (void)setCdLongitude:(double)cdLongitude
{
    [[NSUserDefaults standardUserDefaults] setDouble:cdLongitude forKey:@"user_cd_lon"];
}


- (NSString*)menuFrame
{
    return [[NSUserDefaults standardUserDefaults] valueForKey:@"menu_frame"];
}

- (void)setMenuFrame:(NSString *)menuFrame
{
    [[NSUserDefaults standardUserDefaults] setValue:menuFrame forKey:@"menu_frame"];
}

- (BOOL)enableTapToWalk
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"enable_tap_walk"];
}

- (void)setEnableTapToWalk:(BOOL)enableTapToWalk
{
    [[NSUserDefaults standardUserDefaults] setBool:enableTapToWalk forKey:@"enable_tap_walk"];
}


- (NSString*)joystickFrame
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"joystick_frame"];
}

- (void)setJoystickFrame:(NSString *)joystickFrame
{
    [[NSUserDefaults standardUserDefaults] setObject:joystickFrame forKey:@"joystick_frame"];
}

- (BOOL)enableTapToTeleport
{
    return [[[NSUserDefaults standardUserDefaults] objectForKey:@"enable_tap_teleport"] boolValue];
}

- (void)setEnableTapToTeleport:(BOOL)enableTapToTeleport
{
    [[NSUserDefaults standardUserDefaults] setBool:enableTapToTeleport forKey:@"enable_tap_teleport"];
}


@end

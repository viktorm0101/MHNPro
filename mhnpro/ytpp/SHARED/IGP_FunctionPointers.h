//
//  IGP_FunctionPointers.h
//  igppogosl
//
//  Created by igppogo on 5/13/20.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

#define IL2Functions [IGP_FunctionPointers sharedManager]

@interface IGP_FunctionPointers : NSObject

+ (IGP_FunctionPointers *)sharedManager;

- (void)SetUp;

- (void*)GetIl2StringFromNSString:(NSString*)str;
- (NSString*)GetNSStringFromIl2String:(void*)str;

@end

NS_ASSUME_NONNULL_END

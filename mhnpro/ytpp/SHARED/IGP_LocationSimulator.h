//
//  IGP_LocationSimulator.h
//  sppro
//
//  Created by MadHatter on 9/1/22.
//

#import <Foundation/Foundation.h>
#import "JoystickController.h"

NS_ASSUME_NONNULL_BEGIN

#define LocationSimulator [IGP_LocationSimulator sharedManager]

@interface IGP_LocationSimulator : NSObject

@property (nonatomic) CLLocationCoordinate2D currentFakeLocation;
@property (nonatomic) CLLocationCoordinate2D previousFakeLocation;
@property (nonatomic) CLLocationCoordinate2D patrolBaseLocation;
@property (nonatomic) CLLocationCoordinate2D destinationFakeLocation;
@property (nonatomic) CLLocationCoordinate2D currentRealLocation;
@property (nonatomic) CLLocationDegrees currentHeading;
@property (nonatomic) CLLocationDistance totalDistance;

@property (nonatomic) double timeToDestination;
@property (nonatomic) BOOL isPatrolling;
@property (nonatomic) BOOL isRouteWalking;
@property (nonatomic) BOOL isDirectWalking;
@property (nonatomic) BOOL isTeleporting;
@property (nonatomic) BOOL isJoyWalking;
@property (nonatomic) BOOL isWalkPaused;

@property (nonatomic, strong) NSMutableArray *wayPoints;
@property (nonatomic, strong) NSString *currentRoute;
@property (nonatomic, strong) JoystickController *joyStick;

+ (IGP_LocationSimulator *)sharedManager;


- (double)GetHeadingForDirectionFromCoordinate:(CLLocationCoordinate2D)fromLoc
                                  ToCoordinate:(CLLocationCoordinate2D)toLoc;

- (CLLocationCoordinate2D)LocationInDirection:(CLLocationDegrees)heading
                                   InDistance:(CLLocationDistance)distance
                               FromCoordinate:(CLLocationCoordinate2D)fromCoordinate;

- (void)ResetFakeLocation;
- (void)SetupMapPositions:(CLLocationCoordinate2D)location;
- (void)UpdateLocation;
- (void)JumpToPreviousLocation;
- (void)TeleportToLocationString:(NSString*)locationString;
- (void)TeleportToLocationNew:(CLLocationCoordinate2D)location;
- (void)JumpToLocationNew:(CLLocationCoordinate2D)location;
- (void)WalkToLocationDirect:(CLLocationCoordinate2D)location;
- (void)StopWalkToLocationDirect;
- (void)PauseWalk;
- (void)ResumeWalk;
- (JoystickController *)joyStickForPoint:(CGPoint)point;

@end

NS_ASSUME_NONNULL_END

//
//  swizzles.m
//  spooferxsl
//
//  Created by spooferx on 12/14/19.
//

#include <objc/runtime.h>
#include <objc/message.h>
#include <CoreFoundation/CFString.h>
#include <CoreFoundation/CoreFoundation.h>
#include <dlfcn.h>


typedef void (*OriginalImpTypeDel)(id self, SEL selector, UIApplication *application);
static OriginalImpTypeDel originalImpBecomeActive;
static OriginalImpTypeDel originalImpResignActive;
static OriginalImpTypeDel originalImpWillTerminate;

#pragma mark UnityAppController
@interface FakeUnityAppController : NSObject

- (void)applicationDidBecomeActive:(UIApplication *)application;
- (void)applicationWillResignActive:(UIApplication *)application;
- (void)applicationWillTerminate:(UIApplication *)application;

@end

@implementation FakeUnityAppController

extern "C" CFNotificationCenterRef CFNotificationCenterGetDistributedCenter(void);
static void RestoreJailbreakFiles() {
#define REQUEST_RESTORE_FILES "com.spooferpro.restore_files"
    CFNotificationCenterPostNotification(CFNotificationCenterGetDistributedCenter(), CFSTR(REQUEST_RESTORE_FILES), NULL, NULL, YES);
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    DBG(@"Application - DidBecomeActive");
    originalImpBecomeActive(self,@selector(applicationDidBecomeActive:),application);
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    DBG(@"Application - WillResignActive");
    originalImpResignActive(self,@selector(applicationWillResignActive:),application);
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    if(Preferences.IsJailbroken == YES) {
        RestoreJailbreakFiles();
    }
    DBG(@"Application - WillTerminate");
    originalImpWillTerminate(self,@selector(applicationWillTerminate:),application);
}

@end


static __attribute__((constructor)) void _swizzles_constructor1()
{
    //UnityAppController - WillTerminate
    Method orig_UnityAppControllerterminate = class_getInstanceMethod(objc_getClass("UnityAppController"), @selector(applicationWillTerminate:));
    originalImpWillTerminate = (OriginalImpTypeDel)method_getImplementation(orig_UnityAppControllerterminate);
    
    Method my_UnityAppControllerTerminate = class_getInstanceMethod(objc_getClass("FakeUnityAppController"), @selector(applicationWillTerminate:));
    method_exchangeImplementations(orig_UnityAppControllerterminate, my_UnityAppControllerTerminate);
    
//    //UnityAppController - BecomeActive
//    Method orig_UnityAppControllerBegin = class_getInstanceMethod(objc_getClass("UnityAppController"), @selector(applicationDidBecomeActive:));
//    originalImpBecomeActive = (OriginalImpTypeDel)method_getImplementation(orig_UnityAppControllerBegin);
//    
//    Method my_UnityAppControllerBegin = class_getInstanceMethod(objc_getClass("FakeUnityAppController"), @selector(applicationDidBecomeActive:));
//    method_exchangeImplementations(orig_UnityAppControllerBegin, my_UnityAppControllerBegin);
    
//    //UnityAppController - ResignActive
//    Method orig_UnityAppControllerResign = class_getInstanceMethod(objc_getClass("UnityAppController"), @selector(applicationWillResignActive:));
//    originalImpResignActive = (OriginalImpTypeDel)method_getImplementation(orig_UnityAppControllerResign);
//
//    Method my_UnityAppControllerResign = class_getInstanceMethod(objc_getClass("FakeUnityAppController"), @selector(applicationWillResignActive:));
//    method_exchangeImplementations(orig_UnityAppControllerResign, my_UnityAppControllerResign);
    
    DBG(@"Swizzled AppDelegate");
}

//
//  IGP_ApplicationSwizzler.h
//  SDWebImage
//
//  Created by MADHATTER on 4/20/22.
//  Copyright © 2022 Dailymotion. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface IGP_ApplicationSwizzler : NSObject

+ (IGP_ApplicationSwizzler *)sharedSwizzler;

- (BOOL)hook_application:(UIApplication *)app
                 openURL:(NSURL *)url
                 options:(NSDictionary<UIApplicationOpenURLOptionsKey, id> *)options API_AVAILABLE(ios(9.0));

@end

NS_ASSUME_NONNULL_END

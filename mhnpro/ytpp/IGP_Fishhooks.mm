//
//  IGP_Fishhooks.m
//  spooferprosl
//
//  Created by MADHATTER on 12/18/22.
//

#import "IGP_Fishhooks.h"
#import "fishhook.h"
#import "IGP_FunctionPointers.h"
#include <iostream>
#include <dlfcn.h>
#include <execinfo.h>
#include <typeinfo>
#include <string>
#include <memory>
#include <cxxabi.h>
#include <cstdlib>
#include <unistd.h>

#import <Constants.h>
#import <IGP_AddressManager.h>

const char *white = "/usr/lib/system/libsystem_pthread.dylib";

static NSArray *black_paths = @[
    @"/usr/lib/substrate",
    @"/usr/lib/TweakInject",
    @"/MobileSubstrate/DynamicLibraries",
    @"/Library/Application Support/",
    @"/var/jb",
    @"/private/preboot",
    @"/usr/lib/libsubstrate.dylib",
    @"/usr/lib/librocketbootstrap.dylib",
    @"7CLUydP3nIVBHBL.dylib",
    @"gzFSZ2qOytcUJhB.dylib",
    //@"spooferpro.dylib",
    @"spooferpro.bundle",
    @"igopro.dylib",
    @"igopro.bundle",
    @"tassets.bundle",
    //@"systemhook.dylib",
    @"libinjector.dylib",
    @"TweakInject",
    @"substitute-inserter.dylib",
    @"TweakInject.dylib",
    @"SubstrateInserter.dylib",
    @"substitute-loader.dylib",
    @"SubstrateLoader.dylib",
    @"   Choicy.dylib",
    @"libellekit.dylib",
];

static NSArray *sys_paths = @[
    @"/System/Library/",
    @"/usr/lib/"
];

@implementation IGP_Fishhooks

static Il2CppString* (*Ex_ToString)(void *ex) = (Il2CppString* (*)(void *))[OffMan GetPtr:@"ADR_EX_ToString"];

+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{

        struct rebinding rebindingHandler = {};
        rebindingHandler.name = "dladdr";
        rebindingHandler.replacement = (void *)igp_dladdr;
        rebindingHandler.replaced = (void **)&org_dladdr;
        struct rebinding rebindings[] = {rebindingHandler};
        int hooked = rebind_symbols(rebindings, 1);
        DBG(@"Registered for %s -> %d",rebindingHandler.name,hooked);

        struct rebinding rebindingHandler1 = {};
        rebindingHandler1.name = "_dyld_get_image_name";
        rebindingHandler1.replacement = (void *)_igp_dyld_get_image_name;
        rebindingHandler1.replaced = (void **)&org_dyld_get_image_name;
        struct rebinding rebindings1[] = {rebindingHandler1};
        int hooked1 = rebind_symbols(rebindings1, 1);
        DBG(@"Registered for %s -> %d",rebindingHandler1.name,hooked1);

//#ifdef DEBUG
//        struct rebinding rebindingHandler2 = {};
//        rebindingHandler2.name = "__cxa_throw";
//        rebindingHandler2.replacement = (void *)RPL__cxa_throw;
//        rebindingHandler2.replaced = (void **)&ORG__cxa_throw;
//        struct rebinding rebindings2[] = {rebindingHandler2};
//        int hooked2 = rebind_symbols(rebindings2, 1);
//        DBG(@"Registered for %s -> %d",rebindingHandler2.name,hooked2);
//        
//        setlinebuf(stdout);
//        setlinebuf(stderr);
//        stdout->_write = stdout_redirect_nslog;
//        stderr->_write = stderr_redirect_nslog;
//        DBG(@"Redirected stderr and stdout to nslog");
//#endif
    });
}

static int redirect_nslog(const char *prefix, const char *buffer, int size) {
    DBGB(@"%s (%d bytes): %.*s", prefix, size, size, buffer);
    return size;
}

static int stderr_redirect_nslog(void *inFD, const char *buffer, int size) {
    return redirect_nslog("stderr", buffer, size);
}

static int stdout_redirect_nslog(void *inFD, const char *buffer, int size) {
    return redirect_nslog("stdout", buffer, size);
}

+ (BOOL)is_blacklisted2:(const char *)path {
    NSString *nstr = [NSString stringWithUTF8String:path];
    for (NSString *bp in black_paths) {
        if([nstr containsString:bp] == YES) {
            return YES;
        }
    }

    return NO;
}

//+ (BOOL)is_systemPath:(const char *)path {
//    NSString *nstr = [NSString stringWithUTF8String:path];
//    for (NSString *bp in sys_paths) {
//
//        if([nstr containsString:bp] == YES) {
//            DBG(@"bp -> %@",bp);
//            return YES;
//        }
//    }
//
//    return NO;
//}

+ (BOOL)is_whitePath:(const char *)path {
    NSString *nstr = [NSString stringWithUTF8String:path];
    if([nstr containsString:@"/System/Library/Frameworks"]) {
        return YES;
    }
    
    if([nstr containsString:@"/System/Library/PrivateFrameworks"]) {
        return YES;
    }
    
    if([nstr hasPrefix:@"/usr/lib"]) {
        return YES;
    }
    
    if([nstr containsString:@"/usr/lib/swift"]) {
        return YES;
    }
    
    return NO;
}

int (*org_dladdr)(const void* addr, Dl_info* info);
extern "C" int igp_dladdr(const void* addr, Dl_info* info);
int igp_dladdr(const void* addr, Dl_info* info) {
    auto result = org_dladdr(addr,info);
    if (!result) return result;
//
//    BOOL sysp = [IGP_Fishhooks is_whitePath:info->dli_fname];
//    if(sysp == NO) {
//        NSString *nstr = [NSString stringWithUTF8String:info->dli_fname];
//        DBG(@"DLADDR -> %@",nstr);
//    }
    
    NSString *pt = [NSString stringWithUTF8String:info->dli_fname];
    if([pt containsString:@"/private/preboot"]) {
        info->dli_fname = white;
        return result;
    }

    BOOL black = [IGP_Fishhooks is_blacklisted2:info->dli_fname];
    if (black == YES) {
        info->dli_fname = white;
        return result;
    }

    return result;
}

const char* (*org_dyld_get_image_name)(uint32_t image_index);
extern "C" const char* _igp_dyld_get_image_name(uint32_t image_index);
const char* _igp_dyld_get_image_name(uint32_t image_index) {
    auto result = org_dyld_get_image_name(image_index);
    if (!result) return result;
//
//    BOOL sysp = [IGP_Fishhooks is_whitePath:result];
//    if(sysp == NO) {
//        NSString *nstr = [NSString stringWithUTF8String:result];
//        DBG(@"DGIN -> %@",nstr);
//    }
    
    NSString *pt = [NSString stringWithUTF8String:result];
    if([pt containsString:@"/private/preboot"]) {
        return white;
    }

    BOOL black = [IGP_Fishhooks is_blacklisted2:result];
    if(black == YES) {
        return white;
    }

    return result;
}

void * last_frames[10];
size_t last_size;
std::string exception_name;

std::string demangle(const char *name);
std::string demangle(const char *name) {
  int status;
  std::unique_ptr<char,void(*)(void*)> realname(abi::__cxa_demangle(name, 0, 0, &status), &std::free);
  return status ? "failed" : &*realname;
}

void (*ORG__cxa_throw)(void* thrown_exception, std::type_info* tinfo, void (*dest)(void*));
extern "C" void RPL__cxa_throw(void* thrown_exception, std::type_info* tinfo, void (*dest)(void*));
void RPL__cxa_throw(void* thrown_exception, std::type_info* tinfo, void (*dest)(void*)) {
    if (thrown_exception == nullptr)
        return;
    
#ifdef DEBUG
    std::string exception_name = demangle(reinterpret_cast<const std::type_info*>(tinfo)->name());
    std::string exception_type("Il2CppExceptionWrapper");
    if(exception_name.compare(exception_type) == 0) {
        Il2CppExceptionWrapper *exw = (Il2CppExceptionWrapper*)thrown_exception;
        Il2CppString *exstr = Ex_ToString(exw->ex);
        NSString *exStr =[IL2Functions GetNSStringFromIl2String:exstr];
        DBGB(@"%@",exStr);
    }
    last_size = backtrace(last_frames, sizeof last_frames/sizeof(void*));
    backtrace_symbols_fd(last_frames, last_size, 2);
#endif
    
    ORG__cxa_throw(thrown_exception, tinfo, dest);
}

@end



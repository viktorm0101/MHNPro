//
//  IGP_HooksManager.m
//  igppogosl
//
//  Created by MADHATTER on 11/10/22.
//

#import "IGP_HooksManager.h"
#import "StaticHooks.hpp"
#import "DynamicHooks.hpp"
#import "Constants.h"
#import <mach-o/dyld.h>
#import <dlfcn.h>
#import <IGP_AddressManager.h>

@interface IGP_HooksManager ()

@property (nonatomic,strong) NSArray *SL_hooksList;
@property (nonatomic,strong) NSArray *JB_hooksList;

@end

@implementation IGP_HooksManager

+ (IGP_HooksManager *)sharedManager {
    static IGP_HooksManager *sharedMyManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    if (self = [super init]) {
    }
    return self;
}

- (void)enableHooks {
    if(Preferences.IsRootless == true || Preferences.IsJailbroken == true) {
        [self enableJBHooks];
    } else {
        [self enableSLHooks];
    }
}

- (void)disableHooks {
    if(Preferences.IsRootless == true || Preferences.IsJailbroken == true) {
        [self disableJBHooks];
    } else {
        [self disableSLHooks];
    }
}

- (void)enableJBHooks {
    @autoreleasepool
    {
        int imgcount = _dyld_image_count();
        for (int i = 0; i < imgcount; i++) {
            const char *imagename = _dyld_get_image_name(i);
            if (strstr(imagename, "/UnityFramework")) {
                Preferences.unityIndex = i;
                DBG(@"UnityIndex -> %d",Preferences.unityIndex);
                break;
            }
        }
        
        auto vmslide = _dyld_get_image_vmaddr_slide(Preferences.unityIndex);
        FridaEngine *fe = new FridaEngine();
        //NSString *dylib = [[[NSBundle mainBundle] bundlePath] stringByAppendingString:@"/Frameworks/igppogo.dylib"];
        //void *libHandle = dlopen([dylib UTF8String], RTLD_LAZY);
        self.JB_hooksList = [[[OffMan patchesList] nonConcurrentDictionary] allKeys];
        DBG(@"Hooks JB %ld Functions",self.JB_hooksList.count);
        for (NSString *key in self.JB_hooksList) {
            NSDictionary *hook = [OffMan GetPatch:key];
            unsigned long hookadr = [[hook objectForKey:@"hook_addr"] longValue];
            NSString *rplname = [hook objectForKey:@"rpl_name"];
            void *rplsym = dlsym(RTLD_DEFAULT, [rplname UTF8String]);
            NSString *orgname = [hook objectForKey:@"org_name"];
            void *orgsym = dlsym(RTLD_DEFAULT, [orgname UTF8String]);
            if(rplsym != 0x0 && orgsym != 0x0)
            {
                bool enabled = fe->Hook(reinterpret_cast<void*>(vmslide + hookadr),
                                        reinterpret_cast<void*>(rplsym),
                                        reinterpret_cast<void**>(orgsym));
                DBG(@"Enabled -> %@ -> %@",orgname,NSStringFromBool(enabled));
            }
            else
            {
                DBG(@"Failed -> %@",orgname);
            }
        }
        
        DBG(@"Dynamic Hooks Loaded");
    }
}

- (void)enableSLHooks {
    @autoreleasepool {
        auto header = _dyld_get_image_header(Preferences.unityIndex);
        auto manager = StaticHooks::InitForImage(header);
        if (manager == nullptr) {
            return;
        }
        
        auto vmslide = _dyld_get_image_vmaddr_slide(Preferences.unityIndex);
        //NSString *dylib = [[[NSBundle mainBundle] bundlePath] stringByAppendingString:@"/Frameworks/igppogo.dylib"];
        //void *libHandle = dlopen([dylib UTF8String], RTLD_LAZY);
        self.SL_hooksList = [[[OffMan patchesList] nonConcurrentDictionary] allKeys];
        DBG(@"Hooks SL %ld Functions",self.SL_hooksList.count);
        for (NSString *key in self.SL_hooksList) {
            NSDictionary *hook = [OffMan GetPatch:key];
            unsigned long hookadr = [[hook objectForKey:@"hook_addr"] longValue];
            NSString *rplname = [hook objectForKey:@"rpl_name"];
            void *rplsym = dlsym(RTLD_DEFAULT, [rplname UTF8String]);
            NSString *orgname = [hook objectForKey:@"org_name"];
            void *orgsym = dlsym(RTLD_DEFAULT, [orgname UTF8String]);
            if(rplsym != 0x0 && orgsym != 0x0)
            {
                bool enabled = manager->Enable(reinterpret_cast<void*>(vmslide + hookadr),
                                reinterpret_cast<void*>(rplsym),
                                reinterpret_cast<void**>(orgsym));
                DBG(@"Enabled -> %@ -> %@",orgname,NSStringFromBool(enabled));
            }
            else
            {
                DBG(@"Failed -> %@",orgname);
            }
        }
        
        DBG(@"Static Hooks Loaded");
    }
}

- (void)disableJBHooks {
    @autoreleasepool {
        DBG(@"Dynamic Hooks Unloaded");
    }
}

- (void)disableSLHooks {
    @autoreleasepool {
        auto header = _dyld_get_image_header(Preferences.unityIndex);
        auto manager = StaticHooks::InitForImage(header);
        if (manager == nullptr) {
            return;
        }
        
        auto vmslide = _dyld_get_image_vmaddr_slide(Preferences.unityIndex);
        for (NSDictionary *hook in self.SL_hooksList) {
            unsigned long hookadr = [[hook objectForKey:@"hook_addr"] longValue];
            NSString *orgname = [hook objectForKey:@"org_name"];
            bool disabled = manager->Disable(reinterpret_cast<void*>(vmslide + hookadr));
            DBG(@"Disabled -> %@ -> %@",orgname,NSStringFromBool(disabled));
        }
        
        DBG(@"Static Hooks Unloaded");
    }
}

@end
